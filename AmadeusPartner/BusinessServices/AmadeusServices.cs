﻿using System.Linq;
using System.Text;
using AmadeusPartner.BusinessServices.Interface;
using Newtonsoft.Json;
using BusinessEntities;
using AmadeusPartner.Helper;
using System.Threading.Tasks;
using BusinessEntities.Common;
using System.Collections.Generic;
using AmadeusPartner.Models;

namespace AmadeusPartner.BusinessServices
{
    using System;
    using BusinessEntities.Extensions;
    using Models.Enumrations;

    public class AmadeusServices : IAmadeusServices
    {
        private readonly ISoapHelperAsync soapHelperAsync;
        private string password;
        private string uuId;

        public string LoginId { get; private set; }

        public AmadeusServices(ISoapHelperAsync _soapHelperAsync)
        {
            this.soapHelperAsync = _soapHelperAsync;
        }

        // 1. Get Amadeus FL Search Response from Common
        // 1. Get Amadeus FL Search Response from Common
        public async Task<FLcommonRS> MptbSearch_1(Rootobject mptbRQComObject)
        {
            try
            {
                //JObject mptbRQComObject = JObject.Parse(RQJson);
                string currency = mptbRQComObject.Currency;
                string loginid = mptbRQComObject.SupplierAgencyDetails[0].UserName;
                string password = mptbRQComObject.SupplierAgencyDetails[0].Password;
                string uuid = mptbRQComObject.SupplierAgencyDetails[0].AccountNumber;
                int noresult = mptbRQComObject.CommonRequestSearch.NumberOfUnits;
                string typeOfUnit = mptbRQComObject.CommonRequestSearch.TypeOfUnit;
                int ad = mptbRQComObject.PassengerTypeQuantity.ADT;
                int ch = mptbRQComObject.PassengerTypeQuantity.CHD;
                int inf = mptbRQComObject.PassengerTypeQuantity.INF;
                string cabin = mptbRQComObject.cabin;
                string AgencyCode = mptbRQComObject.CommonRequestSearch.AgencyCode;
                long nopaxunit = ad + ch;

                FlightSearchRq flightSearchRq = new FlightSearchRq()
                {
                    LoginId = mptbRQComObject.SupplierAgencyDetails[0].UserName,
                    Password = mptbRQComObject.SupplierAgencyDetails[0].Password,
                    UuId = mptbRQComObject.SupplierAgencyDetails[0].AccountNumber
                };
                flightSearchRq.FareMasterPricerTravelBoardSearch.NumberOfUnit.UnitNumberDetail.Add(new UnitNumberDetail()
                {
                    NumberOfUnits = 200,
                    TypeOfUnit = "RC"
                });
                flightSearchRq.FareMasterPricerTravelBoardSearch.NumberOfUnit.UnitNumberDetail.Add(new UnitNumberDetail()
                {
                    NumberOfUnits = nopaxunit,
                    TypeOfUnit = typeOfUnit
                });

                AddPassengerType(flightSearchRq, ad, PassengerType.ADT);
                //int acount1 = Convert.ToInt32(ad);
                AddPassengerType(flightSearchRq, ch, PassengerType.CHD, ad);
                AddPassengerType(flightSearchRq, inf, PassengerType.INF);

                var numunit = "{\"fareMasterPricerTravelBoardSearch\":{\"numberOfUnit\":{\"unitNumberDetail\":[{\"numberOfUnits\":200,\"typeOfUnit\":\"RC\"},{\"numberOfUnits\":"
                   + nopaxunit + ",\"typeOfUnit\":\"" + typeOfUnit + "\"}]},";

                var paxreference = "";
                int acount = Convert.ToInt32(ad);
                if (Convert.ToInt32(ad) > 0)
                {
                    paxreference += "\"paxReference\":[{\"ptc\":[\"ADT\"],\"traveller\":[";
                    for (int i = 0; i < Convert.ToInt32(ad); i++)
                    {
                        int count = i + 1;
                        paxreference += "{\"ref\":" + count + "},";

                    }

                    paxreference = paxreference.Trim(',');
                    paxreference += "]}";
                }

                if (Convert.ToInt32(ch) > 0)
                {

                    paxreference += ",{\"ptc\":[\"CH\"],\"traveller\":[";
                    for (int i = 0; i < Convert.ToInt32(ch); i++)
                    {
                        int count = i + acount + 1;
                        paxreference += "{\"ref\":" + count + "},";

                    }
                    paxreference = paxreference.Trim(',');

                    paxreference += "]}";

                }

                if (Convert.ToInt32(inf) > 0)
                {

                    paxreference += ",{\"ptc\":[\"INF\"],\"traveller\":[";
                    for (int i = 0; i < Convert.ToInt32(inf); i++)
                    {
                        int a = i + 1;

                        paxreference += "{\"ref\":" + a + ",\"infantIndicator\":" + a + "},";

                    }

                    paxreference = paxreference.Trim(',');
                    paxreference += "]}";
                }

                FareOptions fareOptions = flightSearchRq.FareMasterPricerTravelBoardSearch.FareOptions;

                fareOptions.ConversionRate.ConversionRateDetail.Add(new ConversionRateDetail
                {
                    Currency = mptbRQComObject.Currency
                });
                var values = EnumUtil.GetValues<PriceType>().ToList();
                values.ForEach(pt => flightSearchRq.FareMasterPricerTravelBoardSearch.FareOptions.PricingTickInfo.PricingTicketing.PriceType.Add(pt.ToString()));

                Models.TravelFlightInfo travelFlightInfo = flightSearchRq.FareMasterPricerTravelBoardSearch.TravelFlightInfo;
                if (cabin != Cabin.All.ToString())
                {
                    travelFlightInfo.CabinId.CabinQualifier = CabinQualifiers.MC.ToString();
                    travelFlightInfo.CabinId.Cabin.Add(cabin);
                }

                var fareoption = "],\"fareOptions\":{\"pricingTickInfo\":{\"pricingTicketing\":{\"priceType\":[\"CUC\",\"IFS\"]}},\"conversionRate\":{\"conversionRateDetail\":[{\"currency\":\"" + currency + "\"}]}},";
                var travelflightinfo = "";
                if (cabin != "All")
                {
                    travelflightinfo = "\"travelFlightInfo\":{\"cabinId\":{\"cabinQualifier\":\"MC\",\"cabin\":[\""
                        + cabin + "\"]}},";
                }
                else
                {
                    travelflightinfo = "";
                }

                int segRef1 = 1;
                List<Itinerary> Itineraries = flightSearchRq.FareMasterPricerTravelBoardSearch.Itinerary;
                foreach (var legItem in mptbRQComObject.OriginDestinationInformation)
                {
                    Itinerary _itinerary = new Itinerary();
                    _itinerary.RequestedSegmentRef.SegRef = segRef1;
                    _itinerary.DepartureLocalization.DeparturePoint.LocationId = legItem.OriginLocation;
                    _itinerary.ArrivalLocalization.ArrivalPointDetails.LocationId = legItem.DestinationLocation;
                    _itinerary.TimeDetails.FirstDateTimeDetail.Date = legItem.DepartureDate.GetFormatedDate();
                    Itineraries.Add(_itinerary);
                    segRef1++;
                }

                var itinerary = "\"itinerary\":[";
                int segRef = 1;
                foreach (var legItem in mptbRQComObject.OriginDestinationInformation)
                {
                    string from = legItem.OriginLocation;
                    string to = legItem.DestinationLocation;

                    string DepartureDate = legItem.DepartureDate;
                    string DD = DepartureDate.Split('-')[0];
                    string MM = DepartureDate.Split('-')[1];
                    string YY = DepartureDate.Split('-')[2];
                    YY = YY.Substring(YY.Length - 2);

                    itinerary += "{\"requestedSegmentRef\":{\"segRef\":" + segRef + "},\"departureLocalization\":{\"departurePoint\":{\"locationId\":\""
                       + from + "\"}},\"arrivalLocalization\":{\"arrivalPointDetails\":{\"locationId\":\""
                       + to + "\"}},\"timeDetails\":{\"firstDateTimeDetail\":{\"date\":\"" + DD + MM + YY + "\"}}},";
                    segRef++;
                }
                itinerary = itinerary.TrimEnd(',');
                itinerary += "]},";

                var login = "\"loginId\":\"" + loginid + "\",\"password\":\"" + password + "\",\"uuId\":\"" + uuid + "\"}";

                var sb = new StringBuilder();
                sb.Append(numunit);
                sb.Append(paxreference);
                sb.Append(fareoption);
                sb.Append(travelflightinfo);
                sb.Append(itinerary);
                sb.Append(login);

                var data = sb.ToString();
                // var data1 = Serialize.ToJson(flightSearchRq);

                // string RQUrl = mptbRQComObject.SupplierAgencyDetails[0].BaseUrl + "/masterpricer";
                string RQUrl = "http://70.32.24.42:9000/masterpricer";
                //string strResult = await soapHelperAsync.SendRESTRequestAsync(sb.ToString(), RQUrl, "POST");
                string strResult = await soapHelperAsync.SendRESTRequestAsync(data, RQUrl, "POST");
                if (string.IsNullOrEmpty(strResult))
                    return null;
                else
                    return await MPTBtoCommonTest(strResult, AgencyCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public async Task<FLcommonRS> MptbSearch(Rootobject mptbRQComObject)
        {
            try
            {
                int noresult = mptbRQComObject.CommonRequestSearch.NumberOfUnits;
                string typeOfUnit = mptbRQComObject.CommonRequestSearch.TypeOfUnit;
                int ad = mptbRQComObject.PassengerTypeQuantity.ADT;
                int ch = mptbRQComObject.PassengerTypeQuantity.CHD;
                int inf = mptbRQComObject.PassengerTypeQuantity.INF;
                string cabin = mptbRQComObject.cabin;
                string AgencyCode = mptbRQComObject.CommonRequestSearch.AgencyCode;
                long nopaxunit = ad + ch;
                string scount = Convert.ToString(ad + ch + inf);
                //string loginid = "fazil";
                //string password = "123";
                //string uuid = "1Qwer2Asdf3Zxcv!";
                string loginid = mptbRQComObject.SupplierAgencyDetails[0].UserName;
                string password = mptbRQComObject.SupplierAgencyDetails[0].Password;
                string uuid = mptbRQComObject.SupplierAgencyDetails[0].AccountNumber;

                FlightSearchRq flightSearchRq = new FlightSearchRq()
                {
                    LoginId = loginid,
                    Password = password,
                    UuId = uuid
                };
                flightSearchRq.FareMasterPricerTravelBoardSearch.NumberOfUnit.UnitNumberDetail.Add(new UnitNumberDetail()
                {
                    NumberOfUnits = 200,
                    TypeOfUnit = "RC"
                });
                flightSearchRq.FareMasterPricerTravelBoardSearch.NumberOfUnit.UnitNumberDetail.Add(new UnitNumberDetail()
                {
                    NumberOfUnits = nopaxunit,
                    TypeOfUnit = typeOfUnit
                });

                AddPassengerType(flightSearchRq, ad, PassengerType.ADT);
                AddPassengerType(flightSearchRq, ch, PassengerType.CHD, ad);
                AddPassengerType(flightSearchRq, inf, PassengerType.INF);

                FareOptions fareOptions = flightSearchRq.FareMasterPricerTravelBoardSearch.FareOptions;

                fareOptions.ConversionRate.ConversionRateDetail.Add(new ConversionRateDetail
                {
                    Currency = mptbRQComObject.Currency
                });
                var values = EnumUtil.GetValues<PriceType>().ToList();
                values.ForEach(pt => flightSearchRq.FareMasterPricerTravelBoardSearch.FareOptions.PricingTickInfo.PricingTicketing.PriceType.Add(pt.ToString()));

                Models.TravelFlightInfo travelFlightInfo = flightSearchRq.FareMasterPricerTravelBoardSearch.TravelFlightInfo;
                if (cabin == "all")
                {
                    cabin = "Y";
                }
                if (cabin != Cabin.All.ToString())
                {
                    travelFlightInfo.CabinId.CabinQualifier = CabinQualifiers.MC.ToString();
                    travelFlightInfo.CabinId.Cabin.Add(cabin);
                }

                int segRef = 1;
                List<Itinerary> Itineraries = flightSearchRq.FareMasterPricerTravelBoardSearch.Itinerary;
                foreach (var legItem in mptbRQComObject.OriginDestinationInformation)
                {
                    Itinerary _itinerary = new Itinerary();
                    _itinerary.RequestedSegmentRef.SegRef = segRef;
                    _itinerary.DepartureLocalization.DeparturePoint.LocationId = legItem.OriginLocation;
                    _itinerary.ArrivalLocalization.ArrivalPointDetails.LocationId = legItem.DestinationLocation;
                    _itinerary.TimeDetails.FirstDateTimeDetail.Date = legItem.DepartureDate.GetFormatedDate();
                    Itineraries.Add(_itinerary);
                    segRef++;
                }

                var jsonData = Serialize.ToJson(flightSearchRq);
                string RQUrl = "http://70.32.24.42:9000/masterpricer";
                //  string RQUrl = "http://192.168.1.114:9000/masterpricer";
                string strResult = await soapHelperAsync.SendRESTRequestAsync(jsonData, RQUrl, "POST");
                //Save Log
                string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(flightSearchRq);

                CommonServices.SaveLog("FLSEARCH", mptbRQComObject.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, strResult);
                //Save Log

                // string strResult = "{'fareMasterPricerTravelBoardSearchReply':{'replyStatus':{'status':[{'advisoryTypeInfo':'FQX'}]},'conversionRate':{'conversionRateDetail':[{'currency':'AED'}]},'flightIndex':[{'requestedSegmentRef':{'segRef':1},'groupOfFlights':[{'propFlightGrDetail':{'flightProposal':[{'ref':'1'},{'ref':'0400','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1210','dateOfArrival':'070418','timeOfArrival':'1740'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'527','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0400'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'2'},{'ref':'0725','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0445','dateOfArrival':'070418','timeOfArrival':'0605'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6558','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0805','dateOfArrival':'070418','timeOfArrival':'1340'},'location':[{'locationId':'AUH','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'575','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'3'},{'ref':'1220','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2350','dateOfArrival':'080418','timeOfArrival':'0110','dateVariation':1},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6567','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0805','dateOfArrival':'080418','timeOfArrival':'1340'},'location':[{'locationId':'AUH','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'575','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'4'},{'ref':'1340','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2230','dateOfArrival':'070418','timeOfArrival':'2350'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6566','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0805','dateOfArrival':'080418','timeOfArrival':'1340'},'location':[{'locationId':'AUH','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'575','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'5'},{'ref':'0355','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1335','dateOfArrival':'070418','timeOfArrival':'1900'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'934','productDetail':{'equipmentType':'788'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0355'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'6'},{'ref':'0720','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2325','dateOfArrival':'080418','timeOfArrival':'0405','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'557','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0620','dateOfArrival':'080418','timeOfArrival':'0815'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'436','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0155'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'7'},{'ref':'0830','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2215','dateOfArrival':'080418','timeOfArrival':'0250','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'543','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0305'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0620','dateOfArrival':'080418','timeOfArrival':'0815'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'436','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0155'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'8'},{'ref':'0905','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0500','dateOfArrival':'070418','timeOfArrival':'0945'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'579','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0315'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1335','dateOfArrival':'070418','timeOfArrival':'1535'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'403','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'9'},{'ref':'1020','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2325','dateOfArrival':'080418','timeOfArrival':'0405','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'557','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0915','dateOfArrival':'080418','timeOfArrival':'1115'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'407','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'10'},{'ref':'1130','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2215','dateOfArrival':'080418','timeOfArrival':'0250','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'543','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0305'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0915','dateOfArrival':'080418','timeOfArrival':'1115'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'407','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'11'},{'ref':'1440','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2325','dateOfArrival':'080418','timeOfArrival':'0405','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'557','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1335','dateOfArrival':'080418','timeOfArrival':'1535'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'403','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'12'},{'ref':'1550','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2215','dateOfArrival':'080418','timeOfArrival':'0250','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'543','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0305'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1335','dateOfArrival':'080418','timeOfArrival':'1535'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'403','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'13'},{'ref':'0830','unitQualifier':'EFT'},{'ref':'UL','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2250','dateOfArrival':'080418','timeOfArrival':'0500','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'CMB'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'226','productDetail':{'equipmentType':'333'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0440'}]},'flightCharacteristics':{'inFlightSrv':['4','15','18','20']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0730','dateOfArrival':'080418','timeOfArrival':'0850'},'location':[{'locationId':'CMB'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'165','productDetail':{'equipmentType':'320'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]},'flightCharacteristics':{'inFlightSrv':['4']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'14'},{'ref':'1505','unitQualifier':'EFT'},{'ref':'UL','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2250','dateOfArrival':'080418','timeOfArrival':'0500','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'CMB'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'226','productDetail':{'equipmentType':'333'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0440'}]},'flightCharacteristics':{'inFlightSrv':['4','15','18','20']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1405','dateOfArrival':'080418','timeOfArrival':'1525'},'location':[{'locationId':'CMB'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'167','productDetail':{'equipmentType':'32B'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'15'},{'ref':'2730','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2330','dateOfArrival':'080418','timeOfArrival':'0500','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MAA','terminal':'4'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'906','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0400'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'090418','timeOfDeparture':'0200','dateOfArrival':'090418','timeOfArrival':'0430'},'location':[{'locationId':'MAA'},{'locationId':'COK'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'1626','productDetail':{'equipmentType':'CR7'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0230'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'16'},{'ref':'0925','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2330','dateOfArrival':'080418','timeOfArrival':'0500','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MAA','terminal':'4'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'906','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0400'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0900','dateOfArrival':'080418','timeOfArrival':'1025'},'location':[{'locationId':'MAA','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'509','productDetail':{'equipmentType':'319'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0125'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'17'},{'ref':'0410','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2010','dateOfArrival':'080418','timeOfArrival':'0150','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'441','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0410'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'18'},{'ref':'1035','unitQualifier':'EFT'},{'ref':'KU','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1310','dateOfArrival':'070418','timeOfArrival':'1355'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'KWI','terminal':'M'}],'companyId':{'marketingCarrier':'KU','operatingCarrier':'KU'},'flightOrtrainNumber':'672','productDetail':{'equipmentType':'332'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0145'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1740','dateOfArrival':'080418','timeOfArrival':'0115','dateVariation':1},'location':[{'locationId':'KWI','terminal':'M'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'KU','operatingCarrier':'KU'},'flightOrtrainNumber':'357','productDetail':{'equipmentType':'332'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0505'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'19'},{'ref':'2655','unitQualifier':'EFT'},{'ref':'KU','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2050','dateOfArrival':'070418','timeOfArrival':'2135'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'KWI','terminal':'M'}],'companyId':{'marketingCarrier':'KU','operatingCarrier':'KU'},'flightOrtrainNumber':'676','productDetail':{'equipmentType':'77W'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0145'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1740','dateOfArrival':'090418','timeOfArrival':'0115','dateVariation':1},'location':[{'locationId':'KWI','terminal':'M'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'KU','operatingCarrier':'KU'},'flightOrtrainNumber':'357','productDetail':{'equipmentType':'332'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0505'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'20'},{'ref':'1735','unitQualifier':'EFT'},{'ref':'KU','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0610','dateOfArrival':'070418','timeOfArrival':'0655'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'KWI','terminal':'M'}],'companyId':{'marketingCarrier':'KU','operatingCarrier':'KU'},'flightOrtrainNumber':'678','productDetail':{'equipmentType':'320'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0145'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1740','dateOfArrival':'080418','timeOfArrival':'0115','dateVariation':1},'location':[{'locationId':'KWI','terminal':'M'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'KU','operatingCarrier':'KU'},'flightOrtrainNumber':'357','productDetail':{'equipmentType':'332'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0505'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'21'},{'ref':'2850','unitQualifier':'EFT'},{'ref':'KU','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1855','dateOfArrival':'070418','timeOfArrival':'1940'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'KWI','terminal':'M'}],'companyId':{'marketingCarrier':'KU','operatingCarrier':'KU'},'flightOrtrainNumber':'674','productDetail':{'equipmentType':'320'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0145'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1740','dateOfArrival':'090418','timeOfArrival':'0115','dateVariation':1},'location':[{'locationId':'KWI','terminal':'M'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'KU','operatingCarrier':'KU'},'flightOrtrainNumber':'357','productDetail':{'equipmentType':'332'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0505'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'22'},{'ref':'0615','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2340','dateOfArrival':'080418','timeOfArrival':'0410','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'984','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0300'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0530','dateOfArrival':'080418','timeOfArrival':'0725'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'54','productDetail':{'equipmentType':'319'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0155'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'23'},{'ref':'1805','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2340','dateOfArrival':'080418','timeOfArrival':'0410','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'984','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0300'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1715','dateOfArrival':'080418','timeOfArrival':'1915'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'681','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'24'},{'ref':'0600','unitQualifier':'EFT'},{'ref':'WY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2325','dateOfArrival':'080418','timeOfArrival':'0040','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MCT'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'614','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0140','dateOfArrival':'080418','timeOfArrival':'0655'},'location':[{'locationId':'MCT'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'225','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0345'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'25'},{'ref':'0615','unitQualifier':'EFT'},{'ref':'WY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0605','dateOfArrival':'070418','timeOfArrival':'0720'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MCT'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'602','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0835','dateOfArrival':'070418','timeOfArrival':'1350'},'location':[{'locationId':'MCT'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'223','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0345'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'26'},{'ref':'0640','unitQualifier':'EFT'},{'ref':'WY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2245','dateOfArrival':'080418','timeOfArrival':'0005','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MCT'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'612','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0140','dateOfArrival':'080418','timeOfArrival':'0655'},'location':[{'locationId':'MCT'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'225','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0345'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'27'},{'ref':'1210','unitQualifier':'EFT'},{'ref':'WY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1715','dateOfArrival':'070418','timeOfArrival':'1830'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MCT'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'610','productDetail':{'equipmentType':'E75'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0140','dateOfArrival':'080418','timeOfArrival':'0655'},'location':[{'locationId':'MCT'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'225','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0345'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'28'},{'ref':'1255','unitQualifier':'EFT'},{'ref':'WY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2325','dateOfArrival':'080418','timeOfArrival':'0040','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MCT'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'614','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0835','dateOfArrival':'080418','timeOfArrival':'1350'},'location':[{'locationId':'MCT'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'223','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0345'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'29'},{'ref':'1335','unitQualifier':'EFT'},{'ref':'WY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2245','dateOfArrival':'080418','timeOfArrival':'0005','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MCT'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'612','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0835','dateOfArrival':'080418','timeOfArrival':'1350'},'location':[{'locationId':'MCT'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'223','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0345'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'30'},{'ref':'1700','unitQualifier':'EFT'},{'ref':'WY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1225','dateOfArrival':'070418','timeOfArrival':'1340'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MCT'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'606','productDetail':{'equipmentType':'E75'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0140','dateOfArrival':'080418','timeOfArrival':'0655'},'location':[{'locationId':'MCT'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'225','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0345'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'31'},{'ref':'1820','unitQualifier':'EFT'},{'ref':'WY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1105','dateOfArrival':'070418','timeOfArrival':'1220'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MCT'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'604','productDetail':{'equipmentType':'332'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0140','dateOfArrival':'080418','timeOfArrival':'0655'},'location':[{'locationId':'MCT'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'225','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0345'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'32'},{'ref':'1905','unitQualifier':'EFT'},{'ref':'WY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1715','dateOfArrival':'070418','timeOfArrival':'1830'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MCT'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'610','productDetail':{'equipmentType':'E75'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0835','dateOfArrival':'080418','timeOfArrival':'1350'},'location':[{'locationId':'MCT'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'223','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0345'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'33'},{'ref':'2320','unitQualifier':'EFT'},{'ref':'WY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0605','dateOfArrival':'070418','timeOfArrival':'0720'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'MCT'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'602','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0140','dateOfArrival':'080418','timeOfArrival':'0655'},'location':[{'locationId':'MCT'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'WY','operatingCarrier':'WY'},'flightOrtrainNumber':'225','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0345'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'34'},{'ref':'0800','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2320','dateOfArrival':'080418','timeOfArrival':'0520','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'CMB'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'547','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0430'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0730','dateOfArrival':'080418','timeOfArrival':'0850'},'location':[{'locationId':'CMB'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'165','productDetail':{'equipmentType':'320'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]},'flightCharacteristics':{'inFlightSrv':['4']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'35'},{'ref':'1220','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0135','dateOfArrival':'070418','timeOfArrival':'1030'},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'CMB'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'559','productDetail':{'equipmentType':'73H','techStopNumber':1},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0725'}]},'technicalStop':[{'stopDetails':[{'dateQualifier':'AA','date':'070418','firstTime':'0705','locationId':'MLE'},{'dateQualifier':'AD','date':'070418','firstTime':'0835'}]}]},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1405','dateOfArrival':'070418','timeOfArrival':'1525'},'location':[{'locationId':'CMB'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'167','productDetail':{'equipmentType':'32B'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'36'},{'ref':'1225','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1855','dateOfArrival':'080418','timeOfArrival':'0110','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'CMB'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'557','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0445'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0730','dateOfArrival':'080418','timeOfArrival':'0850'},'location':[{'locationId':'CMB'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'165','productDetail':{'equipmentType':'320'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]},'flightCharacteristics':{'inFlightSrv':['4']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'37'},{'ref':'1435','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2320','dateOfArrival':'080418','timeOfArrival':'0520','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'CMB'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'547','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0430'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1405','dateOfArrival':'080418','timeOfArrival':'1525'},'location':[{'locationId':'CMB'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'167','productDetail':{'equipmentType':'32B'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'38'},{'ref':'1510','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1610','dateOfArrival':'080418','timeOfArrival':'0035','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'CMB'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'555','productDetail':{'equipmentType':'73H','techStopNumber':1},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0655'}]},'technicalStop':[{'stopDetails':[{'dateQualifier':'AA','date':'070418','firstTime':'2140','locationId':'MLE'},{'dateQualifier':'AD','date':'070418','firstTime':'2240'}]}]},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0730','dateOfArrival':'080418','timeOfArrival':'0850'},'location':[{'locationId':'CMB'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'165','productDetail':{'equipmentType':'320'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]},'flightCharacteristics':{'inFlightSrv':['4']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'39'},{'ref':'1900','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1855','dateOfArrival':'080418','timeOfArrival':'0110','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'CMB'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'557','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0445'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1405','dateOfArrival':'080418','timeOfArrival':'1525'},'location':[{'locationId':'CMB'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'167','productDetail':{'equipmentType':'32B'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'40'},{'ref':'2145','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1610','dateOfArrival':'080418','timeOfArrival':'0035','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'CMB'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'555','productDetail':{'equipmentType':'73H','techStopNumber':1},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0655'}]},'technicalStop':[{'stopDetails':[{'dateQualifier':'AA','date':'070418','firstTime':'2140','locationId':'MLE'},{'dateQualifier':'AD','date':'070418','firstTime':'2240'}]}]},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1405','dateOfArrival':'080418','timeOfArrival':'1525'},'location':[{'locationId':'CMB'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'UL','operatingCarrier':'UL'},'flightOrtrainNumber':'167','productDetail':{'equipmentType':'32B'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'41'},{'ref':'0715','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2330','dateOfArrival':'080418','timeOfArrival':'0410','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'445','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0620','dateOfArrival':'080418','timeOfArrival':'0815'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'436','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0155'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'42'},{'ref':'1015','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2330','dateOfArrival':'080418','timeOfArrival':'0410','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'445','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0915','dateOfArrival':'080418','timeOfArrival':'1115'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'407','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'43'},{'ref':'1435','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2330','dateOfArrival':'080418','timeOfArrival':'0410','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'445','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1335','dateOfArrival':'080418','timeOfArrival':'1535'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'403','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'44'},{'ref':'2855','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2205','dateOfArrival':'080418','timeOfArrival':'0400','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'MAA','terminal':'1'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'447','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0425'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'090418','timeOfDeparture':'0200','dateOfArrival':'090418','timeOfArrival':'0430'},'location':[{'locationId':'MAA'},{'locationId':'COK'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'1626','productDetail':{'equipmentType':'CR7'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0230'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'45'},{'ref':'1050','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2205','dateOfArrival':'080418','timeOfArrival':'0400','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'MAA','terminal':'1'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'447','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0425'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0900','dateOfArrival':'080418','timeOfArrival':'1025'},'location':[{'locationId':'MAA','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'509','productDetail':{'equipmentType':'319'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0125'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'46'},{'ref':'1925','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2205','dateOfArrival':'080418','timeOfArrival':'0255','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'DEL','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'547','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0320'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1555','dateOfArrival':'080418','timeOfArrival':'1900'},'location':[{'locationId':'DEL','terminal':'3'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'912','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0305'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'47'},{'ref':'2220','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1910','dateOfArrival':'080418','timeOfArrival':'0015','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'DEL','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'587','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0335'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1555','dateOfArrival':'080418','timeOfArrival':'1900'},'location':[{'locationId':'DEL','terminal':'3'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'912','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0305'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'48'},{'ref':'0705','unitQualifier':'EFT'},{'ref':'GF','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1910','dateOfArrival':'070418','timeOfArrival':'1925'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BAH'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'509','productDetail':{'equipmentType':'320'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2025','dateOfArrival':'080418','timeOfArrival':'0345','dateVariation':1},'location':[{'locationId':'BAH'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'270','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0450'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'49'},{'ref':'0915','unitQualifier':'EFT'},{'ref':'GF','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1700','dateOfArrival':'070418','timeOfArrival':'1715'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BAH'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'507','productDetail':{'equipmentType':'320'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2025','dateOfArrival':'080418','timeOfArrival':'0345','dateVariation':1},'location':[{'locationId':'BAH'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'270','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0450'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'50'},{'ref':'1315','unitQualifier':'EFT'},{'ref':'GF','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1300','dateOfArrival':'070418','timeOfArrival':'1315'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BAH'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'505','productDetail':{'equipmentType':'320'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2025','dateOfArrival':'080418','timeOfArrival':'0345','dateVariation':1},'location':[{'locationId':'BAH'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'270','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0450'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'51'},{'ref':'1600','unitQualifier':'EFT'},{'ref':'GF','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1015','dateOfArrival':'070418','timeOfArrival':'1030'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BAH'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'503','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2025','dateOfArrival':'080418','timeOfArrival':'0345','dateVariation':1},'location':[{'locationId':'BAH'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'270','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0450'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'52'},{'ref':'1815','unitQualifier':'EFT'},{'ref':'GF','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0800','dateOfArrival':'070418','timeOfArrival':'0815'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BAH'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'501','productDetail':{'equipmentType':'320'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2025','dateOfArrival':'080418','timeOfArrival':'0345','dateVariation':1},'location':[{'locationId':'BAH'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'270','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0450'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'53'},{'ref':'0725','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0445','dateOfArrival':'070418','timeOfArrival':'0605'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5411','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0805','dateOfArrival':'070418','timeOfArrival':'1340'},'location':[{'locationId':'AUH','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'9W'},'flightOrtrainNumber':'8705','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'54'},{'ref':'1220','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2350','dateOfArrival':'080418','timeOfArrival':'0110','dateVariation':1},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5429','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0805','dateOfArrival':'080418','timeOfArrival':'1340'},'location':[{'locationId':'AUH','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'9W'},'flightOrtrainNumber':'8705','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'55'},{'ref':'0735','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1030','dateOfArrival':'070418','timeOfArrival':'1150'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5417','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1400','dateOfArrival':'070418','timeOfArrival':'1935'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'282','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'56'},{'ref':'0800','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1740','dateOfArrival':'070418','timeOfArrival':'1900'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5421','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2135','dateOfArrival':'080418','timeOfArrival':'0310','dateVariation':1},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'280','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'57'},{'ref':'1105','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0700','dateOfArrival':'070418','timeOfArrival':'0820'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5415','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1400','dateOfArrival':'070418','timeOfArrival':'1935'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'282','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'NDR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'58'},{'ref':'1205','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0600','dateOfArrival':'070418','timeOfArrival':'0720'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5413','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1400','dateOfArrival':'070418','timeOfArrival':'1935'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'282','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'59'},{'ref':'1320','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0445','dateOfArrival':'070418','timeOfArrival':'0605'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5411','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1400','dateOfArrival':'070418','timeOfArrival':'1935'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'282','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'60'},{'ref':'1340','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1200','dateOfArrival':'070418','timeOfArrival':'1320'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5419','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2135','dateOfArrival':'080418','timeOfArrival':'0310','dateVariation':1},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'280','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'61'},{'ref':'1510','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1030','dateOfArrival':'070418','timeOfArrival':'1150'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5417','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2135','dateOfArrival':'080418','timeOfArrival':'0310','dateVariation':1},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'280','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'62'},{'ref':'1815','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2350','dateOfArrival':'080418','timeOfArrival':'0110','dateVariation':1},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5429','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1400','dateOfArrival':'080418','timeOfArrival':'1935'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'282','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'63'},{'ref':'1840','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0700','dateOfArrival':'070418','timeOfArrival':'0820'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5415','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2135','dateOfArrival':'080418','timeOfArrival':'0310','dateVariation':1},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'280','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'64'},{'ref':'1940','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0600','dateOfArrival':'070418','timeOfArrival':'0720'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5413','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2135','dateOfArrival':'080418','timeOfArrival':'0310','dateVariation':1},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'280','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'65'},{'ref':'2055','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0445','dateOfArrival':'070418','timeOfArrival':'0605'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5411','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2135','dateOfArrival':'080418','timeOfArrival':'0310','dateVariation':1},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'280','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'66'},{'ref':'2550','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2350','dateOfArrival':'080418','timeOfArrival':'0110','dateVariation':1},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5429','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'2135','dateOfArrival':'090418','timeOfArrival':'0310','dateVariation':1},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'280','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'67'},{'ref':'1815','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2330','dateOfArrival':'080418','timeOfArrival':'0410','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'445','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1715','dateOfArrival':'080418','timeOfArrival':'1915'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'681','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'68'},{'ref':'1915','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2215','dateOfArrival':'080418','timeOfArrival':'0305','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'DEL','terminal':'3'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'431','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0320'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1555','dateOfArrival':'080418','timeOfArrival':'1900'},'location':[{'locationId':'DEL','terminal':'3'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'912','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0305'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'69'},{'ref':'0710','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2350','dateOfArrival':'080418','timeOfArrival':'0110','dateVariation':1},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6567','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6616','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'70'},{'ref':'0735','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1030','dateOfArrival':'070418','timeOfArrival':'1150'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6561','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1400','dateOfArrival':'070418','timeOfArrival':'1935'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6527','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'71'},{'ref':'0800','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1740','dateOfArrival':'070418','timeOfArrival':'1900'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6563','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2135','dateOfArrival':'080418','timeOfArrival':'0310','dateVariation':1},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6106','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'72'},{'ref':'0830','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2230','dateOfArrival':'070418','timeOfArrival':'2350'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6566','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6616','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'73'},{'ref':'1105','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0700','dateOfArrival':'070418','timeOfArrival':'0820'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6560','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1400','dateOfArrival':'070418','timeOfArrival':'1935'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6527','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'74'},{'ref':'1130','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1930','dateOfArrival':'070418','timeOfArrival':'2050'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6565','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6616','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'75'},{'ref':'1200','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1900','dateOfArrival':'070418','timeOfArrival':'2020'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6564','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6616','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'76'},{'ref':'1205','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0600','dateOfArrival':'070418','timeOfArrival':'0720'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6559','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1400','dateOfArrival':'070418','timeOfArrival':'1935'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6527','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'77'},{'ref':'1640','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1930','dateOfArrival':'070418','timeOfArrival':'2050'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'EY'},'flightOrtrainNumber':'6565','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0805','dateOfArrival':'080418','timeOfArrival':'1340'},'location':[{'locationId':'AUH','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'575','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'78'},{'ref':'1535','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1510','dateOfArrival':'070418','timeOfArrival':'2000'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'535','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0320'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0620','dateOfArrival':'080418','timeOfArrival':'0815'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'436','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0155'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'79'},{'ref':'1210','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1835','dateOfArrival':'070418','timeOfArrival':'2320'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'537','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0315'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0620','dateOfArrival':'080418','timeOfArrival':'0815'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'436','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0155'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'80'},{'ref':'1510','unitQualifier':'EFT'},{'ref':'9W','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1835','dateOfArrival':'070418','timeOfArrival':'2320'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'537','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0315'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0915','dateOfArrival':'080418','timeOfArrival':'1115'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'407','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'81'},{'ref':'0710','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2350','dateOfArrival':'080418','timeOfArrival':'0110','dateVariation':1},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5429','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'246','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'82'},{'ref':'0830','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2230','dateOfArrival':'070418','timeOfArrival':'2350'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5427','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'246','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'83'},{'ref':'1130','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1930','dateOfArrival':'070418','timeOfArrival':'2050'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5425','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'246','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'84'},{'ref':'1200','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1900','dateOfArrival':'070418','timeOfArrival':'2020'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5423','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'246','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'85'},{'ref':'1320','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1740','dateOfArrival':'070418','timeOfArrival':'1900'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5421','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'246','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'86'},{'ref':'1900','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1200','dateOfArrival':'070418','timeOfArrival':'1320'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5419','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'246','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'87'},{'ref':'2030','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1030','dateOfArrival':'070418','timeOfArrival':'1150'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5417','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'246','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'88'},{'ref':'2400','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0700','dateOfArrival':'070418','timeOfArrival':'0820'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5415','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'246','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'89'},{'ref':'2500','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0600','dateOfArrival':'070418','timeOfArrival':'0720'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5413','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'246','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'90'},{'ref':'2615','unitQualifier':'EFT'},{'ref':'EY','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0445','dateOfArrival':'070418','timeOfArrival':'0605'},'location':[{'locationId':'XNB'},{'locationId':'AUH','terminal':'1'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'5411','productDetail':{'equipmentType':'BUS'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0120'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0255','dateOfArrival':'080418','timeOfArrival':'0830'},'location':[{'locationId':'AUH','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EY','operatingCarrier':'EY'},'flightOrtrainNumber':'246','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'91'},{'ref':'1835','unitQualifier':'EFT'},{'ref':'GF','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0740','dateOfArrival':'070418','timeOfArrival':'0755'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BAH'}],'companyId':{'marketingCarrier':'CX','operatingCarrier':'CX'},'flightOrtrainNumber':'745','productDetail':{'equipmentType':'333'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0115'}]},'flightCharacteristics':{'inFlightSrv':['1','4','10','12','15']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2025','dateOfArrival':'080418','timeOfArrival':'0345','dateVariation':1},'location':[{'locationId':'BAH'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'GF','operatingCarrier':'GF'},'flightOrtrainNumber':'270','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0450'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'92'},{'ref':'0405','unitQualifier':'EFT'},{'ref':'EK','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2130','dateOfArrival':'080418','timeOfArrival':'0305','dateVariation':1},'location':[{'locationId':'DXB','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EK','operatingCarrier':'EK'},'flightOrtrainNumber':'532','productDetail':{'equipmentType':'77W'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0405'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'93'},{'ref':'0410','unitQualifier':'EFT'},{'ref':'EK','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0320','dateOfArrival':'070418','timeOfArrival':'0900'},'location':[{'locationId':'DXB','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'EK','operatingCarrier':'EK'},'flightOrtrainNumber':'530','productDetail':{'equipmentType':'773'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0410'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'94'},{'ref':'1915','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2215','dateOfArrival':'080418','timeOfArrival':'0305','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'DEL','terminal':'3'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'431','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0320'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1100','dateOfArrival':'080418','timeOfArrival':'1900'},'location':[{'locationId':'DEL'},{'locationId':'COK'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'1629','productDetail':{'equipmentType':'788'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0800'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'95'},{'ref':'1915','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2215','dateOfArrival':'080418','timeOfArrival':'0305','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'DEL','terminal':'3'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'431','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0320'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1100','dateOfArrival':'080418','timeOfArrival':'1900'},'location':[{'locationId':'DEL'},{'locationId':'COK'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'1631','productDetail':{'equipmentType':'788'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0800'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'96'},{'ref':'1110','unitQualifier':'EFT'},{'ref':'SV','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2230','dateOfArrival':'070418','timeOfArrival':'2335'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'RUH','terminal':'2'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'553','productDetail':{'equipmentType':'330'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0205'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0345','dateOfArrival':'080418','timeOfArrival':'1110'},'location':[{'locationId':'RUH','terminal':'2'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'892','productDetail':{'equipmentType':'333'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0455'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'97'},{'ref':'1310','unitQualifier':'EFT'},{'ref':'SV','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2030','dateOfArrival':'070418','timeOfArrival':'2135'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'RUH','terminal':'2'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'561','productDetail':{'equipmentType':'330'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0205'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0345','dateOfArrival':'080418','timeOfArrival':'1110'},'location':[{'locationId':'RUH','terminal':'2'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'892','productDetail':{'equipmentType':'333'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0455'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'98'},{'ref':'1520','unitQualifier':'EFT'},{'ref':'SV','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1820','dateOfArrival':'070418','timeOfArrival':'1925'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'RUH','terminal':'2'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'555','productDetail':{'equipmentType':'773'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0205'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0345','dateOfArrival':'080418','timeOfArrival':'1110'},'location':[{'locationId':'RUH','terminal':'2'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'892','productDetail':{'equipmentType':'333'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0455'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'99'},{'ref':'1525','unitQualifier':'EFT'},{'ref':'SV','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1820','dateOfArrival':'070418','timeOfArrival':'1925'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'RUH','terminal':'2'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'555','productDetail':{'equipmentType':'773'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0205'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0345','dateOfArrival':'080418','timeOfArrival':'1110'},'location':[{'locationId':'RUH','terminal':'2'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'892','productDetail':{'equipmentType':'333'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0455'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'100'},{'ref':'1535','unitQualifier':'EFT'},{'ref':'SV','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2300','dateOfArrival':'080418','timeOfArrival':'0120','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'JED','terminal':'S'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'595','productDetail':{'equipmentType':'330'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0320'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0745','dateOfArrival':'080418','timeOfArrival':'1605'},'location':[{'locationId':'JED','terminal':'S'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'784','productDetail':{'equipmentType':'333'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0550'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'101'},{'ref':'1740','unitQualifier':'EFT'},{'ref':'SV','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1600','dateOfArrival':'070418','timeOfArrival':'1705'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'RUH','terminal':'2'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'559','productDetail':{'equipmentType':'789'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0205'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0345','dateOfArrival':'080418','timeOfArrival':'1110'},'location':[{'locationId':'RUH','terminal':'2'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'892','productDetail':{'equipmentType':'333'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0455'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'102'},{'ref':'2125','unitQualifier':'EFT'},{'ref':'FZ','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2215','dateOfArrival':'080418','timeOfArrival':'0305','dateVariation':1},'location':[{'locationId':'DXB','terminal':'2'},{'locationId':'DEL','terminal':'3'}],'companyId':{'marketingCarrier':'FZ','operatingCarrier':'FZ'},'flightOrtrainNumber':'431','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0320'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1805','dateOfArrival':'080418','timeOfArrival':'2110'},'location':[{'locationId':'DEL','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'48','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0305'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'103'},{'ref':'1005','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2340','dateOfArrival':'080418','timeOfArrival':'0410','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'984','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0300'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0915','dateOfArrival':'080418','timeOfArrival':'1115'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'407','productDetail':{'equipmentType':'738'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'104'},{'ref':'1425','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2340','dateOfArrival':'080418','timeOfArrival':'0410','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'984','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0300'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1335','dateOfArrival':'080418','timeOfArrival':'1535'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'403','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'105'},{'ref':'1805','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2340','dateOfArrival':'080418','timeOfArrival':'0410','dateVariation':1},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'984','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0300'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'1715','dateOfArrival':'080418','timeOfArrival':'1915'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'AC','operatingCarrier':'AI'},'flightOrtrainNumber':'6448','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'N','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'106'},{'ref':'0800','unitQualifier':'EFT'},{'ref':'EK','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2155','dateOfArrival':'080418','timeOfArrival':'0230','dateVariation':1},'location':[{'locationId':'DXB','terminal':'3'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'EK','operatingCarrier':'EK'},'flightOrtrainNumber':'500','productDetail':{'equipmentType':'388'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0305'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0530','dateOfArrival':'080418','timeOfArrival':'0725'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'54','productDetail':{'equipmentType':'319'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0155'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'107'},{'ref':'0810','unitQualifier':'EFT'},{'ref':'EK','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0935','dateOfArrival':'070418','timeOfArrival':'1405'},'location':[{'locationId':'DXB','terminal':'3'},{'locationId':'BOM','terminal':'2'}],'companyId':{'marketingCarrier':'EK','operatingCarrier':'EK'},'flightOrtrainNumber':'506','productDetail':{'equipmentType':'77W'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0300'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1715','dateOfArrival':'070418','timeOfArrival':'1915'},'location':[{'locationId':'BOM','terminal':'2'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'681','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'108'},{'ref':'1155','unitQualifier':'EFT'},{'ref':'EK','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'2100','dateOfArrival':'080418','timeOfArrival':'0240','dateVariation':1},'location':[{'locationId':'DXB','terminal':'3'},{'locationId':'MAA','terminal':'4'}],'companyId':{'marketingCarrier':'EK','operatingCarrier':'EK'},'flightOrtrainNumber':'542','productDetail':{'equipmentType':'77W'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0410'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'080418','timeOfDeparture':'0900','dateOfArrival':'080418','timeOfArrival':'1025'},'location':[{'locationId':'MAA','terminal':'1'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'509','productDetail':{'equipmentType':'319'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0125'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'109'},{'ref':'1310','unitQualifier':'EFT'},{'ref':'SV','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0125','dateOfArrival':'070418','timeOfArrival':'0225'},'location':[{'locationId':'DXB','terminal':'3'},{'locationId':'RUH','terminal':'1'}],'companyId':{'marketingCarrier':'EK','operatingCarrier':'EK'},'flightOrtrainNumber':'815','productDetail':{'equipmentType':'77W'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AIP'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0200'}]}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0840','dateOfArrival':'070418','timeOfArrival':'1605'},'location':[{'locationId':'RUH','terminal':'2'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'SV','operatingCarrier':'SV'},'flightOrtrainNumber':'774','productDetail':{'equipmentType':'333'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0455'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'110'},{'ref':'1725','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0005','dateOfArrival':'070418','timeOfArrival':'0445'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'DEL','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'996','productDetail':{'equipmentType':'788'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1100','dateOfArrival':'070418','timeOfArrival':'1900'},'location':[{'locationId':'DEL'},{'locationId':'COK'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'1629','productDetail':{'equipmentType':'788'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0800'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'111'},{'ref':'1725','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0005','dateOfArrival':'070418','timeOfArrival':'0445'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'DEL','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'996','productDetail':{'equipmentType':'788'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1100','dateOfArrival':'070418','timeOfArrival':'1900'},'location':[{'locationId':'DEL'},{'locationId':'COK'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'1631','productDetail':{'equipmentType':'788'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0800'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'112'},{'ref':'1725','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0005','dateOfArrival':'070418','timeOfArrival':'0445'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'DEL','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'996','productDetail':{'equipmentType':'788'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1555','dateOfArrival':'070418','timeOfArrival':'1900'},'location':[{'locationId':'DEL','terminal':'3'},{'locationId':'COK','terminal':'2'}],'companyId':{'marketingCarrier':'9W','operatingCarrier':'9W'},'flightOrtrainNumber':'912','productDetail':{'equipmentType':'73H'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'LCA'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0305'}]}}]},{'propFlightGrDetail':{'flightProposal':[{'ref':'113'},{'ref':'1935','unitQualifier':'EFT'},{'ref':'AI','unitQualifier':'MCX'}]},'flightDetails':[{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'0005','dateOfArrival':'070418','timeOfArrival':'0445'},'location':[{'locationId':'DXB','terminal':'1'},{'locationId':'DEL','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'996','productDetail':{'equipmentType':'788'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0310'}]},'flightCharacteristics':{'inFlightSrv':['1']}},{'flightInformation':{'productDateTime':{'dateOfDeparture':'070418','timeOfDeparture':'1805','dateOfArrival':'070418','timeOfArrival':'2110'},'location':[{'locationId':'DEL','terminal':'3'},{'locationId':'COK','terminal':'3'}],'companyId':{'marketingCarrier':'AI','operatingCarrier':'AI'},'flightOrtrainNumber':'48','productDetail':{'equipmentType':'321'},'addProductDetail':{'electronicTicketing':'Y','productDetailQualifier':'AVR'},'attributeDetails':[{'attributeType':'EFT','attributeDescription':'0305'}]},'flightCharacteristics':{'inFlightSrv':['1']}}]}]}],'recommendation':[{'itemNumber':{'itemNumberId':{'number':'1'}},'recPriceInfo':{'monetaryDetail':[{'amount':1598},{'amount':348}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':1},{'refQualifier':'B','refNumber':1}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':1598,'totalTaxAmount':348,'codeShareDetails':[{'transportStageQualifier':'V','company':'9W'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':800,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'44'},'description':['LAST TKT DTE','17JAN18',' - SEE SALES RSTNS']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'P','cabin':'C','avlStatus':'3'},'fareProductDetail':{'fareBasis':'P2LSTOAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'2'}},'recPriceInfo':{'monetaryDetail':[{'amount':1598},{'amount':348}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':2},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':3},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':4},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':1598,'totalTaxAmount':348,'codeShareDetails':[{'transportStageQualifier':'V','company':'9W'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':800,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'44'},'description':['LAST TKT DTE','17JAN18',' - SEE SALES RSTNS']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'P','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'P2LSTOAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'P','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'P2LSTOAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'3'}},'recPriceInfo':{'monetaryDetail':[{'amount':1600},{'amount':120}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':5},{'refQualifier':'B','refNumber':1}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':1600,'totalTaxAmount':120,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':300,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'3'},'fareProductDetail':{'fareBasis':'ZLOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'4'}},'recPriceInfo':{'monetaryDetail':[{'amount':1768},{'amount':388}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':6},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':7},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':8},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':9},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':10},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':1768,'totalTaxAmount':388,'codeShareDetails':[{'transportStageQualifier':'V','company':'9W'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'70'},'description':['TICKETS ARE NON-REFUNDABLE']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'44'},'description':['LAST TKT DTE','17JAN18',' - SEE SALES RSTNS']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'SUR','informationType':'79'},'description':['FARE VALID FOR E TICKET ONLY']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'P','cabin':'C','avlStatus':'3'},'fareProductDetail':{'fareBasis':'P2LSTOAE','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'O','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'O2VIFOW','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'5'}},'recPriceInfo':{'monetaryDetail':[{'amount':1828},{'amount':388}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':11},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':12},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':1828,'totalTaxAmount':388,'codeShareDetails':[{'transportStageQualifier':'V','company':'9W'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'70'},'description':['TICKETS ARE NON-REFUNDABLE']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'44'},'description':['LAST TKT DTE','17JAN18',' - SEE SALES RSTNS']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'SUR','informationType':'79'},'description':['FARE VALID FOR E TICKET ONLY']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'P','cabin':'C','avlStatus':'3'},'fareProductDetail':{'fareBasis':'P2LSTOAE','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'H','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'H2VIFOW','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'6'}},'recPriceInfo':{'monetaryDetail':[{'amount':1850},{'amount':400}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':13},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':14},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':1850,'totalTaxAmount':400,'codeShareDetails':[{'transportStageQualifier':'V','company':'UL'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':430,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'I','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'IOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'I','cabin':'C','avlStatus':'5'},'fareProductDetail':{'fareBasis':'IOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'7'}},'recPriceInfo':{'monetaryDetail':[{'amount':1900},{'amount':120}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':15},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':1900,'totalTaxAmount':120,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':300,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'3'},'fareProductDetail':{'fareBasis':'ZLOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'M','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'MIP','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'8'}},'recPriceInfo':{'monetaryDetail':[{'amount':1910},{'amount':130}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':16},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':1910,'totalTaxAmount':130,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':300,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'3'},'fareProductDetail':{'fareBasis':'ZLOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'M','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'MIP','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'9'}},'recPriceInfo':{'monetaryDetail':[{'amount':2050},{'amount':160}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':17},{'refQualifier':'B','refNumber':1}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2050,'totalTaxAmount':160,'codeShareDetails':[{'transportStageQualifier':'V','company':'FZ'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':200,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'40'},'description':['LAST TKT DTE','07APR18',' - SEE ADV PURCHASE']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'COW1AE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'10'}},'recPriceInfo':{'monetaryDetail':[{'amount':2070},{'amount':210}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':18},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':19},{'refQualifier':'B','refNumber':3}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2070,'totalTaxAmount':210,'codeShareDetails':[{'transportStageQualifier':'V','company':'KU'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':300,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'CLOWAE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'CLOWAE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'11'}},'recPriceInfo':{'monetaryDetail':[{'amount':2070},{'amount':210}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':20},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':21},{'refQualifier':'B','refNumber':3}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2070,'totalTaxAmount':210,'codeShareDetails':[{'transportStageQualifier':'V','company':'KU'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':300,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'8'},'fareProductDetail':{'fareBasis':'CLOWAE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'CLOWAE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'12'}},'recPriceInfo':{'monetaryDetail':[{'amount':2270},{'amount':130}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':22},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':23},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2270,'totalTaxAmount':130,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':300,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'ZLOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'M','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'MIP','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'13'}},'recPriceInfo':{'monetaryDetail':[{'amount':2310},{'amount':150}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':24},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':25},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':26},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':27},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':28},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':29},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':30},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':31},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':32},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':33},{'refQualifier':'B','refNumber':3}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2310,'totalTaxAmount':150,'codeShareDetails':[{'transportStageQualifier':'V','company':'WY'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':400,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'DVBOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'DVBOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'14'}},'recPriceInfo':{'monetaryDetail':[{'amount':2310},{'amount':400}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':34},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':35},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':36},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':37},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':38},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':39},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':40},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2310,'totalTaxAmount':400,'codeShareDetails':[{'transportStageQualifier':'V','company':'UL'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':230,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'40'},'description':['LAST TKT DTE','01APR18',' - SEE ADV PURCHASE']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'ZOW1AE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'L','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'LAPOWLK','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'15'}},'recPriceInfo':{'monetaryDetail':[{'amount':2400},{'amount':160}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':41},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':42},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2400,'totalTaxAmount':160,'codeShareDetails':[{'transportStageQualifier':'V','company':'FZ'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'70'},'description':['TICKETS ARE NON-REFUNDABLE']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'40'},'description':['LAST TKT DTE','07APR18',' - SEE ADV PURCHASE']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'SUR','informationType':'79'},'description':['FARE VALID FOR E TICKET ONLY']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'COW1AE1','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'O','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'O2VIFOW','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'16'}},'recPriceInfo':{'monetaryDetail':[{'amount':2400},{'amount':160}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':43},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2400,'totalTaxAmount':160,'codeShareDetails':[{'transportStageQualifier':'V','company':'FZ'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'70'},'description':['TICKETS ARE NON-REFUNDABLE']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'40'},'description':['LAST TKT DTE','07APR18',' - SEE ADV PURCHASE']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'SUR','informationType':'79'},'description':['FARE VALID FOR E TICKET ONLY']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'COW1AE1','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'O','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'O2VIFOW','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'17'}},'recPriceInfo':{'monetaryDetail':[{'amount':2750},{'amount':160}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':44},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2750,'totalTaxAmount':160,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':200,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'40'},'description':['LAST TKT DTE','07APR18',' - SEE ADV PURCHASE']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'COW1AE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'Y','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'Y','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'18'}},'recPriceInfo':{'monetaryDetail':[{'amount':2760},{'amount':170}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':45},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2760,'totalTaxAmount':170,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':200,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'40'},'description':['LAST TKT DTE','07APR18',' - SEE ADV PURCHASE']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'COW1AE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'Y','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'Y','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'19'}},'recPriceInfo':{'monetaryDetail':[{'amount':2828},{'amount':398}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':46},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':47},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2828,'totalTaxAmount':398,'codeShareDetails':[{'transportStageQualifier':'V','company':'9W'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'70'},'description':['TICKETS ARE NON-REFUNDABLE']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'44'},'description':['LAST TKT DTE','17JAN18',' - SEE SALES RSTNS']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'SUR','informationType':'79'},'description':['FARE VALID FOR E TICKET ONLY']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'P','cabin':'C','avlStatus':'3'},'fareProductDetail':{'fareBasis':'P2LSTOAE','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'P','cabin':'C','avlStatus':'3'},'fareProductDetail':{'fareBasis':'P2VIFOW','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'20'}},'recPriceInfo':{'monetaryDetail':[{'amount':2925},{'amount':325}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':48},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':49},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':50},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':51},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':52},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2925,'totalTaxAmount':325,'codeShareDetails':[{'transportStageQualifier':'V','company':'GF'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':190,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'COWAE8','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'COWAE8','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'21'}},'recPriceInfo':{'monetaryDetail':[{'amount':2964},{'amount':344}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':53},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':54},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2964,'totalTaxAmount':344,'codeShareDetails':[{'transportStageQualifier':'V','company':'EY'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':800,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'DLAPOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'},'ticketInfos':{'additionalFareDetails':{'ticketDesignator':'JF'}}},{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'DLAPOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'},'ticketInfos':{'additionalFareDetails':{'ticketDesignator':'JF'}}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'22'}},'recPriceInfo':{'monetaryDetail':[{'amount':2964},{'amount':344}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':55},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':56},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':57},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':58},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':59},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':60},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':61},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':62},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':63},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':64},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':65},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':66},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':2964,'totalTaxAmount':344,'codeShareDetails':[{'transportStageQualifier':'V','company':'EY'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':800,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'DLAPOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'},'ticketInfos':{'additionalFareDetails':{'ticketDesignator':'JF'}}},{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'DLAPOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'},'ticketInfos':{'additionalFareDetails':{'ticketDesignator':'JF'}}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'23'}},'recPriceInfo':{'monetaryDetail':[{'amount':3280},{'amount':170}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':67},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':3280,'totalTaxAmount':170,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':200,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'40'},'description':['LAST TKT DTE','07APR18',' - SEE ADV PURCHASE']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'COW1AE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'Y','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'Y','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'24'}},'recPriceInfo':{'monetaryDetail':[{'amount':3440},{'amount':160}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':68},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':3440,'totalTaxAmount':160,'codeShareDetails':[{'transportStageQualifier':'V','company':'FZ'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'70'},'description':['TICKETS ARE NON-REFUNDABLE']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'40'},'description':['LAST TKT DTE','07APR18',' - SEE ADV PURCHASE']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'SUR','informationType':'79'},'description':['FARE VALID FOR E TICKET ONLY']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'COW1AE1','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'P','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'P2VIFOW','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'25'}},'recPriceInfo':{'monetaryDetail':[{'amount':3638},{'amount':348}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':69},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':70},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':71},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':72},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':73},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':74},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':75},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':76},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':3638,'totalTaxAmount':348,'codeShareDetails':[{'transportStageQualifier':'V','company':'9W'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'Z2LOWA','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'Z2LOWA','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'26'}},'recPriceInfo':{'monetaryDetail':[{'amount':3638},{'amount':348}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':77},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':3638,'totalTaxAmount':348,'codeShareDetails':[{'transportStageQualifier':'V','company':'9W'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'Z2LOWA','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'Z2LOWA','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'27'}},'recPriceInfo':{'monetaryDetail':[{'amount':3898},{'amount':398}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':78},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':3898,'totalTaxAmount':398,'codeShareDetails':[{'transportStageQualifier':'V','company':'9W'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'Z2LOWA','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'Z2LOWA','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'28'}},'recPriceInfo':{'monetaryDetail':[{'amount':3908},{'amount':398}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':79},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':80},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':3908,'totalTaxAmount':398,'codeShareDetails':[{'transportStageQualifier':'V','company':'9W'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'Z2LOWA','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'Z2LOWA','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'29'}},'recPriceInfo':{'monetaryDetail':[{'amount':4104},{'amount':344}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':81},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':82},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':83},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':84},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':85},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':86},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':87},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':88},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':89},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':90},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':4104,'totalTaxAmount':344,'codeShareDetails':[{'transportStageQualifier':'V','company':'EY'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':800,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'CLOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'},'ticketInfos':{'additionalFareDetails':{'ticketDesignator':'JF'}}},{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'2'},'fareProductDetail':{'fareBasis':'CLOWAE','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'},'ticketInfos':{'additionalFareDetails':{'ticketDesignator':'JF'}}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'30'}},'recPriceInfo':{'monetaryDetail':[{'amount':4115},{'amount':385}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':91},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':4115,'totalTaxAmount':385,'codeShareDetails':[{'transportStageQualifier':'V','company':'GF'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':190,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Y','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'YOW','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'6'},'fareProductDetail':{'fareBasis':'DOWBH8','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'31'}},'recPriceInfo':{'monetaryDetail':[{'amount':4120},{'amount':1180}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':92},{'refQualifier':'B','refNumber':1}]},{'referencingDetail':[{'refQualifier':'S','refNumber':93},{'refQualifier':'B','refNumber':1}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':4120,'totalTaxAmount':1180,'codeShareDetails':[{'transportStageQualifier':'V','company':'EK'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':800,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'I','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'ILSOSAE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'32'}},'recPriceInfo':{'monetaryDetail':[{'amount':4790},{'amount':170}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':94},{'refQualifier':'B','refNumber':4}]},{'referencingDetail':[{'refQualifier':'S','refNumber':95},{'refQualifier':'B','refNumber':4}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':4790,'totalTaxAmount':170,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':370,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'40'},'description':['LAST TKT DTE','07APR18',' - SEE ADV PURCHASE']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'W','cabin':'M','avlStatus':'4'},'fareProductDetail':{'fareBasis':'WOW2AE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'C','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'33'}},'recPriceInfo':{'monetaryDetail':[{'amount':4960},{'amount':230}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':96},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':97},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':98},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':99},{'refQualifier':'B','refNumber':3}]},{'referencingDetail':[{'refQualifier':'S','refNumber':100},{'refQualifier':'B','refNumber':3}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':4960,'totalTaxAmount':230,'codeShareDetails':[{'transportStageQualifier':'V','company':'SV'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'75'},'description':['PENALTY APPLIES - CHECK RULES']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'SUR','informationType':'79'},'description':['FARE VALID FOR E TICKET ONLY']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'DOWSVR','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'DOWSVR','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'34'}},'recPriceInfo':{'monetaryDetail':[{'amount':4960},{'amount':230}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':101},{'refQualifier':'B','refNumber':3}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':4960,'totalTaxAmount':230,'codeShareDetails':[{'transportStageQualifier':'V','company':'SV'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'75'},'description':['PENALTY APPLIES - CHECK RULES']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'SUR','informationType':'79'},'description':['FARE VALID FOR E TICKET ONLY']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'DOWSVR','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'D','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'DOWSVR','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'35'}},'recPriceInfo':{'monetaryDetail':[{'amount':5250},{'amount':170}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':102},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':5250,'totalTaxAmount':170,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':200,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'40'},'description':['LAST TKT DTE','07APR18',' - SEE ADV PURCHASE']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'COW1AE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'C','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'36'}},'recPriceInfo':{'monetaryDetail':[{'amount':6880},{'amount':230}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':103},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':104},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':6880,'totalTaxAmount':230,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'CIF','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'CIF','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'37'}},'recPriceInfo':{'monetaryDetail':[{'amount':6900},{'amount':250}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':105},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':6900,'totalTaxAmount':250,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'},{'company':'AC'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'CIF','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'Z','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'CIF','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'38'}},'recPriceInfo':{'monetaryDetail':[{'amount':7720},{'amount':1190}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':106},{'refQualifier':'B','refNumber':2}]},{'referencingDetail':[{'refQualifier':'S','refNumber':107},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':7720,'totalTaxAmount':1190,'codeShareDetails':[{'transportStageQualifier':'V','company':'EK'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'COOWFAE7','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'J','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'COOWFAE7','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'39'}},'recPriceInfo':{'monetaryDetail':[{'amount':8050},{'amount':1190}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':108},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':8050,'totalTaxAmount':1190,'codeShareDetails':[{'transportStageQualifier':'V','company':'EK'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'7'},'fareProductDetail':{'fareBasis':'COOWFAE7','passengerType':'ADT','fareType':['RP']},'breakPoint':'N'}},{'productInformation':{'cabinProduct':{'rbd':'J','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'COOWFAE7','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'40'}},'recPriceInfo':{'monetaryDetail':[{'amount':8870},{'amount':390}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':109},{'refQualifier':'B','refNumber':3}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':8870,'totalTaxAmount':390,'codeShareDetails':[{'transportStageQualifier':'V','company':'EK'}],'pricingTicketing':{'priceType':['OBV']}},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':400,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'M','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'MSSOSAE1','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'CIF','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'41'}},'recPriceInfo':{'monetaryDetail':[{'amount':10750},{'amount':130}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':110},{'refQualifier':'B','refNumber':4}]},{'referencingDetail':[{'refQualifier':'S','refNumber':111},{'refQualifier':'B','refNumber':4}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':10750,'totalTaxAmount':130,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':150,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'Y','cabin':'M','avlStatus':'9'},'fareProductDetail':{'fareBasis':'YIF','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'C','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'42'}},'recPriceInfo':{'monetaryDetail':[{'amount':15070},{'amount':170}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':112},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':15070,'totalTaxAmount':170,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'70'},'description':['TICKETS ARE NON-REFUNDABLE']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'SUR','informationType':'79'},'description':['FARE VALID FOR E TICKET ONLY']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'CIF','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'P','cabin':'C','avlStatus':'9'},'fareProductDetail':{'fareBasis':'P2VIFOW','passengerType':'ADT','fareType':['RP','ET']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]},{'itemNumber':{'itemNumberId':{'number':'43'}},'recPriceInfo':{'monetaryDetail':[{'amount':16770},{'amount':130}]},'segmentFlightRef':[{'referencingDetail':[{'refQualifier':'S','refNumber':113},{'refQualifier':'B','refNumber':2}]}],'paxFareProduct':[{'paxFareDetail':{'paxFareNum':'1','totalFareAmount':16770,'totalTaxAmount':130,'codeShareDetails':[{'transportStageQualifier':'V','company':'AI'}]},'paxReference':[{'ptc':['ADT'],'traveller':[{'ref':1}]}],'fare':[{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'PEN','informationType':'73'},'description':['PENALTY APPLIES']},'monetaryInformation':{'monetaryDetail':[{'amountType':'MT','amount':150,'currency':'AED'}]}},{'pricingMessage':{'freeTextQualification':{'textSubjectQualifier':'LTD','informationType':'41'},'description':['LAST TKT DTE','07APR18',' - DATE OF ORIGIN']}}],'fareDetails':[{'segmentRef':{'segRef':1},'groupOfFares':[{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'CIF','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}},{'productInformation':{'cabinProduct':{'rbd':'C','cabin':'C','avlStatus':'4'},'fareProductDetail':{'fareBasis':'C','passengerType':'ADT','fareType':['RP']},'breakPoint':'Y'}}],'majCabin':[{'bookingClassDetails':[{'designator':'C'}]}]}]}]}],'serviceFeesGrp':[{'serviceTypeInfo':{'carrierFeeDetails':{'type':'FBA'}},'serviceCoverageInfoGrp':[{'itemNumberInfo':{'itemNumber':{'number':'1'}},'serviceCovInfoGrp':[{'paxRefInfo':{'travellerDetails':[{'referenceNumber':'1'}]},'coveragePerFlightsInfo':[{'numberOfItemsDetails':{'referenceQualifier':'RS','refNum':'1'},'lastItemsDetails':[{'refOfLeg':'1'}]}],'refInfo':{'referencingDetail':[{'refQualifier':'F','refNumber':1}]}}]},{'itemNumberInfo':{'itemNumber':{'number':'2'}},'serviceCovInfoGrp':[{'paxRefInfo':{'travellerDetails':[{'referenceNumber':'1'}]},'coveragePerFlightsInfo':[{'numberOfItemsDetails':{'referenceQualifier':'RS','refNum':'1'},'lastItemsDetails':[{'refOfLeg':'1'},{'refOfLeg':'2'}]}],'refInfo':{'referencingDetail':[{'refQualifier':'F','refNumber':1}]}}]},{'itemNumberInfo':{'itemNumber':{'number':'3'}},'serviceCovInfoGrp':[{'paxRefInfo':{'travellerDetails':[{'referenceNumber':'1'}]},'coveragePerFlightsInfo':[{'numberOfItemsDetails':{'referenceQualifier':'RS','refNum':'1'},'lastItemsDetails':[{'refOfLeg':'1'},{'refOfLeg':'2'}]}],'refInfo':{'referencingDetail':[{'refQualifier':'F','refNumber':2}]}}]},{'itemNumberInfo':{'itemNumber':{'number':'4'}},'serviceCovInfoGrp':[{'paxRefInfo':{'travellerDetails':[{'referenceNumber':'1'}]},'coveragePerFlightsInfo':[{'numberOfItemsDetails':{'referenceQualifier':'RS','refNum':'1'},'lastItemsDetails':[{'refOfLeg':'1'},{'refOfLeg':'2'}]}],'refInfo':{'referencingDetail':[{'refQualifier':'F','refNumber':3}]}}]}],'globalMessageMarker':{},'freeBagAllowanceGrp':[{'freeBagAllownceInfo':{'baggageDetails':{'freeAllowance':40,'quantityCode':'W','unitQualifier':'K'}},'itemNumberInfo':{'itemNumberDetails':[{'number':1}]}},{'freeBagAllownceInfo':{'baggageDetails':{'freeAllowance':2,'quantityCode':'N'}},'itemNumberInfo':{'itemNumberDetails':[{'number':2}]}},{'freeBagAllownceInfo':{'baggageDetails':{'freeAllowance':30,'quantityCode':'W','unitQualifier':'K'}},'itemNumberInfo':{'itemNumberDetails':[{'number':3}]}}]}]},'display':'ALL','complianceSet':['1','2','3','4','5']}";
                if (string.IsNullOrEmpty(strResult))
                    return null;
                else

                    return await MPTBtoCommonTest(strResult, AgencyCode);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Amadeus Json to Common Json
        public string MPTBtoCommon(string amresult)
        {
            var FLAmRS = JsonConvert.DeserializeObject<FLAmRS>(amresult);

            BusinessEntities.FLcommonRS fLcommonRS = new BusinessEntities.FLcommonRS();

            BusinessEntities.Faremasterpricertravelboardsearchreply faremasterpricertravelboardsearchreply = new BusinessEntities.Faremasterpricertravelboardsearchreply();
            BusinessEntities.Replystatus replystatus = new BusinessEntities.Replystatus();
            BusinessEntities.Status status = new BusinessEntities.Status()
            {
                advisoryTypeInfo = FLAmRS.fareMasterPricerTravelBoardSearchReply.replyStatus.status[0].advisoryTypeInfo
            };
            replystatus.status = status;
            faremasterpricertravelboardsearchreply.replyStatus = replystatus;
            BusinessEntities.Conversionrate conversionrate = new BusinessEntities.Conversionrate();
            BusinessEntities.Conversionratedetail conversionratedetail = new BusinessEntities.Conversionratedetail()
            {
                currency = FLAmRS.fareMasterPricerTravelBoardSearchReply.conversionRate.conversionRateDetail[0].currency
            };
            conversionrate.SessionId = "";
            conversionrate.conversionRateDetail = conversionratedetail;
            faremasterpricertravelboardsearchreply.conversionRate = conversionrate;

            int totalflights = FLAmRS.fareMasterPricerTravelBoardSearchReply.recommendation.Length;
            BusinessEntities.Flightindex[] Flightindexes = new BusinessEntities.Flightindex[totalflights];
            //Need For each
            BusinessEntities.Flightindex flightindex = new BusinessEntities.Flightindex();
            BusinessEntities.Segmentref segmentref = new BusinessEntities.Segmentref();
            BusinessEntities.Typeref typeref = new BusinessEntities.Typeref();
            BusinessEntities.Reccount reccount = new BusinessEntities.Reccount();
            BusinessEntities.Fare fare = new BusinessEntities.Fare();
            BusinessEntities.Faredetails faredetails = new BusinessEntities.Faredetails();
            BusinessEntities.Bagdetails bagdetails = new BusinessEntities.Bagdetails();
            BusinessEntities.Description description = new BusinessEntities.Description();
            BusinessEntities.Groupofflight groupofflight = new BusinessEntities.Groupofflight();
            BusinessEntities.Flightproposal flightproposal = new BusinessEntities.Flightproposal();
            BusinessEntities.Flightdetail flightdetail = new BusinessEntities.Flightdetail();
            //Finfo finfo = new Finfo();
            BusinessEntities.Datetime datetime = new BusinessEntities.Datetime();
            BusinessEntities.Location location = new BusinessEntities.Location();
            BusinessEntities.Companyid companyid = new BusinessEntities.Companyid();
            BusinessEntities.Flightcharacteristics flightcharacteristics = new BusinessEntities.Flightcharacteristics();

            //flightindex loop start
            int resultscound = 1;
            for (int flights = 0; flights < totalflights; flights++)
            {
                flightindex = new BusinessEntities.Flightindex();
                int totalgroups = Convert.ToInt16(FLAmRS.fareMasterPricerTravelBoardSearchReply.recommendation[flights].segmentFlightRef.Length);
                BusinessEntities.Groupofflight[] Groupofflights = new BusinessEntities.Groupofflight[totalgroups];

                string flightfromkey = FLAmRS.fareMasterPricerTravelBoardSearchReply.flightIndex[flights].groupOfFlights[0].flightDetails[0].flightInformation.location[0].locationId;

                //string flighttokey = response.PricedItineraries[flights].OriginDestinationOptions[totalgroups - 1].FlightSegments[response.PricedItineraries[flights].OriginDestinationOptions[totalgroups - 1].FlightSegments.Length - 1].DepartureAirportLocationCode;
                //string flightdepdatekey = response.PricedItineraries[flights].OriginDestinationOptions[0].FlightSegments[0].DepartureDateTime.ToString("dd-MM-yy-hh-mm-ss");
                //string yearkey = flightdepdatekey.Split('-')[2];
                //string monthkey = flightdepdatekey.Split('-')[1];
                //string daykey = flightdepdatekey.Split('-')[0];
                //string hourkey = flightdepdatekey.Split('-')[3];
                //string minkey = flightdepdatekey.Split('-')[4];
                //string codekey = response.PricedItineraries[flights].OriginDestinationOptions[0].FlightSegments[0].OperatingAirline.Code;
                //string flightnumberkey = response.PricedItineraries[flights].OriginDestinationOptions[0].FlightSegments[0].OperatingAirline.FlightNumber;
                //string flightkey = flightfromkey + "-" + flighttokey + "-" + daykey + monthkey + yearkey + "-" + hourkey + minkey + "-" + codekey + "-" + flightnumberkey;
                //string FareSourceCodeMysti = response.PricedItineraries[flights].AirItineraryPricingInfo.FareSourceCode;
            }




            string CommonSearchRS = JsonConvert.SerializeObject(fLcommonRS, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Include });
            return CommonSearchRS;
        }
        public async Task<FLcommonRS> MPTBtoCommonTest(string amresult, string AgencyCode)
        {


            string fullstring = "";
            string flightstring = "";
            string eachflightstringcommon = "";
            string eachflightstring = "";
            string loopclosing = "";
            int amentidlength;
            string arraycloasing = "";
            string lastcloasing = "";
            string fullreq = "";
            string segflight = "";
            string eachlegstring = "";
            string groupclosing = "";
            string laegllopstring = "";
            string combined = "";
            string abc = "";
            int bagint;
            int bagminus;
            int bagref;
            string amenitiesid = "";
            string bagunit;
            string bagallow;
            string bagcode;
            int legint;
            int actlegs;
            int noofleg;
            int bagcnt;
            string refund;
            double totalfare;
            double taxfare;
            double basefare;
            string farebasiscode = "";
            int legRefNo;
            int actualLegRefNo;
            int numberofconnectionflight;
            int actualnumberofconnectionflight;

            int Lastconnectionflight;
            int actuallastconnectionflight;
            string elpsdtime = "";
            string depdate = "";
            string arrdate = "";
            string deptime = "";
            string arrtime = "";
            int datevar;
            string ToGo = "";
            string FromGo = "";
            string euqid = "";
            string mcarrier = "";
            string ocarrier = "";
            string flightno = "";
            string toterm = "";
            string fromterm = "";

            var list = JsonConvert.DeserializeObject<FLAmRS>(amresult);

            int norecom = list.fareMasterPricerTravelBoardSearchReply.recommendation.Length;

            string minamount = list.fareMasterPricerTravelBoardSearchReply.recommendation[0].recPriceInfo.monetaryDetail[0].amount.ToString();
            string maxamount = list.fareMasterPricerTravelBoardSearchReply.recommendation[norecom - 1].recPriceInfo.monetaryDetail[0].amount.ToString();
            string response = "{\"fareMasterPricerTravelBoardSearchReply\":{\"replyStatus\":{\"status\":{\"advisoryTypeInfo\":\"" + AgencyCode + "\"}},\"conversionRate\":{\"conversionRateDetail\":{\"currency\":\"AED\"},\"SessionId\":\"cdws2343454ds-fsdf34554\"},";

            flightstring = "\"flightIndex\":[";


            // list.fareMasterPricerTravelBoardSearchReply.recommendation[0].segmentFlightRef


            fullreq = "";
            segflight = "";


            for (int reccount = 0; reccount < norecom; reccount++)
            {


                int combi = list.fareMasterPricerTravelBoardSearchReply.recommendation[reccount].segmentFlightRef.Length;

                int actbagminus1 = 0;

                int type = 0;
                for (int combinationcountref = 0; combinationcountref < combi; combinationcountref++)
                {


                    combined = "";
                    loopclosing = "";
                    eachlegstring = "";
                    eachflightstring = "";






                    noofleg = list.fareMasterPricerTravelBoardSearchReply.recommendation[reccount].segmentFlightRef[combinationcountref].referencingDetail.Length;
                    bagcnt = (noofleg - 1);
                    try
                    {


                        bagint = list.fareMasterPricerTravelBoardSearchReply.recommendation[reccount].segmentFlightRef[combinationcountref].referencingDetail[1].refNumber;
                        bagminus = (bagint - 1);

                        bagref = list.fareMasterPricerTravelBoardSearchReply.serviceFeesGrp[0].serviceCoverageInfoGrp[bagminus].serviceCovInfoGrp[0].refInfo.referencingDetail[0].refNumber;
                        actbagminus1 = (bagref - 1);

                    }

                    catch (Exception)
                    {
                        actbagminus1 = ' ';
                    }



                    if (noofleg == 1)
                    {
                        legint = noofleg;
                        actlegs = legint;

                    }
                    else
                    {
                        legint = noofleg;
                        actlegs = legint - 1;

                    }
                    groupclosing = "";
                    var cabinclass = list.fareMasterPricerTravelBoardSearchReply.recommendation[reccount].paxFareProduct[0].fareDetails[0].groupOfFares[0].productInformation.cabinProduct.cabin;
                    refund = list.fareMasterPricerTravelBoardSearchReply.recommendation[reccount].paxFareProduct[0].fare[0].pricingMessage.description[0];
                    totalfare = Convert.ToDouble(list.fareMasterPricerTravelBoardSearchReply.recommendation[reccount].recPriceInfo.monetaryDetail[0].amount.ToString());
                    taxfare = Convert.ToDouble(list.fareMasterPricerTravelBoardSearchReply.recommendation[reccount].recPriceInfo.monetaryDetail[1].amount.ToString());
                    basefare = totalfare - taxfare;
                    farebasiscode = list.fareMasterPricerTravelBoardSearchReply.recommendation[reccount].paxFareProduct[0].fareDetails[0].groupOfFares[0].productInformation.fareProductDetail.fareBasis;


                    if (actbagminus1 == ' ')
                    {
                        bagallow = "NA";
                        bagcode = "NA";

                    }
                    else
                    {

                        bagallow = list.fareMasterPricerTravelBoardSearchReply.serviceFeesGrp[0].freeBagAllowanceGrp[actbagminus1].freeBagAllownceInfo.baggageDetails.freeAllowance.ToString();


                        bagcode = list.fareMasterPricerTravelBoardSearchReply.serviceFeesGrp[0].freeBagAllowanceGrp[actbagminus1].freeBagAllownceInfo.baggageDetails.quantityCode.ToString();

                        try
                        {

                            bagunit = list.fareMasterPricerTravelBoardSearchReply.serviceFeesGrp[0].freeBagAllowanceGrp[actbagminus1].freeBagAllownceInfo.baggageDetails.unitQualifier.ToString();
                        }
                        catch (Exception)
                        {
                            bagunit = "NA";
                        }



                        for (int eachleg = 0; eachleg < actlegs; eachleg++)
                        {

                            legRefNo = list.fareMasterPricerTravelBoardSearchReply.recommendation[reccount].segmentFlightRef[combinationcountref].referencingDetail[eachleg].refNumber;
                            actualLegRefNo = legRefNo - 1;
                            numberofconnectionflight = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails.Length;
                            actualnumberofconnectionflight = (numberofconnectionflight - 1);

                            Lastconnectionflight = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[actualnumberofconnectionflight].flightInformation.location.Length;
                            actuallastconnectionflight = (Lastconnectionflight - 1);


                            loopclosing = "";

                            laegllopstring = "";
                            eachflightstring = "";


                            elpsdtime = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].propFlightGrDetail.flightProposal[1].@ref;
                            mcarrier = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[0].flightInformation.companyId.marketingCarrier;
                            ocarrier = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[0].flightInformation.companyId.operatingCarrier;




                            laegllopstring = " {\"FlightProposal\":{\"elapse\":\"" + elpsdtime +
                                "\",\"unitQualifier\":\"EFT\",\"VAirline\":\"" + mcarrier +
                                "\",\"Ocarrier\":\"" + ocarrier + "\"},\"flightDetails\":[";

                            FromGo = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[0].flightInformation.location[0].locationId;
                            ToGo = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[actualnumberofconnectionflight].flightInformation.location[actuallastconnectionflight].locationId;
                            euqid = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[actualnumberofconnectionflight].flightInformation.productDetail.equipmentType;
                            toterm = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[actualnumberofconnectionflight].flightInformation.location[actuallastconnectionflight].terminal;

                            string amenitiesidsplit = " ";
                            for (int noofconnection = 0; noofconnection < numberofconnectionflight; noofconnection++)
                            {

                                depdate = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.productDateTime.dateOfDeparture;
                                arrdate = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.productDateTime.dateOfArrival;
                                deptime = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.productDateTime.timeOfDeparture;
                                arrtime = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.productDateTime.timeOfArrival;
                                datevar = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.productDateTime.dateVariation;




                                var FromGoSeg = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.location[0].locationId;

                                flightno = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.flightOrtrainNumber;

                                fromterm = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.location[0].terminal;

                                var ToGoseg = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.location[actuallastconnectionflight].locationId;
                                var totermseg = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.location[actuallastconnectionflight].terminal;

                                var mcarrierseg = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.companyId.marketingCarrier;
                                var ocarrierseg = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.companyId.operatingCarrier;
                                var euqidseg = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightInformation.productDetail.equipmentType;

                                //try
                                //{
                                //    amentidlength = 0;

                                //    amentidlength = list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightCharacteristics.inFlightSrv.Length;

                                //    amenitiesidsplit = " ";
                                //    for (var i = 0; i < amentidlength; i++)

                                //    {

                                //        amenitiesid += list.fareMasterPricerTravelBoardSearchReply.flightIndex[eachleg].groupOfFlights[actualLegRefNo].flightDetails[noofconnection].flightCharacteristics.inFlightSrv[i];
                                //        amenitiesidsplit += amenitiesid + " ";
                                //        amenitiesid = " ";
                                //    }

                                //}
                                //catch (Exception)
                                //{
                                //    amenitiesid = " ";
                                //}


                                eachflightstring += "{\"Finfo\":{\"DateTime\":{\"Depdate\":\"" + depdate + "\",\"Deptime\":\"" + deptime + "\",\"Arrdate\":\""
                                    + arrdate + "\",\"Arrtime\":\"" + arrtime + "\",\"Variation\":\"" + datevar + "\"},\"location\":{\"locationFrom\":\""
                                    + FromGoSeg + "\",\"Fromterminal\":\"" + fromterm + "\",\"LocationTo\":\"" + ToGoseg + "\",\"Toterminal\":\"" + totermseg +
                                    "\"},\"companyId\":{\"mCarrier\":\"" + mcarrierseg + "\",\"oCarrier\":\"" + ocarrierseg + "\"},\"flightNo\":\"" + flightno +
                                    "\",\"Bookingclass\":\"" + cabinclass + "\",\"eqpType\":\"" + euqidseg + "\",\"eTicketing\":\"Y\",\"attributeType\":\"EFT\",\"Elapsedtime\":\""
                                    + elpsdtime + "\"},\"flightCharacteristics\":{\"inFlightSrv\":\"NA\"}},";


                            }

                            eachflightstring = eachflightstring.Substring(0, eachflightstring.Length - 1);

                            loopclosing += "]},";

                            combined += laegllopstring + eachflightstring + loopclosing;

                        }



                        combined = combined.Substring(0, combined.Length - 1);

                        groupclosing = " ]},";
                        abc = FromGo + "-" + ToGo + "-" + depdate + "-" + deptime + "-" + ocarrier + "-" + flightno;
                        eachflightstringcommon = "{\"SegmentRef\":{\"segRef\":\"1\",\"PSessionId\":\"NA\",\"supplier\":\"AMA001\",\"key\":\""
                            + abc + "\"},\"TypeRef\":{\"type\":\"" + type + "\"},\"reccount\":{\"rec_id\":\""
                            + reccount + "\",\"combi_id\":\"" + combi + "\"},\"fare\":{\"amount\":\""
                            + totalfare + "\",\"taxfare\":\"" + taxfare + "\",\"basefare\":\""
                            + basefare + "\",\"MarkupFare\":\"0\",\"currency\":\"AED\"},\"fareDetails\":{\"fareBasis\":\""
                            + farebasiscode + "\"},\"Bagdetails\":{\"freeAllowance\":\"" + bagallow +
                            "\",\"Qcode\":\"" + bagcode + "\",\"unit\":\"" + bagunit +
                            "\"},\"Description\":{\"Pricingmsg\":\"" + refund + "\"},";
                        eachlegstring += " \"groupOfFlights\":[";

                        segflight += eachflightstringcommon + eachlegstring + combined + groupclosing;

                    }


                    type = 1;
                }




            }

            segflight = segflight.Substring(0, segflight.Length - 1);
            // fullreq = fullreq.Substring(0, fullreq.Length - 1);

            arraycloasing = "]";

            lastcloasing = "}}";



            #region --test code--

            string abc1 = "";
            int bagint1;
            int bagminus1;
            int bagref1;
            string amenitiesid1 = "";
            string bagunit1;
            string bagallow1;
            string bagcode1;
            int legint1;
            int amentidlength1;
            int actlegs1;
            int noofleg1;
            int bagcnt1;
            string refund1;
            double totalfare1;
            double taxfare1;
            double basefare1;
            string farebasiscode1 = "";
            int legRefNo1;
            int actualLegRefNo1;
            int numberofconnectionflight1;
            int actualnumberofconnectionflight1;
            var type1 = 0;
            int Lastconnectionflight1;
            int actuallastconnectionflight1;
            string elpsdtime1 = "";
            string depdate1 = "";
            string arrdate1 = "";
            string deptime1 = "";
            string arrtime1 = "";
            int? datevar1;
            string ToGo1 = "";
            string FromGo1 = "";
            string euqid1 = "";
            string mcarrier1 = "";
            string ocarrier1 = "";
            string flightno1 = "";
            string toterm1 = "";
            string fromterm1 = "";
            string elapsedtime = "";

            var flightSearchRs = JsonConvert.DeserializeObject<FlightSearchRs>(amresult);
            ResponseEntity responseEntity = new ResponseEntity();
            responseEntity.FareMasterPricerTravelBoardSearchReply.ReplyStatus.Status.Add(new Models.Status
            {
                AdvisoryTypeInfo = AgencyCode
            });
            responseEntity.FareMasterPricerTravelBoardSearchReply.ConversionRate.ConversionRateDetail.Add(new ConversionRateDetail()
            {
                Currency = CurrencyEnum.AED.ToString()
            });
            responseEntity.FareMasterPricerTravelBoardSearchReply.ConversionRate.SessionId = "cdws2343454ds-fsdf34554";

            int i1 = 0;
            int j1 = 0;


            List<string> refunds = new List<string>();
            var cabin = "";
            var gof = new List<GroupOfFlight>();
            flightSearchRs.FareMasterPricerTravelBoardSearchReply.Recommendation.ForEach(r =>
            {
                i1++;
                int actbagminus2 = 0;
                r.SegmentFlightRef.ForEach(s =>
                {
                    j1++;


                    noofleg1 = s.ReferencingDetail.Count();
                    bagcnt1 = (noofleg1 - 1);
                    try
                    {
                        bagint1 = s.ReferencingDetail[1].RefNumber;
                        bagminus1 = (bagint1 - 1);

                        //   bagref = list.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].ServiceCoverageInfoGrp[bagminus].ServiceCovInfoGrp[0].RefInfo.ReferencingDetail[0].RefNumber;
                        bagref1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].ServiceCoverageInfoGrp[bagminus1].ServiceCovInfoGrp[0].RefInfo.ReferencingDetail[0].RefNumber;
                        actbagminus2 = (bagref1 - 1);

                    }

                    catch (Exception)
                    {
                        actbagminus2 = ' ';
                    }

                    switch (noofleg1)
                    {
                        case 1:
                            legint1 = noofleg1;
                            actlegs1 = legint1;
                            break;
                        default:
                            legint1 = noofleg1;
                            actlegs1 = legint1 - 1;
                            break;
                    }

                    //  groupclosing = "";
                    var cabinclass1 = r.PaxFareProduct.First().FareDetails.First().GroupOfFares.First().ProductInformation.CabinProduct.Cabin;
                    refund1 = r.PaxFareProduct[0].Fare[0].PricingMessage.Description[0];
                    totalfare1 = Convert.ToDouble(r.RecPriceInfo.MonetaryDetail[0].Amount.ToString());
                    taxfare1 = Convert.ToDouble(r.RecPriceInfo.MonetaryDetail[1].Amount.ToString());
                    basefare1 = totalfare1 - taxfare1;
                    farebasiscode1 = r.PaxFareProduct[0].FareDetails[0].GroupOfFares[0].ProductInformation.FareProductDetail.FareBasis;
                    if (actbagminus2 == ' ')
                    {
                        bagallow1 = CheckNA.NA.ToString();
                        bagcode1 = CheckNA.NA.ToString();
                    }
                    else
                    {
                        bagallow1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].FreeBagAllowanceGrp[actbagminus2].FreeBagAllownceInfo.BaggageDetails.FreeAllowance.ToString();
                        bagcode1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].FreeBagAllowanceGrp[actbagminus2].FreeBagAllownceInfo.BaggageDetails.QuantityCode.ToString();

                        try
                        {
                            bagunit1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].FreeBagAllowanceGrp[actbagminus2].FreeBagAllownceInfo.BaggageDetails.UnitQualifier.ToString();
                        }
                        catch (Exception)
                        {
                            bagunit1 = CheckNA.NA.ToString();
                        }
                        for (int eachleg = 0; eachleg < actlegs1; eachleg++)
                        {
                            legRefNo1 = s.ReferencingDetail[eachleg].RefNumber;
                            actualLegRefNo1 = legRefNo1 - 1;
                            numberofconnectionflight1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails.Count();
                            actualnumberofconnectionflight1 = (numberofconnectionflight1 - 1);

                            Lastconnectionflight1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[actualnumberofconnectionflight1].FlightInformation.Location.Count();
                            actuallastconnectionflight1 = (Lastconnectionflight1 - 1);



                            elpsdtime1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].PropFlightGrDetail.FlightProposal[1].Ref;
                            mcarrier1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails.First().FlightInformation.CompanyId.MarketingCarrier;
                            ocarrier1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails.First().FlightInformation.CompanyId.OperatingCarrier;


                            FlightProposal flightProposal = new FlightProposal();
                            flightProposal.Elapse = elpsdtime1;
                            flightProposal.Ocarrier = ocarrier1;
                            flightProposal.VAirline = mcarrier1;
                            flightProposal.UnitQualifier = "EFT";


                            FromGo1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails.First().FlightInformation.Location.First().LocationId;
                            ToGo1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[actualnumberofconnectionflight1].FlightInformation.Location[actuallastconnectionflight1].LocationId;
                            euqid1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[actualnumberofconnectionflight1].FlightInformation.ProductDetail.EquipmentType;
                            toterm1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[actualnumberofconnectionflight1].FlightInformation.Location[actuallastconnectionflight1].Terminal;

                            string amenitiesidsplit = " ";

                            for (int noofconnection = 0; noofconnection < numberofconnectionflight1; noofconnection++)
                            {

                                depdate1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.ProductDateTime.DateOfDeparture;
                                arrdate1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.ProductDateTime.DateOfArrival;
                                deptime1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.ProductDateTime.TimeOfDeparture;
                                arrtime1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.ProductDateTime.TimeOfArrival;
                                datevar1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.ProductDateTime.DateVariation;
                                elapsedtime = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.AttributeDetails != null ?
                                                  flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.AttributeDetails[0].AttributeDescription
                                                  : null;
                                var FromGoSeg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.Location.First().LocationId;

                                flightno1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.FlightOrtrainNumber;

                                fromterm1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.Location.First().Terminal;

                                var ToGoseg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.Location[actuallastconnectionflight1].LocationId;
                                var totermseg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.Location[actuallastconnectionflight1].Terminal;

                                var mcarrierseg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.CompanyId.MarketingCarrier;
                                var ocarrierseg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.CompanyId.OperatingCarrier;
                                var euqidseg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightInformation.ProductDetail.EquipmentType;




                                // amentidlength1 = 0;
                                //amentidlength1 = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightCharacteristics.InFlightSrv.Count();

                                //    try
                                //    {

                                //        amenitiesidsplit = " ";

                                //        for (var i = 0; i < amentidlength1; i++)

                                //        {
                                //            amenitiesid1 += flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo1].FlightDetails[noofconnection].FlightCharacteristics.InFlightSrv[i];
                                //            amenitiesidsplit += amenitiesid1 + " ";
                                //            amenitiesid1 = " ";
                                //        }

                                //    }
                                //    catch (Exception)
                                //    {
                                //        amenitiesid1 = " ";
                                //    }



                                Models.Finfo finfo = new Models.Finfo();
                                try
                                {
                                    finfo.AttributeType ="";
                                    finfo.Bookingclass = cabinclass1;
                                    finfo.CompanyId.MCarrier = mcarrier1;
                                    finfo.CompanyId.OCarrier = ocarrier1;
                                    finfo.DateTime.Arrdate = arrdate1;
                                    finfo.DateTime.Arrtime = arrtime1;
                                    finfo.DateTime.Depdate = depdate1;
                                    finfo.DateTime.Deptime = deptime1;
                                    finfo.Elapsedtime = elpsdtime1;
                                    finfo.EqpType = euqid1;
                                    finfo.ETicketing = "Y";
                                    finfo.FlightNo = flightno1;
                                    finfo.Location.LocationFrom = FromGoSeg;
                                    finfo.Location.LocationTo = ToGoseg;
                                    finfo.Location.Fromterminal = fromterm1;
                                    finfo.Location.Toterminal = toterm1;
                                }
                                catch (Exception ex)
                                {

                                    throw;
                                }


                                //var flightCharacteristics = new List<FlightCharacteristics>();
                                //flightCharacteristics.Add(new FlightCharacteristics
                                //{
                                //    InFlightSrv = amenitiesid
                                //});

                                //var inFlightSrv = new List<InFlightSrv>();
                                //inFlightSrv.Add(new InFlightSrv
                                //{

                                //});

                                var flightDetails = new List<FlightDetail>();
                                flightDetails.Add(new FlightDetail()
                                {
                                    Finfo = finfo
                                });
                                // var gof = new List<GroupOfFlight>();
                                gof.Add(new GroupOfFlight()
                                {

                                    FlightProposal = flightProposal,
                                    FlightDetails = flightDetails
                                });
                                //responseEntity.FareMasterPricerTravelBoardSearchReply.FlightIndex.Add(new FlightIndex()
                                //{
                                //    SegmentRef= segmentRef,
                                //    GroupOfFlights = gof

                                //});


                            }

                            abc1 = FromGo1 + "-" + ToGo1 + "-" + depdate1 + "-" + deptime1 + "-" + ocarrier1 + "-" + flightno1;
                            SegmentRef segmentRef = new SegmentRef();
                            segmentRef.Key = abc1;
                            segmentRef.PSessionId = "";
                            segmentRef.SegRef = 1;
                            segmentRef.Supplier = AgencyCode;




                            Models.Reccount Reccount = new Models.Reccount();
                            Reccount.RecId = i1;
                            Reccount.CombiId = j1;


                            TypeRef typeref = new TypeRef();
                            typeref.Type = type1;

                            Models.Fare fare = new Models.Fare();
                            fare.Amount = totalfare1;
                            fare.Taxfare = taxfare1;
                            fare.Basefare = basefare1;
                            fare.MarkupFare = 0;
                            fare.Currency = CurrencyEnum.AED.ToString();




                            FareDetails fareDetails = new FareDetails();
                            fareDetails.FareBasis = farebasiscode1;


                            Models.Bagdetails bagdetails = new Models.Bagdetails();
                            bagdetails.FreeAllowance = bagallow1;
                            bagdetails.Qcode = bagcode1;
                            bagdetails.Unit = bagunit1;




                            Models.Description description = new Models.Description();
                            description.Pricingmsg = refund1;






                            responseEntity.FareMasterPricerTravelBoardSearchReply.FlightIndex.Add(new FlightIndex()
                            {
                                SegmentRef = segmentRef,
                                Reccount = Reccount,
                                TypeRef = typeref,
                                Fare = fare,
                                FareDetails = fareDetails,
                                BagDetails = bagdetails,
                                Description = description,
                                GroupOfFlights = gof

                            });

                        }
                        type1 = 1;
                    }

                });
            });

            #endregion

            fullstring = response + flightstring + segflight + arraycloasing + lastcloasing;

            FLcommonRS m = JsonConvert.DeserializeObject<FLcommonRS>(fullstring);

            string jsonData = JsonConvert.SerializeObject(responseEntity);

            return m;



        }
        //public FLcommonRS MPTBtoCommonTest_1(string amresult, string AgencyCode)
        //{
        //    string fullstring = "";
        //    string flightstring = "";
        //    string eachflightstringcommon = "";
        //    string eachflightstring = "";
        //    string loopclosing = "";
        //    int amentidlength;
        //    string arraycloasing = "";
        //    string lastcloasing = "";
        //    string fullreq = "";
        //    string segflight = "";
        //    string eachlegstring = "";
        //    string groupclosing = "";
        //    string laegllopstring = "";
        //    string combined = "";
        //    string abc = "";
        //    int bagint;
        //    int bagminus;
        //    int bagref;
        //    string amenitiesid = "";
        //    string bagunit;
        //    string bagallow;
        //    string bagcode;
        //    int legint;
        //    int actlegs;
        //    int noofleg;
        //    int bagcnt;
        //    string refund;
        //    double totalfare;
        //    double taxfare;
        //    double basefare;
        //    string farebasiscode = "";
        //    int legRefNo;
        //    int actualLegRefNo;
        //    int numberofconnectionflight;
        //    int actualnumberofconnectionflight;
        //    var type = 0;
        //    int Lastconnectionflight;
        //    int actuallastconnectionflight;
        //    string elpsdtime = "";
        //    string depdate = "";
        //    string arrdate = "";
        //    string deptime = "";
        //    string arrtime = "";
        //    int? datevar;
        //    string ToGo = "";
        //    string FromGo = "";
        //    string euqid = "";
        //    string mcarrier = "";
        //    string ocarrier = "";
        //    string flightno = "";
        //    string toterm = "";
        //    string fromterm = "";

        //    //var list = JsonConvert.DeserializeObject<FLAmRS>(amresult);
        //    var list = JsonConvert.DeserializeObject<FlightSearchRs>(amresult);
        //    var flightSearchRs = JsonConvert.DeserializeObject<FlightSearchRs>(amresult);

        //    //int norecom = list.FareMasterPricerTravelBoardSearchReply.Recommendation.Length;
        //    int norecom = list.FareMasterPricerTravelBoardSearchReply.Recommendation.Count();

        //    //string minamount = list.FareMasterPricerTravelBoardSearchReply.Recommendation[0].RecPriceInfo.MonetaryDetail[0].Amount.ToString();
        //    //string maxamount = list.FareMasterPricerTravelBoardSearchReply.Recommendation[norecom - 1].RecPriceInfo.MonetaryDetail[0].Amount.ToString();
        //    string minamount = list.FareMasterPricerTravelBoardSearchReply.Recommendation[0].RecPriceInfo.MonetaryDetail[0].Amount.ToString();
        //    string maxamount = list.FareMasterPricerTravelBoardSearchReply.Recommendation[norecom - 1].RecPriceInfo.MonetaryDetail[0].Amount.ToString();
        //    //  int recount=list.FareMasterPricerTravelBoardSearchReply.Recommendation[].SegmentFlightRef.


        //    string response = "{\"FareMasterPricerTravelBoardSearchReply\":{\"replyStatus\":{\"status\":{\"advisoryTypeInfo\":\"" + AgencyCode + "\"}},\"conversionRate\":{\"conversionRateDetail\":{\"currency\":\"AED\"},\"SessionId\":\"cdws2343454ds-fsdf34554\"},";



        //    flightstring = "\"FlightIndex\":[";

        //    fullreq = "";
        //    segflight = "";

        //    #region --test code--
        //    ResponseEntity responseEntity = new ResponseEntity();
        //    responseEntity.FareMasterPricerTravelBoardSearchReply.ReplyStatus.Status.Add(new Models.Status
        //    {
        //        AdvisoryTypeInfo = AgencyCode
        //    });
        //    responseEntity.FareMasterPricerTravelBoardSearchReply.ConversionRate.ConversionRateDetail.Add(new ConversionRateDetail()
        //    {
        //        Currency = "AED"
        //    });
        //    responseEntity.FareMasterPricerTravelBoardSearchReply.ConversionRate.SessionId = "cdws2343454ds-fsdf34554";

        //    int i1 = 0;
        //    int j1 = 0;


        //    List<string> refunds = new List<string>();
        //    var cabin = "";
        //    var gof = new List<GroupOfFlight>();
        //    flightSearchRs.FareMasterPricerTravelBoardSearchReply.Recommendation.ForEach(r =>
        //    {
        //        i1++;
        //        int actbagminus2 = 0;
        //        r.SegmentFlightRef.ForEach(s =>
        //        {
        //            j1++;
        //            combined = "";
        //            loopclosing = "";
        //            eachlegstring = "";
        //            eachflightstring = "";

        //            noofleg = s.ReferencingDetail.Count();
        //            bagcnt = (noofleg - 1);
        //            try
        //            {
        //                bagint = s.ReferencingDetail[1].RefNumber;
        //                bagminus = (bagint - 1);

        //                //   bagref = list.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].ServiceCoverageInfoGrp[bagminus].ServiceCovInfoGrp[0].RefInfo.ReferencingDetail[0].RefNumber;
        //                bagref = flightSearchRs.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].ServiceCoverageInfoGrp[bagminus].ServiceCovInfoGrp[0].RefInfo.ReferencingDetail[0].RefNumber;
        //                actbagminus2 = (bagref - 1);

        //            }

        //            catch (Exception)
        //            {
        //                actbagminus2 = ' ';
        //            }

        //            switch (noofleg)
        //            {
        //                case 1:
        //                    legint = noofleg;
        //                    actlegs = legint;
        //                    break;
        //                default:
        //                    legint = noofleg;
        //                    actlegs = legint - 1;
        //                    break;
        //            }

        //            groupclosing = "";
        //            var cabinclass = r.PaxFareProduct.First().FareDetails.First().GroupOfFares.First().ProductInformation.CabinProduct.Cabin;
        //            refund = r.PaxFareProduct[0].Fare[0].PricingMessage.Description[0];
        //            totalfare = Convert.ToDouble(r.RecPriceInfo.MonetaryDetail[0].Amount.ToString());
        //            taxfare = Convert.ToDouble(r.RecPriceInfo.MonetaryDetail[1].Amount.ToString());
        //            basefare = totalfare - taxfare;
        //            farebasiscode = r.PaxFareProduct[0].FareDetails[0].GroupOfFares[0].ProductInformation.FareProductDetail.FareBasis;
        //            if (actbagminus2 == ' ')
        //            {
        //                bagallow = "NA";
        //                bagcode = "NA";
        //            }
        //            else
        //            {
        //                bagallow = list.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].FreeBagAllowanceGrp[actbagminus2].FreeBagAllownceInfo.BaggageDetails.FreeAllowance.ToString();
        //                bagcode = list.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].FreeBagAllowanceGrp[actbagminus2].FreeBagAllownceInfo.BaggageDetails.QuantityCode.ToString();

        //                try
        //                {
        //                    bagunit = list.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].FreeBagAllowanceGrp[actbagminus2].FreeBagAllownceInfo.BaggageDetails.UnitQualifier.ToString();
        //                }
        //                catch (Exception)
        //                {
        //                    bagunit = "NA";
        //                }
        //                for (int eachleg = 0; eachleg < actlegs; eachleg++)
        //                {
        //                    legRefNo = s.ReferencingDetail[eachleg].RefNumber;
        //                    actualLegRefNo = legRefNo - 1;
        //                    numberofconnectionflight = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails.Count();
        //                    actualnumberofconnectionflight = (numberofconnectionflight - 1);

        //                    Lastconnectionflight = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[actualnumberofconnectionflight].FlightInformation.Location.Count();
        //                    actuallastconnectionflight = (Lastconnectionflight - 1);

        //                    loopclosing = "";

        //                    laegllopstring = "";
        //                    eachflightstring = "";

        //                    elpsdtime = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].PropFlightGrDetail.FlightProposal[1].Ref;
        //                    mcarrier = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails.First().FlightInformation.CompanyId.MarketingCarrier;
        //                    ocarrier = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails.First().FlightInformation.CompanyId.OperatingCarrier;


        //                    FlightProposal flightProposal = new FlightProposal();
        //                    flightProposal.Elapse = elpsdtime;
        //                    flightProposal.Ocarrier = ocarrier;
        //                    flightProposal.VAirline = mcarrier;
        //                    flightProposal.UnitQualifier = "EFT";


        //                    FromGo = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails.First().FlightInformation.Location.First().LocationId;
        //                    ToGo = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[actualnumberofconnectionflight].FlightInformation.Location[actuallastconnectionflight].LocationId;
        //                    euqid = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[actualnumberofconnectionflight].FlightInformation.ProductDetail.EquipmentType;
        //                    toterm = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[actualnumberofconnectionflight].FlightInformation.Location[actuallastconnectionflight].Terminal;

        //                    string amenitiesidsplit = " ";

        //                    for (int noofconnection = 0; noofconnection < numberofconnectionflight; noofconnection++)
        //                    {

        //                        depdate = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDateTime.DateOfDeparture;
        //                        arrdate = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDateTime.DateOfArrival;
        //                        deptime = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDateTime.TimeOfDeparture;
        //                        arrtime = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDateTime.TimeOfArrival;
        //                        datevar = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDateTime.DateVariation;

        //                        var FromGoSeg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.Location.First().LocationId;

        //                        flightno = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.FlightOrtrainNumber;

        //                        fromterm = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.Location.First().Terminal;

        //                        var ToGoseg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.Location[actuallastconnectionflight].LocationId;
        //                        var totermseg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.Location[actuallastconnectionflight].Terminal;

        //                        var mcarrierseg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.CompanyId.MarketingCarrier;
        //                        var ocarrierseg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.CompanyId.OperatingCarrier;
        //                        var euqidseg = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDetail.EquipmentType;

        //                        try
        //                        {
        //                            amentidlength = 0;

        //                            amentidlength = flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightCharacteristics.InFlightSrv.Count();

        //                            amenitiesidsplit = " ";
        //                            for (var i = 0; i < amentidlength; i++)

        //                            {
        //                                amenitiesid += flightSearchRs.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightCharacteristics.InFlightSrv[i];
        //                                amenitiesidsplit += amenitiesid + " ";
        //                                amenitiesid = " ";
        //                            }

        //                        }
        //                        catch (Exception)
        //                        {
        //                            amenitiesid = " ";
        //                        }

        //                        Models.Finfo finfo = new Models.Finfo();
        //                        finfo.AttributeType = "NA";
        //                        finfo.Bookingclass = cabin;
        //                        finfo.CompanyId.MCarrier = mcarrier;
        //                        finfo.CompanyId.OCarrier = ocarrier;
        //                        finfo.DateTime.Arrdate = arrdate;
        //                        finfo.DateTime.Arrtime = arrtime;
        //                        finfo.DateTime.Depdate = depdate;
        //                        finfo.DateTime.Deptime = deptime;
        //                        finfo.Elapsedtime = elpsdtime;
        //                        finfo.EqpType = euqid;
        //                        finfo.ETicketing = "Y";
        //                        finfo.FlightNo = flightno;
        //                        finfo.Location.LocationFrom = FromGoSeg;
        //                        finfo.Location.LocationTo = ToGoseg;
        //                        finfo.Location.Fromterminal = fromterm;
        //                        finfo.Location.Toterminal = toterm;

        //                        //var flightCharacteristics = new List<FlightCharacteristics>();
        //                        //flightCharacteristics.Add(new FlightCharacteristics
        //                        //{
        //                        //    InFlightSrv = amenitiesid
        //                        //});

        //                        //var inFlightSrv = new List<InFlightSrv>();
        //                        //inFlightSrv.Add(new InFlightSrv
        //                        //{


        //                        //});



        //                        var flightDetails = new List<FlightDetail>();
        //                        flightDetails.Add(new FlightDetail()
        //                        {
        //                            Finfo = finfo
        //                        });
        //                        // var gof = new List<GroupOfFlight>();
        //                        gof.Add(new GroupOfFlight()
        //                        {

        //                            FlightProposal = flightProposal,
        //                            FlightDetails = flightDetails
        //                        });
        //                        //responseEntity.FareMasterPricerTravelBoardSearchReply.FlightIndex.Add(new FlightIndex()
        //                        //{
        //                        //    SegmentRef= segmentRef,
        //                        //    GroupOfFlights = gof

        //                        //});


        //                    }




        //                    abc = FromGo + "-" + ToGo + "-" + depdate + "-" + deptime + "-" + ocarrier + "-" + flightno;
        //                    SegmentRef segmentRef = new SegmentRef();
        //                    segmentRef.Key = abc;
        //                    segmentRef.PSessionId = "";
        //                    segmentRef.SegRef = 1;
        //                    segmentRef.Supplier = AgencyCode;




        //                    Models.Reccount Reccount = new Models.Reccount();
        //                    Reccount.RecId = i1;
        //                    Reccount.CombiId = j1;


        //                    TypeRef typeref = new TypeRef();
        //                    typeref.Type = type;

        //                    Models.Fare fare = new Models.Fare();
        //                    fare.Amount = totalfare;
        //                    fare.Taxfare = taxfare;
        //                    fare.Basefare = basefare;
        //                    fare.MarkupFare = 0;
        //                    fare.Currency = "AED";




        //                    FareDetails fareDetails = new FareDetails();
        //                    fareDetails.FareBasis = farebasiscode;


        //                    Models.Bagdetails bagdetails = new Models.Bagdetails();
        //                    bagdetails.FreeAllowance = bagallow;
        //                    bagdetails.Qcode = bagcode;
        //                    bagdetails.Unit = bagunit;




        //                    Models.Description description = new Models.Description();
        //                    description.Pricingmsg = refund;






        //                    responseEntity.FareMasterPricerTravelBoardSearchReply.FlightIndex.Add(new FlightIndex()
        //                    {
        //                        SegmentRef = segmentRef,
        //                        Reccount = Reccount,
        //                        TypeRef = typeref,
        //                        Fare = fare,
        //                        FareDetails = fareDetails,
        //                        BagDetails = bagdetails,
        //                        Description = description,
        //                        GroupOfFlights = gof

        //                    });

        //                }
        //                type = 1;
        //            }

        //        });
        //    });

        //    #endregion

        //    //for (int reccount = 0; reccount < norecom; reccount++)
        //    //{
        //    //    int combi = list.FareMasterPricerTravelBoardSearchReply.Recommendation[reccount].SegmentFlightRef.Count();
        //    //    int actbagminus1 = 0;

        //    //    int type1 = 0;
        //    //    for (int combinationcountref = 0; combinationcountref < combi; combinationcountref++)
        //    //    {
        //    //        combined = "";
        //    //        loopclosing = "";
        //    //        eachlegstring = "";
        //    //        eachflightstring = "";

        //    //        noofleg = list.FareMasterPricerTravelBoardSearchReply.Recommendation[reccount].SegmentFlightRef[combinationcountref].ReferencingDetail.Count();
        //    //        bagcnt = (noofleg - 1);
        //    //        try
        //    //        {
        //    //            bagint = list.FareMasterPricerTravelBoardSearchReply.Recommendation[reccount].SegmentFlightRef[combinationcountref].ReferencingDetail[1].RefNumber;
        //    //            bagminus = (bagint - 1);

        //    //            bagref = list.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].ServiceCoverageInfoGrp[bagminus].ServiceCovInfoGrp[0].RefInfo.ReferencingDetail[0].RefNumber;
        //    //            actbagminus1 = (bagref - 1);

        //    //        }

        //    //        catch (Exception)
        //    //        {
        //    //            actbagminus1 = ' ';
        //    //        }

        //    //        if (noofleg == 1)
        //    //        {
        //    //            legint = noofleg;
        //    //            actlegs = legint;
        //    //        }
        //    //        else
        //    //        {
        //    //            legint = noofleg;
        //    //            actlegs = legint - 1;
        //    //        }
        //    //        groupclosing = "";
        //    //        var cabinclass = list.FareMasterPricerTravelBoardSearchReply.Recommendation[reccount].PaxFareProduct[0].FareDetails[0].GroupOfFares[0].ProductInformation.CabinProduct.Cabin;
        //    //        refund = list.FareMasterPricerTravelBoardSearchReply.Recommendation[reccount].PaxFareProduct[0].Fare[0].PricingMessage.Description[0];
        //    //        totalfare = Convert.ToDouble(list.FareMasterPricerTravelBoardSearchReply.Recommendation[reccount].RecPriceInfo.MonetaryDetail[0].Amount.ToString());
        //    //        taxfare = Convert.ToDouble(list.FareMasterPricerTravelBoardSearchReply.Recommendation[reccount].RecPriceInfo.MonetaryDetail[1].Amount.ToString());
        //    //        basefare = totalfare - taxfare;
        //    //        farebasiscode = list.FareMasterPricerTravelBoardSearchReply.Recommendation[reccount].PaxFareProduct[0].FareDetails[0].GroupOfFares[0].ProductInformation.FareProductDetail.FareBasis;

        //    //        if (actbagminus1 == ' ')
        //    //        {
        //    //            bagallow = "NA";
        //    //            bagcode = "NA";
        //    //        }
        //    //        else
        //    //        {
        //    //            bagallow = list.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].FreeBagAllowanceGrp[actbagminus1].FreeBagAllownceInfo.BaggageDetails.FreeAllowance.ToString();
        //    //            bagcode = list.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].FreeBagAllowanceGrp[actbagminus1].FreeBagAllownceInfo.BaggageDetails.QuantityCode.ToString();

        //    //            try
        //    //            {
        //    //                bagunit = list.FareMasterPricerTravelBoardSearchReply.ServiceFeesGrp[0].FreeBagAllowanceGrp[actbagminus1].FreeBagAllownceInfo.BaggageDetails.UnitQualifier.ToString();
        //    //            }
        //    //            catch (Exception)
        //    //            {
        //    //                bagunit = "NA";
        //    //            }

        //    //            for (int eachleg = 0; eachleg < actlegs; eachleg++)
        //    //            {
        //    //                legRefNo = list.FareMasterPricerTravelBoardSearchReply.Recommendation[reccount].SegmentFlightRef[combinationcountref].ReferencingDetail[eachleg].RefNumber;
        //    //                actualLegRefNo = legRefNo - 1;
        //    //                numberofconnectionflight = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails.Count();
        //    //                actualnumberofconnectionflight = (numberofconnectionflight - 1);

        //    //                Lastconnectionflight = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[actualnumberofconnectionflight].FlightInformation.Location.Count();
        //    //                actuallastconnectionflight = (Lastconnectionflight - 1);

        //    //                loopclosing = "";

        //    //                laegllopstring = "";
        //    //                eachflightstring = "";

        //    //                elpsdtime = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].PropFlightGrDetail.FlightProposal[1].Ref;
        //    //                mcarrier = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[0].FlightInformation.CompanyId.MarketingCarrier;
        //    //                ocarrier = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[0].FlightInformation.CompanyId.OperatingCarrier;

        //    //                laegllopstring = " {\"FlightProposal\":{\"elapse\":\"" + elpsdtime +
        //    //                    "\",\"unitQualifier\":\"EFT\",\"VAirline\":\"" + mcarrier +
        //    //                    "\",\"Ocarrier\":\"" + ocarrier + "\"},\"FlightDetails\":[";

        //    //                FromGo = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[0].FlightInformation.Location[0].LocationId;
        //    //                ToGo = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[actualnumberofconnectionflight].FlightInformation.Location[actuallastconnectionflight].LocationId;
        //    //                euqid = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[actualnumberofconnectionflight].FlightInformation.ProductDetail.EquipmentType;
        //    //                toterm = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[actualnumberofconnectionflight].FlightInformation.Location[actuallastconnectionflight].Terminal;

        //    //                string amenitiesidsplit = " ";
        //    //                for (int noofconnection = 0; noofconnection < numberofconnectionflight; noofconnection++)
        //    //                {

        //    //                    depdate = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDateTime.DateOfDeparture;
        //    //                    arrdate = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDateTime.DateOfArrival;
        //    //                    deptime = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDateTime.TimeOfDeparture;
        //    //                    arrtime = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDateTime.TimeOfArrival;
        //    //                    datevar = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDateTime.DateVariation;

        //    //                    var FromGoSeg = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.Location[0].LocationId;

        //    //                    flightno = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.FlightOrtrainNumber;

        //    //                    fromterm = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.Location[0].Terminal;

        //    //                    var ToGoseg = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.Location[actuallastconnectionflight].LocationId;
        //    //                    var totermseg = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.Location[actuallastconnectionflight].Terminal;

        //    //                    var mcarrierseg = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.CompanyId.MarketingCarrier;
        //    //                    var ocarrierseg = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.CompanyId.OperatingCarrier;
        //    //                    var euqidseg = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightInformation.ProductDetail.EquipmentType;

        //    //                    try
        //    //                    {
        //    //                        amentidlength = 0;

        //    //                        amentidlength = list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightCharacteristics.InFlightSrv.Count();

        //    //                        amenitiesidsplit = " ";
        //    //                        for (var i = 0; i < amentidlength; i++)

        //    //                        {
        //    //                            amenitiesid += list.FareMasterPricerTravelBoardSearchReply.FlightIndex[eachleg].GroupOfFlights[actualLegRefNo].FlightDetails[noofconnection].FlightCharacteristics.InFlightSrv[i];
        //    //                            amenitiesidsplit += amenitiesid + " ";
        //    //                            amenitiesid = " ";
        //    //                        }

        //    //                    }
        //    //                    catch (Exception)
        //    //                    {
        //    //                        amenitiesid = " ";
        //    //                    }

        //    //                    eachflightstring += "{\"Finfo\":{\"DateTime\":{\"Depdate\":\"" + depdate + "\",\"Deptime\":\"" + deptime + "\",\"Arrdate\":\""
        //    //                        + arrdate + "\",\"Arrtime\":\"" + arrtime + "\",\"Variation\":\"" + datevar + "\"},\"Location\":{\"locationFrom\":\""
        //    //                        + FromGoSeg + "\",\"Fromterminal\":\"" + fromterm + "\",\"LocationTo\":\"" + ToGoseg + "\",\"Toterminal\":\"" + totermseg +
        //    //                        "\"},\"CompanyId\":{\"mCarrier\":\"" + mcarrierseg + "\",\"oCarrier\":\"" + ocarrierseg + "\"},\"flightNo\":\"" + flightno +
        //    //                        "\",\"Bookingclass\":\"" + cabinclass + "\",\"eqpType\":\"" + euqidseg + "\",\"eTicketing\":\"Y\",\"attributeType\":\"EFT\",\"Elapsedtime\":\""
        //    //                        + elpsdtime + "\"},\"FlightCharacteristics\":{\"inFlightSrv\":\"NA\"}},";
        //    //                }

        //    //                eachflightstring = eachflightstring.Substring(0, eachflightstring.Length - 1);

        //    //                loopclosing += "]},";

        //    //                combined += laegllopstring + eachflightstring + loopclosing;

        //    //            }

        //    //            combined = combined.Substring(0, combined.Length - 1);

        //    //            groupclosing = " ]},";
        //    //            abc = FromGo + "-" + ToGo + "-" + depdate + "-" + deptime + "-" + ocarrier + "-" + flightno;


        //    //            eachflightstringcommon = "{\"SegmentRef\":{\"segRef\":\"1\",\"PSessionId\":\"NA\",\"supplier\":\"AMA001\",\"key\":\""
        //    //                + abc + "\"},\"TypeRef\":{\"type\":\"" + type1 + "\"},\"reccount\":{\"rec_id\":\""
        //    //                + reccount + "\",\"combi_id\":\"" + combi + "\"},\"Fare\":{\"Amount\":\""
        //    //                + totalfare + "\",\"taxfare\":\"" + taxfare + "\",\"basefare\":\""
        //    //                + basefare + "\",\"MarkupFare\":\"0\",\"currency\":\"AED\"},\"FareDetails\":{\"fareBasis\":\""
        //    //                + farebasiscode + "\"},\"Bagdetails\":{\"FreeAllowance\":\"" + bagallow +
        //    //                "\",\"Qcode\":\"" + bagcode + "\",\"unit\":\"" + bagunit +
        //    //                "\"},\"Description\":{\"Pricingmsg\":\"" + refund + "\"},";
        //    //            eachlegstring += " \"GroupOfFlights\":[";

        //    //            segflight += eachflightstringcommon + eachlegstring + combined + groupclosing;

        //    //        }


        //    //        type = 1;
        //    //    }

        //    //}

        //    //segflight = segflight.Substring(0, segflight.Length - 1);
        //    //// fullreq = fullreq.Substring(0, fullreq.Length - 1);

        //    //arraycloasing = "]";

        //    //lastcloasing = "}}";
        //    //fullstring = response + flightstring + segflight + arraycloasing + lastcloasing;

        //    //FLcommonRS m = JsonConvert.DeserializeObject<FLcommonRS>(fullstring);

        //    string jsonData = JsonConvert.SerializeObject(responseEntity);

        //    return m;
        //}

        //   2. fareInformativeBestPricingWithoutPNR
        //public FareRevalidateRS fareInformativeBestPricingWithoutPNR_1(FareRevalidateRQ FareRevalidateRQCom)
        //{
        //    FareRevalidateAmRQ _FareRevalidateAmRQ = new FareRevalidateAmRQ()
        //    {
        //        //loginId = FareRevalidateRQCom.SupplierAgencyDetails[0].UserName,
        //        //password = FareRevalidateRQCom.SupplierAgencyDetails[0].Password,
        //        //uuId = FareRevalidateRQCom.SupplierAgencyDetails[0].AccountNumber
        //        loginId = "",
        //        password = "",
        //        uuId = ""
        //    };
        //    Fareinformativebestpricingwithoutpnr _Fareinformativebestpricingwithoutpnr = new Fareinformativebestpricingwithoutpnr();

        //    int adt = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.ADT;
        //    int chd = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.CHD;
        //    int inf = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.INF;

        //    int totpassgroup = 0;
        //    if (adt > 0)
        //    {
        //        totpassgroup++;
        //    }
        //    if (chd > 0)
        //    {
        //        totpassgroup++;
        //    }
        //    if (inf > 0)
        //    {
        //        totpassgroup++;
        //    }

        //    //PassengersGroup
        //    Passengersgroup[] __Passengersgroup = new Passengersgroup[totpassgroup];




        //    if (adt > 0)
        //    {
        //        Passengersgroup _Passengersgroup = new Passengersgroup();
        //        Segmentcontroldetail _Segmentcontroldetail = new Segmentcontroldetail()
        //        {
        //            quantity = 1,
        //            numberOfUnits = adt
        //        };
        //        Segmentcontroldetail[] _Segmentcontroldetails = new Segmentcontroldetail[] { _Segmentcontroldetail };
        //        Segmentrepetitioncontrol _Segmentrepetitioncontrol = new Segmentrepetitioncontrol();
        //        _Segmentrepetitioncontrol.segmentControlDetails = _Segmentcontroldetails;
        //        _Passengersgroup.segmentRepetitionControl = _Segmentrepetitioncontrol;

        //        Travellersid _Travellersid = new Travellersid();
        //        Travellerdetails _Travellerdetail = new Travellerdetails()
        //        {
        //            measurementValue = 1
        //        };
        //        Travellerdetails[] __Travellerdetail = new Travellerdetails[] { _Travellerdetail };
        //        _Travellersid.travellerDetails = __Travellerdetail;
        //        _Passengersgroup.travellersID = _Travellersid;

        //        Discountptc _Discountptc = new Discountptc()
        //        {
        //            valueQualifier = "ADT"
        //        };
        //        _Passengersgroup.discountPtc = _Discountptc;
        //        __Passengersgroup[0] = _Passengersgroup;
        //    }
        //    if (chd > 0)
        //    {
        //        Passengersgroup _Passengersgroup = new Passengersgroup();
        //        Segmentcontroldetail _Segmentcontroldetail = new Segmentcontroldetail()
        //        {
        //            quantity = 2,
        //            numberOfUnits = chd
        //        };
        //        Segmentcontroldetail[] _Segmentcontroldetails = new Segmentcontroldetail[] { _Segmentcontroldetail };
        //        Segmentrepetitioncontrol _Segmentrepetitioncontrol = new Segmentrepetitioncontrol();
        //        _Segmentrepetitioncontrol.segmentControlDetails = _Segmentcontroldetails;
        //        _Passengersgroup.segmentRepetitionControl = _Segmentrepetitioncontrol;

        //        Travellersid _Travellersid = new Travellersid();
        //        Travellerdetails _Travellerdetail = new Travellerdetails()
        //        {
        //            measurementValue = 2
        //        };
        //        Travellerdetails[] __Travellerdetail = new Travellerdetails[] { _Travellerdetail };
        //        _Travellersid.travellerDetails = __Travellerdetail;
        //        _Passengersgroup.travellersID = _Travellersid;

        //        Discountptc _Discountptc = new Discountptc()
        //        {
        //            valueQualifier = "CH"
        //        };
        //        _Passengersgroup.discountPtc = _Discountptc;
        //        __Passengersgroup[1] = _Passengersgroup;
        //    }
        //    if (inf > 0)
        //    {
        //        Passengersgroup _Passengersgroup = new Passengersgroup();
        //        Segmentcontroldetail _Segmentcontroldetail = new Segmentcontroldetail()
        //        {
        //            quantity = 3,
        //            numberOfUnits = inf
        //        };
        //        Segmentcontroldetail[] _Segmentcontroldetails = new Segmentcontroldetail[] { _Segmentcontroldetail };
        //        Segmentrepetitioncontrol _Segmentrepetitioncontrol = new Segmentrepetitioncontrol();
        //        _Segmentrepetitioncontrol.segmentControlDetails = _Segmentcontroldetails;
        //        _Passengersgroup.segmentRepetitionControl = _Segmentrepetitioncontrol;

        //        Travellersid _Travellersid = new Travellersid();
        //        Travellerdetails _Travellerdetail = new Travellerdetails()
        //        {
        //            measurementValue = 1
        //        };
        //        Travellerdetails[] __Travellerdetail = new Travellerdetails[] { _Travellerdetail };
        //        _Travellersid.travellerDetails = __Travellerdetail;
        //        _Passengersgroup.travellersID = _Travellersid;

        //        Discountptc _Discountptc = new Discountptc()
        //        {
        //            valueQualifier = "INF"
        //        };
        //        _Passengersgroup.discountPtc = _Discountptc;
        //        __Passengersgroup[2] = _Passengersgroup;
        //    }



        //    _Fareinformativebestpricingwithoutpnr.passengersGroup = __Passengersgroup;

        //    //segmentGroup
        //    int numseg = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup.Length;

        //    //Segmentgroup[] __Segmentgroup = new Segmentgroup[] { _Segmentgroup };
        //    Segmentgroup[] __Segmentgroup = new Segmentgroup[numseg];


        //    for (int iseg = 0; iseg < numseg; iseg++)
        //    {
        //        Segmentgroup _Segmentgroup = new Segmentgroup();
        //        Segmentinformation _segmentInformation = new Segmentinformation();

        //        _segmentInformation.itemNumber = iseg + 1;

        //        Flightdate _Flightdate = new Flightdate();
        //        _Flightdate.departureDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.departureDate;
        //        _Flightdate.departureTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.departureTime;
        //        _Flightdate.arrivalDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.arrivalDate;
        //        _Flightdate.arrivalTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.arrivalTime;

        //        _segmentInformation.flightDate = _Flightdate;

        //        Boardpointdetails _Boardpointdetails = new Boardpointdetails();
        //        _Boardpointdetails.trueLocationId = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.boardPointDetails.trueLocationId;

        //        _segmentInformation.boardPointDetails = _Boardpointdetails;

        //        Offpointdetails _Offpointdetails = new Offpointdetails();
        //        _Offpointdetails.trueLocationId = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.offpointDetails.trueLocationId;

        //        _segmentInformation.offpointDetails = _Offpointdetails;

        //        Companydetails _Companydetails = new Companydetails();
        //        _Companydetails.marketingCompany = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.companyDetails.marketingCompany;
        //        _Companydetails.operatingCompany = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.companyDetails.operatingCompany;

        //        _segmentInformation.companyDetails = _Companydetails;

        //        Flightidentification _Flightidentification = new Flightidentification();
        //        _Flightidentification.flightNumber = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightIdentification.flightNumber;
        //        _Flightidentification.bookingClass = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightIdentification.bookingClass;

        //        _segmentInformation.flightIdentification = _Flightidentification;

        //        string[] No = new string[] { FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightTypeDetails.flightIndicator };

        //        Flighttypedetails _Flighttypedetails = new Flighttypedetails()
        //        {
        //            flightIndicator = No
        //        };
        //        _segmentInformation.flightTypeDetails = _Flighttypedetails;

        //        _Segmentgroup.segmentInformation = _segmentInformation;
        //        __Segmentgroup[iseg] = _Segmentgroup;
        //    }

        //    _Fareinformativebestpricingwithoutpnr.segmentGroup = __Segmentgroup;

        //    //pricingOptionGroup
        //    Pricingoptiongroup _Pricingoptiongroup = new Pricingoptiongroup();
        //    Pricingoptiongroup[] __Pricingoptiongroup = new Pricingoptiongroup[] { _Pricingoptiongroup };
        //    _Fareinformativebestpricingwithoutpnr.pricingOptionGroup = __Pricingoptiongroup;

        //    Pricingoptionkey _Pricingoptionkey = new Pricingoptionkey();
        //    _Pricingoptionkey.pricingOptionKey = "FCO";

        //    __Pricingoptiongroup[0].pricingOptionKey = _Pricingoptionkey;

        //    Currency _Currency = new Currency();
        //    Firstcurrencydetails _Firstcurrencydetails = new Firstcurrencydetails();
        //    _Firstcurrencydetails.currencyQualifier = "FCO";
        //    _Firstcurrencydetails.currencyIsoCode = "AED";

        //    _Currency.firstCurrencyDetails = _Firstcurrencydetails;
        //    __Pricingoptiongroup[0].currency = _Currency;

        //    _FareRevalidateAmRQ.fareInformativeBestPricingWithoutPNR = _Fareinformativebestpricingwithoutpnr;

        //    string json = JsonConvert.SerializeObject(_FareRevalidateAmRQ, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Include });
        //    //string RQUrl = FareRevalidateRQCom.SupplierAgencyDetails[0].BaseUrl + "/PriceWithoutPNR";
        //    string RQUrl = "http://70.32.24.42:9000/PriceWithoutPNR";

        //    string strResult = SOAPHelper.SendRESTRequest(json, RQUrl, "POST");

        //    return fareInformativeBestPricingWithoutPNRtoCom(strResult, adt, chd, inf, FareRevalidateRQCom);
        //}





        //public FareRevalidateRS fareInformativeBestPricingWithoutPNR_1(FareRevalidateRQ FareRevalidateRQCom)
        //{
        //    FareRevalidateAmRQ _FareRevalidateAmRQ = new FareRevalidateAmRQ()
        //    {
        //        //loginId = FareRevalidateRQCom.SupplierAgencyDetails[0].UserName,
        //        //password = FareRevalidateRQCom.SupplierAgencyDetails[0].Password,
        //        //uuId = FareRevalidateRQCom.SupplierAgencyDetails[0].AccountNumber
        //        loginId = "",
        //        password = "",
        //        uuId = ""
        //    };
        //    Fareinformativebestpricingwithoutpnr _Fareinformativebestpricingwithoutpnr = new Fareinformativebestpricingwithoutpnr();

        //    int adt = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.ADT;
        //    int chd = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.CHD;
        //    int inf = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.INF;

        //    int totpassgroup = 0;
        //    if (adt > 0)
        //    {
        //        totpassgroup++;
        //    }
        //    if (chd > 0)
        //    {
        //        totpassgroup++;
        //    }
        //    if (inf > 0)
        //    {
        //        totpassgroup++;
        //    }

        //    //PassengersGroup
        //    Passengersgroup[] __Passengersgroup = new Passengersgroup[totpassgroup];




        //    if (adt > 0)
        //    {
        //        Passengersgroup _Passengersgroup = new Passengersgroup();
        //        Segmentcontroldetail _Segmentcontroldetail = new Segmentcontroldetail()
        //        {
        //            quantity = 1,
        //            numberOfUnits = adt
        //        };
        //        Segmentcontroldetail[] _Segmentcontroldetails = new Segmentcontroldetail[] { _Segmentcontroldetail };
        //        Segmentrepetitioncontrol _Segmentrepetitioncontrol = new Segmentrepetitioncontrol();
        //        _Segmentrepetitioncontrol.segmentControlDetails = _Segmentcontroldetails;
        //        _Passengersgroup.segmentRepetitionControl = _Segmentrepetitioncontrol;

        //        Travellersid _Travellersid = new Travellersid();
        //        Travellerdetails _Travellerdetail = new Travellerdetails()
        //        {
        //            measurementValue = 1
        //        };
        //        Travellerdetails[] __Travellerdetail = new Travellerdetails[] { _Travellerdetail };
        //        _Travellersid.travellerDetails = __Travellerdetail;
        //        _Passengersgroup.travellersID = _Travellersid;

        //        Discountptc _Discountptc = new Discountptc()
        //        {
        //            valueQualifier = "ADT"
        //        };
        //        _Passengersgroup.discountPtc = _Discountptc;
        //        __Passengersgroup[0] = _Passengersgroup;
        //    }
        //    if (chd > 0)
        //    {
        //        Passengersgroup _Passengersgroup = new Passengersgroup();
        //        Segmentcontroldetail _Segmentcontroldetail = new Segmentcontroldetail()
        //        {
        //            quantity = 2,
        //            numberOfUnits = chd
        //        };
        //        Segmentcontroldetail[] _Segmentcontroldetails = new Segmentcontroldetail[] { _Segmentcontroldetail };
        //        Segmentrepetitioncontrol _Segmentrepetitioncontrol = new Segmentrepetitioncontrol();
        //        _Segmentrepetitioncontrol.segmentControlDetails = _Segmentcontroldetails;
        //        _Passengersgroup.segmentRepetitionControl = _Segmentrepetitioncontrol;

        //        Travellersid _Travellersid = new Travellersid();
        //        Travellerdetails _Travellerdetail = new Travellerdetails()
        //        {
        //            measurementValue = 2
        //        };
        //        Travellerdetails[] __Travellerdetail = new Travellerdetails[] { _Travellerdetail };
        //        _Travellersid.travellerDetails = __Travellerdetail;
        //        _Passengersgroup.travellersID = _Travellersid;

        //        Discountptc _Discountptc = new Discountptc()
        //        {
        //            valueQualifier = "CH"
        //        };
        //        _Passengersgroup.discountPtc = _Discountptc;
        //        __Passengersgroup[1] = _Passengersgroup;
        //    }
        //    if (inf > 0)
        //    {
        //        Passengersgroup _Passengersgroup = new Passengersgroup();
        //        Segmentcontroldetail _Segmentcontroldetail = new Segmentcontroldetail()
        //        {
        //            quantity = 3,
        //            numberOfUnits = inf
        //        };
        //        Segmentcontroldetail[] _Segmentcontroldetails = new Segmentcontroldetail[] { _Segmentcontroldetail };
        //        Segmentrepetitioncontrol _Segmentrepetitioncontrol = new Segmentrepetitioncontrol();
        //        _Segmentrepetitioncontrol.segmentControlDetails = _Segmentcontroldetails;
        //        _Passengersgroup.segmentRepetitionControl = _Segmentrepetitioncontrol;

        //        Travellersid _Travellersid = new Travellersid();
        //        Travellerdetails _Travellerdetail = new Travellerdetails()
        //        {
        //            measurementValue = 1
        //        };
        //        Travellerdetails[] __Travellerdetail = new Travellerdetails[] { _Travellerdetail };
        //        _Travellersid.travellerDetails = __Travellerdetail;
        //        _Passengersgroup.travellersID = _Travellersid;

        //        Discountptc _Discountptc = new Discountptc()
        //        {
        //            valueQualifier = "INF"
        //        };
        //        _Passengersgroup.discountPtc = _Discountptc;
        //        __Passengersgroup[2] = _Passengersgroup;
        //    }



        //    _Fareinformativebestpricingwithoutpnr.passengersGroup = __Passengersgroup;

        //    //segmentGroup
        //    int numseg = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup.Length;

        //    //Segmentgroup[] __Segmentgroup = new Segmentgroup[] { _Segmentgroup };
        //    Segmentgroup[] __Segmentgroup = new Segmentgroup[numseg];


        //    for (int iseg = 0; iseg < numseg; iseg++)
        //    {
        //        Segmentgroup _Segmentgroup = new Segmentgroup();
        //        Segmentinformation _segmentInformation = new Segmentinformation();

        //        //_segmentInformation.itemNumber = iseg + 1;

        //        Flightdate _Flightdate = new Flightdate();
        //        _Flightdate.departureDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.departureDate;
        //        _Flightdate.departureTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.departureTime;
        //        _Flightdate.arrivalDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.arrivalDate;
        //        _Flightdate.arrivalTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.arrivalTime;

        //        _segmentInformation.flightDate = _Flightdate;

        //        Boardpointdetails _Boardpointdetails = new Boardpointdetails();
        //        _Boardpointdetails.trueLocationId = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.boardPointDetails.trueLocationId;

        //        _segmentInformation.boardPointDetails = _Boardpointdetails;

        //        Offpointdetails _Offpointdetails = new Offpointdetails();
        //        _Offpointdetails.trueLocationId = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.offpointDetails.trueLocationId;

        //        _segmentInformation.offpointDetails = _Offpointdetails;

        //        Companydetails _Companydetails = new Companydetails();
        //        _Companydetails.marketingCompany = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.companyDetails.marketingCompany;
        //        _Companydetails.operatingCompany = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.companyDetails.operatingCompany;

        //        _segmentInformation.companyDetails = _Companydetails;

        //        Flightidentification _Flightidentification = new Flightidentification();
        //        _Flightidentification.flightNumber = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightIdentification.flightNumber;
        //        _Flightidentification.bookingClass = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightIdentification.bookingClass;

        //        _segmentInformation.flightIdentification = _Flightidentification;

        //        string[] No = new string[] { FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightTypeDetails.flightIndicator };

        //        Flighttypedetails _Flighttypedetails = new Flighttypedetails()
        //        {
        //            flightIndicator = No
        //        };
        //        _segmentInformation.flightTypeDetails = _Flighttypedetails;

        //        _Segmentgroup.segmentInformation = _segmentInformation;
        //        __Segmentgroup[iseg] = _Segmentgroup;
        //    }

        //    _Fareinformativebestpricingwithoutpnr.segmentGroup = __Segmentgroup;

        //    //pricingOptionGroup
        //    Pricingoptiongroup _Pricingoptiongroup = new Pricingoptiongroup();
        //    Pricingoptiongroup[] __Pricingoptiongroup = new Pricingoptiongroup[] { _Pricingoptiongroup };
        //    _Fareinformativebestpricingwithoutpnr.pricingOptionGroup = __Pricingoptiongroup;

        //    Pricingoptionkey _Pricingoptionkey = new Pricingoptionkey();
        //    _Pricingoptionkey.pricingOptionKey = "FCO";

        //    __Pricingoptiongroup[0].pricingOptionKey = _Pricingoptionkey;

        //    Currency _Currency = new Currency();
        //    Firstcurrencydetails _Firstcurrencydetails = new Firstcurrencydetails();
        //    _Firstcurrencydetails.currencyQualifier = "FCO";
        //    _Firstcurrencydetails.currencyIsoCode = "AED";

        //    _Currency.firstCurrencyDetails = _Firstcurrencydetails;
        //    __Pricingoptiongroup[0].currency = _Currency;

        //    _FareRevalidateAmRQ.fareInformativeBestPricingWithoutPNR = _Fareinformativebestpricingwithoutpnr;

        //    string json = JsonConvert.SerializeObject(_FareRevalidateAmRQ, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Include });
        //    //string RQUrl = FareRevalidateRQCom.SupplierAgencyDetails[0].BaseUrl + "/PriceWithoutPNR";
        //    string RQUrl = "http://70.32.24.42:9000/PriceWithoutPNR";

        //    string strResult = SOAPHelper.SendRESTRequest(json, RQUrl, "POST");

        //    return FareInformativeBestPricingWithoutPNRtoCom(strResult, adt, chd, inf, FareRevalidateRQCom);
        //}


        //Refactor_code

        public async Task<FareRevalidateRS> FareInformativeBestPricingWithoutPnr(FareRevalidateRQ FareRevalidateRQCom)
        {

            #region --test code2--

            string AgencyCode = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.ArAgencyCode;

            // farePricer = new FarePricer();
            //{
            //    LoginId = "";
            //    password = "";
            //    uuId = "";
            //};


            BestPricingRq bestPricingRq = new BestPricingRq()
            {
                LoginId = FareRevalidateRQCom.CommonRequestFarePricer.SupplierAgencyDetails.First().UserName,
                Password = FareRevalidateRQCom.CommonRequestFarePricer.SupplierAgencyDetails.First().Password,
                UuId = FareRevalidateRQCom.CommonRequestFarePricer.SupplierAgencyDetails.First().AccountNumber

            };


            int adt1 = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.Adt;
            int chd1 = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.Chd;
            int inf1 = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.Inf;

            int totpassgroup1 = 0;
            if (adt1 > 0)
            {
                totpassgroup1++;
            }
            if (chd1 > 0)
            {
                totpassgroup1++;
            }
            if (inf1 > 0)
            {
                totpassgroup1++;
            }

            var __Passengersgroup1 = bestPricingRq.FareInformativeBestPricingWithoutPnr.PassengersGroup;
            var segmentRepetitionControl = new SegmentRepetitionControl();
            var segmentRepetitionControl1 = new SegmentRepetitionControl();
            var segmentRepetitionControl2 = new SegmentRepetitionControl();
            var travellersId = new TravellersId();
            var travellersId1 = new TravellersId();
            var travellersId2 = new TravellersId();
            var discountPtc1 = new DiscountPtc();
            var discountPtc2 = new DiscountPtc();
            var discountPtc3 = new DiscountPtc();
            int mvalue = 1;
            var seg = new List<SegmentControlDetail>();
            //new List<Passengersgroup>(totpassgroup1);
            if (adt1 > 0)
            {


                segmentRepetitionControl.SegmentControlDetails.Add(new SegmentControlDetail

                {
                    Quantity = 1,
                    NumberOfUnits = adt1
                });

                for (int i = 0; i < adt1; i++)
                {
                    travellersId.TravellerDetails.Add(new TravellerDetail
                    {
                        MeasurementValue = mvalue

                    });
                    mvalue++;
                }
                discountPtc1.ValueQualifier = PaxTypeEnum.ADT.ToString();
                __Passengersgroup1.Add(new PassengersGroup()
                {

                    SegmentRepetitionControl = segmentRepetitionControl,
                    TravellersId = travellersId,
                    DiscountPtc = discountPtc1
                });

            }

            if (chd1 > 0)
            {

                segmentRepetitionControl1.SegmentControlDetails.Add(new SegmentControlDetail
                {
                    Quantity = 2,
                    NumberOfUnits = chd1
                });
                for (int i = 0; i < chd1; i++)
                {
                    travellersId1.TravellerDetails.Add(new TravellerDetail
                    {
                        MeasurementValue = mvalue

                    });
                    mvalue++;
                }
                discountPtc2.ValueQualifier = PaxTypeEnum.CH.ToString();
                __Passengersgroup1.Add(new PassengersGroup()
                {

                    SegmentRepetitionControl = segmentRepetitionControl1,
                    TravellersId = travellersId1,
                    DiscountPtc = discountPtc2
                });



            }
            if (inf1 > 0)
            {

                segmentRepetitionControl2.SegmentControlDetails.Add(new SegmentControlDetail
                {
                    Quantity = 3,
                    NumberOfUnits = inf1
                });

                travellersId2.TravellerDetails.Add(new TravellerDetail
                {
                    MeasurementValue = 3

                });

                discountPtc3.ValueQualifier = PaxTypeEnum.INF.ToString();
                __Passengersgroup1.Add(new PassengersGroup()
                {

                    SegmentRepetitionControl = segmentRepetitionControl2,
                    TravellersId = travellersId2,
                    DiscountPtc = discountPtc3
                });


            }

            //segmentgroup


            var __Segmentgroup1 = bestPricingRq.FareInformativeBestPricingWithoutPnr.SegmentGroup;

            int iseg = 0;
            FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup.ForEach(numseg =>
            {


                //for (int iseg = 0; iseg < numseg; iseg++)
                //{
                Models.SegmentInformation _segmentInformation = new Models.SegmentInformation();

                _segmentInformation.FlightDate.DepartureDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.FlightDate.DepartureDate;
                _segmentInformation.FlightDate.DepartureTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.FlightDate.DepartureTime;
                _segmentInformation.FlightDate.ArrivalDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.FlightDate.ArrivalDate;
                _segmentInformation.FlightDate.ArrivalTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.FlightDate.ArrivalTime;
                _segmentInformation.BoardPointDetails.TrueLocationId = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.BoardPointDetails.TrueLocationId;
                _segmentInformation.OffpointDetails.TrueLocationId = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.OffpointDetails.TrueLocationId;
                _segmentInformation.CompanyDetails.MarketingCompany = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.CompanyDetails.MarketingCompany;
                _segmentInformation.CompanyDetails.OperatingCompany = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.CompanyDetails.OperatingCompany;
                _segmentInformation.FlightIdentification.FlightNumber = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.FlightIdentification.FlightNumber;
                _segmentInformation.FlightIdentification.BookingClass = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.FlightIdentification.BookingClass;

                //  string[] No = new string[] 

                //  FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightTypeDetails.flightIndicator };

                List<string> ind = new List<string>();
                ind.Add(FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[iseg].SegmentInformation.FlightTypeDetails.FlightIndicator.ToString());

                _segmentInformation.ItemNumber = iseg + 1;
                _segmentInformation.FlightTypeDetails.FlightIndicator = ind;


                bestPricingRq.FareInformativeBestPricingWithoutPnr.SegmentGroup.Add(new Models.SegmentGroup()
                { SegmentInformation = _segmentInformation });
                //FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup.Add(new Models.SegmentGroup()
                //{

                //    SegmentInformation = _segmentInformation

                //});
                iseg++;
            });

            //pricingoptiongroup

            var _Pricingoptiongroup1 = bestPricingRq.FareInformativeBestPricingWithoutPnr.PricingOptionGroup;
            var pricing = new Models.PricingOptionKey();
            var currency = new Models.Currency();
            currency.FirstCurrencyDetails.CurrencyIsoCode = FareRevalidateRQCom.CommonRequestFarePricer.SupplierAgencyDetails.First().ToCurrency;
            currency.FirstCurrencyDetails.CurrencyQualifier = CurrencyEnum.FCO.ToString();
            pricing.PurplePricingOptionKey = CurrencyEnum.FCO.ToString();

            _Pricingoptiongroup1.Add(new Models.PricingOptionGroup
            {
                PricingOptionKey = pricing,
                Currency = currency

            });


            var jsonData = BestPricingRqSerialize.ToJson(bestPricingRq);


            #endregion




            #region --old code--

            ////old code
            //FareRevalidateAmRQ _FareRevalidateAmRQ = new FareRevalidateAmRQ()
            //    {
            //        //loginId = FareRevalidateRQCom.SupplierAgencyDetails[0].UserName,
            //        //password = FareRevalidateRQCom.SupplierAgencyDetails[0].Password,
            //        //uuId = FareRevalidateRQCom.SupplierAgencyDetails[0].AccountNumber
            //        loginId = "",
            //        password = "",
            //        uuId = ""
            //    };
            //    Fareinformativebestpricingwithoutpnr _Fareinformativebestpricingwithoutpnr = new Fareinformativebestpricingwithoutpnr();

            //    int adt = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.ADT;
            //    int chd = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.CHD;
            //    int inf = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.INF;

            //    int totpassgroup = 0;
            //    if (adt > 0)
            //    {
            //        totpassgroup++;
            //    }
            //    if (chd > 0)
            //    {
            //        totpassgroup++;
            //    }
            //    if (inf > 0)
            //    {
            //        totpassgroup++;
            //    }

            //    //PassengersGroup
            //    Passengersgroup[] __Passengersgroup = new Passengersgroup[totpassgroup];

            //    if (adt > 0)
            //    {
            //        Passengersgroup _Passengersgroup = new Passengersgroup();
            //        Segmentcontroldetail _Segmentcontroldetail = new Segmentcontroldetail()
            //        {
            //            quantity = 1,
            //            numberOfUnits = adt
            //        };
            //        Segmentcontroldetail[] _Segmentcontroldetails = new Segmentcontroldetail[] { _Segmentcontroldetail };
            //        Segmentrepetitioncontrol _Segmentrepetitioncontrol = new Segmentrepetitioncontrol();
            //        _Segmentrepetitioncontrol.segmentControlDetails = _Segmentcontroldetails;
            //        _Passengersgroup.segmentRepetitionControl = _Segmentrepetitioncontrol;

            //        Travellersid _Travellersid = new Travellersid();
            //        Travellerdetails _Travellerdetail = new Travellerdetails()
            //        {
            //            measurementValue = 1
            //        };
            //        Travellerdetails[] __Travellerdetail = new Travellerdetails[] { _Travellerdetail };
            //        _Travellersid.travellerDetails = __Travellerdetail;
            //        _Passengersgroup.travellersID = _Travellersid;

            //        Discountptc _Discountptc = new Discountptc()
            //        {
            //            valueQualifier = "ADT"
            //        };
            //        _Passengersgroup.discountPtc = _Discountptc;
            //        __Passengersgroup[0] = _Passengersgroup;
            //    }
            //    if (chd > 0)
            //    {
            //        Passengersgroup _Passengersgroup = new Passengersgroup();
            //        Segmentcontroldetail _Segmentcontroldetail = new Segmentcontroldetail()
            //        {
            //            quantity = 2,
            //            numberOfUnits = chd
            //        };
            //        Segmentcontroldetail[] _Segmentcontroldetails = new Segmentcontroldetail[] { _Segmentcontroldetail };
            //        Segmentrepetitioncontrol _Segmentrepetitioncontrol = new Segmentrepetitioncontrol();
            //        _Segmentrepetitioncontrol.segmentControlDetails = _Segmentcontroldetails;
            //        _Passengersgroup.segmentRepetitionControl = _Segmentrepetitioncontrol;

            //        Travellersid _Travellersid = new Travellersid();
            //        Travellerdetails _Travellerdetail = new Travellerdetails()
            //        {
            //            measurementValue = 2
            //        };
            //        Travellerdetails[] __Travellerdetail = new Travellerdetails[] { _Travellerdetail };
            //        _Travellersid.travellerDetails = __Travellerdetail;
            //        _Passengersgroup.travellersID = _Travellersid;

            //        Discountptc _Discountptc = new Discountptc()
            //        {
            //            valueQualifier = "CH"
            //        };
            //        _Passengersgroup.discountPtc = _Discountptc;
            //        __Passengersgroup[1] = _Passengersgroup;
            //    }
            //    if (inf > 0)
            //    {
            //        Passengersgroup _Passengersgroup = new Passengersgroup();
            //        Segmentcontroldetail _Segmentcontroldetail = new Segmentcontroldetail()
            //        {
            //            quantity = 3,
            //            numberOfUnits = inf
            //        };
            //        Segmentcontroldetail[] _Segmentcontroldetails = new Segmentcontroldetail[] { _Segmentcontroldetail };
            //        Segmentrepetitioncontrol _Segmentrepetitioncontrol = new Segmentrepetitioncontrol();
            //        _Segmentrepetitioncontrol.segmentControlDetails = _Segmentcontroldetails;
            //        _Passengersgroup.segmentRepetitionControl = _Segmentrepetitioncontrol;

            //        Travellersid _Travellersid = new Travellersid();
            //        Travellerdetails _Travellerdetail = new Travellerdetails()
            //        {
            //            measurementValue = 1
            //        };
            //        Travellerdetails[] __Travellerdetail = new Travellerdetails[] { _Travellerdetail };
            //        _Travellersid.travellerDetails = __Travellerdetail;
            //        _Passengersgroup.travellersID = _Travellersid;

            //        Discountptc _Discountptc = new Discountptc()
            //        {
            //            valueQualifier = "INF"
            //        };
            //        _Passengersgroup.discountPtc = _Discountptc;
            //        __Passengersgroup[2] = _Passengersgroup;
            //    }



            //    _Fareinformativebestpricingwithoutpnr.passengersGroup = __Passengersgroup;

            //    //segmentGroup
            //    int numseg1 = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup.Length;

            //    //Segmentgroup[] __Segmentgroup = new Segmentgroup[] { _Segmentgroup };
            //    Segmentgroup[] __Segmentgroup = new Segmentgroup[numseg];


            //    for (int iseg = 0; iseg < numseg1; iseg++)
            //    {
            //        Segmentgroup _Segmentgroup = new Segmentgroup();
            //        Segmentinformation _segmentInformation = new Segmentinformation();

            //        _segmentInformation.itemNumber = iseg + 1;

            //        Flightdate _Flightdate = new Flightdate();
            //        _Flightdate.departureDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.departureDate;
            //        _Flightdate.departureTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.departureTime;
            //        _Flightdate.arrivalDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.arrivalDate;
            //        _Flightdate.arrivalTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightDate.arrivalTime;

            //        _segmentInformation.flightDate = _Flightdate;

            //        Boardpointdetails _Boardpointdetails = new Boardpointdetails();
            //        _Boardpointdetails.trueLocationId = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.boardPointDetails.trueLocationId;

            //        _segmentInformation.boardPointDetails = _Boardpointdetails;

            //        Offpointdetails _Offpointdetails = new Offpointdetails();
            //        _Offpointdetails.trueLocationId = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.offpointDetails.trueLocationId;

            //        _segmentInformation.offpointDetails = _Offpointdetails;

            //        Companydetails _Companydetails = new Companydetails();
            //        _Companydetails.marketingCompany = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.companyDetails.marketingCompany;
            //        _Companydetails.operatingCompany = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.companyDetails.operatingCompany;

            //        _segmentInformation.companyDetails = _Companydetails;

            //        Flightidentification _Flightidentification = new Flightidentification();
            //        _Flightidentification.flightNumber = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightIdentification.flightNumber;
            //        _Flightidentification.bookingClass = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightIdentification.bookingClass;

            //        _segmentInformation.flightIdentification = _Flightidentification;

            //        string[] No = new string[] { FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[iseg].segmentInformation.flightTypeDetails.flightIndicator };

            //        Flighttypedetails _Flighttypedetails = new Flighttypedetails()
            //        {
            //            flightIndicator = No
            //        };
            //        _segmentInformation.flightTypeDetails = _Flighttypedetails;

            //        _Segmentgroup.segmentInformation = _segmentInformation;
            //        __Segmentgroup[iseg] = _Segmentgroup;
            //    }

            //    _Fareinformativebestpricingwithoutpnr.segmentGroup = __Segmentgroup;

            //    //pricingOptionGroup
            //    Pricingoptiongroup _Pricingoptiongroup = new Pricingoptiongroup();
            //    Pricingoptiongroup[] __Pricingoptiongroup = new Pricingoptiongroup[] { _Pricingoptiongroup };
            //    _Fareinformativebestpricingwithoutpnr.pricingOptionGroup = __Pricingoptiongroup;

            //    Pricingoptionkey _Pricingoptionkey = new Pricingoptionkey();
            //    _Pricingoptionkey.pricingOptionKey = "FCO";

            //    __Pricingoptiongroup[0].pricingOptionKey = _Pricingoptionkey;

            //    Currency _Currency = new Currency();
            //    Firstcurrencydetails _Firstcurrencydetails = new Firstcurrencydetails();
            //    _Firstcurrencydetails.currencyQualifier = "FCO";
            //    _Firstcurrencydetails.currencyIsoCode = "AED";

            //    _Currency.firstCurrencyDetails = _Firstcurrencydetails;
            //    __Pricingoptiongroup[0].currency = _Currency;

            //    _FareRevalidateAmRQ.fareInformativeBestPricingWithoutPNR = _Fareinformativebestpricingwithoutpnr;

            //    string json = JsonConvert.SerializeObject(_FareRevalidateAmRQ, Newtonsoft.Json.Formatting.Indented, new JsonSerializerSettings() { DefaultValueHandling = DefaultValueHandling.Include });
            //string RQUrl = FareRevalidateRQCom.SupplierAgencyDetails[0].BaseUrl + "/PriceWithoutPNR";

            #endregion


            string RQUrl = "http://70.32.24.42:9000/PriceWithoutPNR";

            string scount = (adt1 + chd1 + inf1).ToString();
            //string strResult = "{'fareInformativeBestPricingWithoutPNRReply':{'messageDetails':{'messageFunctionDetails':{'businessFunction':'1','messageFunction':'741','responsibleAgency':'1A','additionalMessageFunction':'170'},'responseType':'A'},'mainGroup':{'dummySegment':'','convertionRate':{'conversionRateDetails':{'rateType':'BSR','pricingAmount':'0.105176'},'otherConvRateDetails':{'rateType':'USR','pricingAmount':'3.67296','dutyTaxFeeType':'FR'}},'generalIndicatorsGroup':{'generalIndicators':{'priceTicketDetails':{'indicators':'I'}}},'pricingGroupLevelGroup':[{'numberOfPax':{'segmentControlDetails':{'quantity':'1','numberOfUnits':'1'}},'passengersID':{'travellerDetails':{'measurementValue':'1'}},'fareInfoGroup':{'emptySegment':'','pricingIndicators':{'priceTariffType':'I','productDateTimeDetails':{'departureDate':'260817'},'companyDetails':{'otherCompany':'WY'}},'fareAmount':{'monetaryDetails':{'typeQualifier':'B','amount':'500','currency':'AED'},'otherMonetaryDetails':[{'typeQualifier':'E','amount':'53.000','currency':'OMR'},{'typeQualifier':'712','amount':'67.200','currency':'OMR'}]},'textData':[{'freeTextQualification':{'textSubjectQualifier':'4','informationType':'15'},'freeText':'26AUG17DXB WY X/SLL WY CCJ134.76NUC134.76END ROE3.672960'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1P3'},'freeText':'PENALTY APPLIES'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'10'},'freeText':'2PC/30KG/VALIDONWY'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A49'},'freeText':'- DATE OF ORIGIN'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A0'},'freeText':'BAG AT A CHARGE MAY BE AVAILABLE-ENTER FXK'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A0'},'freeText':'TICKET STOCK RESTRICTION'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A0'},'freeText':'BG CXR: 2*WY'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A0'},'freeText':'PRICED WITH VALIDATING CARRIER WY - REPRICE IF DIFFERENT VC'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A0'},'freeText':'PRICED BY BEST PRICER WITHOUT PNR'}],'surchargesGroup':{'taxesAmount':{'taxDetails':[{'rate':'1.000','countryCode':'YR','type':'VA'},{'rate':'7.900','countryCode':'AE','type':'AD'},{'rate':'3.700','countryCode':'F6','type':'TO'},{'rate':'0.600','countryCode':'TP','type':'SE'},{'rate':'1.000','countryCode':'I2','type':'AS'}]},'penaltiesAmount':{'discountPenaltyQualifier':'700','discountPenaltyDetails':{'amount':'19','currency':'OMR'}}},'segmentLevelGroup':[{'segmentInformation':{'flightDate':{'departureDate':'260817'},'boardPointDetails':{'trueLocationId':'DXB'},'offpointDetails':{'trueLocationId':'SLL'},'companyDetails':{'marketingCompany':'WY'},'flightIdentification':{'flightNumber':'624','bookingClass':'N','operationalSuffix':'X'},'flightTypeDetails':{'flightIndicator':'T'},'itemNumber':'1'},'additionalInformation':{'priceTicketDetails':{'indicators':'LF'}},'fareBasis':{'additionalFareDetails':{'rateClass':'NVESOWAECH','secondRateClass':'N'}},'cabinGroup':{'cabinSegment':{'bookingClassDetails':{'designator':'N','option':'M'}}},'baggageAllowance':{'baggageDetails':{'freeAllowance':'2','quantityCode':'N'}},'ptcSegment':{'quantityDetails':{'numberOfUnit':'1','unitQualifier':'CH'}}},{'segmentInformation':{'flightDate':{'departureDate':'270817'},'boardPointDetails':{'trueLocationId':'SLL'},'offpointDetails':{'trueLocationId':'CCJ'},'companyDetails':{'marketingCompany':'WY'},'flightIdentification':{'flightNumber':'295','bookingClass':'N','operationalSuffix':'X'},'itemNumber':'2'},'additionalInformation':{'priceTicketDetails':{'indicators':'LF'}},'fareBasis':{'additionalFareDetails':{'rateClass':'NVESOWAECH','secondRateClass':'N'}},'cabinGroup':{'cabinSegment':{'bookingClassDetails':{'designator':'N','option':'M'}}},'baggageAllowance':{'baggageDetails':{'freeAllowance':'2','quantityCode':'N'}},'ptcSegment':{'quantityDetails':{'numberOfUnit':'1','unitQualifier':'CH'}}}],'fareComponentDetailsGroup':{'fareComponentID':{'itemNumberDetails':{'number':'1','type':'FC'}},'marketFareComponent':{'boardPointDetails':{'trueLocationId':'DXB'},'offpointDetails':{'trueLocationId':'CCJ'}},'monetaryInformation':{'monetaryDetails':{'typeQualifier':'TFC','amount':'134.76','currency':'NUC'}},'componentClassInfo':{'fareBasisDetails':{'rateTariffClass':'NVESOWAECH'}},'fareQualifiersDetail':{'discountDetails':{'fareQualifier':'756'}},'couponDetailsGroup':[{'productId':{'referenceDetails':{'type':'ST','value':'1'}}},{'productId':{'referenceDetails':{'type':'ST','value':'2'}}}]}}},{'numberOfPax':{'segmentControlDetails':{'quantity':'2','numberOfUnits':'1'}},'passengersID':{'travellerDetails':{'measurementValue':'1'}},'fareInfoGroup':{'emptySegment':'','pricingIndicators':{'priceTariffType':'I','productDateTimeDetails':{'departureDate':'260817'},'companyDetails':{'otherCompany':'WY'}},'fareAmount':{'monetaryDetails':{'typeQualifier':'B','amount':'960','currency':'AED'},'otherMonetaryDetails':[{'typeQualifier':'E','amount':'101.000','currency':'OMR'},{'typeQualifier':'712','amount':'115.200','currency':'OMR'}]},'textData':[{'freeTextQualification':{'textSubjectQualifier':'4','informationType':'15'},'freeText':'26AUG17DXB WY X/SLL WY CCJ259.32NUC259.32END ROE3.672960'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1P3'},'freeText':'PENALTY APPLIES'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'10'},'freeText':'2PC/30KG/VALIDONWY'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A49'},'freeText':'- DATE OF ORIGIN'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A0'},'freeText':'BAG AT A CHARGE MAY BE AVAILABLE-ENTER FXK'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A0'},'freeText':'TICKET STOCK RESTRICTION'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A0'},'freeText':'BG CXR: 2*WY'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A0'},'freeText':'PRICED WITH VALIDATING CARRIER WY - REPRICE IF DIFFERENT VC'},{'freeTextQualification':{'textSubjectQualifier':'1','informationType':'1A0'},'freeText':'PRICED BY BEST PRICER WITHOUT PNR'}],'surchargesGroup':{'taxesAmount':{'taxDetails':[{'rate':'1.000','countryCode':'YR','type':'VA'},{'rate':'7.900','countryCode':'AE','type':'AD'},{'rate':'3.700','countryCode':'F6','type':'TO'},{'rate':'0.600','countryCode':'TP','type':'SE'},{'rate':'1.000','countryCode':'I2','type':'AS'}]},'penaltiesAmount':{'discountPenaltyQualifier':'700','discountPenaltyDetails':{'amount':'19','currency':'OMR'}}},'segmentLevelGroup':[{'segmentInformation':{'flightDate':{'departureDate':'260817'},'boardPointDetails':{'trueLocationId':'DXB'},'offpointDetails':{'trueLocationId':'SLL'},'companyDetails':{'marketingCompany':'WY'},'flightIdentification':{'flightNumber':'624','bookingClass':'K','operationalSuffix':'X'},'flightTypeDetails':{'flightIndicator':'T'},'itemNumber':'1'},'additionalInformation':{'priceTicketDetails':{'indicators':'LF'}},'fareBasis':{'additionalFareDetails':{'rateClass':'KFESOWAECH','secondRateClass':'K'}},'cabinGroup':{'cabinSegment':{'bookingClassDetails':{'designator':'K','option':'M'}}},'baggageAllowance':{'baggageDetails':{'freeAllowance':'2','quantityCode':'N'}},'ptcSegment':{'quantityDetails':{'numberOfUnit':'1','unitQualifier':'CH'}}},{'segmentInformation':{'flightDate':{'departureDate':'270817'},'boardPointDetails':{'trueLocationId':'SLL'},'offpointDetails':{'trueLocationId':'CCJ'},'companyDetails':{'marketingCompany':'WY'},'flightIdentification':{'flightNumber':'295','bookingClass':'K','operationalSuffix':'X'},'itemNumber':'2'},'additionalInformation':{'priceTicketDetails':{'indicators':'LF'}},'fareBasis':{'additionalFareDetails':{'rateClass':'KFESOWAECH','secondRateClass':'K'}},'cabinGroup':{'cabinSegment':{'bookingClassDetails':{'designator':'K','option':'M'}}},'baggageAllowance':{'baggageDetails':{'freeAllowance':'2','quantityCode':'N'}},'ptcSegment':{'quantityDetails':{'numberOfUnit':'1','unitQualifier':'CH'}}}],'fareComponentDetailsGroup':{'fareComponentID':{'itemNumberDetails':{'number':'1','type':'FC'}},'marketFareComponent':{'boardPointDetails':{'trueLocationId':'DXB'},'offpointDetails':{'trueLocationId':'CCJ'}},'monetaryInformation':{'monetaryDetails':{'typeQualifier':'TFC','amount':'259.32','currency':'NUC'}},'componentClassInfo':{'fareBasisDetails':{'rateTariffClass':'KFESOWAECH'}},'fareQualifiersDetail':{'discountDetails':{'fareQualifier':'756'}},'couponDetailsGroup':[{'productId':{'referenceDetails':{'type':'ST','value':'1'}}},{'productId':{'referenceDetails':{'type':'ST','value':'2'}}}]}}}]}}}";
            //string strResult = SOAPHelper.SendRESTRequest(jsonData1, RQUrl, "POST");
            string response = await soapHelperAsync.SendRESTRequestAsync(jsonData, RQUrl, "POST");
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(bestPricingRq);
            CommonServices.SaveLog("BEST_PRICING", FareRevalidateRQCom.CommonRequestFarePricer.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, response);
            string farecheckrulesRS = await Farecheckrules(scount, FareRevalidateRQCom);
            return FareInformativeBestPricingWithoutPNRtoCom(response, farecheckrulesRS, adt1, chd1, inf1, FareRevalidateRQCom);
        }


        //fareInformativeBestPricingWithoutPNR to Common

        //public FareRevalidateRS fareInformativeBestPricingWithoutPNRtoCom_1(string amresult, int adt, int chd, int inf, FareRevalidateRQ FareRevalidateRQCom)
        //{
        //    var response = JsonConvert.DeserializeObject<FareRevalidateAmRS>(amresult);

        //    //pax price start
        //    string adtamount = 0.ToString();
        //    string adttax = 0.ToString();
        //    string chdamount = 0.ToString();
        //    string chdttax = 0.ToString();
        //    string infamount = 0.ToString();
        //    string inftax = 0.ToString();

        //    if (adt > 0 && chd > 0 && inf > 0)
        //    {
        //        int length = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails.Length;
        //        if (length >= 2)
        //        {
        //            adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
        //            adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;

        //            chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
        //            chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;

        //            infamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[2].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
        //            inftax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[2].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //        }
        //        else
        //        {
        //            adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //            adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.monetaryDetails.amount;

        //            chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //            chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.monetaryDetails.amount;

        //            infamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[2].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //            inftax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[2].fareInfoGroup.fareAmount.monetaryDetails.amount;
        //        }
        //    }
        //    else if (adt > 0 && chd > 0)
        //    {
        //        int length = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails.Length;
        //        if (length >= 2)
        //        {
        //            adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
        //            adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;

        //            chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
        //            chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //        }
        //        else
        //        {
        //            adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //            adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.monetaryDetails.amount;

        //            chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //            chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.monetaryDetails.amount;
        //        }
        //    }
        //    else if (adt > 0 && inf > 0)
        //    {
        //        int length = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails.Length;
        //        if (length >= 2)
        //        {
        //            adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
        //            adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;

        //            infamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
        //            inftax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //        }

        //        else
        //        {
        //            adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //            adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.monetaryDetails.amount;

        //            infamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //            inftax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.monetaryDetails.amount;
        //        }
        //    }
        //    else if (adt > 0)
        //    {
        //        int length = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails.Length;
        //        if (length >= 2)
        //        {
        //            adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
        //            adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //        }
        //        else
        //        {
        //            adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //            adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.monetaryDetails.amount;
        //        }
        //    }
        //    else if (chd > 0)
        //    {
        //        var length = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails.Length;
        //        if (length >= 2)
        //        {
        //            chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
        //            chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //        }
        //        else
        //        {
        //            chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //            chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.monetaryDetails.amount;
        //        }
        //    }

        //    Double adttotal = Convert.ToDouble(adtamount) * Convert.ToDouble(adt);
        //    Double adttaxtotal = Convert.ToDouble(adttax) * Convert.ToDouble(adt);
        //    Double basefareadt = adttotal - adttaxtotal;

        //    Double chdtotal = Convert.ToDouble(chdamount) * Convert.ToDouble(chd);
        //    Double chdtaxtotal = Convert.ToDouble(chdttax) * Convert.ToDouble(chd);
        //    Double basefarechd = chdtotal - chdtaxtotal;

        //    Double inftotal = Convert.ToDouble(infamount) * Convert.ToDouble(inf);
        //    Double inftaxtotal = Convert.ToDouble(inftax) * Convert.ToDouble(inf);
        //    Double basefareinf = inftotal - inftaxtotal;
        //    //pax price end

        //    int totalpaxunits = 0;
        //    if (adt > 0)
        //    {
        //        totalpaxunits++;
        //    }
        //    if (chd > 0)
        //    {
        //        totalpaxunits++;
        //    }
        //    if (inf > 0)
        //    {
        //        totalpaxunits++;
        //    }
        //    //int totalpaxunits = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup.Length;

        //    int totalpax = adt + chd + inf;
        //    double totalfare = adttotal + chdtotal + inftotal;
        //    double basefare = basefareadt + basefarechd + basefareinf;
        //    double taxfare = adttaxtotal + chdtaxtotal + inftaxtotal;

        //    FareRevalidateRS fareRevalidateRS = new FareRevalidateRS();
        //    Fareinformationwithoutpnrreply fareinformationwithoutpnrreply = new Fareinformationwithoutpnrreply()
        //    {
        //        Fsupplier = "AMA001"
        //    };

        //    Airfarerule[] _Airfarerules = new Airfarerule[0];
        //    fareinformationwithoutpnrreply.AirFareRule = _Airfarerules;
        //    //FlightFareDetails
        //    Flightfaredetails flightdetail = new Flightfaredetails()
        //    {
        //        TotalPax = totalpax.ToString(),
        //        TotalFare = totalfare.ToString(),
        //        Basefare = basefare.ToString(),
        //        TaxFare = taxfare.ToString(),
        //        currency = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].currency
        //    };
        //    BusinessEntities.surchargesGroup surchargesgroup = new BusinessEntities.surchargesGroup()
        //    {
        //        Amount = "NA",
        //        countryCode = "NA"

        //    };
        //    flightdetail.SurchargesGroup = surchargesgroup;
        //    fareinformationwithoutpnrreply.FlightFareDetails = flightdetail;

        //    //PaxFareDetails
        //    BusinessEntities.Paxfaredetail paxfaredetail = new BusinessEntities.Paxfaredetail();
        //    //Fareinformation fareinformation = new Fareinformation();
        //    BusinessEntities.Paxfaredetail[] paxfaredetails = new BusinessEntities.Paxfaredetail[totalpaxunits];
        //    BusinessEntities.Surcharges surcharges = new BusinessEntities.Surcharges();
        //    for (int FLpaxes = 0; FLpaxes < totalpaxunits; FLpaxes++)
        //    {
        //        int totalpaxbyunit = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes].fareInfoGroup.segmentLevelGroup[0].ptcSegment.quantityDetails.numberOfUnit;
        //        //string totaltype = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes].fareInfoGroup.segmentLevelGroup[0].ptcSegment.quantityDetails.unitQualifier;

        //        //string totalpaxbyunit = response.PricedItineraries[0].AirItineraryPricingInfo.PTC_FareBreakdowns[FLpaxes].PassengerTypeQuantity.Quantity.ToString();
        //        //string totaltype = response.PricedItineraries[0].AirItineraryPricingInfo.PTC_FareBreakdowns[FLpaxes].PassengerTypeQuantity.Code.ToString();

        //        string totalpaxfare = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes].fareInfoGroup.fareAmount.monetaryDetails.amount;
        //        string paxcurrencycode = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes].fareInfoGroup.fareAmount.monetaryDetails.currency;
        //        //string taxpaxfare = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes].fareInfoGroup.fareAmount.monetaryDetails.amount;

        //        string baseplustax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
        //        double taxamount = Convert.ToDouble(baseplustax) - Convert.ToDouble(totalpaxfare);
        //        string taxamounts = taxamount.ToString();

        //        string PaxType = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes].fareInfoGroup.segmentLevelGroup[0].ptcSegment.quantityDetails.unitQualifier;
        //        if (PaxType == "AD")
        //        {
        //            PaxType = "ADT";
        //        }
        //        else if (PaxType == "CH")
        //        {
        //            PaxType = "CHD";
        //        }
        //        else if (PaxType == "IN")
        //        {
        //            PaxType = "INF";
        //        }

        //        paxfaredetail = new BusinessEntities.Paxfaredetail()
        //        {
        //            TotalFare = baseplustax,
        //            Basefare = totalpaxfare,
        //            TaxFare = taxamounts,
        //            currency = paxcurrencycode,
        //            TotalPax = totalpax.ToString(),
        //            TotalPaxUnites = totalpaxbyunit.ToString(),
        //            PaxType = PaxType
        //        };


        //        surcharges = new BusinessEntities.Surcharges()
        //        {
        //            Amount = "NA",
        //            countryCode = "NA"
        //        };
        //        paxfaredetail.surcharges = surcharges;
        //        //paxfaredetail.FareInformation = fareinformation;
        //        paxfaredetails[FLpaxes] = paxfaredetail;
        //    }
        //    fareinformationwithoutpnrreply.PaxFareDetails = paxfaredetails;

        //    //AirSegments
        //    int totalsegments = Convert.ToInt16(response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup.Length);
        //    Airsegment[] airsegments = new Airsegment[totalsegments];
        //    int segflag = 0;

        //    Airsegment airsegment = new Airsegment();
        //    Seginfo seginfo = new Seginfo();
        //    Fldata fldata = new Fldata();
        //    Dateandtimedetails dateandtimedetails = new Dateandtimedetails();
        //    BusinessEntities.Cabingroup cabingroup = new BusinessEntities.Cabingroup();
        //    BusinessEntities.Baggageinformation[] Baggageinformationarr = new BusinessEntities.Baggageinformation[1];
        //    BusinessEntities.Baggageinformation Baggageinformation = new BusinessEntities.Baggageinformation();

        //    //leg
        //    int FLseg = 0;
        //    int legcount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareComponentDetailsGroup.Length;
        //    for (int legl = 0; legl < legcount; legl++)
        //    {
        //        int legsegcount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareComponentDetailsGroup[legl].couponDetailsGroup.Length;
        //        string legfrom = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareComponentDetailsGroup[legl].marketFareComponent.boardPointDetails.trueLocationId;
        //        string legto = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareComponentDetailsGroup[legl].marketFareComponent.offpointDetails.trueLocationId;
        //        for (int lsegl = 0; lsegl < legsegcount; lsegl++)
        //        {
        //            airsegment = new Airsegment();
        //            seginfo = new Seginfo();
        //            Baggageinformation = new Baggageinformation();
        //            fldata = new Fldata()
        //            {
        //                BoardPoint = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].segmentInformation.boardPointDetails.trueLocationId,
        //                BoardTerminal = "NA",
        //                Offpoint = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].segmentInformation.offpointDetails.trueLocationId,
        //                OffTerminal = "NA",
        //                MarketingCompany = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].segmentInformation.companyDetails.marketingCompany,
        //                OperatingCompany = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].segmentInformation.companyDetails.marketingCompany,
        //                FlightNumber = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].segmentInformation.flightIdentification.flightNumber,
        //                BookingClass = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].segmentInformation.flightIdentification.bookingClass,
        //                Equiptype = "NA",
        //                durationtime = "NA"
        //            };
        //            seginfo.FLData = fldata;
        //            dateandtimedetails = new Dateandtimedetails()
        //            {
        //                DepartureDate = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].segmentInformation.flightDate.departureDate,
        //                DepartureTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[FLseg].segmentInformation.flightDate.departureTime,
        //                //ArrivalDate = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].additionalInformation.productDateTimeDetails.arrivalDate,
        //                ArrivalDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[FLseg].segmentInformation.flightDate.arrivalDate,
        //                ArrivalTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[FLseg].segmentInformation.flightDate.arrivalTime
        //            };
        //            seginfo.DateandTimeDetails = dateandtimedetails;
        //            airsegment.SegDirectionID = legl;
        //            airsegment.OriginCity = legfrom;
        //            airsegment.DepartureCity = legto;
        //            airsegment.FareSourceCode = "NA";
        //            cabingroup = new BusinessEntities.Cabingroup()
        //            {
        //                designator = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].cabinGroup[0].cabinSegment.bookingClassDetails[0].designator
        //            };
        //            airsegment.CabinGroup = cabingroup;
        //            Baggageinformation = new BusinessEntities.Baggageinformation()
        //            {
        //                Baggage = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].baggageAllowance.baggageDetails.freeAllowance.ToString(),
        //                BagPaxType = "ADT",
        //                Unit = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].baggageAllowance.baggageDetails.quantityCode
        //                //FreeAllowance = ,
        //                //QuantityCode = 
        //            };
        //            Baggageinformationarr[0] = Baggageinformation;
        //            airsegment.Baggageinformations = Baggageinformationarr;
        //            airsegment.SegInfo = seginfo;
        //            airsegments[segflag] = airsegment;
        //            segflag++;
        //            FLseg++;
        //        }
        //    }

        //    //for (int FLseg = 0; FLseg < totalsegments; FLseg++)
        //    //{

        //    //}

        //    //Segments end here
        //    fareinformationwithoutpnrreply.AirSegments = airsegments;
        //    fareRevalidateRS.FareInformationWithoutPNRReply = fareinformationwithoutpnrreply;

        //    return fareRevalidateRS;
        //}

        //2.1 Rules



        public FareRevalidateRS FareInformativeBestPricingWithoutPNRtoCom(string amresult, string fareRS, int adt1, int chd1, int inf1, FareRevalidateRQ FareRevalidateRQCom)
        {

            #region-----testresponse---

            var bestPricingRs = BestPricingRs.FromJson(amresult);
            var farecheckRS = FarecheckRs.FromJson(fareRS);
            //var bestPricingRs = JsonConvert.DeserializeObject<BestPricingRs>(amresult);

            //  RevalidateEntity revalidateEntity = new RevalidateEntity();

            string adtamount1 = 0.ToString();
            string adttax1 = 0.ToString();
            string chdamount1 = 0.ToString();
            string chdttax1 = 0.ToString();
            string infamount1 = 0.ToString();
            string inftax1 = 0.ToString();
            string PaxType = "";


            int length = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails.Count();

            if (adt1 > 0 && chd1 > 0 && inf1 > 0)
            {

                if (length >= 2)
                {
                    adtamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    adttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;

                    chdamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    chdttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;

                    infamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[2].FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    inftax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[2].FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                }
                else
                {


                    adtamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                    adttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.MonetaryDetails.Amount;

                    chdamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                    chdttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.MonetaryDetails.Amount;

                    infamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[2].FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                    inftax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[2].FareInfoGroup.FareAmount.MonetaryDetails.Amount;
                }

            }
            else if (adt1 > 0 && chd1 > 0)
            {
                if (length >= 2)
                {

                    adtamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    adttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;

                    chdamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    chdttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                }

                else
                {


                    adtamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                    adttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.MonetaryDetails.Amount;

                    chdamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                    chdttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.MonetaryDetails.Amount;

                }

            }


            else if (adt1 > 0 && inf1 > 0)
            {
                if (length >= 2)
                {
                    adtamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    adttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;

                    infamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    inftax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;

                }
                else
                {
                    adtamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                    adttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.MonetaryDetails.Amount;

                    infamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    inftax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                }
            }

            if (adt1 > 0)
            {
                if (length >= 2)
                {
                    adtamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    adttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails[0].Amount;

                }
                else
                {
                    adtamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                    adttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareAmount.MonetaryDetails.Amount;
                }
            }

            if (chd1 > 0)
            {
                if (length >= 2)
                {
                    chdamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    chdttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                }
                else
                {
                    chdamount1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.OtherMonetaryDetails.First().Amount;
                    chdttax1 = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[1].FareInfoGroup.FareAmount.MonetaryDetails.Amount;
                }
            }


            Double adttotal1 = Convert.ToDouble(adtamount1) * Convert.ToDouble(adt1);
            Double basefareadt1 = Convert.ToDouble(adttax1) * Convert.ToDouble(adt1);
            Double adttaxtotal1 = adttotal1 - basefareadt1;

            Double chdtotal1 = Convert.ToDouble(chdamount1) * Convert.ToDouble(chd1);
            Double basefarechd1 = Convert.ToDouble(chdttax1) * Convert.ToDouble(chd1);
            Double chdtaxtotal1 = chdtotal1 - basefarechd1;

            Double inftotal1 = Convert.ToDouble(infamount1) * Convert.ToDouble(inf1);
            Double basefareinf1 = Convert.ToDouble(inftax1) * Convert.ToDouble(inf1);
            Double inftaxtotal1 = inftotal1 - basefareinf1;

            //paxprice end

            int totalpaxunits = 0;
            if (adt1 > 0)
            {
                totalpaxunits++;
            }
            if (chd1 > 0)
            {
                totalpaxunits++;
            }
            if (inf1 > 0)
            {
                totalpaxunits++;
            }


            int totalpax = adt1 + chd1 + inf1;
            double totalfare = adttotal1 + chdtotal1 + inftotal1;
            double basefare = basefareadt1 + basefarechd1 + basefareinf1;
            double taxfare = adttaxtotal1 + chdtaxtotal1 + inftaxtotal1;

            FareRevalidateRS fareRevalidateRs = new FareRevalidateRS();
            fareRevalidateRs.FareInformationWithoutPnrReply.FSupplier = "AMA001";

            FlightFareDetails flightFareDetails = fareRevalidateRs.FareInformationWithoutPnrReply.FlightFareDetails;

            flightFareDetails.TotalPax = totalpax.ToString();
            flightFareDetails.TotalFare = totalfare.ToString();
            flightFareDetails.Basefare = basefare.ToString();
            flightFareDetails.TaxFare = taxfare.ToString();
            flightFareDetails.Currency = CurrencyEnum.AED.ToString();
            fareRevalidateRs.FareInformationWithoutPnrReply.SupplierOGPrice = totalfare.ToString();
            var _Airfarerules = fareRevalidateRs.FareInformationWithoutPnrReply.AirFareRule;

            BusinessEntities.Surcharges surchargesGroup = fareRevalidateRs.FareInformationWithoutPnrReply.FlightFareDetails.SurchargesGroup;
            {
                surchargesGroup.Amount = "" ;
                surchargesGroup.CountryCode = "";
            }

            flightFareDetails.SurchargesGroup = surchargesGroup;
            var paxfaredetail1 = fareRevalidateRs.FareInformationWithoutPnrReply.PaxFareDetails;

            for (int FLpaxes = 0; FLpaxes < totalpaxunits; FLpaxes++)
            {
                int totalpaxbyunit = Convert.ToInt16(bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[FLpaxes].FareInfoGroup.SegmentLevelGroup.First().PtcSegment.QuantityDetails.NumberOfUnit);
                string totalpaxfare = "";
                string paxcurrencycode = "";
                string baseplustax = "";
                if (length >= 2)
                {
                    totalpaxfare = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[FLpaxes].FareInfoGroup.FareAmount.OtherMonetaryDetails[1].Amount;
                    paxcurrencycode = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[FLpaxes].FareInfoGroup.FareAmount.OtherMonetaryDetails[0].Currency;
                    //string taxpaxfare = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes].fareInfoGroup.fareAmount.monetaryDetails.amount;
                }

                else
                {

                    totalpaxfare = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[FLpaxes].FareInfoGroup.FareAmount.OtherMonetaryDetails[0].Amount;
                    paxcurrencycode = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[FLpaxes].FareInfoGroup.FareAmount.OtherMonetaryDetails[0].Currency;
                }


                if (length >= 2)
                {
                    baseplustax = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[FLpaxes].FareInfoGroup.FareAmount.OtherMonetaryDetails[0].Amount;
                }

                else
                {
                    baseplustax = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[FLpaxes].FareInfoGroup.FareAmount.MonetaryDetails.Amount;
                }
                double taxamount = Convert.ToDouble(totalpaxfare) - Convert.ToDouble(baseplustax);
                string taxamounts = taxamount.ToString();

                PaxType = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[FLpaxes].FareInfoGroup.SegmentLevelGroup[0].PtcSegment.QuantityDetails.UnitQualifier;
                int totalpax1 = 0;


                //if (PaxType == "AD")
                //{
                //    PaxType = PaxTypeEnum.ADT.ToString();
                //    totalpax1++;
                //}
                //else if (PaxType == "CH")
                //{
                //    PaxType = PaxTypeEnum.CHD.ToString();
                //    totalpax1++;
                //}
                //else if (PaxType == "IN")
                //{
                //    PaxType = PaxTypeEnum.INF.ToString();
                //    totalpax1++;
                //}

                switch (PaxType)
                {
                    case "AD":
                        PaxType = PaxTypeEnum.ADT.ToString();
                        totalpax1++;
                        break;

                    case "CH":
                        PaxType = PaxTypeEnum.CHD.ToString();
                        totalpax1++;
                        break;

                    case "IN":
                        PaxType = PaxTypeEnum.INF.ToString();
                        totalpax1++;
                        break;


                }


                paxfaredetail1.Add(new BusinessEntities.PaxFareDetail()
                {
                    TotalFare = totalpaxfare,
                    Basefare = baseplustax,
                    TaxFare = taxamounts,
                    Currency = paxcurrencycode,
                    TotalPax = totalpax1.ToString(),
                    TotalPaxUnites = totalpaxbyunit.ToString(),
                    PaxType = PaxType,
                    Surcharges = surchargesGroup

                });
            }

            //rule section start

            var datafarerule = "";
            var fullFareData = "";
            var category = "";
            var ordata = "";
            var fdetailrule = new List<FareRuleDetail>();
            if (farecheckRS.FareCheckRulesReply.ErrorInfo == null)
            {
                var airline = farecheckRS.FareCheckRulesReply.FlightDetails.First().TransportService.First().CompanyIdentification.MarketingCompany;
                var faresegfrom = farecheckRS.FareCheckRulesReply.FlightDetails.First().OdiGrp.First().OriginDestination.Destination;
                var faresegto = farecheckRS.FareCheckRulesReply.FlightDetails.First().OdiGrp.First().OriginDestination.Origin;

                var errors = "";
                if (farecheckRS.FareCheckRulesReply.TariffInfo == null)
                {
                    fullFareData = "";
                    fullFareData = "Farerules information not available";

                }

                else
                {

                    datafarerule = "";
                    int i = 0;

                    farecheckRS.FareCheckRulesReply.TariffInfo.ForEach(rcount =>
                    {
                        category = farecheckRS.FareCheckRulesReply.TariffInfo[i].FareRuleText[0].FreeText[0].ToString();
                        int j = 1;

                        farecheckRS.FareCheckRulesReply.TariffInfo[i].FareRuleText.ForEach(frule =>
                        {
                            if (j < farecheckRS.FareCheckRulesReply.TariffInfo[i].FareRuleText.Count())
                            {


                                if (farecheckRS.FareCheckRulesReply.TariffInfo[i].FareRuleText[j].FreeText[0] != "")
                                {

                                    ordata = farecheckRS.FareCheckRulesReply.TariffInfo[i].FareRuleText[j].FreeText[0];


                                    datafarerule += ordata;
                                    datafarerule += ' ';
                                }

                                j++;
                            }
                        });
                        // datafarerule += '</p>';


                        i++;



                        fdetailrule.Add(new FareRuleDetail()
                        {
                            Rulehead = category,
                            RuleBody = datafarerule
                        });



                    });


                    _Airfarerules.Add(new AirFareRule()
                    {
                        Airline = airline,
                        Departure = faresegfrom,
                        Arrival = faresegto,
                        FareRuleDetails = fdetailrule




                    });

                }
            }
            else
            {
                List<AirFareRule> airFareRuleList = new List<AirFareRule>();
                _Airfarerules = airFareRuleList;


            }
            //Rulesection end


            //var bag = new List<BaggageInformation>();
            int totalsegments = Convert.ToInt16(bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.SegmentLevelGroup.Count);
            var airsegments = fareRevalidateRs.FareInformationWithoutPnrReply.AirSegments;

            FlData fldata = new FlData();
            DateandTimeDetails dt = new DateandTimeDetails();
            BusinessEntities.CabinGroup cabin = new BusinessEntities.CabinGroup();


            int FLseg = 0;

            int legcount = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup.First().FareInfoGroup.FareComponentDetailsGroup.Count();

            //int legl = 0;
            for (int legl = 0; legl < legcount; legl++)

            {
                int bag1 = 0;


                string legfrom = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.FareComponentDetailsGroup[legl].MarketFareComponent.BoardPointDetails.TrueLocationId;
                string legto = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.FareComponentDetailsGroup[legl].MarketFareComponent.OffPointDetails.TrueLocationId;
                int legsegcount = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.FareComponentDetailsGroup[legl].CouponDetailsGroup.Count();
                //  int lseg1 = 0;

                // bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.FareComponentDetailsGroup[legl].CouponDetailsGroup.ForEach(leg =>
                SegInfo seginfo = new SegInfo();

                for (int lsegl = 0; lsegl < legsegcount; lsegl++)
                {
                    var bag = new List<BaggageInformation>();
                    seginfo = new SegInfo();
                    fldata = new FlData();
                    dt = new DateandTimeDetails();
                    cabin = new BusinessEntities.CabinGroup();

                    fldata.BoardPoint = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.SegmentLevelGroup[FLseg].SegmentInformation.BoardPointDetails.TrueLocationId;
                    fldata.BoardTerminal = "";
                    fldata.Offpoint = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.SegmentLevelGroup[FLseg].SegmentInformation.OffpointDetails.TrueLocationId;
                    fldata.OffTerminal = "";
                    fldata.MarketingCompany = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.SegmentLevelGroup[FLseg].SegmentInformation.CompanyDetails.MarketingCompany;
                    fldata.OperatingCompany = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.SegmentLevelGroup[FLseg].SegmentInformation.CompanyDetails.MarketingCompany;
                    fldata.FlightNumber = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.SegmentLevelGroup[FLseg].SegmentInformation.FlightIdentification.FlightNumber;
                    fldata.BookingClass = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.SegmentLevelGroup[FLseg].SegmentInformation.FlightIdentification.BookingClass;
                    fldata.Equiptype = "";
                    fldata.Durationtime ="";



                    seginfo.FlData = fldata;
                    //to be


                    dt.DepartureDate = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.SegmentLevelGroup[FLseg].SegmentInformation.FlightDate.DepartureDate;
                    dt.DepartureTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[FLseg].SegmentInformation.FlightDate.DepartureTime;
                    //ArrivalDate = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].additionalInformation.productDateTimeDetails.arrivalDate,
                    dt.ArrivalDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[FLseg].SegmentInformation.FlightDate.ArrivalDate;
                    dt.ArrivalTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.SegmentGroup[FLseg].SegmentInformation.FlightDate.ArrivalTime;

                    seginfo.DateandTimeDetails = dt;

                    cabin.Designator = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[0].FareInfoGroup.SegmentLevelGroup[FLseg].CabinGroup[0].CabinSegment.BookingClassDetails[0].Designator;
                    //for (int FLpaxes = 0; FLpaxes < totalpaxunits; FLpaxes++)
                    //{

                    PaxType = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[bag1].FareInfoGroup.SegmentLevelGroup[0].PtcSegment.QuantityDetails.UnitQualifier;
                    switch (PaxType)
                    {
                        case "AD":
                            PaxType = PaxTypeEnum.ADT.ToString();
                            break;

                        case "CH":
                            PaxType = PaxTypeEnum.CHD.ToString();
                            break;

                        case "IN":
                            PaxType = PaxTypeEnum.INF.ToString();
                            break;


                    }
                    var baggage = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[bag1].FareInfoGroup.SegmentLevelGroup[FLseg].BaggageAllowance.BaggageDetails.FreeAllowance;
                    var unit = bestPricingRs.FareInformativeBestPricingWithoutPnrReply.MainGroup.PricingGroupLevelGroup[bag1].FareInfoGroup.SegmentLevelGroup[FLseg].BaggageAllowance.BaggageDetails.QuantityCode;

                    bag1++;
                    bag.Add(new BaggageInformation()
                    {

                        Baggage = baggage,
                        BagPaxType = PaxType,
                        Unit = unit

                    });

                    //  bag1++;

                    //}
                    airsegments.Add(new AirSegment()
                    {
                        SegInfo = seginfo,
                        SegDirectionId = legl,
                        OriginCity = legfrom,
                        DepartureCity = legto,
                        FareSourceCode = "",
                        CabinGroup = cabin,
                        BaggageInformations = bag


                    });
                    FLseg++;
                }
            }



            #endregion


            #region------old_code-----
            ////oldcode

            //var response = JsonConvert.DeserializeObject<FareRevalidateAmRS>(amresult);

            ////pax price start
            //string adtamount = 0.ToString();
            //string adttax = 0.ToString();
            //string chdamount = 0.ToString();
            //string chdttax = 0.ToString();
            //string infamount = 0.ToString();
            //string inftax = 0.ToString();

            //if (adt1 > 0 && chd1 > 0 && inf1 > 0)
            //{
            //    int length1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails.Length;
            //    if (length1 >= 2)
            //    {
            //        adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
            //        adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;

            //        chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
            //        chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;

            //        infamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[2].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
            //        inftax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[2].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //    }
            //    else
            //    {
            //        adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //        adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.monetaryDetails.amount;

            //        chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //        chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.monetaryDetails.amount;

            //        infamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[2].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //        inftax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[2].fareInfoGroup.fareAmount.monetaryDetails.amount;
            //    }
            //}
            //else if (adt1 > 0 && chd1 > 0)
            //{
            //    int length1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails.Length;
            //    if (length1 >= 2)
            //    {
            //        adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
            //        adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;

            //        chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
            //        chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //    }
            //    else
            //    {
            //        adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //        adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.monetaryDetails.amount;

            //        chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //        chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.monetaryDetails.amount;
            //    }
            //}
            //else if (adt1 > 0 && inf1 > 0)
            //{
            //    int length1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails.Length;
            //    if (length1 >= 2)
            //    {
            //        adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
            //        adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;

            //        infamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
            //        inftax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //    }

            //    else
            //    {
            //        adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //        adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.monetaryDetails.amount;

            //        infamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //        inftax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[1].fareInfoGroup.fareAmount.monetaryDetails.amount;
            //    }
            //}
            //else if (adt1 > 0)
            //{
            //    int length1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails.Length;
            //    if (length1 >= 2)
            //    {
            //        adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
            //        adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //    }
            //    else
            //    {
            //        adtamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //        adttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.monetaryDetails.amount;
            //    }
            //}
            //else if (chd1 > 0)
            //{
            //    var length1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails.Length;
            //    if (length1 >= 2)
            //    {
            //        chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[1].amount;
            //        chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //    }
            //    else
            //    {
            //        chdamount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //        chdttax = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.monetaryDetails.amount;
            //    }
            //}

            //Double adttotal = Convert.ToDouble(adtamount) * Convert.ToDouble(adt1);
            //Double adttaxtotal = Convert.ToDouble(adttax) * Convert.ToDouble(adt1);
            //Double basefareadt = adttotal - adttaxtotal;

            //Double chdtotal = Convert.ToDouble(chdamount) * Convert.ToDouble(chd1);
            //Double chdtaxtotal = Convert.ToDouble(chdttax) * Convert.ToDouble(chd1);
            //Double basefarechd = chdtotal - chdtaxtotal;

            //Double inftotal = Convert.ToDouble(infamount) * Convert.ToDouble(inf1);
            //Double inftaxtotal = Convert.ToDouble(inftax) * Convert.ToDouble(inf1);
            //Double basefareinf = inftotal - inftaxtotal;
            ////pax price end

            //int totalpaxunits1 = 0;
            //if (adt1 > 0)
            //{
            //    totalpaxunits1++;
            //}
            //if (chd1 > 0)
            //{
            //    totalpaxunits1++;
            //}
            //if (inf1 > 0)
            //{
            //    totalpaxunits1++;
            //}
            ////int totalpaxunits = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup.Length;

            //int totalpax2 = adt1 + chd1 + inf1;
            //double totalfare1 = adttotal + chdtotal + inftotal;
            //double basefare1 = basefareadt + basefarechd + basefareinf;
            //double taxfare1 = adttaxtotal + chdtaxtotal + inftaxtotal;

            //FareRevalidateRS fareRevalidateRS = new FareRevalidateRS();
            //Fareinformationwithoutpnrreply fareinformationwithoutpnrreply = new Fareinformationwithoutpnrreply()
            //{
            //    Fsupplier = "AMA001"
            //};

            //Airfarerule[] _Airfarerules1 = new Airfarerule[0];
            //fareinformationwithoutpnrreply.AirFareRule = _Airfarerules1;
            ////FlightFareDetails
            //Flightfaredetails flightdetail = new Flightfaredetails()
            //{
            //    TotalPax = totalpax2.ToString(),
            //    TotalFare = totalfare1.ToString(),
            //    Basefare = basefare1.ToString(),
            //    TaxFare = taxfare1.ToString(),
            //    currency = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareAmount.otherMonetaryDetails[0].currency
            //};
            //BusinessEntities.surchargesGroup surchargesgroup = new BusinessEntities.surchargesGroup()
            //{
            //    Amount = "NA",
            //    countryCode = "NA"

            //};
            //flightdetail.SurchargesGroup = surchargesgroup;
            //fareinformationwithoutpnrreply.FlightFareDetails = flightdetail;

            ////PaxFareDetails
            //BusinessEntities.Paxfaredetail paxfaredetail = new BusinessEntities.Paxfaredetail();
            ////Fareinformation fareinformation = new Fareinformation();
            //BusinessEntities.Paxfaredetail[] paxfaredetails = new BusinessEntities.Paxfaredetail[totalpaxunits1];
            //BusinessEntities.Surcharges surcharges = new BusinessEntities.Surcharges();



            //for (int FLpaxes1 = 0; FLpaxes1 < totalpaxunits1; FLpaxes1++)
            //{
            //    int totalpaxbyunit1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes1].fareInfoGroup.segmentLevelGroup[0].ptcSegment.quantityDetails.numberOfUnit;
            //    //string totaltype = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes].fareInfoGroup.segmentLevelGroup[0].ptcSegment.quantityDetails.unitQualifier;

            //    //string totalpaxbyunit = response.PricedItineraries[0].AirItineraryPricingInfo.PTC_FareBreakdowns[FLpaxes].PassengerTypeQuantity.Quantity.ToString();
            //    //string totaltype = response.PricedItineraries[0].AirItineraryPricingInfo.PTC_FareBreakdowns[FLpaxes].PassengerTypeQuantity.Code.ToString();

            //    string totalpaxfare1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes1].fareInfoGroup.fareAmount.monetaryDetails.amount;
            //    string paxcurrencycode1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes1].fareInfoGroup.fareAmount.monetaryDetails.currency;
            //    //string taxpaxfare = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes].fareInfoGroup.fareAmount.monetaryDetails.amount;

            //    string baseplustax1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes1].fareInfoGroup.fareAmount.otherMonetaryDetails[0].amount;
            //    double taxamount1 = Convert.ToDouble(baseplustax1) - Convert.ToDouble(totalpaxfare1);
            //    string taxamounts1 = taxamount1.ToString();

            //    string PaxType1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[FLpaxes1].fareInfoGroup.segmentLevelGroup[0].ptcSegment.quantityDetails.unitQualifier;
            //    if (PaxType1 == "AD")
            //    {
            //        PaxType1 = "ADT";
            //    }
            //    else if (PaxType1 == "CH")
            //    {
            //        PaxType1 = "CHD";
            //    }
            //    else if (PaxType1 == "IN")
            //    {
            //        PaxType1 = "INF";
            //    }

            //    paxfaredetail = new BusinessEntities.Paxfaredetail()
            //    {
            //        TotalFare = baseplustax1,
            //        Basefare = totalpaxfare1,
            //        TaxFare = taxamounts1,
            //        currency = paxcurrencycode1,
            //        TotalPax = totalpax2.ToString(),
            //        TotalPaxUnites = totalpaxbyunit1.ToString(),
            //        PaxType = PaxType1
            //    };


            //    surcharges = new BusinessEntities.Surcharges()
            //    {
            //        Amount = "NA",
            //        countryCode = "NA"
            //    };
            //    paxfaredetail.surcharges = surcharges;
            //    //paxfaredetail.FareInformation = fareinformation;
            //    paxfaredetails[FLpaxes1] = paxfaredetail;
            //}
            //fareinformationwithoutpnrreply.PaxFareDetails = paxfaredetails;

            ////AirSegments
            //int totalsegments1 = Convert.ToInt16(response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup.Length);
            //Airsegment[] airsegments1 = new Airsegment[totalsegments1];
            //int segflag = 0;

            //Airsegment airsegment = new Airsegment();
            //Seginfo seginfo1 = new Seginfo();
            //Fldata fldata1 = new Fldata();
            //Dateandtimedetails dateandtimedetails = new Dateandtimedetails();
            //BusinessEntities.Cabingroup cabingroup = new BusinessEntities.Cabingroup();
            //BusinessEntities.Baggageinformation[] Baggageinformationarr = new BusinessEntities.Baggageinformation[1];
            //BusinessEntities.Baggageinformation Baggageinformation = new BusinessEntities.Baggageinformation();

            //////leg
            //int FLseg1 = 0;
            //int legcount1 = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareComponentDetailsGroup.Length;
            //for (int legl1 = 0; legl1 < legcount1; legl1++)
            //{
            //    int legsegcount = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareComponentDetailsGroup[legl1].couponDetailsGroup.Length;
            //    string legfrom = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareComponentDetailsGroup[legl1].marketFareComponent.boardPointDetails.trueLocationId;
            //    string legto = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.fareComponentDetailsGroup[legl1].marketFareComponent.offpointDetails.trueLocationId;
            //    for (int lsegl = 0; lsegl < legsegcount; lsegl++)
            //    {
            //        airsegment = new Airsegment();
            //        seginfo1 = new Seginfo();
            //        Baggageinformation = new Baggageinformation();
            //        fldata1 = new Fldata()
            //        {
            //            BoardPoint = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg1].segmentInformation.boardPointDetails.trueLocationId,
            //            BoardTerminal = "NA",
            //            Offpoint = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg1].segmentInformation.offpointDetails.trueLocationId,
            //            OffTerminal = "NA",
            //            MarketingCompany = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg1].segmentInformation.companyDetails.marketingCompany,
            //            OperatingCompany = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg1].segmentInformation.companyDetails.marketingCompany,
            //            FlightNumber = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg1].segmentInformation.flightIdentification.flightNumber,
            //            BookingClass = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg1].segmentInformation.flightIdentification.bookingClass,
            //            Equiptype = "NA",
            //            durationtime = "NA"
            //        };
            //        seginfo1.FLData = fldata1;
            //        dateandtimedetails = new Dateandtimedetails()
            //        {
            //            DepartureDate = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg1].segmentInformation.flightDate.departureDate,
            //            DepartureTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[FLseg1].segmentInformation.flightDate.departureTime,
            //            //ArrivalDate = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg].additionalInformation.productDateTimeDetails.arrivalDate,
            //            ArrivalDate = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[FLseg1].segmentInformation.flightDate.arrivalDate,
            //            ArrivalTime = FareRevalidateRQCom.CommonRequestFarePricer.Body.AirRevalidate.segmentGroup[FLseg1].segmentInformation.flightDate.arrivalTime
            //        };
            //        seginfo1.DateandTimeDetails = dateandtimedetails;
            //        airsegment.SegDirectionID = legl1;
            //        airsegment.OriginCity = legfrom;
            //        airsegment.DepartureCity = legto;
            //        airsegment.FareSourceCode = "NA";
            //        cabingroup = new BusinessEntities.Cabingroup()
            //        {
            //            designator = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg1].cabinGroup[0].cabinSegment.bookingClassDetails[0].designator
            //        };
            //        airsegment.CabinGroup = cabingroup;
            //        Baggageinformation = new BusinessEntities.Baggageinformation()
            //        {
            //            Baggage = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg1].baggageAllowance.baggageDetails.freeAllowance.ToString(),
            //            BagPaxType = "ADT",
            //            Unit = response.fareInformativeBestPricingWithoutPNRReply.mainGroup.pricingGroupLevelGroup[0].fareInfoGroup.segmentLevelGroup[FLseg1].baggageAllowance.baggageDetails.quantityCode
            //            //FreeAllowance = ,
            //            //QuantityCode = 
            //        };
            //        Baggageinformationarr[0] = Baggageinformation;
            //        airsegment.Baggageinformations = Baggageinformationarr;
            //        airsegment.SegInfo = seginfo1;
            //        airsegments1[segflag] = airsegment;
            //        segflag++;
            //        FLseg1++;
            //    }
            //}

            ////for (int FLseg = 0; FLseg < totalsegments; FLseg++)
            ////{

            ////}

            ////Segments end here
            //fareinformationwithoutpnrreply.AirSegments = airsegments1;
            //fareRevalidateRS.FareInformationWithoutPNRReply = fareinformationwithoutpnrreply;
            #endregion


            string jsonData = JsonConvert.SerializeObject(fareRevalidateRs);


            return fareRevalidateRs;

        }



        public async Task<string> Farecheckrules(string pascount, FareRevalidateRQ FareRevalidateRQCom)
        {
            Farecheck farecheck = new Farecheck();
            farecheck.FareCheckRules.MsgType.MessageFunctionDetails.MessageFunction = "712";
            farecheck.FareCheckRules.ItemNumber.ItemNumberDetails.Add(new ItemNumberDetail()
            {
                Number = "1"

            });

            farecheck.FareCheckRules.FareRule.TarifFareRule.RuleSectionId.Add("AP");
            farecheck.FareCheckRules.FareRule.TarifFareRule.RuleSectionId.Add("TF");




            var jsonData = FarecheckSerialize.ToJson(farecheck);
            String InterfaceURL = "http://70.32.24.42:9000/farecheckrules";
            string response = await soapHelperAsync.SendRESTRequestAsync(jsonData, InterfaceURL, "POST");
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(farecheck);
            CommonServices.SaveLog("CHECK_RULES", FareRevalidateRQCom.CommonRequestFarePricer.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, response);
            return response;
        }

        //3. Book Flight
        public async Task<BookFlightComRS> BookFlight(FLBookRQ FLbookRQ)
        {
            string adtcount = (FLbookRQ.BookFlight.Adt);
            string chdcount = FLbookRQ.BookFlight.Chd;
            string infcount = FLbookRQ.BookFlight.Inf;
            string airSellFromRecommendationRS = await AirSellFromRecommendation(FLbookRQ);
            //JObject airSellFromRecommendationRSObj = JObject.Parse(airSellFromRecommendationRS);

            string PnrAddRS = await PnrAdd(FLbookRQ);

            string farepricePNRRS = await farepricePNR(FLbookRQ);

            //string ticketcreateRS = await ticketcreate(FLbookRQ);

            //string PNRcommitRS = await PNRcommit(FLbookRQ);

            //return BookFlighttoCom(PNRcommitRS);
            string PNRcommitRS = "";
            var response = JsonConvert.DeserializeObject<BookingClassRs>(farepricePNRRS);
            string prevFare = FLbookRQ.BookFlight.FDetails.SupplierOGPrice;
            #region--comparison---


            float crntadtfare;
            float crntadtbasefare;
            float crntchdfare;
            float crntchdbasefare;
            float crntinffare;
            float crntinfbasefare;
            float currentfare;
            float currentbasefare;

            var arraylength = response.FarePricePnrWithBookingClassReply.FareList.Count();
            var length = response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation.Count();
            var currentcurency = response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[0].FareCurrency;

            if (arraylength == 3)
            {
                if (length < 3)
                {


                    crntadtfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[1].FareAmount);
                    crntadtbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[0].FareAmount);

                    float tax = (crntadtfare) - (crntadtbasefare);
                    //paxactualfare.push(crntadtbasefare);
                    //paxaxamount.push(tax);

                    //paxbaseamount.push(crntadtbasefare);


                    crntchdfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[1].FareDataInformation.FareDataSupInformation[1].FareAmount);
                    crntchdbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[1].FareDataInformation.FareDataSupInformation[0].FareAmount);

                    var tax1 = crntchdfare - crntchdbasefare;
                    //paxactualfare.push(crntchdbasefare);
                    //paxaxamount.push(tax1);

                    //paxbaseamount.push(crntchdbasefare);

                    crntinffare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[2].FareDataInformation.FareDataSupInformation[1].FareAmount);
                    crntinfbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[2].FareDataInformation.FareDataSupInformation[0].FareAmount);

                    var tax2 = crntinffare - crntinfbasefare;
                    //paxactualfare.push(crntinfbasefare);
                    //paxaxamount.push(tax2);

                    //paxbaseamount.push(crntinfbasefare);

                    currentfare = (crntadtfare) * Convert.ToInt32(adtcount) + (crntchdfare) * Convert.ToInt32(chdcount) + (crntinffare) * Convert.ToInt32(infcount);
                    currentbasefare = (crntadtbasefare) * Convert.ToInt32(adtcount) + (crntchdbasefare) * Convert.ToInt32(chdcount) + (crntinfbasefare) * Convert.ToInt32(infcount);

                }
                else
                {
                    crntadtfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[2].FareAmount);
                    crntadtbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[1].FareAmount);

                    float tax = (crntadtfare) - (crntadtbasefare);
                    //paxactualfare.push(crntadtbasefare);
                    //paxaxamount.push(tax);

                    //paxbaseamount.push(crntadtbasefare);


                    crntchdfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[1].FareDataInformation.FareDataSupInformation[2].FareAmount);
                    crntchdbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[1].FareDataInformation.FareDataSupInformation[1].FareAmount);

                    var tax1 = crntchdfare - crntchdbasefare;
                    //paxactualfare.push(crntchdbasefare);
                    //paxaxamount.push(tax1);

                    //paxbaseamount.push(crntchdbasefare);

                    crntinffare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[2].FareDataInformation.FareDataSupInformation[2].FareAmount);
                    crntinfbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[2].FareDataInformation.FareDataSupInformation[1].FareAmount);

                    var tax2 = crntinffare - crntinfbasefare;
                    //paxactualfare.push(crntinfbasefare);
                    //paxaxamount.push(tax2);

                    //paxbaseamount.push(crntinfbasefare);

                    currentfare = (crntadtfare) * Convert.ToInt32(adtcount) + (crntchdfare) * Convert.ToInt32(chdcount) + (crntinffare) * Convert.ToInt32(infcount);
                    currentbasefare = (crntadtbasefare) * Convert.ToInt32(adtcount) + (crntchdbasefare) * Convert.ToInt32(chdcount) + (crntinfbasefare) * Convert.ToInt32(infcount);
                }
            }

            else if (arraylength == 2)
            {

                if (Convert.ToInt32(chdcount) != 0)
                {
                    if (length < 3)
                    {


                        crntadtfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[1].FareAmount);
                        crntadtbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[0].FareAmount);
                        var tax = crntadtfare - crntadtbasefare;
                        //paxactualfare.push(crntadtbasefare);
                        //paxaxamount.push(tax);

                        //paxbaseamount.push(crntadtbasefare);


                        crntchdfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[1].FareDataInformation.FareDataSupInformation[1].FareAmount);
                        crntchdbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[1].FareDataInformation.FareDataSupInformation[0].FareAmount);
                        var tax1 = crntchdfare - crntchdbasefare;

                        //paxactualfare.push(crntchdbasefare);
                        //paxaxamount.push(tax1);

                        //paxbaseamount.push(crntchdbasefare);


                        currentfare = (crntadtfare) * Convert.ToInt32(adtcount) + ((crntchdfare) * Convert.ToInt32(chdcount));
                        currentbasefare = (crntadtbasefare) * Convert.ToInt32(adtcount) + (crntchdbasefare) * Convert.ToInt32(chdcount);
                    }
                    else
                    {
                        crntadtfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[2].FareAmount);
                        crntadtbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[1].FareAmount);
                        var tax = crntadtfare - crntadtbasefare;
                        //paxactualfare.push(crntadtbasefare);
                        //paxaxamount.push(tax);

                        //paxbaseamount.push(crntadtbasefare);


                        crntchdfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[1].FareDataInformation.FareDataSupInformation[2].FareAmount);
                        crntchdbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[1].FareDataInformation.FareDataSupInformation[1].FareAmount);
                        var tax1 = crntchdfare - crntchdbasefare;

                        //paxactualfare.push(crntchdbasefare);
                        //paxaxamount.push(tax1);

                        //paxbaseamount.push(crntchdbasefare);


                        currentfare = (crntadtfare) * Convert.ToInt32(adtcount) + ((crntchdfare) * Convert.ToInt32(chdcount));
                        currentbasefare = (crntadtbasefare) * Convert.ToInt32(adtcount) + (crntchdbasefare) * Convert.ToInt32(chdcount);

                    }

                }
                else
                {
                    if (length < 3)
                    {


                        crntadtfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[1].FareAmount);
                        crntadtbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[0].FareAmount);
                        var tax = crntadtfare - crntadtbasefare;

                        //paxactualfare.push(crntadtbasefare);
                        //paxaxamount.push(tax);

                        //paxbaseamount.push(crntadtbasefare);


                        crntinffare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[2].FareDataInformation.FareDataSupInformation[1].FareAmount);
                        crntinfbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[2].FareDataInformation.FareDataSupInformation[0].FareAmount);
                        var tax1 = crntinffare - crntinfbasefare;

                        //paxactualfare.push(crntchdbasefare);
                        //paxaxamount.push(tax1);

                        //paxbaseamount.push(crntchdbasefare);
                        currentfare = (crntadtfare) * Convert.ToInt32(adtcount) + (crntinffare) * Convert.ToInt32(infcount);
                        currentbasefare = (crntadtbasefare) * Convert.ToInt32(adtcount) + (crntinfbasefare) * Convert.ToInt32(infcount);

                    }

                    else
                    {
                        crntadtfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[2].FareAmount);
                        crntadtbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[1].FareAmount);
                        var tax = crntadtfare - crntadtbasefare;

                        //paxactualfare.push(crntadtbasefare);
                        //paxaxamount.push(tax);

                        //paxbaseamount.push(crntadtbasefare);


                        crntinffare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[2].FareDataInformation.FareDataSupInformation[2].FareAmount);
                        crntinfbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[2].FareDataInformation.FareDataSupInformation[1].FareAmount);
                        var tax1 = crntinffare - crntinfbasefare;

                        //paxactualfare.push(crntchdbasefare);
                        //paxaxamount.push(tax1);

                        //paxbaseamount.push(crntchdbasefare);
                        currentfare = (crntadtfare) * Convert.ToInt32(adtcount) + (crntinffare) * Convert.ToInt32(infcount);
                        currentbasefare = (crntadtbasefare) * Convert.ToInt32(adtcount) + (crntinfbasefare) * Convert.ToInt32(infcount);
                    }


                }


            }
            else
            {
                if (length < 3)
                {

                    crntadtfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[1].FareAmount);
                    crntadtbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[0].FareAmount);
                    var tax = crntadtfare - crntadtbasefare;


                    currentfare = ((crntadtfare)) * Convert.ToInt32(adtcount);
                    currentbasefare = (crntadtbasefare) * Convert.ToInt32(adtcount);
                }
                else
                {
                    crntadtfare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[2].FareAmount);
                    crntadtbasefare = float.Parse(response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[1].FareAmount);
                    var tax = crntadtfare - crntadtbasefare;


                    currentfare = ((crntadtfare)) * Convert.ToInt32(adtcount);
                    currentbasefare = (crntadtbasefare) * Convert.ToInt32(adtcount);
                }

                //paxactualfare.push(crntadtbasefare);
                //paxaxamount.push(tax);

                //paxbaseamount.push(crntadtbasefare);


            }

            #endregion

            // string newFare = response.FarePricePnrWithBookingClassReply.FareList[0].FareDataInformation.FareDataSupInformation[1].FareAmount;
            if (float.Parse(prevFare) == currentfare)
            {
                string ticketcreateRS = await ticketcreate(FLbookRQ);
                PNRcommitRS = await PNRcommit(FLbookRQ);
            }

            return BookFlighttoCom(PNRcommitRS, currentfare.ToString());
        }




        public BookFlightComRS BookFlighttoCom(string PNRcommitRS, string updatedPrice = null)
        {

            var response = !string.IsNullOrEmpty(PNRcommitRS)
                               ? JsonConvert.DeserializeObject<PnrRetrieveAmRs>(PNRcommitRS)
                               : null;
            string controlNumber = response != null
                                       ? response.PnrReply.PnrHeader[0].ReservationInfo.Reservation[0].ControlNumber
                                       : null;
            BookFlightComRS _BookFlightComRS = new BookFlightComRS();

            BookFlightResult _Bookflightresult = new BookFlightResult();
            _Bookflightresult.Airlinepnr = controlNumber;
            _Bookflightresult.AgencyCode = "";
            _Bookflightresult.Faresourcecode = "";
            _Bookflightresult.Sessionid = "";
            _Bookflightresult.Status = string.IsNullOrEmpty(PNRcommitRS) ? "PRICECHANGED" : "";
            _Bookflightresult.Success = string.IsNullOrEmpty(PNRcommitRS) ? "False" : "True";
            _Bookflightresult.Target = "";
            _Bookflightresult.TktTimeLimit = "";
            _Bookflightresult.UniqueId = "";
            _Bookflightresult.NewPrice = string.IsNullOrEmpty(PNRcommitRS) ? updatedPrice : "";
            _Bookflightresult.NewPriceCurrencyConv = string.IsNullOrEmpty(PNRcommitRS) ? updatedPrice : "";


            _Bookflightresult.Errors.Add(new Error()
            {
                Meassage = "",
                Code = ""

            });

            _BookFlightComRS.BookFlightResult = _Bookflightresult;

            return _BookFlightComRS;
        }


        //   3.1 Book Flight

        //public string AirSellFromRecommendation_1(FLBookRQ FLbookRQ)
        //{
        //    var legcount = FLbookRQ.BookFlight.FLLegGroup.Length;
        //    int adtchd_count = Convert.ToInt32(FLbookRQ.BookFlight.ADT) + Convert.ToInt32(FLbookRQ.BookFlight.CHD);

        //    StringBuilder sbaircell = new StringBuilder();
        //    sbaircell.Append("{");
        //    sbaircell.Append("  \"airSellFromRecommendation\": {");
        //    sbaircell.Append("    \"messageActionDetails\": {");
        //    sbaircell.Append("      \"messageFunctionDetails\": {");
        //    sbaircell.Append("        \"messageFunction\": \"183\",");
        //    sbaircell.Append("        \"additionalMessageFunction\": [ \"M1\" ]");
        //    sbaircell.Append("      }");
        //    sbaircell.Append("    },");

        //    sbaircell.Append("    \"itineraryDetails\": [");
        //    for (int iair = 0; iair < legcount; iair++)
        //    {
        //        string from = FLbookRQ.BookFlight.FLLegGroup[iair].@from;
        //        string to = FLbookRQ.BookFlight.FLLegGroup[iair].to;

        //        sbaircell.Append("      {");
        //        sbaircell.Append("        \"originDestinationDetails\": {");
        //        sbaircell.Append("          \"origin\": \"" + from + "\",");
        //        sbaircell.Append("          \"destination\": \"" + to + "\"");
        //        sbaircell.Append("        },");
        //        sbaircell.Append("        \"message\": { \"messageFunctionDetails\": { \"messageFunction\": \"183\" } },");
        //        sbaircell.Append("        \"segmentInformation\": [");

        //        for (int iseg = 0; iseg < FLbookRQ.BookFlight.FLLegGroup[iair].segments.Length; iseg++)
        //        {
        //            string seg_DepartureDate = FLbookRQ.BookFlight.FLLegGroup[iair].segments[iseg].DepartureDate;
        //            seg_DepartureDate = seg_DepartureDate.Replace("-", "");
        //            seg_DepartureDate = seg_DepartureDate.Remove(4, 2);
        //            //  seg_DepartureDate = seg_DepartureDate.Replace("20", "");
        //            string seg_DepartureFrom = FLbookRQ.BookFlight.FLLegGroup[iair].segments[iseg].DepartureFrom;
        //            string seg_DepartureTo = FLbookRQ.BookFlight.FLLegGroup[iair].segments[iseg].DepartureTo;
        //            string seg_MarketingCompany = FLbookRQ.BookFlight.FLLegGroup[iair].segments[iseg].MarketingCompany;
        //            string seg_FlightNumber = FLbookRQ.BookFlight.FLLegGroup[iair].segments[iseg].FlightNumber;
        //            string seg_BookingClass = FLbookRQ.BookFlight.FLLegGroup[iair].segments[iseg].BookingClass;

        //            sbaircell.Append("          {");
        //            sbaircell.Append("            \"travelProductInformation\": {");
        //            sbaircell.Append("              \"flightDate\": { \"departureDate\": \"" + seg_DepartureDate + "\" },");
        //            sbaircell.Append("              \"boardPointDetails\": { \"trueLocationId\": \"" + seg_DepartureFrom + "\" },");
        //            sbaircell.Append("              \"offpointDetails\": { \"trueLocationId\": \"" + seg_DepartureTo + "\" },");
        //            sbaircell.Append("              \"companyDetails\": { \"marketingCompany\": \"" + seg_MarketingCompany + "\" },");
        //            sbaircell.Append("              \"flightIdentification\": {");
        //            sbaircell.Append("                \"flightNumber\": \"" + seg_FlightNumber + "\",");
        //            sbaircell.Append("                \"bookingClass\": \"" + seg_BookingClass + "\"");
        //            sbaircell.Append("              }");
        //            sbaircell.Append("            },");
        //            sbaircell.Append("            \"relatedproductInformation\": {");
        //            sbaircell.Append("              \"quantity\": " + adtchd_count + ",");
        //            sbaircell.Append("              \"statusCode\": [ \"NN\" ]");
        //            sbaircell.Append("            }");
        //            sbaircell.Append("          },");
        //        }
        //        sbaircell.Length--;
        //        sbaircell.Append("        ]");
        //        sbaircell.Append("      },");
        //    }
        //    sbaircell.Length--;
        //    sbaircell.Append("    ]");
        //    sbaircell.Append("  }");
        //    sbaircell.Append("}");

        //    //string RQUrl = FLbookRQ.BookFlight.SupplierAgencyDetailsBook[0].BaseUrl + "/SellFromRecommendation";
        //    string RQUrl = "http://70.32.24.42:9000/SellFromRecommendation";
        //    //string RQUrl = "http://192.168.1.102:9000/SellFromRecommendation";
        //    // string RQUrl = "http://192.168.1.105:9000/SellFromRecommendation";
        //    string strResult = SOAPHelper.SendRESTRequest(sbaircell.ToString(), RQUrl, "POST");


        //    return strResult;
        //}

        public async Task<string> AirSellFromRecommendation(FLBookRQ FLbookRQ)
        {
            //var legcount = FLbookRQ.BookFlight.FLLegGroup.Length;
            int adtchd_count = Convert.ToInt32(FLbookRQ.BookFlight.Adt) + Convert.ToInt32(FLbookRQ.BookFlight.Chd);

            AirSell airSell = new AirSell();
            MessageFunctionDetails messageFunctionDetails = airSell.AirSellFromRecommendation.MessageActionDetails.MessageFunctionDetails;
            messageFunctionDetails.MessageFunction = "183";
            List<string> msg = new List<string>();
            msg.Add("M1");
            messageFunctionDetails.AdditionalMessageFunction = msg;

            //var msgf= airSell.AirSellFromRecommendation.MessageActionDetails.MessageFunctionDetails.MessageFunction;

            //var msg = airSell.AirSellFromRecommendation.MessageActionDetails.MessageFunctionDetails.AdditionalMessageFunction;
            //msg.Add("M1");

            //int iair = 0;

            //OriginDestinationDetails originDestinationDetails = new OriginDestinationDetails();
            //TravelProductInformation travelProductInformation = new TravelProductInformation();
            //RelatedproductInformation relatedproductInformation = new RelatedproductInformation();
            ////var segment = new List<SegmentInformation>();            
            //var itinerary = airSell.AirSellFromRecommendation.ItineraryDetails;
            //FLbookRQ.BookFlight.FlLegGroup.ForEach(Leginfo =>
            //{
            //    originDestinationDetails = new OriginDestinationDetails();
            //    int iseg = 0;
            //    string from = FLbookRQ.BookFlight.FlLegGroup[iair].From;
            //    string to = FLbookRQ.BookFlight.FlLegGroup[iair].To;

            //    originDestinationDetails.Destination = to;
            //    originDestinationDetails.Origin = from;

            //    //segment = new List<SegmentInformation>();
            //    List<SegmentInformation1> segmentInformation = new List<SegmentInformation1>();
            //    FLbookRQ.BookFlight.FlLegGroup[iair].Segments.ForEach(seg =>
            //    {

            //        string seg_DepartureDate = FLbookRQ.BookFlight.FlLegGroup[iair].Segments[iseg].DepartureDate;
            //        seg_DepartureDate = seg_DepartureDate.Replace("-", "");
            //        seg_DepartureDate = seg_DepartureDate.Remove(4, 2);
            //        string seg_DepartureFrom = FLbookRQ.BookFlight.FlLegGroup[iair].Segments[iseg].DepartureFrom;
            //        string seg_DepartureTo = FLbookRQ.BookFlight.FlLegGroup[iair].Segments[iseg].DepartureTo;
            //        string seg_MarketingCompany = FLbookRQ.BookFlight.FlLegGroup[iair].Segments[iseg].MarketingCompany;
            //        string seg_FlightNumber = FLbookRQ.BookFlight.FlLegGroup[iair].Segments[iseg].FlightNumber;
            //        string seg_BookingClass = FLbookRQ.BookFlight.FlLegGroup[iair].Segments[iseg].BookingClass;



            //        travelProductInformation.FlightDate.DepartureDate = seg_DepartureDate;
            //        travelProductInformation.BoardPointDetails.TrueLocationId = seg_DepartureFrom;
            //        travelProductInformation.OffpointDetails.TrueLocationId = seg_DepartureTo;
            //        travelProductInformation.CompanyDetails.MarketingCompany = seg_MarketingCompany;
            //        travelProductInformation.FlightIdentification.FlightNumber = seg_FlightNumber;
            //        travelProductInformation.FlightIdentification.BookingClass = seg_BookingClass;


            //        relatedproductInformation.Quantity = adtchd_count;
            //        List<string> code = new List<string>();
            //        code.Add("NN");
            //        relatedproductInformation.StatusCode = code;

            //        iseg++;
            //        segmentInformation.Add(new SegmentInformation1()
            //        {
            //            TravelProductInformation = travelProductInformation,
            //            RelatedproductInformation = relatedproductInformation,

            //        });                 
            //    });
            //    iair++;
            //    Message message = new Message();
            //    message.MessageFunctionDetails.MessageFunction = "183";
            //    ItineraryDetail itineraryDetail = new ItineraryDetail()
            //    {
            //        Message = message,
            //        OriginDestinationDetails = originDestinationDetails,
            //        SegmentInformation = segmentInformation
            //    };
            //    itinerary.Add(itineraryDetail);


            //});


            List<ItineraryDetail> itineraryDetailList = new List<ItineraryDetail>();
            var Sq = 0;
            foreach (var item in FLbookRQ.BookFlight.FlLegGroup)
            {
                ItineraryDetail itineraryDetail = new ItineraryDetail();
                OriginDestinationDetails originDestinationDetails = new OriginDestinationDetails();
                
                     string from = item.From;
                     string to =item.To;
                     originDestinationDetails.Destination = to;
                     originDestinationDetails.Origin = from;
              
                itineraryDetail.OriginDestinationDetails = originDestinationDetails;
                Message message = new Message();
                MessageFunctionDetails messagefunctiondetails = new MessageFunctionDetails()
                {
                    MessageFunction="183"
                };
                message.MessageFunctionDetails = messagefunctiondetails;
                itineraryDetail.Message = message;

                List<SegmentInformation1> segmentinformationList = new List<SegmentInformation1>();
                foreach(var seg in item.Segments)
                {
                    SegmentInformation1 segmentinformation1 = new SegmentInformation1();

                    TravelProductInformation travelProductInformation = new TravelProductInformation();
                    //   travelProductInformation.FlightDate.DepartureDate = "";

                    string seg_DepartureDate = seg.DepartureDate;
                    seg_DepartureDate = seg_DepartureDate.Replace("-", "");
                    seg_DepartureDate = seg_DepartureDate.Remove(4, 2);
                    string seg_DepartureFrom = seg.DepartureFrom;
                    string seg_DepartureTo = seg.DepartureTo;
                    string seg_MarketingCompany = seg.MarketingCompany;
                    string seg_FlightNumber = seg.FlightNumber;
                    string seg_BookingClass = seg.BookingClass;

                    travelProductInformation.FlightDate.DepartureDate = seg_DepartureDate;
                    travelProductInformation.BoardPointDetails.TrueLocationId = seg_DepartureFrom;
                    travelProductInformation.OffpointDetails.TrueLocationId = seg_DepartureTo;
                    travelProductInformation.CompanyDetails.MarketingCompany = seg_MarketingCompany;
                    travelProductInformation.FlightIdentification.FlightNumber = seg_FlightNumber;
                    travelProductInformation.FlightIdentification.BookingClass = seg_BookingClass;



                    RelatedproductInformation relatedproductInformation = new RelatedproductInformation();
                    relatedproductInformation.Quantity = adtchd_count;
                    List<string> code = new List<string>();
                    code.Add("NN");
                    relatedproductInformation.StatusCode = code;

                    segmentinformation1.RelatedproductInformation = relatedproductInformation;
                    segmentinformation1.TravelProductInformation = travelProductInformation;
                    segmentinformationList.Add(segmentinformation1);
                    itineraryDetail.SegmentInformation = segmentinformationList;
                }

                airSell.AirSellFromRecommendation.ItineraryDetails.Add(itineraryDetail);
            }
           // airSell.AirSellFromRecommendation.ItineraryDetails.Add(itineraryDetailList);


            // itinerary.Add(itineraryDetail);

            //string loginid = "fazil";
            //string password = "123";
            //string uuid = "1Qwer2Asdf3Zxcv!";

            string loginid = FLbookRQ.BookFlight.SupplierAgencyDetails.First().UserName;
            string password = FLbookRQ.BookFlight.SupplierAgencyDetails.First().Password;
            string uuid = FLbookRQ.BookFlight.SupplierAgencyDetails.First().AccountNumber;

            airSell.LoginId = loginid;
            airSell.Password = password;
            airSell.UuId = uuid;




            #region----airsellold----
            //var legcount = FLbookRQ.BookFlight.FlLegGroup.Count;
            //StringBuilder sbaircell = new StringBuilder();
            //sbaircell.Append("{");
            //sbaircell.Append("  \"airSellFromRecommendation\": {");
            //sbaircell.Append("    \"messageActionDetails\": {");
            //sbaircell.Append("      \"messageFunctionDetails\": {");
            //sbaircell.Append("        \"messageFunction\": \"183\",");
            //sbaircell.Append("        \"additionalMessageFunction\": [ \"M1\" ]");
            //sbaircell.Append("      }");
            //sbaircell.Append("    },");

            //sbaircell.Append("    \"itineraryDetails\": [");
            //for (int iair1 = 0; iair < legcount; iair1++)
            //{
            //    string from = FLbookRQ.BookFlight.FlLegGroup[iair].From;
            //    string to = FLbookRQ.BookFlight.FlLegGroup[iair].To;

            //    sbaircell.Append("      {");
            //    sbaircell.Append("        \"originDestinationDetails\": {");
            //    sbaircell.Append("          \"origin\": \"" + from + "\",");
            //    sbaircell.Append("          \"destination\": \"" + to + "\"");
            //    sbaircell.Append("        },");
            //    sbaircell.Append("        \"message\": { \"messageFunctionDetails\": { \"messageFunction\": \"183\" } },");
            //    sbaircell.Append("        \"segmentInformation\": [");

            //    for (int iseg1 = 0; iseg < FLbookRQ.BookFlight.FlLegGroup[iair1].Segments.Count; iseg1++)
            //    {
            //        string seg_DepartureDate = FLbookRQ.BookFlight.FlLegGroup[iair1].Segments[iseg1].DepartureDate;
            //        seg_DepartureDate = seg_DepartureDate.Replace("-", "");
            //        seg_DepartureDate = seg_DepartureDate.Remove(4, 2);
            //        //  seg_DepartureDate = seg_DepartureDate.Replace("20", "");
            //        string seg_DepartureFrom = FLbookRQ.BookFlight.FlLegGroup[iair1].Segments[iseg1].DepartureFrom;
            //        string seg_DepartureTo = FLbookRQ.BookFlight.FlLegGroup[iair1].Segments[iseg1].DepartureTo;
            //        string seg_MarketingCompany = FLbookRQ.BookFlight.FlLegGroup[iair1].Segments[iseg1].MarketingCompany;
            //        string seg_FlightNumber = FLbookRQ.BookFlight.FlLegGroup[iair1].Segments[iseg1].FlightNumber;
            //        string seg_BookingClass = FLbookRQ.BookFlight.FlLegGroup[iair1].Segments[iseg1].BookingClass;

            //        sbaircell.Append("          {");
            //        sbaircell.Append("            \"travelProductInformation\": {");
            //        sbaircell.Append("              \"flightDate\": { \"departureDate\": \"" + seg_DepartureDate + "\" },");
            //        sbaircell.Append("              \"boardPointDetails\": { \"trueLocationId\": \"" + seg_DepartureFrom + "\" },");
            //        sbaircell.Append("              \"offpointDetails\": { \"trueLocationId\": \"" + seg_DepartureTo + "\" },");
            //        sbaircell.Append("              \"companyDetails\": { \"marketingCompany\": \"" + seg_MarketingCompany + "\" },");
            //        sbaircell.Append("              \"flightIdentification\": {");
            //        sbaircell.Append("                \"flightNumber\": \"" + seg_FlightNumber + "\",");
            //        sbaircell.Append("                \"bookingClass\": \"" + seg_BookingClass + "\"");
            //        sbaircell.Append("              }");
            //        sbaircell.Append("            },");
            //        sbaircell.Append("            \"relatedproductInformation\": {");
            //        sbaircell.Append("              \"quantity\": " + adtchd_count + ",");
            //        sbaircell.Append("              \"statusCode\": [ \"NN\" ]");
            //        sbaircell.Append("            }");
            //        sbaircell.Append("          },");
            //    }
            //    sbaircell.Length--;
            //    sbaircell.Append("        ]");
            //    sbaircell.Append("      },");
            //}
            //sbaircell.Length--;
            //sbaircell.Append("    ]");
            //sbaircell.Append("  }");
            //sbaircell.Append("}");

            #endregion
            var jsonData = AirSellSerialize.ToJson(airSell);

            string RQUrl = "http://70.32.24.42:9000/SellFromRecommendation";
            //string RQUrl = "http://192.168.1.102:9000/SellFromRecommendation";
            // string RQUrl = "http://192.168.1.105:9000/SellFromRecommendation";
            string strResult = await soapHelperAsync.SendRESTRequestAsync(jsonData, RQUrl, "POST");





            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(airSell);
            CommonServices.SaveLog("AIRSELL", FLbookRQ.BookFlight.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, strResult);


            return strResult;
        }
        //  3.2 PNR Add
        public string PnrAdd_1(FLBookRQ FLbookRQ)
        {
            try
            {
                var ADT_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == "ADT").ToList<TravelerInfo>();
                var CHD_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == "CHD").ToList<TravelerInfo>();
                var INF_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == "INF").ToList<TravelerInfo>();

                var pnrcountstgring = "";
                var passportstring = "";

                int adtcount = ADT_Items.Count;
                int chdcount = CHD_Items.Count;
                int infcount = INF_Items.Count;

                int totalpasscount = adtcount + chdcount + infcount;

                var pcount = 0;
                var i = 0;
                int pnrcount = 1;
                var paxcount = 2;
                if (infcount > 0)
                {
                    pnrcount = 2;

                    for (var eachinfref = 0; eachinfref < infcount; eachinfref++)
                    {
                        var adultfname = ADT_Items[i].GivenName;
                        var adultsurname = ADT_Items[i].Surname;
                        var adpassportno = ADT_Items[i].DocId;

                        var adultnationality = ADT_Items[i].Nationality;
                        var adtnationalitysplit = adultnationality;

                        var adultissue = ADT_Items[i].DocIssueCountry;
                        var adtissuesplit = adultissue;

                        DateTime adtdob1 = CommonServices.GetDateTime(ADT_Items[i].BirthDate);

                        var adtdobsplit = adtdob1.GetFormatedDate();  //adtdob1.ToString("ddMMyyyy");
                        var adtdobsplitMMM = adtdob1.ToString("ddMMMyy");

                        DateTime adtexp = CommonServices.GetDateTime(ADT_Items[i].ExpireDate);

                        var adtexpsplit = adtexp.ToString("ddMMyy");
                        var adtexpsplitMMM = adtexp.ToString("ddMMMyy");

                        var inftfname = INF_Items[i].GivenName;
                        var inftsurname = INF_Items[i].Surname;
                        var infpassportno = INF_Items[i].DocId;

                        var infnationality = INF_Items[i].Nationality;
                        var infnationalitysplit = infnationality;

                        var infissue = INF_Items[i].DocIssueCountry;
                        var infissuesplit = infissue;



                        DateTime infdob1 = CommonServices.GetDateTime(INF_Items[i].BirthDate);

                        var infdobsplit = infdob1.ToString("ddMMyyyy");
                        var infdobsplitMMM = infdob1.ToString("ddMMMyy");

                        DateTime infexp1 = CommonServices.GetDateTime(INF_Items[i].ExpireDate);

                        var infexpsplit = infexp1.ToString("ddMMyy");
                        var infexpsplitMMM = infexp1.ToString("ddMMMyy");


                        pnrcountstgring += "{\"elementManagementPassenger\":{\"reference\":{\"qualifier\":\"PR\",\"number\":\"" + pnrcount + "\"},\"segmentName\":\"NM\"},\"passengerData\":[{\"travellerInformation\":{\"traveller\":{\"surname\":\"" + adultsurname + "\",\"quantity\":2},\"passenger\":[{\"firstName\":\"" + adultfname + "\",\"type\":\"ADT\",";
                        pnrcountstgring += "\"infantIndicator\":\"3\"}]}},{\"travellerInformation\":{\"traveller\":{\"surname\":\"" + inftfname + "\"},\"passenger\":[{\"firstName\":\"" + inftsurname + "\",\"type\":\"INF\"}]},\"dateOfBirth\":{\"dateAndTimeDetails\":{\"date\":\"" + infdobsplit + "\"";
                        passportstring += "{\"elementManagementData\":{\"segmentName\":\"SSR\"},\"serviceRequest\":{\"ssr\":{\"type\":\"DOCS\",\"status\":\"HK\",\"quantity\":1,\"freetext\":[\"P-" + adtnationalitysplit + "-" + adpassportno + "-" + adtissuesplit + "-" + adtdobsplitMMM + "-M-" + adtexpsplitMMM + "-" + adultfname + "-" + adultsurname + "\"]}},\"referenceForDataElement\":{\"reference\":[{\"qualifier\":\"PT\",\"number\":\"" + paxcount + "\"}]}},";
                        passportstring += "{\"elementManagementData\":{\"segmentName\":\"SSR\"},\"serviceRequest\":{\"ssr\":{\"type\":\"DOCS\",\"status\":\"HK\",\"quantity\":1,\"freetext\":[\"P-" + infnationalitysplit + "-" + infpassportno + "-" + infissuesplit + "-" + infdobsplitMMM + "-MI-" + infexpsplitMMM + "-" + inftfname + "-" + inftsurname + "\"]}},\"referenceForDataElement\":{\"reference\":[{\"qualifier\":\"PT\",\"number\":\"" + paxcount + "\"}]}},";

                        i++;

                        pcount += 2;
                        paxcount++;
                        pnrcount++;

                        if (pcount >= totalpasscount)
                        {
                            pnrcountstgring += " }}}]}],";
                            passportstring = passportstring.Substring(0, passportstring.Length - 1);
                            passportstring += "]}}}";
                        }
                        else
                        {
                            pnrcountstgring += " }}}]},";
                        }
                        adtcount--;
                    }
                }
                if (adtcount > 0)
                {
                    for (var eachadtref = 0; eachadtref < adtcount; eachadtref++)
                    {
                        var adultfname = ADT_Items[i].GivenName;
                        var adultsurname = ADT_Items[i].Surname;
                        var adpassportno = ADT_Items[i].DocId;

                        var adtdob1 = ADT_Items[i].BirthDate;
                        DateTime adtdob2 = CommonServices.GetDateTime(ADT_Items[i].BirthDate);

                        var adtdobsplit = adtdob2.ToString("ddMMyyyy");
                        var adtdobsplitMMM = adtdob2.ToString("ddMMMyy");

                        DateTime adtexp = CommonServices.GetDateTime(ADT_Items[i].ExpireDate);

                        var adtexpsplit = adtexp.ToString("ddMMyy");
                        var adtexpsplitMMM = adtexp.ToString("ddMMMyy");

                        var adtnationalitysplit = ADT_Items[i].Nationality;

                        var adtissuesplit = ADT_Items[i].DocIssueCountry;

                        pnrcountstgring += "{\"elementManagementPassenger\":{\"reference\":{\"qualifier\":\"PR\",\"number\":\"" + pnrcount + "\"},\"segmentName\":\"NM\"},\"passengerData\":[{\"travellerInformation\":{\"traveller\":{\"surname\":\"" + adultsurname + "\",\"quantity\":1},\"passenger\":[{\"firstName\":\"" + adultfname + "\",\"type\":\"ADT\"";
                        //  passportstring += '{\"elementManagementData\":{\"segmentName\":\"SSR\"},\"serviceRequest\":{\"ssr\":{\"type\":\"DOCS\",\"status\":\"HK\",\"quantity\":1,\"freetext\":[\"P-' + adtnationalitysplit + '-' + adpassportno + '-' + adtissuesplit + '-17AUG95-M-20OCT25-' + adultfname + '-' + adultsurname + '\"]}},\"referenceForDataElement\":{\"reference\":[{\"qualifier\":\"PT\",\"number\":\"' + paxcount + '\"}]}},';

                        passportstring += "{\"elementManagementData\":{\"segmentName\":\"SSR\"},\"serviceRequest\":{\"ssr\":{\"type\":\"DOCS\",\"status\":\"HK\",\"quantity\":1,\"freetext\":[\"P-" + adtnationalitysplit.ToUpper() + "-" + adpassportno.ToUpper() + "-" + adtissuesplit.ToUpper() + "-" + adtdobsplitMMM.ToUpper() + "-M-" + adtexpsplitMMM.ToUpper() + "-" + adultfname.ToUpper() + "-" + adultsurname.ToUpper() + "\"]}},\"referenceForDataElement\":{\"reference\":[{\"qualifier\":\"PT\",\"number\":\"" + paxcount + "\"}]}},";

                        i++;
                        pcount++;
                        paxcount++;
                        pnrcount++;
                        if (pcount >= totalpasscount)
                        {

                            pnrcountstgring += " }]}}]}],";
                            passportstring = passportstring.Substring(0, passportstring.Length - 1);
                            passportstring += "]}}}";
                        }
                        else
                        {
                            pnrcountstgring += " }]}}]},";
                        }
                    }
                }
                var j = 0;
                if (chdcount > 0)
                {
                    for (var eachchdref = 0; eachchdref < chdcount; eachchdref++)
                    {
                        var chdfname = CHD_Items[j].GivenName;
                        var chdsurname = CHD_Items[j].Surname;
                        var chdpassportno = CHD_Items[j].DocId;

                        var chnationalitysplit = CHD_Items[j].Nationality;
                        var chissuesplit = CHD_Items[j].DocIssueCountry;

                        DateTime chdob1 = CommonServices.GetDateTime(CHD_Items[j].BirthDate);

                        var chdobsplit = chdob1.ToString("ddMMyyyy");
                        var chdobsplitMMM = chdob1.ToString("ddMMMyy");

                        DateTime chdexp1 = CommonServices.GetDateTime(CHD_Items[j].ExpireDate);

                        var chexpsplit = chdexp1.ToString("ddMMyy");
                        var chexpsplitMMM = chdexp1.ToString("ddMMMyy");

                        pnrcountstgring += "{\"elementManagementPassenger\":{\"reference\":{\"qualifier\":\"PR\",\"number\":\"" + pnrcount + "\"},\"segmentName\":\"NM\"},\"passengerData\":[{\"travellerInformation\":{\"traveller\":{\"surname\":\"" + chdsurname + "\",\"quantity\":1},\"passenger\":[{\"firstName\":\"" + chdfname + "\",\"type\":\"CHD\"";

                        passportstring += "{\"elementManagementData\":{\"segmentName\":\"SSR\"},\"serviceRequest\":{\"ssr\":{\"type\":\"DOCS\",\"status\":\"HK\",\"quantity\":1,\"freetext\":[\"P-" + chnationalitysplit + "-" + chdpassportno + "-" + chissuesplit + "-" + chdobsplitMMM + "-M-" + chexpsplitMMM + "-" + chdfname + "-" + chdsurname + "\"]}},\"referenceForDataElement\":{\"reference\":[{\"qualifier\":\"PT\",\"number\":\"" + paxcount + "\"}]}},";
                        j++;
                        pcount++;
                        paxcount++;
                        pnrcount++;
                        if (pcount >= totalpasscount)
                        {
                            pnrcountstgring += " }]}}]}],";
                            passportstring = passportstring.Substring(0, passportstring.Length - 1);
                            passportstring += "]}}}";
                        }
                        else
                        {
                            pnrcountstgring += " }]}}]},";
                        }
                    }
                }




                string phonenumber = FLbookRQ.BookFlight.PhoneNumber;
                string email = FLbookRQ.BookFlight.Email;

                StringBuilder sbpnradd = new StringBuilder();

                sbpnradd.Append("{");
                sbpnradd.Append("  \"pnrAddMultiElements\": {");
                sbpnradd.Append("    \"reservationInfo\": { \"reservation\": { } },");
                sbpnradd.Append("    \"pnrActions\": { \"optionCode\": [ 0 ] },");
                sbpnradd.Append("    \"travellerInfo\": [");

                //sbpnradd.Append("      {");
                //sbpnradd.Append("        \"elementManagementPassenger\": {");
                //sbpnradd.Append("          \"reference\": {");
                //sbpnradd.Append("            \"qualifier\": \"PR\",");
                //sbpnradd.Append("            \"number\": \"1\"");
                //sbpnradd.Append("          },");
                //sbpnradd.Append("          \"segmentName\": \"NM\"");
                //sbpnradd.Append("        },");
                //sbpnradd.Append("        \"passengerData\": [");
                //sbpnradd.Append("          {");
                //sbpnradd.Append("            \"travellerInformation\": {");
                //sbpnradd.Append("              \"traveller\": {");
                //sbpnradd.Append("                \"surname\": \"aaaaaaa\",");
                //sbpnradd.Append("                \"quantity\": 1");
                //sbpnradd.Append("              },");
                //sbpnradd.Append("              \"passenger\": [");
                //sbpnradd.Append("                {");
                //sbpnradd.Append("                  \"firstName\": \"aaaaaaaaa\",");
                //sbpnradd.Append("                  \"type\": \"ADT\"");
                //sbpnradd.Append("                }");
                //sbpnradd.Append("              ]");
                //sbpnradd.Append("            }");
                //sbpnradd.Append("          }");
                //sbpnradd.Append("        ]");
                //sbpnradd.Append("      }");
                //sbpnradd.Append("    ],");
                sbpnradd.Append(pnrcountstgring);

                sbpnradd.Append("    \"dataElementsMaster\": {");
                sbpnradd.Append("      \"marker1\": { },");
                sbpnradd.Append("      \"dataElementsIndiv\": [");

                sbpnradd.Append("        {");
                sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"FP\" },");
                sbpnradd.Append("          \"formOfPayment\": { \"fop\": [ { \"identification\": \"CA\" } ] }");
                sbpnradd.Append("        },");
                sbpnradd.Append("        {");
                sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"RF\" },");
                sbpnradd.Append("          \"freetextData\": {");
                sbpnradd.Append("            \"freetextDetail\": {");
                sbpnradd.Append("              \"subjectQualifier\": \"3\",");
                sbpnradd.Append("              \"type\": \"P23\"");
                sbpnradd.Append("            },");
                sbpnradd.Append("            \"longFreetext\": \"Example\"");
                sbpnradd.Append("          }");
                sbpnradd.Append("        },");

                sbpnradd.Append("        {");
                sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"AP\" },");
                sbpnradd.Append("          \"freetextData\": {");
                sbpnradd.Append("            \"freetextDetail\": {");
                sbpnradd.Append("              \"subjectQualifier\": \"3\",");
                sbpnradd.Append("              \"type\": \"4\"");
                sbpnradd.Append("            },");
                sbpnradd.Append("            \"longFreetext\": \"" + phonenumber + "\"");
                sbpnradd.Append("          }");
                sbpnradd.Append("        },");
                sbpnradd.Append("        {");
                sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"AP\" },");
                sbpnradd.Append("          \"freetextData\": {");
                sbpnradd.Append("            \"freetextDetail\": {");
                sbpnradd.Append("              \"subjectQualifier\": \"3\",");
                sbpnradd.Append("              \"type\": \"4\"");
                sbpnradd.Append("            },");
                sbpnradd.Append("            \"longFreetext\": \"" + email + "\"");
                sbpnradd.Append("          }");
                sbpnradd.Append("        },");

                sbpnradd.Append("        {");
                sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"TK\" },");
                sbpnradd.Append("          \"ticketElement\": { \"ticket\": { \"indicator\": \"OK\" } }");
                sbpnradd.Append("        },");

                //sbpnradd.Append("        {");
                //sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"SSR\" },");
                //sbpnradd.Append("          \"serviceRequest\": {");
                //sbpnradd.Append("            \"ssr\": {");
                //sbpnradd.Append("              \"type\": \"DOCS\",");
                //sbpnradd.Append("              \"status\": \"HK\",");
                //sbpnradd.Append("              \"quantity\": 1,");
                //sbpnradd.Append("              \"freetext\": [ \"P-AND-aaaaaaa-AND-01Feb03-M-01Mar19-aaaaaaaaa-aaaaaaa\" ]");
                //sbpnradd.Append("            }");
                //sbpnradd.Append("          },");
                //sbpnradd.Append("          \"referenceForDataElement\": {");
                //sbpnradd.Append("            \"reference\": [");
                //sbpnradd.Append("              {");
                //sbpnradd.Append("                \"qualifier\": \"PT\",");
                //sbpnradd.Append("                \"number\": \"2\"");
                //sbpnradd.Append("              }");
                //sbpnradd.Append("            ]");
                //sbpnradd.Append("          }");
                //sbpnradd.Append("        }");
                //sbpnradd.Append("      ]");
                //sbpnradd.Append("    }");
                //sbpnradd.Append("  }");
                //sbpnradd.Append("}");
                sbpnradd.Append(passportstring);
                string test = sbpnradd.ToString();

                //string RQUrl = FLbookRQ.BookFlight.SupplierAgencyDetailsBook[0].BaseUrl + "/pnradd";
                //string RQUrl = "http://192.168.1.105:9000/pnradd";
                string RQUrl = "http://70.32.24.42:9000/pnradd";
                //string RQUrl = "http://192.168.1.102:9000/pnradd";

                string strResult = SOAPHelper.SendRESTRequest(sbpnradd.ToString(), RQUrl, "POST");

                return strResult;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public async Task<string> PnrAdd(FLBookRQ FLbookRQ)
        {
            try
            {

                PnrAdd pnrAdd = new PnrAdd();

                var ADT_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == PaxTypeEnum.ADT.ToString()).ToList<TravelerInfo>();
                var CHD_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == PaxTypeEnum.CHD.ToString()).ToList<TravelerInfo>();
                var INF_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == PaxTypeEnum.INF.ToString()).ToList<TravelerInfo>();

                int adtcount = ADT_Items.Count;
                int chdcount = CHD_Items.Count;
                int infcount = INF_Items.Count;

                ElementManagementPassenger elementManagementPassenger1 = new ElementManagementPassenger();
                ElementManagementPassenger elementManagementPassenger = new ElementManagementPassenger();
                ElementManagementPassenger elementManagementPassengeradt = new ElementManagementPassenger();
                TravellerInformation travellerInformation = new TravellerInformation();
                TravellerInformation travellerInformationadt = new TravellerInformation();
                TravellerInformation travellerInformation2 = new TravellerInformation();
                TravellerInformation travellerInformation1 = new TravellerInformation();
                ElementManagementData elementManagementData = new ElementManagementData();
                ElementManagementData elementManagementDatas = new ElementManagementData();
                ElementManagementData elementManagementDatadt = new ElementManagementData();
                ServiceRequest serviceRequest = new ServiceRequest();
                ServiceRequest serviceRequest1 = new ServiceRequest();
                ServiceRequest serviceRequestadt = new ServiceRequest();
                ServiceRequest serviceRequestinf = new ServiceRequest();

                ReferenceForDataElement referenceForDataElement = new ReferenceForDataElement();
                ReferenceForDataElement referenceForDataElement1 = new ReferenceForDataElement();
                ReferenceForDataElement referenceForDataElementadt = new ReferenceForDataElement();
                List<string> passport = new List<string>();
                List<string> passport1 = new List<string>();
                List<string> passportinf = new List<string>();
                DateOfBirth dateOfBirth = new DateOfBirth();
                DateAndTimeDetails dateAndTimeDetails = new DateAndTimeDetails();

                var pcount = 0;
                var i = 0;
                int pnrcount = 1;
                var paxcount = 2;
                if (infcount > 0)
                {
                    pnrcount = 2;

                    INF_Items.ForEach(infc =>
                {

                    var adultfname = ADT_Items[i].GivenName;
                    var adultsurname = ADT_Items[i].Surname;
                    var adpassportno = ADT_Items[i].DocId;

                    var adultnationality = ADT_Items[i].Nationality;
                    var adtnationalitysplit = adultnationality;

                    var adultissue = ADT_Items[i].DocIssueCountry;
                    var adtissuesplit = adultissue;

                    DateTime adtdob1 = CommonServices.GetDateTime(ADT_Items[i].BirthDate);

                    var adtdobsplit = adtdob1.GetFormatedDate();  //adtdob1.ToString("ddMMyyyy");
                    var adtdobsplitMMM = adtdob1.GetFormatedDate1(); // adtdob1.ToString("ddMMMyy");

                    DateTime adtexp = CommonServices.GetDateTime(ADT_Items[i].ExpireDate);

                    var adtexpsplit = adtexp.GetFormatedDate();
                    var adtexpsplitMMM = adtexp.GetFormatedDate1();


                    var inftfname = INF_Items[i].GivenName;
                    var inftsurname = INF_Items[i].Surname;
                    var infpassportno = INF_Items[i].DocId;

                    var infnationality = INF_Items[i].Nationality;
                    var infnationalitysplit = infnationality;

                    var infissue = INF_Items[i].DocIssueCountry;
                    var infissuesplit = infissue;



                    DateTime infdob1 = CommonServices.GetDateTime(INF_Items[i].BirthDate);

                    var infdobsplit = infdob1.GetFormatedDate();
                    var infdobsplitMMM = infdob1.GetFormatedDate1();

                    DateTime infexp1 = CommonServices.GetDateTime(INF_Items[i].ExpireDate);

                    var infexpsplit = infexp1.GetFormatedDate();
                    var infexpsplitMMM = infexp1.GetFormatedDate1();



                    elementManagementPassenger1.Reference.Qualifier = "PR";
                    elementManagementPassenger1.Reference.Number = pnrcount.ToString();
                    elementManagementPassenger1.SegmentName = "NM";


                    travellerInformation.Traveller.Surname = adultsurname;
                    travellerInformation.Traveller.Quantity = "2";
                    travellerInformation2.Traveller.Surname = inftfname;
                    if (travellerInformation.Traveller.Quantity == "0")
                    {

                        travellerInformation.Traveller.Quantity = null;
                    }


                    travellerInformation.Passenger.Add(new global::Passenger()
                    {
                        FirstName = adultfname,
                        Type = "ADT",
                        InfantIndicator = "3"

                    });
                    travellerInformation2.Passenger.Add(new global::Passenger()
                    {
                        FirstName = inftsurname,
                        Type = "INF"

                    });

                    dateAndTimeDetails.Date = infdobsplit;
                    dateOfBirth.DateAndTimeDetails = dateAndTimeDetails;


                    elementManagementData.SegmentName = "SSR";


                    serviceRequest.Ssr.Type = "DOCS";
                    serviceRequest.Ssr.Status = "HK";
                    serviceRequest.Ssr.Quantity = 1;


                    serviceRequestinf.Ssr.Type = "DOCS";
                    serviceRequestinf.Ssr.Status = "HK";
                    serviceRequestinf.Ssr.Quantity = 1;

                    passport.Add("P-" + adtnationalitysplit + "-" + adpassportno + "-" + adtissuesplit + "-" + adtdobsplitMMM + "-M-" + adtexpsplitMMM + "-" + adultfname + "-" + adultsurname + "");
                    passportinf.Add("P-" + infnationalitysplit + "-" + infpassportno + "-" + infissuesplit + "-" + infdobsplitMMM + "-MI-" + infexpsplitMMM + "-" + inftfname + "-" + inftsurname + "");
                    serviceRequest.Ssr.Freetext = passport;
                    serviceRequestinf.Ssr.Freetext = passportinf;

                    referenceForDataElement.Reference.Add(new global::Reference()
                    {

                        Qualifier = "PT",
                        Number = paxcount.ToString()


                    });


                    var pass1 = new List<PassengerData>();

                    pass1.Add(new PassengerData()
                    {

                        TravellerInformation = travellerInformation,

                    });


                    pass1.Add(new PassengerData()
                    {

                        TravellerInformation = travellerInformation2,
                        DateOfBirth = dateOfBirth
                    });

                    pnrAdd.PnrAddMultiElements.TravellerInfo.Add(new TravellerInfo()
                    {

                        PassengerData = pass1,
                        ElementManagementPassenger = elementManagementPassenger1


                    });

                    i++;
                    paxcount++;
                    pnrcount++;
                    adtcount--;

                });


                    pnrAdd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(new DataElementsIndiv()
                    {
                        ElementManagementData = elementManagementData,
                        ServiceRequest = serviceRequest,
                        ReferenceForDataElement = referenceForDataElement



                    });
                    pnrAdd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(new DataElementsIndiv()
                    {
                        ElementManagementData = elementManagementData,
                        ServiceRequest = serviceRequestinf,
                        ReferenceForDataElement = referenceForDataElement



                    });

                }

                if (adtcount > 0)
                {
                    for (var eachadtref = 0; eachadtref < adtcount; eachadtref++)
                    {


                        var adultfname = ADT_Items[i].GivenName;
                        var adultsurname = ADT_Items[i].Surname;
                        var adpassportno = ADT_Items[i].DocId;

                        var adultnationality = ADT_Items[i].Nationality;
                        var adtnationalitysplit = adultnationality;

                        var adultissue = ADT_Items[i].DocIssueCountry;
                        var adtissuesplit = adultissue;

                        DateTime adtdob1 = CommonServices.GetDateTime(ADT_Items[i].BirthDate);

                        var adtdobsplit = adtdob1.GetFormatedDate();  //adtdob1.ToString("ddMMyyyy");
                        var adtdobsplitMMM = adtdob1.GetFormatedDate1(); // adtdob1.ToString("ddMMMyy");

                        DateTime adtexp = CommonServices.GetDateTime(ADT_Items[i].ExpireDate);

                        var adtexpsplit = adtexp.GetFormatedDate();
                        var adtexpsplitMMM = adtexp.GetFormatedDate1();



                        elementManagementPassengeradt.Reference.Qualifier = "PR";
                        elementManagementPassengeradt.Reference.Number = pnrcount.ToString();
                        elementManagementPassengeradt.SegmentName = "NM";

                        travellerInformationadt.Traveller.Surname = adultsurname;
                        travellerInformationadt.Traveller.Quantity = "1";
                        travellerInformationadt.Passenger.Add(new global::Passenger()
                        {
                            FirstName = adultfname,
                            Type = PaxTypeEnum.ADT.ToString()


                        });

                        elementManagementDatadt.SegmentName = "SSR";


                        serviceRequestadt.Ssr.Type = "DOCS";
                        serviceRequestadt.Ssr.Status = "HK";
                        serviceRequestadt.Ssr.Quantity = 1;
                        List<string> passportadt = new List<string>();
                        passportadt.Add("P-" + adtnationalitysplit + "-" + adpassportno + "-" + adtissuesplit + "-" + adtdobsplitMMM + "-M-" + adtexpsplitMMM + "-" + adultfname + "-" + adultsurname + "");
                        serviceRequestadt.Ssr.Freetext = passportadt;


                        referenceForDataElementadt.Reference.Add(new global::Reference()
                        {

                            Qualifier = "PT",
                            Number = paxcount.ToString()


                        });
                        var pass2 = new List<PassengerData>();
                        pass2.Add(new PassengerData()
                        {

                            TravellerInformation = travellerInformationadt,

                        });

                        pnrAdd.PnrAddMultiElements.TravellerInfo.Add(new TravellerInfo()
                        {

                            PassengerData = pass2,
                            ElementManagementPassenger = elementManagementPassengeradt


                        });


                        i++;
                        pcount++;
                        paxcount++;
                        pnrcount++;
                    }
                    pnrAdd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(new DataElementsIndiv()
                    {
                        ElementManagementData = elementManagementDatadt,
                        ServiceRequest = serviceRequestadt,
                        ReferenceForDataElement = referenceForDataElementadt



                    });
                }

                var j = 0;
                if (chdcount > 0)
                {
                    CHD_Items.ForEach(chdc =>
                    {

                        var chdfname = CHD_Items[j].GivenName;
                        var chdsurname = CHD_Items[j].Surname;
                        var chdpassportno = CHD_Items[j].DocId;

                        var chnationalitysplit = CHD_Items[j].Nationality;
                        var chissuesplit = CHD_Items[j].DocIssueCountry;

                        DateTime chdob1 = CommonServices.GetDateTime(CHD_Items[j].BirthDate);

                        var chdobsplit = chdob1.GetFormatedDate();
                        var chdobsplitMMM = chdob1.GetFormatedDate1();
                        DateTime chdexp1 = CommonServices.GetDateTime(CHD_Items[j].ExpireDate);

                        var chexpsplit = chdexp1.GetFormatedDate();
                        var chexpsplitMMM = chdexp1.GetFormatedDate1();


                        elementManagementPassenger.Reference.Qualifier = "PR";
                        elementManagementPassenger.Reference.Number = pnrcount.ToString();
                        elementManagementPassenger.SegmentName = "NM";

                        travellerInformation1.Traveller.Surname = chdsurname;
                        travellerInformation1.Traveller.Quantity = "1";

                        travellerInformation1.Passenger.Add(new global::Passenger()
                        {
                            FirstName = chdfname,
                            Type = PaxTypeEnum.CHD.ToString()


                        });

                        elementManagementDatas.SegmentName = "SSR";


                        serviceRequest1.Ssr.Type = "DOCS";
                        serviceRequest1.Ssr.Status = "HK";
                        serviceRequest1.Ssr.Quantity = 1;
                        passport1.Add("P-" + chnationalitysplit + "-" + chdpassportno + "-" + chissuesplit + "-" + chdobsplitMMM + "-M-" + chexpsplitMMM + "-" + chdfname + "-" + chdsurname + "");
                        serviceRequest1.Ssr.Freetext = passport1;
                        referenceForDataElement1.Reference.Add(new global::Reference()
                        {

                            Qualifier = "PT",
                            Number = paxcount.ToString()


                        });
                        j++;
                        pcount++;
                        paxcount++;
                        pnrcount++;
                    });

                    var pass3 = new List<PassengerData>();
                    pass3.Add(new PassengerData()
                    {

                        TravellerInformation = travellerInformation1,

                    });

                    pnrAdd.PnrAddMultiElements.TravellerInfo.Add(new TravellerInfo()
                    {

                        PassengerData = pass3,
                        ElementManagementPassenger = elementManagementPassenger


                    });



                    pnrAdd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(new DataElementsIndiv()
                    {
                        ElementManagementData = elementManagementDatas,
                        ServiceRequest = serviceRequest1,
                        ReferenceForDataElement = referenceForDataElement1



                    });



                }





                string phonenumber = FLbookRQ.BookFlight.PhoneNumber;
                string email = FLbookRQ.BookFlight.Email;

                pnrAdd.PnrAddMultiElements.PnrActions.OptionCode.Add(0);

                #region---pnradd---
                //var ADT_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == "ADT").ToList<TravelerInfo>();
                //var CHD_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == "CHD").ToList<TravelerInfo>();
                //var INF_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == "INF").ToList<TravelerInfo>();

                //var pnrcountstgring = "";
                //var passportstring = "";

                //int adtcount = ADT_Items.Count;
                //int chdcount = CHD_Items.Count;
                //int infcount = INF_Items.Count;

                //int totalpasscount = adtcount + chdcount + infcount;

                //var pcount = 0;
                //var i = 0;
                //int pnrcount = 1;
                //var paxcount = 2;
                //if (infcount > 0)
                //{
                //    pnrcount = 2;

                //    for (var eachinfref = 0; eachinfref < infcount; eachinfref++)
                //    {
                //        var adultfname = ADT_Items[i].GivenName;
                //        var adultsurname = ADT_Items[i].Surname;
                //        var adpassportno = ADT_Items[i].DocId;

                //        var adultnationality = ADT_Items[i].Nationality;
                //        var adtnationalitysplit = adultnationality;

                //        var adultissue = ADT_Items[i].DocIssueCountry;
                //        var adtissuesplit = adultissue;

                //        DateTime adtdob1 = CommonServices.GetDateTime(ADT_Items[i].BirthDate);

                //        var adtdobsplit = adtdob1.GetFormatedDate();  //adtdob1.ToString("ddMMyyyy");
                //        var adtdobsplitMMM = adtdob1.ToString("ddMMMyy");

                //        DateTime adtexp = CommonServices.GetDateTime(ADT_Items[i].ExpireDate);

                //        var adtexpsplit = adtexp.ToString("ddMMyy");
                //        var adtexpsplitMMM = adtexp.ToString("ddMMMyy");

                //        var inftfname = INF_Items[i].GivenName;
                //        var inftsurname = INF_Items[i].Surname;
                //        var infpassportno = INF_Items[i].DocId;

                //        var infnationality = INF_Items[i].Nationality;
                //        var infnationalitysplit = infnationality;

                //        var infissue = INF_Items[i].DocIssueCountry;
                //        var infissuesplit = infissue;



                //        DateTime infdob1 = CommonServices.GetDateTime(INF_Items[i].BirthDate);

                //        var infdobsplit = infdob1.ToString("ddMMyyyy");
                //        var infdobsplitMMM = infdob1.ToString("ddMMMyy");

                //        DateTime infexp1 = CommonServices.GetDateTime(INF_Items[i].ExpireDate);

                //        var infexpsplit = infexp1.ToString("ddMMyy");
                //        var infexpsplitMMM = infexp1.ToString("ddMMMyy");

                //        ElementManagementPassenger elementManagementPassenger = new ElementManagementPassenger();
                //        elementManagementPassenger.Reference.Qualifier = "PR";
                //        elementManagementPassenger.Reference.Qualifier = pnrcount.ToString();
                //        elementManagementPassenger.SegmentName = "NM";

                //        TravellerInformation travellerInformation = new TravellerInformation();

                //        travellerInformation.Traveller.Surname = adultsurname;
                //        travellerInformation.Traveller.Quantity = 2;
                //        travellerInformation.Traveller.Surname = inftfname;


                //        var pass = new List<Passenger>();
                //        pass.Add(new Passenger()
                //        {
                //            firstName = adultfname,
                //            type = "ADT",
                //            infantIndicator = "3"

                //        });
                //        pass.Add(new Passenger()
                //        {
                //            firstName = inftsurname,
                //            type = "INF"

                //        });

                //        DateOfBirth dateOfBirth = new DateOfBirth();
                //        dateOfBirth.DateAndTimeDetails.Date = infdobsplit;

                //        //ElementManagementData elementManagementData = new ElementManagementData();

                //        //elementManagementData.SegmentName = "SSR";

                //        //ServiceRequest serviceRequest = new ServiceRequest();
                //        //serviceRequest.Ssr.Type = "DOCS";
                //        //serviceRequest.Ssr.Status = "HK";
                //        //serviceRequest.Ssr.Quantity = 1;
                //        //List<string> passport = new List<string>();
                //        //passport.Add("P-" + adtnationalitysplit + "-" + adpassportno + "-" + adtissuesplit + "-" + adtdobsplitMMM + "-M-" + adtexpsplitMMM + "-" + adultfname + "-" + adultsurname + "");
                //        //passport.Add("P - " + infnationalitysplit + " - " + infpassportno + " - " + infissuesplit + " - " + infdobsplitMMM + " - MI - " + infexpsplitMMM + " - " + inftfname + " - " + inftsurname + "");
                //        //serviceRequest.Ssr.Freetext = passport;



                //        //ReferenceForDataElement referenceForDataElement = new ReferenceForDataElement();

                //        referenceForDataElement.Reference.Add(new global::Reference()
                //        {

                //            Qualifier = "1",
                //            Number = paxcount.ToString()


                //        });



                //        pnrAdd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(new DataElementsIndiv()
                //        {
                //            ElementManagementData = elementManagementData,
                //            ServiceRequest = serviceRequest,
                //            ReferenceForDataElement = referenceForDataElement



                //        });

                //        i++;
                //        paxcount++;
                //        pnrcount++;


                //        pnrcountstgring += "{\"elementManagementPassenger\":{\"reference\":{\"qualifier\":\"PR\",\"number\":\"" + pnrcount + "\"},\"segmentName\":\"NM\"},\"passengerData\":[{\"travellerInformation\":{\"traveller\":{\"surname\":\"" + adultsurname + "\",\"quantity\":2},\"passenger\":[{\"firstName\":\"" + adultfname + "\",\"type\":\"ADT\",";
                //        pnrcountstgring += "\"infantIndicator\":\"3\"}]}},{\"travellerInformation\":{\"traveller\":{\"surname\":\"" + inftfname + "\"},\"passenger\":[{\"firstName\":\"" + inftsurname + "\",\"type\":\"INF\"}]},\"dateOfBirth\":{\"dateAndTimeDetails\":{\"date\":\"" + infdobsplit + "\"";
                //        passportstring += "{\"elementManagementData\":{\"segmentName\":\"SSR\"},\"serviceRequest\":{\"ssr\":{\"type\":\"DOCS\",\"status\":\"HK\",\"quantity\":1,\"freetext\":[\"P-" + adtnationalitysplit + "-" + adpassportno + "-" + adtissuesplit + "-" + adtdobsplitMMM + "-M-" + adtexpsplitMMM + "-" + adultfname + "-" + adultsurname + "\"]}},\"referenceForDataElement\":{\"reference\":[{\"qualifier\":\"PT\",\"number\":\"" + paxcount + "\"}]}},";
                //        passportstring += "{\"elementManagementData\":{\"segmentName\":\"SSR\"},\"serviceRequest\":{\"ssr\":{\"type\":\"DOCS\",\"status\":\"HK\",\"quantity\":1,\"freetext\":[\"P-" + infnationalitysplit + "-" + infpassportno + "-" + infissuesplit + "-" + infdobsplitMMM + "-MI-" + infexpsplitMMM + "-" + inftfname + "-" + inftsurname + "\"]}},\"referenceForDataElement\":{\"reference\":[{\"qualifier\":\"PT\",\"number\":\"" + paxcount + "\"}]}},";

                //        i++;

                //        pcount += 2;
                //        paxcount++;
                //        pnrcount++;

                //        adtcount--;

                //        if (pcount >= totalpasscount)
                //        {
                //            pnrcountstgring += " }}}]}],";
                //            passportstring = passportstring.Substring(0, passportstring.Length - 1);
                //            passportstring += "]}}}";
                //        }
                //        else
                //        {
                //            pnrcountstgring += " }}}]},";
                //        }
                //        adtcount--;
                //    }
                //}
                //if (adtcount > 0)
                //{
                //    for (var eachadtref = 0; eachadtref < adtcount; eachadtref++)
                //    {
                //        var adultfname = ADT_Items[i].GivenName;
                //        var adultsurname = ADT_Items[i].Surname;
                //        var adpassportno = ADT_Items[i].DocId;

                //        var adtdob1 = ADT_Items[i].BirthDate;
                //        DateTime adtdob2 = CommonServices.GetDateTime(ADT_Items[i].BirthDate);

                //        var adtdobsplit = adtdob2.ToString("ddMMyyyy");
                //        var adtdobsplitMMM = adtdob2.ToString("ddMMMyy");

                //        DateTime adtexp = CommonServices.GetDateTime(ADT_Items[i].ExpireDate);

                //        var adtexpsplit = adtexp.ToString("ddMMyy");
                //        var adtexpsplitMMM = adtexp.ToString("ddMMMyy");

                //        var adtnationalitysplit = ADT_Items[i].Nationality;

                //        var adtissuesplit = ADT_Items[i].DocIssueCountry;

                //        pnrcountstgring += "{\"elementManagementPassenger\":{\"reference\":{\"qualifier\":\"PR\",\"number\":\"" + pnrcount + "\"},\"segmentName\":\"NM\"},\"passengerData\":[{\"travellerInformation\":{\"traveller\":{\"surname\":\"" + adultsurname + "\",\"quantity\":1},\"passenger\":[{\"firstName\":\"" + adultfname + "\",\"type\":\"ADT\"";
                //        //  passportstring += '{\"elementManagementData\":{\"segmentName\":\"SSR\"},\"serviceRequest\":{\"ssr\":{\"type\":\"DOCS\",\"status\":\"HK\",\"quantity\":1,\"freetext\":[\"P-' + adtnationalitysplit + '-' + adpassportno + '-' + adtissuesplit + '-17AUG95-M-20OCT25-' + adultfname + '-' + adultsurname + '\"]}},\"referenceForDataElement\":{\"reference\":[{\"qualifier\":\"PT\",\"number\":\"' + paxcount + '\"}]}},';

                //        passportstring += "{\"elementManagementData\":{\"segmentName\":\"SSR\"},\"serviceRequest\":{\"ssr\":{\"type\":\"DOCS\",\"status\":\"HK\",\"quantity\":1,\"freetext\":[\"P-" + adtnationalitysplit.ToUpper() + "-" + adpassportno.ToUpper() + "-" + adtissuesplit.ToUpper() + "-" + adtdobsplitMMM.ToUpper() + "-M-" + adtexpsplitMMM.ToUpper() + "-" + adultfname.ToUpper() + "-" + adultsurname.ToUpper() + "\"]}},\"referenceForDataElement\":{\"reference\":[{\"qualifier\":\"PT\",\"number\":\"" + paxcount + "\"}]}},";

                //        i++;
                //        pcount++;
                //        paxcount++;
                //        pnrcount++;
                //        if (pcount >= totalpasscount)
                //        {

                //            pnrcountstgring += " }]}}]}],";
                //            passportstring = passportstring.Substring(0, passportstring.Length - 1);
                //            passportstring += "]}}}";
                //        }
                //        else
                //        {
                //            pnrcountstgring += " }]}}]},";
                //        }
                //    }
                //}
                //var j = 0;
                //if (chdcount > 0)
                //{
                //    for (var eachchdref = 0; eachchdref < chdcount; eachchdref++)
                //    {
                //        var chdfname = CHD_Items[j].GivenName;
                //        var chdsurname = CHD_Items[j].Surname;
                //        var chdpassportno = CHD_Items[j].DocId;

                //        var chnationalitysplit = CHD_Items[j].Nationality;
                //        var chissuesplit = CHD_Items[j].DocIssueCountry;

                //        DateTime chdob1 = CommonServices.GetDateTime(CHD_Items[j].BirthDate);

                //        var chdobsplit = chdob1.ToString("ddMMyyyy");
                //        var chdobsplitMMM = chdob1.ToString("ddMMMyy");

                //        DateTime chdexp1 = CommonServices.GetDateTime(CHD_Items[j].ExpireDate);

                //        var chexpsplit = chdexp1.ToString("ddMMyy");
                //        var chexpsplitMMM = chdexp1.ToString("ddMMMyy");

                //        pnrcountstgring += "{\"elementManagementPassenger\":{\"reference\":{\"qualifier\":\"PR\",\"number\":\"" + pnrcount + "\"},\"segmentName\":\"NM\"},\"passengerData\":[{\"travellerInformation\":{\"traveller\":{\"surname\":\"" + chdsurname + "\",\"quantity\":1},\"passenger\":[{\"firstName\":\"" + chdfname + "\",\"type\":\"CHD\"";

                //        passportstring += "{\"elementManagementData\":{\"segmentName\":\"SSR\"},\"serviceRequest\":{\"ssr\":{\"type\":\"DOCS\",\"status\":\"HK\",\"quantity\":1,\"freetext\":[\"P-" + chnationalitysplit + "-" + chdpassportno + "-" + chissuesplit + "-" + chdobsplitMMM + "-M-" + chexpsplitMMM + "-" + chdfname + "-" + chdsurname + "\"]}},\"referenceForDataElement\":{\"reference\":[{\"qualifier\":\"PT\",\"number\":\"" + paxcount + "\"}]}},";
                //        j++;
                //        pcount++;
                //        paxcount++;
                //        pnrcount++;
                //        if (pcount >= totalpasscount)
                //        {
                //            pnrcountstgring += " }]}}]}],";
                //            passportstring = passportstring.Substring(0, passportstring.Length - 1);
                //            passportstring += "]}}}";
                //        }
                //        else
                //        {
                //            pnrcountstgring += " }]}}]},";
                //        }
                //    }
                //}


                #endregion
                DataElementsIndiv dataelemensdiv = new DataElementsIndiv();
                ElementManagementData elementManagementData1 = new ElementManagementData();
                elementManagementData1.SegmentName = SegmentEnum.FP.ToString();
                dataelemensdiv.ElementManagementData = elementManagementData1;
                Fop fop = new Fop();
                fop.Identification = "CA";
                FormOfPayment formOfPayment = new FormOfPayment();
                formOfPayment.Fop.Add(fop);
                dataelemensdiv.FormOfPayment = formOfPayment;
                pnrAdd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(dataelemensdiv);


                DataElementsIndiv dataelemensdiv1 = new DataElementsIndiv();
                ElementManagementData elementManagementData2 = new ElementManagementData();
                elementManagementData2.SegmentName = SegmentEnum.AP.ToString();
                dataelemensdiv1.ElementManagementData = elementManagementData2;
                FreetextData freetextData = new FreetextData();
                FreetextDetail freetextDetail = new FreetextDetail();
                freetextDetail.SubjectQualifier = "3";
                freetextDetail.Type = "4";
                freetextData.FreetextDetail = freetextDetail;
                freetextData.LongFreetext = phonenumber;
                dataelemensdiv1.FreetextData = freetextData;
                pnrAdd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(dataelemensdiv1);



                DataElementsIndiv dataelementsdiv4 = new DataElementsIndiv();
                ElementManagementData elementManagementData5 = new ElementManagementData();
                elementManagementData5.SegmentName = SegmentEnum.RF.ToString();
                dataelementsdiv4.ElementManagementData = elementManagementData5;
                FreetextData freetextData2 = new FreetextData();
                FreetextDetail freetextDetail2 = new FreetextDetail();
                freetextDetail2.SubjectQualifier = "3";
                freetextDetail2.Type = "P23";
                freetextData2.FreetextDetail = freetextDetail2;
                freetextData2.LongFreetext = "Example";
                dataelementsdiv4.FreetextData = freetextData2;
                pnrAdd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(dataelementsdiv4);

                DataElementsIndiv dataelemensdiv2 = new DataElementsIndiv();
                ElementManagementData elementManagementData3 = new ElementManagementData();
                elementManagementData3.SegmentName = SegmentEnum.AP.ToString();
                dataelemensdiv2.ElementManagementData = elementManagementData3;
                FreetextData freetextData1 = new FreetextData();
                FreetextDetail freetextDetail1 = new FreetextDetail();
                freetextDetail1.SubjectQualifier = "3";
                freetextDetail1.Type = "4";
                freetextData1.FreetextDetail = freetextDetail1;
                freetextData1.LongFreetext = email;
                dataelemensdiv2.FreetextData = freetextData1;
                pnrAdd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(dataelemensdiv2);


                DataElementsIndiv dataelemensdiv3 = new DataElementsIndiv();
                ElementManagementData elementManagementData4 = new ElementManagementData();
                elementManagementData4.SegmentName = SegmentEnum.TK.ToString();
                dataelemensdiv3.ElementManagementData = elementManagementData4;
                TicketElement ticketElement = new TicketElement();
                Ticket1 ticket = new Ticket1();
                ticket.Indicator = "OK";
                ticketElement.Ticket = ticket;
                dataelemensdiv3.TicketElement = ticketElement;
                pnrAdd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(dataelemensdiv3);



                string loginid = FLbookRQ.BookFlight.SupplierAgencyDetails.First().UserName;
                string password = FLbookRQ.BookFlight.SupplierAgencyDetails.First().Password;
                string uuid = FLbookRQ.BookFlight.SupplierAgencyDetails.First().AccountNumber;

                pnrAdd.LoginId = loginid;
                pnrAdd.Password = password;
                pnrAdd.UuId = uuid;
                #region--OLDCODE---
                StringBuilder sbpnradd = new StringBuilder();

                sbpnradd.Append("{");
                sbpnradd.Append("  \"pnrAddMultiElements\": {");
                sbpnradd.Append("    \"reservationInfo\": { \"reservation\": { } },");
                sbpnradd.Append("    \"pnrActions\": { \"optionCode\": [ 0 ] },");
                sbpnradd.Append("    \"travellerInfo\": [");

                //sbpnradd.Append("      {");
                //sbpnradd.Append("        \"elementManagementPassenger\": {");
                //sbpnradd.Append("          \"reference\": {");
                //sbpnradd.Append("            \"qualifier\": \"PR\",");
                //sbpnradd.Append("            \"number\": \"1\"");
                //sbpnradd.Append("          },");
                //sbpnradd.Append("          \"segmentName\": \"NM\"");
                //sbpnradd.Append("        },");
                //sbpnradd.Append("        \"passengerData\": [");
                //sbpnradd.Append("          {");
                //sbpnradd.Append("            \"travellerInformation\": {");
                //sbpnradd.Append("              \"traveller\": {");
                //sbpnradd.Append("                \"surname\": \"aaaaaaa\",");
                //sbpnradd.Append("                \"quantity\": 1");
                //sbpnradd.Append("              },");
                //sbpnradd.Append("              \"passenger\": [");
                //sbpnradd.Append("                {");
                //sbpnradd.Append("                  \"firstName\": \"aaaaaaaaa\",");
                //sbpnradd.Append("                  \"type\": \"ADT\"");
                //sbpnradd.Append("                }");
                //sbpnradd.Append("              ]");
                //sbpnradd.Append("            }");
                //sbpnradd.Append("          }");
                //sbpnradd.Append("        ]");
                //sbpnradd.Append("      }");
                //sbpnradd.Append("    ],");
                //      sbpnradd.Append(pnrcountstgring);





                //sbpnradd.Append("    \"dataElementsMaster\": {");
                //sbpnradd.Append("      \"marker1\": { },");
                //sbpnradd.Append("      \"dataElementsIndiv\": [");

                //sbpnradd.Append("        {");
                //sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"FP\" },");
                //sbpnradd.Append("          \"formOfPayment\": { \"fop\": [ { \"identification\": \"CA\" } ] }");
                //sbpnradd.Append("        },");
                //sbpnradd.Append("        {");
                //sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"RF\" },");
                //sbpnradd.Append("          \"freetextData\": {");
                //sbpnradd.Append("            \"freetextDetail\": {");
                //sbpnradd.Append("              \"subjectQualifier\": \"3\",");
                //sbpnradd.Append("              \"type\": \"P23\"");
                //sbpnradd.Append("            },");
                //sbpnradd.Append("            \"longFreetext\": \"Example\"");
                //sbpnradd.Append("          }");
                //sbpnradd.Append("        },");

                //sbpnradd.Append("        {");
                //sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"AP\" },");
                //sbpnradd.Append("          \"freetextData\": {");
                //sbpnradd.Append("            \"freetextDetail\": {");
                //sbpnradd.Append("              \"subjectQualifier\": \"3\",");
                //sbpnradd.Append("              \"type\": \"4\"");
                //sbpnradd.Append("            },");
                //sbpnradd.Append("            \"longFreetext\": \"" + phonenumber + "\"");
                //sbpnradd.Append("          }");
                //sbpnradd.Append("        },");
                //sbpnradd.Append("        {");
                //sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"AP\" },");
                //sbpnradd.Append("          \"freetextData\": {");
                //sbpnradd.Append("            \"freetextDetail\": {");
                //sbpnradd.Append("              \"subjectQualifier\": \"3\",");
                //sbpnradd.Append("              \"type\": \"4\"");
                //sbpnradd.Append("            },");
                //sbpnradd.Append("            \"longFreetext\": \"" + email + "\"");
                //sbpnradd.Append("          }");
                //sbpnradd.Append("        },");

                //sbpnradd.Append("        {");
                //sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"TK\" },");
                //sbpnradd.Append("          \"ticketElement\": { \"ticket\": { \"indicator\": \"OK\" } }");
                //sbpnradd.Append("        },");

                //sbpnradd.Append("        {");
                //sbpnradd.Append("          \"elementManagementData\": { \"segmentName\": \"SSR\" },");
                //sbpnradd.Append("          \"serviceRequest\": {");
                //sbpnradd.Append("            \"ssr\": {");
                //sbpnradd.Append("              \"type\": \"DOCS\",");
                //sbpnradd.Append("              \"status\": \"HK\",");
                //sbpnradd.Append("              \"quantity\": 1,");
                //sbpnradd.Append("              \"freetext\": [ \"P-AND-aaaaaaa-AND-01Feb03-M-01Mar19-aaaaaaaaa-aaaaaaa\" ]");
                //sbpnradd.Append("            }");
                //sbpnradd.Append("          },");
                //sbpnradd.Append("          \"referenceForDataElement\": {");
                //sbpnradd.Append("            \"reference\": [");
                //sbpnradd.Append("              {");
                //sbpnradd.Append("                \"qualifier\": \"PT\",");
                //sbpnradd.Append("                \"number\": \"2\"");
                //sbpnradd.Append("              }");
                //sbpnradd.Append("            ]");
                //sbpnradd.Append("          }");
                //sbpnradd.Append("        }");
                //sbpnradd.Append("      ]");
                //sbpnradd.Append("    }");
                //sbpnradd.Append("  }");
                //sbpnradd.Append("}");
                // sbpnradd.Append(passportstring);

                string test = sbpnradd.ToString();
                #endregion
                //string RQUrl = FLbookRQ.BookFlight.SupplierAgencyDetailsBook[0].BaseUrl + "/pnradd";
                //string RQUrl = "http://192.168.1.105:9000/pnradd";
                string RQUrl = "http://70.32.24.42:9000/pnradd";
                //string RQUrl = "http://192.168.1.102:9000/pnradd";

                var jsonString = PnrAddSerialize.ToJson(pnrAdd);

                string strResult = await soapHelperAsync.SendRESTRequestAsync(jsonString, RQUrl, "POST");
                string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(pnrAdd);
                CommonServices.SaveLog("PNRADD", FLbookRQ.BookFlight.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, strResult);

                // string strResult = SOAPHelper.SendRESTRequest(sbpnradd.ToString(), RQUrl, "POST");

                return strResult;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        // 3.3 farepricePNR Booking Class

        public string farepricePNR_1(FLBookRQ FLbookRQ)
        {
            StringBuilder sbfarepricePNR = new StringBuilder();
            sbfarepricePNR.Append("{\"farePricePNRWithBookingClass\":{\"pricingOptionGroup\":[{\"pricingOptionKey\":{\"pricingOptionKey\":\"RP\"}},{\"pricingOptionKey\":{\"pricingOptionKey\":\"RLO\"}},{\"pricingOptionKey\":{\"pricingOptionKey\":\"FCO\"},\"currency\":{\"firstCurrencyDetails\":{\"currencyQualifier\":\"FCO\",\"currencyIsoCode\":\"AED\"}}}]}}");

            //string RQUrl = FLbookRQ.BookFlight.SupplierAgencyDetailsBook[0].BaseUrl + "/farepricePNR";
            string RQUrl = "http://70.32.24.42:9000/farepricePNR";
            //string RQUrl = "http://192.168.1.105:9000/farepricePNR";
            string strResult = SOAPHelper.SendRESTRequest(sbfarepricePNR.ToString(), RQUrl, "POST");

            return strResult;
        }
        public async Task<string> farepricePNR(FLBookRQ FLbookRQ)
        {


            BookingClass bookingClass = new BookingClass();
            FarePricePnrWithBookingClass farePricePnrWithBookingClass1 = new FarePricePnrWithBookingClass();
            PricingOptionGroup pricingOptionGroup = new PricingOptionGroup();
            PricingOptionKey pricing = new PricingOptionKey();
            pricing.PurplePricingOptionKey = "RP";
            pricingOptionGroup.PricingOptionKey = pricing;
            farePricePnrWithBookingClass1.PricingOptionGroup.Add(pricingOptionGroup);

            PricingOptionGroup pricingOptionGroup1 = new PricingOptionGroup();
            PricingOptionKey pricing1 = new PricingOptionKey();
            pricing1.PurplePricingOptionKey = "RLO";
            pricingOptionGroup1.PricingOptionKey = pricing1;
            farePricePnrWithBookingClass1.PricingOptionGroup.Add(pricingOptionGroup1);

            //bookingClass.FarePricePnrWithBookingClass.PricingOptionGroup.Add(pricingOptionGroup);
            //bookingClass.FarePricePnrWithBookingClass.PricingOptionGroup.Add(pricingOptionGroup1);


            PricingOptionGroup pricingOptionGroup2 = new PricingOptionGroup();
            PricingOptionKey pricing2 = new PricingOptionKey();
            var currency = new Currency();
            currency.FirstCurrencyDetails.CurrencyIsoCode = FLbookRQ.BookFlight.SupplierAgencyDetails.First().ToCurrency;
            currency.FirstCurrencyDetails.CurrencyQualifier = CurrencyEnum.FCO.ToString();
            pricingOptionGroup2.Currency = currency;
            pricing2.PurplePricingOptionKey = CurrencyEnum.FCO.ToString();
            pricingOptionGroup2.PricingOptionKey = pricing2;

            farePricePnrWithBookingClass1.PricingOptionGroup.Add(pricingOptionGroup2);
            bookingClass.FarePricePnrWithBookingClass = farePricePnrWithBookingClass1;
            //bookingClass.FarePricePnrWithBookingClass.PricingOptionGroup.Add(pricingOptionGroup2);

            string loginid = FLbookRQ.BookFlight.SupplierAgencyDetails.First().UserName;
            string password = FLbookRQ.BookFlight.SupplierAgencyDetails.First().Password;
            string uuid = FLbookRQ.BookFlight.SupplierAgencyDetails.First().AccountNumber;

            bookingClass.LoginId = loginid;
            bookingClass.Password = password;
            bookingClass.UuId = uuid;
            //var jsonString = BookingClassSerialize.ToJson(bookingClass);

            var jsonString = JsonConvert.SerializeObject(bookingClass,
                            Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });

            StringBuilder sbfarepricePNR = new StringBuilder();
            //      sbfarepricePNR.Append("{\"farePricePNRWithBookingClass\":{\"pricingOptionGroup\":[{\"pricingOptionKey\":{\"pricingOptionKey\":\"RP\"}},{\"pricingOptionKey\":{\"pricingOptionKey\":\"RLO\"}},{\"pricingOptionKey\":{\"pricingOptionKey\":\"FCO\"},\"currency\":{\"firstCurrencyDetails\":{\"currencyQualifier\":\"FCO\",\"currencyIsoCode\":\"AED\"}}}]}}");

            //string RQUrl = FLbookRQ.BookFlight.SupplierAgencyDetailsBook[0].BaseUrl + "/farepricePNR";
            string RQUrl = "http://70.32.24.42:9000/farepricePNR";
            //string RQUrl = "http://192.168.1.105:9000/farepricePNR";
            string strResult = await soapHelperAsync.SendRESTRequestAsync(jsonString, RQUrl, "POST");
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(bookingClass);
            CommonServices.SaveLog("BOOKINGCLASS", FLbookRQ.BookFlight.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, strResult);
            //     string strResult = SOAPHelper.SendRESTRequest(sbfarepricePNR.ToString(), RQUrl, "POST");
            return strResult;
        }


        public string ticketcreate_1(FLBookRQ FLbookRQ)
        {


            var ADT_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == "ADT").ToList<TravelerInfo>();
            var CHD_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == "CHD").ToList<TravelerInfo>();
            var INF_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == "INF").ToList<TravelerInfo>();

            int adtcount = ADT_Items.Count;
            int chdcount = CHD_Items.Count;
            int infcount = INF_Items.Count;

            int paxtypecount = 0;
            if (adtcount > 0)
            {
                paxtypecount++;
            }
            if (chdcount > 0)
            {
                paxtypecount++;
            }
            if (infcount > 0)
            {
                paxtypecount++;
            }





            StringBuilder sbticketcreate = new StringBuilder();

            sbticketcreate.Append("{");
            sbticketcreate.Append("  \"ticketCreateTSTFromPricing\": {");
            sbticketcreate.Append("    \"psaList\": [");

            for (int it = 1; it <= paxtypecount; it++)
            {
                sbticketcreate.Append("      {");
                sbticketcreate.Append("        \"itemReference\": {");
                sbticketcreate.Append("          \"referenceType\": \"TST\",");
                sbticketcreate.Append("          \"uniqueReference\": \"" + it + "\"");
                sbticketcreate.Append("        }");
                sbticketcreate.Append("      }");
                if (it != paxtypecount)
                {
                    sbticketcreate.Append("       ,");
                }
            }

            sbticketcreate.Append("    ]");
            sbticketcreate.Append("  }");
            sbticketcreate.Append("}");

            //string RQUrl = FLbookRQ.BookFlight.SupplierAgencyDetailsBook[0].BaseUrl + "/ticketcreate";
            string RQUrl = "http://70.32.24.42:9000/ticketcreate";
            // string RQUrl = "http://192.168.1.105:9000/ticketcreate";
            string strResult = SOAPHelper.SendRESTRequest(sbticketcreate.ToString(), RQUrl, "POST");
            return strResult;
        }
        //   3.4 ticketcreate
        public async Task<string> ticketcreate(FLBookRQ FLbookRQ)
        {

            TicketCreate ticketCreate = new TicketCreate();
            var ADT_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == PaxTypeEnum.ADT.ToString()).ToList<TravelerInfo>();
            var CHD_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == PaxTypeEnum.CHD.ToString()).ToList<TravelerInfo>();
            var INF_Items = FLbookRQ.BookFlight.TravelerInfo.Where(x => x.PassengerTypeQuantity == PaxTypeEnum.INF.ToString()).ToList<TravelerInfo>();

            int adtcount = ADT_Items.Count;
            int chdcount = CHD_Items.Count;
            int infcount = INF_Items.Count;

            int paxtypecount = 0;
            if (adtcount > 0)
            {
                paxtypecount++;
            }
            if (chdcount > 0)
            {
                paxtypecount++;
            }
            if (infcount > 0)
            {
                paxtypecount++;
            }
            PsaList psalist = new PsaList();
            var psalists = ticketCreate.TicketCreateTstFromPricing.PsaList;
            ItemReference itemReference = new ItemReference();

            for (int it = 1; it <= paxtypecount; it++)
            {
                psalist = new PsaList();
                psalist.ItemReference.ReferenceType = "TST";
                psalist.ItemReference.UniqueReference = it;
                psalists.Add(psalist);
            }



            string loginid = FLbookRQ.BookFlight.SupplierAgencyDetails.First().UserName;
            string password = FLbookRQ.BookFlight.SupplierAgencyDetails.First().Password;
            string uuid = FLbookRQ.BookFlight.SupplierAgencyDetails.First().AccountNumber;

            ticketCreate.LoginId = loginid;
            ticketCreate.Password = password;
            ticketCreate.UuId = uuid;
            var jsonString = TicketCreateSerialize.ToJson(ticketCreate);
            //StringBuilder sbticketcreate = new StringBuilder();

            //sbticketcreate.Append("{");
            //sbticketcreate.Append("  \"ticketCreateTSTFromPricing\": {");
            //sbticketcreate.Append("    \"psaList\": [");

            //for (int it = 1; it <= paxtypecount; it++)
            //{
            //    sbticketcreate.Append("      {");
            //    sbticketcreate.Append("        \"itemReference\": {");
            //    sbticketcreate.Append("          \"referenceType\": \"TST\",");
            //    sbticketcreate.Append("          \"uniqueReference\": \"" + it + "\"");
            //    sbticketcreate.Append("        }");
            //    sbticketcreate.Append("      }");
            //    if (it != paxtypecount)
            //    {
            //        sbticketcreate.Append("       ,");
            //    }
            //}

            //sbticketcreate.Append("    ]");
            //sbticketcreate.Append("  }");
            //sbticketcreate.Append("}");

            //string RQUrl = FLbookRQ.BookFlight.SupplierAgencyDetailsBook[0].BaseUrl + "/ticketcreate";
            string RQUrl = "http://70.32.24.42:9000/ticketcreate";
            // string RQUrl = "http://192.168.1.105:9000/ticketcreate";
            string strResult = await soapHelperAsync.SendRESTRequestAsync(jsonString, RQUrl, "POST");
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(ticketCreate);
            CommonServices.SaveLog("TKTCREATE", FLbookRQ.BookFlight.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, strResult);
            //string strResult = SOAPHelper.SendRESTRequest(jsonString, RQUrl, "POST");
            return strResult;
        }


        public string PNRcommit_1(FLBookRQ FLbookRQ)
        {
            //string RQUrl = FLbookRQ.BookFlight.SupplierAgencyDetailsBook[0].BaseUrl + "/pnradd";
            string RQUrl = "http://70.32.24.42:9000/pnradd";
            //  string RQUrl = "http://192.168.1.105:9000/pnradd";
            StringBuilder sbPNRcommit = new StringBuilder();
            sbPNRcommit.Append("{\"pnrAddMultiElements\":{\"pnrActions\":{\"optionCode\":[10]},\"dataElementsMaster\":{\"marker1\":{},\"dataElementsIndiv\":[{\"elementManagementData\":{\"reference\":{\"qualifier\":\"OT\",\"number\":\"1\"},\"segmentName\":\"AMU\"},\"freetextData\":{\"freetextDetail\":{\"subjectQualifier\":\"3\",\"type\":\"6\"},\"longFreetext\":\"04.92.94.70.00 - AGCY\"}}]}}}");
            string strResult = SOAPHelper.SendRESTRequest(sbPNRcommit.ToString(), RQUrl, "POST");
            return strResult;
        }
        //   3.5 PNRcommit
        public async Task<string> PNRcommit(FLBookRQ FLbookRQ)
        {

            PnrAdd pnradd = new global::PnrAdd();
            DataElementsIndiv dataElementsIndiv = new DataElementsIndiv();
            ElementManagementData elementManagementData1 = new ElementManagementData();
            elementManagementData1.SegmentName = "AMU";
            Reference reference = new Reference();
            reference.Qualifier = "OT";
            reference.Number = "1";
            elementManagementData1.Reference = reference;
            //elementManagementData1.Reference.Qualifier = "OT";
            //elementManagementData1.Reference.Number = "1";
            dataElementsIndiv.ElementManagementData = elementManagementData1;

            FreetextData freetextData = new FreetextData();
            freetextData.FreetextDetail.SubjectQualifier = "3";
            freetextData.FreetextDetail.Type = "6";
            freetextData.LongFreetext = "04.92.94.70.00 - AGCY";
            dataElementsIndiv.FreetextData = freetextData;
            pnradd.PnrAddMultiElements.PnrActions.OptionCode.Add(10);
            pnradd.PnrAddMultiElements.DataElementsMaster.DataElementsIndiv.Add(new DataElementsIndiv()
            {


                ElementManagementData = elementManagementData1,
                FreetextData = freetextData

            });


            string loginid = FLbookRQ.BookFlight.SupplierAgencyDetails.First().UserName;
            string password = FLbookRQ.BookFlight.SupplierAgencyDetails.First().Password;
            string uuid = FLbookRQ.BookFlight.SupplierAgencyDetails.First().AccountNumber;

            pnradd.LoginId = loginid;
            pnradd.Password = password;
            pnradd.UuId = uuid;

            string jsonString = PnrAddSerialize.ToJson(pnradd);
            //string RQUrl = FLbookRQ.BookFlight.SupplierAgencyDetailsBook[0].BaseUrl + "/pnradd";
            string RQUrl = "http://70.32.24.42:9000/pnradd";
            //  string RQUrl = "http://192.168.1.105:9000/pnradd";
            StringBuilder sbPNRcommit = new StringBuilder();
            sbPNRcommit.Append("{\"pnrAddMultiElements\":{\"pnrActions\":{\"optionCode\":[10]},\"dataElementsMaster\":{\"marker1\":{},\"dataElementsIndiv\":[{\"elementManagementData\":{\"reference\":{\"qualifier\":\"OT\",\"number\":\"1\"},\"segmentName\":\"AMU\"},\"freetextData\":{\"freetextDetail\":{\"subjectQualifier\":\"3\",\"type\":\"6\"},\"longFreetext\":\"04.92.94.70.00 - AGCY\"}}]}}}");

            string strResult = await soapHelperAsync.SendRESTRequestAsync(jsonString, RQUrl, "POST");
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(pnradd);
            CommonServices.SaveLog("PNRCOMMIT", FLbookRQ.BookFlight.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, strResult);
            // string strResult = SOAPHelper.SendRESTRequest(sbPNRcommit.ToString(), RQUrl, "POST");
            return strResult;
        }

        //4. Ticket Issue
        public async Task<IssueTicketComRS> IssueTicket(IssueTicketComRq IssueTicketComRQ)
        {

            string PNR = IssueTicketComRQ.TicketCreateTstFromPricing.UniqueId;
            string BookingID = IssueTicketComRQ.TicketCreateTstFromPricing.BookingRefId;
            string pnrRetrieveRS = await pnrRetrieve(IssueTicketComRQ, PNR);
            string docissuanceRS = await docissuance(IssueTicketComRQ);
            string pnrRetrieveafterTktRS = await pnrRetrieve(IssueTicketComRQ, PNR);
            return IssueTickettoCom(pnrRetrieveafterTktRS, BookingID);
        }

        //public IssueTicketComRS IssueTickettoCom_1(string pnrRetrieveafterTktRS, string BookingID)
        //{
        //    var response = JsonConvert.DeserializeObject<pnrRetrieve_AM_RS>(pnrRetrieveafterTktRS);

        //    IssueTicketComRS _IssueTicketComRS = new IssueTicketComRS();

        //    Tripdetailsresult tripdetailsresult = new Tripdetailsresult()
        //    {
        //        bookingId = BookingID,
        //        Success = "NA",
        //        Target = "NA",
        //        TicketStatus = "Ticketed",
        //        BookingStatus = "NA",
        //        UniqueID = "NA"

        //    };

        //    int totalerros = 1;
        //    Tkerror[] tkerrors = new Tkerror[totalerros];
        //    Tkerror tkerror = new Tkerror();
        //    if (totalerros > 0)
        //    {
        //        for (int Er = 0; Er < totalerros; Er++)
        //        {
        //            tkerror = new Tkerror()
        //            {
        //                Code = "NA",
        //                Meassage = "NA"
        //            };
        //            tkerrors[Er] = tkerror;
        //        }
        //    }
        //    tripdetailsresult.TKErrors = tkerrors;

        //    var tktnum_array = response.pnrReply.dataElementsMaster.dataElementsIndiv.Where(x => x.elementManagementData.segmentName == "FA").ToList<Dataelementsindiv>();
        //    int totalitineries = response.pnrReply.travellerInfo.Length;
        //    int totalpass = response.pnrReply.tstData.Length;
        //    Itineraryinformation[] itineraryinfos = new Itineraryinformation[tktnum_array.Count];
        //    Itineraryinformation itineraryinfo = new Itineraryinformation();
        //    Customerinformation customerinfo = new Customerinformation();
        //    Etickets etickets = new Etickets();
        //    Itinerarypricing itinerarypricing = new Itinerarypricing();

        //    for (int FLitin = 0; FLitin < totalitineries; FLitin++)
        //    {
        //        customerinfo = new Customerinformation();
        //        itineraryinfo = new Itineraryinformation();
        //        BusinessEntities.Age age = new BusinessEntities.Age()
        //        {
        //            Months = "NA",
        //            Years = "NA"
        //        };
        //        customerinfo.Age = age;
        //        //    string DOB = airTripDetailsRS.TravelItinerary.ItineraryInfo.CustomerInfos[FLitin].Customer.DateOfBirth.ToString();
        //        //    DOB = Convert.ToDateTime(DOB).ToString("dd-MM-yyyy");
        //        //    string PassPortEX = airTripDetailsRS.TravelItinerary.ItineraryInfo.CustomerInfos[FLitin].Customer.PassportExpiresOn.ToString();
        //        //    PassPortEX = Convert.ToDateTime(PassPortEX).ToString("dd-MM-yyyy");



        //        Paxdetails paxdetails = new Paxdetails()
        //        {
        //            PassengerFirstName = response.pnrReply.travellerInfo[FLitin].passengerData[0].travellerInformation.passenger[0].firstName,
        //            PassengerLastName = response.pnrReply.travellerInfo[FLitin].passengerData[0].travellerInformation.traveller.surname,
        //            Title = "NA",
        //            DateOfBirth = "NA",
        //            EmailAddress = "NA",
        //            Gender = "NA",
        //            KnownTravelerNo = "NA",
        //            NameNumber = "NA",
        //            NationalID = "NA",
        //            PassengerNationality = "NA",
        //            PassengerType = response.pnrReply.travellerInfo[FLitin].passengerData[0].travellerInformation.passenger[0].type,
        //            PassportExpiresOn = "NA",
        //            PassportIssueCountry = "NA",
        //            PassportNationality = "NA",
        //            PassportNumber = "NA",
        //            PhoneNumber = "NA",
        //            PostCode = "NA",
        //        };
        //        customerinfo.Paxdetails = paxdetails;



        //        itineraryinfo.CustomerInformation = customerinfo;
        //        string tkt = tktnum_array[FLitin].otherDataFreetext[0].longFreetext;
        //        tkt = tkt.Substring(4, 14);

        //        etickets = new Etickets()
        //        {
        //            ItemRPH = "NA",
        //            eTicketNumber = tkt,
        //            SSRs = "NA"
        //        };
        //        itineraryinfo.ETickets = etickets;
        //        itinerarypricing = new Itinerarypricing()
        //        {
        //            EquiFare = "NA",
        //            Tax = "NA",
        //            TotalFare = "NA",
        //            currency = "NA"
        //        };
        //        itineraryinfo.ItineraryPricing = itinerarypricing;
        //        itineraryinfos[FLitin] = itineraryinfo;



        //    }


        //    for (int FLpax = 0; FLpax < totalitineries; FLpax++)
        //    {
        //        int infcount = FLpax + totalitineries;

        //        var Paxtype = response.pnrReply.travellerInfo[FLpax].passengerData[0].travellerInformation.passenger[0].type;
        //        int typecnt = response.pnrReply.travellerInfo[FLpax].passengerData.Length;
        //        if (Paxtype == "ADT")
        //        {
        //            if (typecnt == 2)
        //            {
        //                customerinfo = new Customerinformation();
        //                itineraryinfo = new Itineraryinformation();
        //                BusinessEntities.Age age = new BusinessEntities.Age()
        //                {
        //                    Months = "NA",
        //                    Years = "NA"
        //                };
        //                customerinfo.Age = age;

        //                Paxdetails paxdetails = new Paxdetails()
        //                {
        //                    PassengerFirstName = response.pnrReply.travellerInfo[FLpax].passengerData[1].travellerInformation.passenger[0].firstName,
        //                    PassengerLastName = response.pnrReply.travellerInfo[FLpax].passengerData[1].travellerInformation.traveller.surname,
        //                    Title = "NA",
        //                    DateOfBirth = "NA",
        //                    EmailAddress = "NA",
        //                    Gender = "NA",
        //                    KnownTravelerNo = "NA",
        //                    NameNumber = "NA",
        //                    NationalID = "NA",
        //                    PassengerNationality = "NA",
        //                    PassengerType = response.pnrReply.travellerInfo[FLpax].passengerData[1].travellerInformation.passenger[0].type,
        //                    PassportExpiresOn = "NA",
        //                    PassportIssueCountry = "NA",
        //                    PassportNationality = "NA",
        //                    PassportNumber = "NA",
        //                    PhoneNumber = "NA",
        //                    PostCode = "NA",
        //                };
        //                customerinfo.Paxdetails = paxdetails;



        //                itineraryinfo.CustomerInformation = customerinfo;
        //                string tkt = tktnum_array[infcount].otherDataFreetext[0].longFreetext;
        //                tkt = tkt.Substring(4, 14);

        //                etickets = new Etickets()
        //                {
        //                    ItemRPH = "NA",
        //                    eTicketNumber = tkt,
        //                    SSRs = "NA"
        //                };
        //                itineraryinfo.ETickets = etickets;
        //                itinerarypricing = new Itinerarypricing()
        //                {
        //                    EquiFare = "NA",
        //                    Tax = "NA",
        //                    TotalFare = "NA",
        //                    currency = "NA"
        //                };
        //                itineraryinfo.ItineraryPricing = itinerarypricing;
        //                itineraryinfos[infcount] = itineraryinfo;

        //            }
        //        }

        //    }

        //    tripdetailsresult.ItineraryInformation = itineraryinfos;

        //    int totalreserved = response.pnrReply.originDestinationDetails[0].itineraryInfo.Length;
        //    Reservationitem reservationitem = new Reservationitem();
        //    Reservationitem[] reservationitems = new Reservationitem[totalreserved];
        //    for (int FLRes = 0; FLRes < totalreserved; FLRes++)
        //    {
        //        string ArrDate = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].travelProduct.product.arrDate;
        //        string DepDate = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].travelProduct.product.depDate;
        //        reservationitem = new Reservationitem()
        //        {
        //            AirEquipmentType = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].flightDetail.productDetails.equipment,
        //            AirlinePNR = "NA",
        //            ArrivalCode = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].travelProduct.offpointDetail.cityCode,
        //            ArrivalDateTime = ArrDate,
        //            ArrivalTer = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].flightDetail.arrivalStationInfo.terminal,
        //            Baggage = "NA",
        //            CabinClassText = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].travelProduct.productDetails.classOfService,
        //            DepartureCode = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].travelProduct.boardpointDetail.cityCode,
        //            DepartureDateTime = DepDate,
        //            DepartureTer = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].flightDetail.departureInformation.departTerminal,
        //            FlightNumber = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].travelProduct.productDetails.identification,
        //            ItemRPH = "NA",
        //            JDuration = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].flightDetail.productDetails.duration,
        //            MarketingCode = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].travelProduct.companyDetail.identification,
        //            Totalpax = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].relatedProduct.quantity.ToString(),
        //            OperatingCode = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].travelProduct.companyDetail.identification,
        //            StopQuantity = response.pnrReply.originDestinationDetails[0].itineraryInfo[FLRes].flightDetail.productDetails.numOfStops.ToString()
        //        };
        //        reservationitems[FLRes] = reservationitem;
        //    }
        //    tripdetailsresult.ReservationItem = reservationitems;

        //    double basefare = 0;
        //    double total = 0;
        //    int numMonetory = response.pnrReply.tstData[0].fareData.monetaryInfo.Length;
        //    if (numMonetory >= 3)
        //    {
        //        basefare = Convert.ToDouble(response.pnrReply.tstData[0].fareData.monetaryInfo[1].amount);
        //        total = Convert.ToDouble(response.pnrReply.tstData[0].fareData.monetaryInfo[2].amount);
        //    }
        //    else
        //    {
        //        basefare = Convert.ToDouble(response.pnrReply.tstData[0].fareData.monetaryInfo[0].amount);
        //        total = Convert.ToDouble(response.pnrReply.tstData[0].fareData.monetaryInfo[1].amount);
        //    }

        //    Triptotalfare triptotalfare = new Triptotalfare()
        //    {
        //        EquiFare = basefare.ToString(),
        //        Tax = Convert.ToString(total - basefare),
        //        TotalFare = total.ToString(),
        //        Currency = response.pnrReply.tstData[0].fareData.monetaryInfo[1].currencyCode
        //    };
        //    tripdetailsresult.TripTotalFare = triptotalfare;
        //    _IssueTicketComRS.TripDetailsResult = tripdetailsresult;

        //    return _IssueTicketComRS;
        //}
        public IssueTicketComRS IssueTickettoCom(string pnrRetrieveafterTktRS, string BookingID)
        {
            //var response = PnrRetrieveAmRs.FromJson(pnrRetrieveafterTktRS);
            var response = JsonConvert.DeserializeObject<PnrRetrieveAmRs>(pnrRetrieveafterTktRS);
            string PNR = response.PnrReply.PnrHeader[0].ReservationInfo.Reservation[0].ControlNumber;
            IssueTicketComRS _IssueTicketComRS = new IssueTicketComRS();

            TripDetailsResult tripdetailsresult = new TripDetailsResult()
            {
                BookingId = BookingID,
                Success = "",
                Target ="",
                TicketStatus = "Ticketed",
                BookingStatus = "Ticketed",
                UniqueId = ""

            };

            int totalerros = 1;
            var tk = new List<TkErrors>();
            //TkErrors[] tkerrors = new Tkerror[totalerros];
            //Tkerror tkerror = new Tkerror();
            if (totalerros > 0)
            {
                for (int Er = 0; Er < totalerros; Er++)
                {
                    tk.Add(new TkErrors()
                    {
                        Code = "",
                        Meassage =""

                    });
                    //tkerror = new Tkerror()
                    //{
                    //    Code = "NA",
                    //    Meassage = "NA"
                    //};
                    //tkerrors[Er] = tkerror;
                }
            }


            tripdetailsresult.TkErrors = tk;
            _IssueTicketComRS.TripDetailsResult = tripdetailsresult;
            var tktnum_array = response.PnrReply.DataElementsMaster.DataElementsIndiv.Where(x => x.ElementManagementData1.SegmentName == "FA").ToList();
            int totalitineries = response.PnrReply.TravellerInfo.Count;
            int totalpass = response.PnrReply.TstData.Count;
            var itineraryinfos = _IssueTicketComRS.TripDetailsResult.ItineraryInformation;
            //  Itineraryinformation[] itineraryinfos = new Itineraryinformation[tktnum_array.Count];
            ItineraryInformation itineraryinfo = new ItineraryInformation();
            CustomerInformation customerinfo = new CustomerInformation();
            ETickets etickets = new ETickets();
            ItineraryPricing itinerarypricing = new ItineraryPricing();

            //  for (int FLitin = 0; FLitin < totalitineries; FLitin++)
            int FLitin = 0;
            response.PnrReply.TravellerInfo.ForEach(it =>
          {



              customerinfo = new CustomerInformation();
              itineraryinfo = new ItineraryInformation();
              BusinessEntities.Age age = new BusinessEntities.Age()
              {
                  Months = "",
                  Years = ""
              };

              //customerinfo.Age.Months = CheckNA.NA.ToString();
              //customerinfo.Age.Years = CheckNA.NA.ToString();
              //    string DOB = airTripDetailsRS.TravelItinerary.ItineraryInfo.CustomerInfos[FLitin].Customer.DateOfBirth.ToString();
              //    DOB = Convert.ToDateTime(DOB).ToString("dd-MM-yyyy");
              //    string PassPortEX = airTripDetailsRS.TravelItinerary.ItineraryInfo.CustomerInfos[FLitin].Customer.PassportExpiresOn.ToString();
              //    PassPortEX = Convert.ToDateTime(PassPortEX).ToString("dd-MM-yyyy");


              //      Paxdetails paxdetails = new Paxdetails()
              //{
              //customerinfo.Paxdetails.PassengerFirstName = response.PnrReply.TravellerInfo[FLitin].PassengerData[0].TravellerInformation.Passenger[0].FirstName;
              //customerinfo.Paxdetails.PassengerLastName = response.PnrReply.TravellerInfo[FLitin].PassengerData[0].TravellerInformation.Traveller.Surname;
              //customerinfo.Paxdetails.Title = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.DateOfBirth = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.EmailAddress = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.Gender = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.KnownTravelerNo = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.NameNumber = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.NationalId = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.PassengerNationality = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.PassengerType = response.PnrReply.TravellerInfo[FLitin].PassengerData[0].TravellerInformation.Passenger[0].Type;
              //customerinfo.Paxdetails.PassportExpiresOn = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.PassportIssueCountry = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.PassportNationality = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.PassportNumber = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.PhoneNumber = CheckNA.NA.ToString();
              //customerinfo.Paxdetails.PostCode = CheckNA.NA.ToString();

              Paxdetails paxdetails = new Paxdetails()
              {
                  PassengerFirstName = response.PnrReply.TravellerInfo[FLitin].PassengerData[0].TravellerInformation.Passenger[0].FirstName,
                  PassengerLastName = response.PnrReply.TravellerInfo[FLitin].PassengerData[0].TravellerInformation.Traveller.Surname,
                  Title = "",
                  DateOfBirth = "",
                  EmailAddress = "",
                  Gender ="",
                  KnownTravelerNo = "",
                  NameNumber = "",
                  NationalId ="",
                  PassengerNationality = "",
                  PassengerType = response.PnrReply.TravellerInfo[FLitin].PassengerData[0].TravellerInformation.Passenger[0].Type,
                  PassportIssueCountry = "",
                  PassportNationality ="",
                  PassportNumber = "",
                  PhoneNumber ="",
                  PostCode = ""
              };
              customerinfo.Paxdetails = paxdetails;



              itineraryinfo.CustomerInformation = customerinfo;
              string tkt = tktnum_array[FLitin].OtherDataFreetext[0].LongFreetext;
              tkt = tkt.Substring(4, 14);

              etickets = new ETickets()
              {
                  ItemRph = "",
                  ETicketNumber = tkt,
                  SsRs = ""
              };
              itineraryinfo.ETickets = etickets;
              itinerarypricing = new ItineraryPricing()
              {
                  EquiFare = "",
                  Tax = "",
                  TotalFare ="",
                  Currency = ""
              };
              itineraryinfo.ItineraryPricing = itinerarypricing;
              itineraryinfos.Add(itineraryinfo);

              FLitin++;




              //  //  itineraryinfo.CustomerInformation = customerinfo;
              //  string tkt = tktnum_array[FLitin].OtherDataFreetext[0].LongFreetext;
              //  tkt = tkt.Substring(4, 14);


              //  etickets.ItemRph = CheckNA.NA.ToString();
              //  etickets.ETicketNumber = tkt;
              //  etickets.SsRs = CheckNA.NA.ToString();

              //  //  itineraryinfo.ETickets = etickets;

              //  itinerarypricing.EquiFare = CheckNA.NA.ToString();
              //  itinerarypricing.Tax = CheckNA.NA.ToString();
              //  itinerarypricing.TotalFare = CheckNA.NA.ToString();
              //  itinerarypricing.Currency = CheckNA.NA.ToString();

              ////itineraryinfo.ItineraryPricing = itinerarypricing;
              ////itineraryinfos[FLitin] = itineraryinfo;

              //FLitin++;

          });


            for (int FLpax = 0; FLpax < totalitineries; FLpax++)
            {
                int infcount = FLpax + totalitineries;

                var Paxtype = response.PnrReply.TravellerInfo[FLpax].PassengerData[0].TravellerInformation.Passenger[0].Type;
                int typecnt = response.PnrReply.TravellerInfo[FLpax].PassengerData.Count;
                if (Paxtype == PaxTypeEnum.ADT.ToString())
                {
                    if (typecnt == 2)
                    {
                        ////customerinfo = new CustomerInformation();
                        ////itineraryinfo = new ItineraryInformation();
                        ////BusinessEntities.Age age = new BusinessEntities.Age()
                        ////{

                        //customerinfo.Age.Months = CheckNA.NA.ToString();
                        //customerinfo.Age.Years = CheckNA.NA.ToString();
                        //// };
                        ////  customerinfo.Age = age;


                        customerinfo = new CustomerInformation();
                        itineraryinfo = new ItineraryInformation();
                        BusinessEntities.Age age = new BusinessEntities.Age()
                        {
                            Months = "",
                            Years = ""
                        };

                        //customerinfo.Paxdetails.PassengerFirstName = response.PnrReply.TravellerInfo[1].PassengerData[0].TravellerInformation.Passenger[0].FirstName;
                        //customerinfo.Paxdetails.PassengerLastName = response.PnrReply.TravellerInfo[1].PassengerData[0].TravellerInformation.Traveller.Surname;
                        //customerinfo.Paxdetails.Title = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.DateOfBirth = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.EmailAddress = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.Gender = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.KnownTravelerNo = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.NameNumber = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.NationalId = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.PassengerNationality = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.PassengerType = response.PnrReply.TravellerInfo[1].PassengerData[0].TravellerInformation.Passenger[0].Type;

                        //customerinfo.Paxdetails.PassportExpiresOn = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.PassportIssueCountry = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.PassportNationality = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.PassportNumber = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.PhoneNumber = CheckNA.NA.ToString();
                        //customerinfo.Paxdetails.PostCode = CheckNA.NA.ToString();


                        Paxdetails paxdetails = new Paxdetails()
                        {
                            PassengerFirstName = response.PnrReply.TravellerInfo[FLpax].PassengerData[1].TravellerInformation.Passenger[0].FirstName,
                            PassengerLastName = response.PnrReply.TravellerInfo[FLpax].PassengerData[1].TravellerInformation.Traveller.Surname,
                            Title = "",
                            DateOfBirth = "",
                            EmailAddress = "",
                            Gender = "",
                            KnownTravelerNo = "",
                            NameNumber = "",
                            NationalId = "",
                            PassengerNationality = "",
                            PassengerType = response.PnrReply.TravellerInfo[FLpax].PassengerData[1].TravellerInformation.Passenger[0].Type,
                            PassportIssueCountry = "",
                            PassportNationality = "",
                            PassportNumber = "",
                            PhoneNumber = "",
                            PostCode = ""

                        };
                        customerinfo.Paxdetails = paxdetails;



                        itineraryinfo.CustomerInformation = customerinfo;
                        string tkt = tktnum_array[infcount].OtherDataFreetext[0].LongFreetext;
                        tkt = tkt.Substring(4, 14);

                        ////etickets = new ETickets()
                        ////{
                        //etickets.ItemRph = CheckNA.NA.ToString();
                        //etickets.ETicketNumber = tkt;
                        //etickets.SsRs = CheckNA.NA.ToString();

                        //// itineraryinfo.ETickets = etickets;
                        ////itinerarypricing = new ItineraryPricing()
                        ////{
                        //itinerarypricing.EquiFare = CheckNA.NA.ToString();
                        //itinerarypricing.Tax = CheckNA.NA.ToString();
                        //itinerarypricing.TotalFare = CheckNA.NA.ToString();
                        //itinerarypricing.Currency = CheckNA.NA.ToString();
                        ////};
                        etickets = new ETickets()
                        {
                            ItemRph = "",
                            ETicketNumber = tkt,
                            SsRs = ""
                        };
                        itineraryinfo.ETickets = etickets;
                        itinerarypricing = new ItineraryPricing()
                        {
                            EquiFare = "",
                            Tax = "",
                            TotalFare = "",
                            Currency =""
                        };
                        itineraryinfo.ItineraryPricing = itinerarypricing;
                        itineraryinfos.Add(itineraryinfo);

                    }
                }

            }

            //_IssueTicketComRS.TripDetailsResult.ItineraryInformation.Add(new ItineraryInformation()
            //{
            //    CustomerInformation = customerinfo,
            //    ETickets = etickets,
            //    ItineraryPricing = itinerarypricing

            //});

            int totalreserved = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo.Count;
            var reservationitems = _IssueTicketComRS.TripDetailsResult.ReservationItem;
            string arrterminal = "";
            string deptterminal = "";
            ReservationItem reservationitem = new ReservationItem();
            //Reservationitem[] reservationitems = new Reservationitem[totalreserved];
            for (int FLRes = 0; FLRes < totalreserved; FLRes++)
            {

                string ArrDate = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.Product.ArrDate;
                string DepDate = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.Product.DepDate;
                if (response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.ArrivalStationInfo != null)
                {
                    arrterminal = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.ArrivalStationInfo.Terminal;
                }

                if (response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.DepartureInformation != null)
                {
                    deptterminal = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.DepartureInformation.DepartTerminal;
                }
                reservationitem = new ReservationItem()

                {
                    AirEquipmentType = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.ProductDetails.Equipment,
                    AirlinePnr = PNR,
                    ArrivalCode = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.OffpointDetail.CityCode,
                    ArrivalDateTime = ArrDate,
                    ArrivalTer = arrterminal,
                    Baggage = "",
                    CabinClassText = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.ProductDetails.ClassOfService,
                    DepartureCode = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.BoardpointDetail.CityCode,
                    DepartureDateTime = DepDate,
                    DepartureTer = deptterminal,
                    FlightNumber = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.ProductDetails.Identification,
                    ItemRph = "",
                    JDuration = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.ProductDetails.Duration,
                    MarketingCode = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.CompanyDetail.Identification,
                    Totalpax = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].RelatedProduct.Quantity.ToString(),
                    OperatingCode = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.CompanyDetail.Identification,
                    StopQuantity = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.ProductDetails.NumOfStops.ToString()
                };



                reservationitems.Add(reservationitem);

                tripdetailsresult.ReservationItem = reservationitems;
                // tripdetailsresult.ReservationItem = reservationitems;
            }
            double basefare = 0;
            double total = 0;
            int numMonetory = response.PnrReply.TstData[0].FareData.MonetaryInfo.Count;
            if (numMonetory >= 3)
            {
                basefare = Convert.ToDouble(response.PnrReply.TstData[0].FareData.MonetaryInfo[1].Amount);
                total = Convert.ToDouble(response.PnrReply.TstData[0].FareData.MonetaryInfo[2].Amount);
            }
            else
            {
                basefare = Convert.ToDouble(response.PnrReply.TstData[0].FareData.MonetaryInfo[0].Amount);
                total = Convert.ToDouble(response.PnrReply.TstData[0].FareData.MonetaryInfo[1].Amount);
            }
            _IssueTicketComRS.TripDetailsResult.TripTotalFare.EquiFare = basefare.ToString();
            _IssueTicketComRS.TripDetailsResult.TripTotalFare.Tax = Convert.ToString(total - basefare);
            _IssueTicketComRS.TripDetailsResult.TripTotalFare.TotalFare = total.ToString();
            _IssueTicketComRS.TripDetailsResult.TripTotalFare.Currency = response.PnrReply.TstData[0].FareData.MonetaryInfo[1].CurrencyCode;
            //TripTotalFare triptotalfare = new TripTotalFare()
            //       {
            //           EquiFare = basefare.ToString(),
            //           Tax = Convert.ToString(total - basefare),
            //           TotalFare = total.ToString(),
            //           Currency = response.pnrReply.tstData[0].fareData.monetaryInfo[1].currencyCode
            //       };
            //   tripdetailsresult.TripTotalFare = triptotalfare;
            //  _IssueTicketComRS.TripDetailsResult = tripdetailsresult;

            return _IssueTicketComRS;
        }
        //   4.1 PNR Retrieve

        public string pnrRetrieve_1(string pnr)
        {

            string RQUrl = "http://70.32.24.42:9000/pnrretrieve";
            //string RQUrl = "http://192.168.1.105:9000/pnrretrieve";

            string sbpnrRetrieve = "{" +
            "  \"pnrRetrieve\": {" +
            "    \"retrievalFacts\": {" +
            "      \"retrieve\": { \"type\": 2 }," +
            "      \"reservationOrProfileIdentifier\": { \"reservation\": [ { \"controlNumber\": \"" + pnr + "\" } ] }" +
            "    }" +
            "  }" +
            "}";

            string strResult = SOAPHelper.SendRESTRequest(sbpnrRetrieve.ToString(), RQUrl, "POST");
            return strResult;
        }
        public async Task<string> pnrRetrieve(IssueTicketComRq IssueTicketComRQ, string pnr)
        {



            PnrRetrieve pnrRetrieve = new PnrRetrieve();
            //if (IssueTicketComRQ.SupplierAgencyDetails.Count() != 0)
            //{
            pnrRetrieve.LoginId = IssueTicketComRQ.SupplierAgencyDetails.First().UserName;
            pnrRetrieve.Password = IssueTicketComRQ.SupplierAgencyDetails.First().Password;
            pnrRetrieve.UuId = IssueTicketComRQ.SupplierAgencyDetails.First().AccountNumber;
            // }
            //   else
            //  {
            //    pnrRetrieve.LoginId = TripRQ.SupplierAgencyDetails.First().UserName;
            //    pnrRetrieve.Password = TripRQ.SupplierAgencyDetails.First().Password;
            //    pnrRetrieve.UuId = TripRQ.SupplierAgencyDetails.First().AccountNumber;
            //}

            pnrRetrieve.PurplePnrRetrieve.RetrievalFacts.Retrieve.Type = 2;

            pnrRetrieve.PurplePnrRetrieve.RetrievalFacts.ReservationOrProfileIdentifier.Reservation.Add(new global::Reservation()
            {

                ControlNumber = pnr
            });

            string jsonString = PnrRetrieveSerialize.ToJson(pnrRetrieve);
            string RQUrl = "http://70.32.24.42:9000/pnrretrieve";
            //string RQUrl = "http://192.168.1.105:9000/pnrretrieve";

            string sbpnrRetrieve = "{" +
            "  \"pnrRetrieve\": {" +
            "    \"retrievalFacts\": {" +
            "      \"retrieve\": { \"type\": 2 }," +
            "      \"reservationOrProfileIdentifier\": { \"reservation\": [ { \"controlNumber\": \"" + pnr + "\" } ] }" +
            "    }" +
            "  }" +
            "}";
            string strResult = await soapHelperAsync.SendRESTRequestAsync(jsonString, RQUrl, "POST");
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(pnrRetrieve);
            //if (IssueTicketComRQ.SupplierAgencyDetails.Count() != 0)
            //{
            CommonServices.SaveLog("PNRETREIVE", IssueTicketComRQ.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, strResult);
            // }
            //else
            //{

            //    CommonServices.SaveLog("PNRETREIVE", TripRQ.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, strResult);
            //}
            //   string strResult = SOAPHelper.SendRESTRequest(sbpnrRetrieve.ToString(), RQUrl, "POST");
            return strResult;
        }
        //   4.2 Doc issue

        public string docissuance_1()
        {
            string RQUrl = "http://70.32.24.42:9000/docissuance";
            // string RQUrl = "http://192.168.1.105:9000/docissuance";

            string jsonstring = "{\"docIssuanceIssueTicket\":{\"optionGroup\":[{\"switches\":{\"statusDetails\":{\"indicator\":\"ET\"}}}]}}";

            string strResult = SOAPHelper.SendRESTRequest(jsonstring, RQUrl, "POST");
            return strResult;
        }

        public async Task<string> docissuance(IssueTicketComRq IssueTicketComRQ)
        {

            DocIssue docIssue = new DocIssue()
            {
                LoginId = IssueTicketComRQ.SupplierAgencyDetails.First().UserName,
                Password = IssueTicketComRQ.SupplierAgencyDetails.First().Password,
                UuId = IssueTicketComRQ.SupplierAgencyDetails.First().AccountNumber,
            };

            var switches = new Switches();
            switches.StatusDetails.Indicator = "ET";

            docIssue.DocIssuanceIssueTicket.OptionGroup.Add(new OptionGroup()
            {

                Switches = switches
            });



            var jsonString = DocIssueSerialize.ToJson(docIssue);



            string RQUrl = "http://70.32.24.42:9000/docissuance";
            // string RQUrl = "http://192.168.1.105:9000/docissuance";

            //  string jsonstring = "{\"docIssuanceIssueTicket\":{\"optionGroup\":[{\"switches\":{\"statusDetails\":{\"indicator\":\"ET\"}}}]}}";
            string strResult = await soapHelperAsync.SendRESTRequestAsync(jsonString, RQUrl, "POST");
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(docIssue);
            CommonServices.SaveLog("DOCISSUE", IssueTicketComRQ.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, strResult);
            //  string strResult = SOAPHelper.SendRESTRequest(jsonstring, RQUrl, "POST");
            return strResult;
        }
        public async Task<CancelComRS> CancelBooking(CancelBooking CancelRQ)

        {
            IssueTicketComRq IssueTicketComRQ = new IssueTicketComRq();

            string PNR = CancelRQ.UniqueId;
            PnrCancel pnrcancel = new PnrCancel();
            pnrcancel.PurplePnrCancel.PnrActions1.OptionCode = "10";
            pnrcancel.PurplePnrCancel.ReservationInfo.Reservation.ControlNumber = PNR;
            pnrcancel.PurplePnrCancel.CancelElements.EntryType = "I";

            //   var jsonData = PnrCancelSerialize.ToJson(pnrcancel);
            var jsonString = JsonConvert.SerializeObject(pnrcancel,
                          Newtonsoft.Json.Formatting.None,
                          new JsonSerializerSettings
                          {
                              NullValueHandling = NullValueHandling.Ignore
                          });
            string RQUrl = "http://70.32.24.42:9000/pnrcancel";
            // string RQUrl = "http://192.168.1.114:9000/pnrcancel";
            string strResult = await soapHelperAsync.SendRESTRequestAsync(jsonString, RQUrl, "POST");
            string rQ = Newtonsoft.Json.JsonConvert.SerializeObject(pnrcancel);
            CommonServices.SaveLog("CANCELPNR", IssueTicketComRQ.SupplierAgencyDetails[0].AgencyCode.ToString(), Suppenum.AMA001.ToString(), rQ, strResult);
            string pnrRetrieveRS = await pnrRetrieve(IssueTicketComRQ, PNR);
            return CancelPNRtoCom(pnrRetrieveRS);
        }

        public CancelComRS CancelPNRtoCom(string strResult)
        {
            CancelComRS cancelComRS = new CancelComRS();
            PnrRetrieveAmRs pnrRetrieveAmRs = new PnrRetrieveAmRs();
            var response = JsonConvert.DeserializeObject<PnrRetrieveAmRs>(strResult);
            string controlNumber = response.PnrReply.PnrHeader[0].ReservationInfo.Reservation[0].ControlNumber;

            var origin = response.PnrReply.OriginDestinationDetails;

            //List<OriginDestinationDetails> originDestinationDetails = new List<OriginDestinationDetails>();
            if (origin != null)
            {
                cancelComRS.Success = false;
                cancelComRS.UniqueId = "";
                cancelComRS.Target = "NA";
                int totalerros = 1;
                var tk = new List<Errors>();
                //TkErrors[] tkerrors = new Tkerror[totalerros];
                //Tkerror tkerror = new Tkerror();
                if (totalerros > 0)
                {
                    for (int Er = 0; Er < totalerros; Er++)
                    {
                        tk.Add(new Errors()
                        {
                            Code1 = CheckNA.NA.ToString(),
                            Message = "Cancellation Failed"

                        });
                    }
                    cancelComRS.Error = tk;
                }
            }
            else
            {
                cancelComRS.Success = true;
                cancelComRS.UniqueId = controlNumber;
                cancelComRS.Target = "NA";
            }



            return cancelComRS;
        }

        public async Task<TripDetails> Tripdetails(TripDetailsRq TripRQ)
        {
            IssueTicketComRq IssueTicketComRQ = new IssueTicketComRq();
            IssueTicketComRQ.SupplierAgencyDetails = TripRQ.SupplierAgencyDetails;

            string PNR = TripRQ.UniqueId;
            string pnrRetrieveRS = await pnrRetrieve(IssueTicketComRQ, PNR);
            return TripDetailsCom(pnrRetrieveRS);
        }


        public TripDetails TripDetailsCom(string strResult)
        {

            var response = JsonConvert.DeserializeObject<PnrRetrieveAmRs>(strResult);
            string PNR = response.PnrReply.PnrHeader[0].ReservationInfo.Reservation[0].ControlNumber;
            TripDetails TripDetails = new TripDetails();
            TripDetails.Errors = new List<object>();
            TripDetails.Success = true;
            TripDetails.Target = "";
            // TravelItinerary travelItinerary = new TravelItinerary();
            TripDetails.TravelItinerary.BookingNotes = new List<object>();
            TripDetails.TravelItinerary.BookingStatus = "Booked";
            TripDetails.TravelItinerary.TicketStatus = "Ticketed";
            TripDetails.TravelItinerary.UniqueId = PNR;
            TripDetails.TravelItinerary.ItineraryInfo.BookedReservedItems = new List<object>();
            //paxdetails
            var tktnum_array = response.PnrReply.DataElementsMaster.DataElementsIndiv.Where(x => x.ElementManagementData1.SegmentName == "FA").ToList();
            int totalitineries = response.PnrReply.TravellerInfo.Count;
            int totalpass = response.PnrReply.TstData.Count;
            var itineraryinfos = TripDetails.TravelItinerary.ItineraryInfo;
            ItineraryInfo1 itineraryInfo = new ItineraryInfo1();
            var custominfo = itineraryinfos.CustomerInfos;
            ItineraryPricing1 itinerarypricing = new ItineraryPricing1();
            int FLitin = 0;


            response.PnrReply.TravellerInfo.ForEach(it =>
            {



                CustomerInfo customerinfo = new CustomerInfo();

                Age age = new Age()
                {
                    Months = "",
                    Years = ""
                };
                PaxName paxName = new PaxName()
                {
                    PassengerFirstName = response.PnrReply.TravellerInfo[FLitin].PassengerData[0].TravellerInformation.Passenger[0].FirstName,
                    PassengerLastName = response.PnrReply.TravellerInfo[FLitin].PassengerData[0].TravellerInformation.Traveller.Surname,
                    PassengerTitle = "",

                };
                //    customerinfo.Customer.Age = age;
                Customer cus = new Customer()
                {
                    Age = age,
                    DateOfBirth = "",
                    EmailAddress = response.PnrReply.DataElementsMaster.DataElementsIndiv[1].OtherDataFreetext[0].LongFreetext,
                    Gender = "",
                    KnownTravelerNo = "",
                    NameNumber = "",
                    NationalId = "",
                    PassengerNationality = "",
                    PassengerType = response.PnrReply.TravellerInfo[FLitin].PassengerData[0].TravellerInformation.Passenger[0].Type,
                    PassportIssuanceCountry = "",
                    PassportNationality = "",
                    PassportNumber = "",
                    PhoneNumber = "",
                    PostCode = "",
                    PaxName = paxName,
                    PassportExpiresOn = "",
                    RedressNo = "",

                };
                //customerinfo.Customer = cus;
                //custominfo.Add(customerinfo);
                string tkt = tktnum_array[FLitin].OtherDataFreetext[0].LongFreetext;
                tkt = tkt.Substring(4, 14);
                var ssr = new List<SsR>();
                ssr.Add(new SsR()
                {
                    ItemRph = 1,
                    MealPreference = "",
                    SeatPreference = ""

                });

                custominfo.Add(new CustomerInfo()
                {

                    Customer = cus,
                    ETickets = new List<object>(),
                    SsRs = ssr

                });
                EquiFare equiFare = new EquiFare()
                {
                    Amount = "",
                    CurrencyCode = "",
                    DecimalPlaces = 0
                };

                itinerarypricing = new ItineraryPricing1()
                {
                    EquiFare = equiFare,
                    Tax = equiFare,
                    TotalFare = equiFare,

                };
                // itineraryInfo.ItineraryPricing= itinerarypricing
                itineraryinfos.ItineraryPricing = itinerarypricing;
                FLitin++;
            });

            for (int FLpax = 0; FLpax < totalitineries; FLpax++)
            {
                int infcount = FLpax + totalitineries;

                var Paxtype = response.PnrReply.TravellerInfo[FLpax].PassengerData[0].TravellerInformation.Passenger[0].Type;
                int typecnt = response.PnrReply.TravellerInfo[FLpax].PassengerData.Count;
                if (Paxtype == PaxTypeEnum.ADT.ToString())
                {
                    if (typecnt == 2)
                    {

                        CustomerInfo customerinfo = new CustomerInfo();

                        Age age = new Age()
                        {
                            Months = "",
                            Years = ""
                        };
                        PaxName paxName = new PaxName()
                        {
                            PassengerFirstName = response.PnrReply.TravellerInfo[FLpax].PassengerData[1].TravellerInformation.Passenger[0].FirstName,
                            PassengerLastName = response.PnrReply.TravellerInfo[FLpax].PassengerData[1].TravellerInformation.Traveller.Surname,
                            PassengerTitle = "",

                        };
                        //    customerinfo.Customer.Age = age;
                        Customer cus = new Customer()
                        {
                            Age = age,
                            DateOfBirth = "",
                            EmailAddress = response.PnrReply.DataElementsMaster.DataElementsIndiv[1].OtherDataFreetext[0].LongFreetext,
                            Gender = "",
                            KnownTravelerNo = "",
                            NameNumber = "",
                            NationalId = "",
                            PassengerNationality = "",
                            PassengerType = response.PnrReply.TravellerInfo[FLpax].PassengerData[1].TravellerInformation.Passenger[0].Type,
                            PassportIssuanceCountry = "",
                            PassportNationality = "",
                            PassportNumber = "",
                            PhoneNumber = "",
                            PostCode = "",
                            PaxName = paxName,
                            PassportExpiresOn = "",
                            RedressNo = "",

                        };
                        //customerinfo.Customer = cus;
                        //custominfo.Add(customerinfo);
                        string tkt = tktnum_array[FLitin].OtherDataFreetext[0].LongFreetext;
                        tkt = tkt.Substring(4, 14);
                        var ssr = new List<SsR>();
                        ssr.Add(new SsR()
                        {
                            ItemRph = 1,
                            MealPreference = "",
                            SeatPreference = ""

                        });

                        custominfo.Add(new CustomerInfo()
                        {

                            Customer = cus,
                            ETickets = new List<object>(),
                            SsRs = ssr

                        });
                        EquiFare equiFare = new EquiFare()
                        {
                            Amount = "",
                            CurrencyCode = "",
                            DecimalPlaces = 0
                        };

                        itinerarypricing = new ItineraryPricing1()
                        {
                            EquiFare = equiFare,
                            Tax = equiFare,
                            TotalFare = equiFare,

                        };
                        // itineraryInfo.ItineraryPricing= itinerarypricing
                        itineraryinfos.ItineraryPricing = itinerarypricing;

                    }
                }
            }


            int totalreserved = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo.Count;
            var reservationitems = TripDetails.TravelItinerary.ItineraryInfo.ReservationItems;
            string arrterminal = "";
            string deptterminal = "";
            ReservationItem1 reservationitem = new ReservationItem1();
            //Reservationitem[] reservationitems = new Reservationitem[totalreserved];
            for (int FLRes = 0; FLRes < totalreserved; FLRes++)
            {

                string ArrDate = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.Product.ArrDate;
                int length = ArrDate.Length;
                string DepDate = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.Product.DepDate;
                string ArrTime = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.Product.ArrTime;
                string DepTime = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.Product.DepTime;

                string comDateDay = ArrDate.Substring(0, 2);
                string comDatemonth = ArrDate.Substring(2, 2);
                string comDateyear = ArrDate.Substring(4, 2);
                string arrhrs = ArrTime.Substring(0, 2);
                string arrmins = ArrTime.Substring(2, 2);


                string Arrival = "20" + comDateyear + "-" + comDatemonth + "-" + comDateDay + "T" + arrhrs + ":" + arrmins + ":00";


                string comDepDay = DepDate.Substring(0, 2);
                string comDepmonth = DepDate.Substring(2, 2);
                string comDepyear = DepDate.Substring(4, 2);
                string dephrs = DepTime.Substring(0, 2);
                string depmins = DepTime.Substring(2, 2);


                string Departure = "20" + comDateyear + "-" + comDatemonth + "-" + comDateDay + "T" + dephrs + ":" + depmins + ":00";

                if (response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.ArrivalStationInfo != null)
                {
                    arrterminal = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.ArrivalStationInfo.Terminal;
                }

                if (response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.DepartureInformation != null)
                {
                    deptterminal = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.DepartureInformation.DepartTerminal;
                }
                reservationitem = new ReservationItem1()

                {
                    AirEquipmentType = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.ProductDetails.Equipment,
                    AirlinePnr = PNR,
                    ArrivalAirportLocationCode = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.OffpointDetail.CityCode,
                    ArrivalDateTime = Convert.ToDateTime(Arrival),
                    ArrivalTerminal = arrterminal,
                    Baggage = "",
                    CabinClassText = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.ProductDetails.ClassOfService,
                    DepartureAirportLocationCode = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.BoardpointDetail.CityCode,
                    DepartureDateTime = Convert.ToDateTime(Departure),
                    DepartureTerminal = deptterminal,
                    FlightNumber = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.ProductDetails.Identification,
                    ItemRph = 0,
                    JourneyDuration = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.ProductDetails.Duration,
                    MarketingAirlineCode = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.CompanyDetail.Identification,
                    NumberInParty = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].RelatedProduct.Quantity,
                    OperatingAirlineCode = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].TravelProduct.CompanyDetail.Identification,
                    StopQuantity = response.PnrReply.OriginDestinationDetails[0].ItineraryInfo[FLRes].FlightDetail1.ProductDetails.NumOfStops
                };

                reservationitems.Add(reservationitem);

                //  tripdetailsresult.ReservationItem = reservationitems;

            }
            double basefare = 0;
            double total = 0;

            foreach (var item in response.PnrReply.TstData)
            {
                int numMonetory = item.FareData.MonetaryInfo.Count;
                if (numMonetory >= 3)
                {
                    basefare = Convert.ToDouble(item.FareData.MonetaryInfo[1].Amount);
                    total = Convert.ToDouble(item.FareData.MonetaryInfo[2].Amount);
                }
                else
                {
                    basefare = Convert.ToDouble(item.FareData.MonetaryInfo[0].Amount);
                    total = Convert.ToDouble(item.FareData.MonetaryInfo[1].Amount);
                }

                EquiFare equifare = new EquiFare()
                {
                    Amount = basefare.ToString(),
                    CurrencyCode = "",
                    DecimalPlaces = 0
                };
                EquiFare tax = new EquiFare()
                {
                    Amount = Convert.ToString(total - basefare),
                    CurrencyCode = "",
                    DecimalPlaces = 0
                };

                EquiFare Total = new EquiFare()
                {
                    Amount = total.ToString(),
                    CurrencyCode = "",
                    DecimalPlaces = 0
                };

                ItineraryPricing1 tripDetailsPassengerFare = new ItineraryPricing1();

                tripDetailsPassengerFare.EquiFare = equifare;
                tripDetailsPassengerFare.Tax = tax;
                tripDetailsPassengerFare.TotalFare = Total;

                TripDetails.TravelItinerary.ItineraryInfo.TripDetailsPtcFareBreakdowns.Add(new TripDetailsPtcFareBreakdown()
                {
                    TripDetailsPassengerFare = tripDetailsPassengerFare

                });
            }
            return TripDetails;
        }

        #region --private methode--
        private void AddPassengerType(FlightSearchRq flightSearchRq, int quantity, PassengerType passengerType, int? quantity1 = null)
        {
            var ptcList = new List<string>();
            var travellers = new List<Models.Traveller>();
            if (quantity > 0)
            {
                ptcList.Add(passengerType.ToString());

                for (int i = 0; i < quantity; i++)
                {
                    int? count = PassengerType.CHD == passengerType ? i + quantity1 + 1 : i + 1;
                    travellers.Add(new Models.Traveller()
                    {
                        Ref = count.Value,
                        InfantIndicator = PassengerType.INF == passengerType ? count : null
                    });
                }

                flightSearchRq.FareMasterPricerTravelBoardSearch.PaxReference.Add(new PaxReference()
                {
                    Ptc = ptcList,
                    Traveller = travellers
                });
            }
        }
        #endregion
    }
}
