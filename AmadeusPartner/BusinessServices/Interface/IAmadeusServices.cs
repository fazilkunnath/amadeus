﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessEntities;

namespace AmadeusPartner.BusinessServices.Interface
{
    public interface IAmadeusServices
    {
        Task<FLcommonRS> MptbSearch(Rootobject data);
        //FareRevalidateRS fareInformativeBestPricingWithoutPNR(FareRevalidateRQ FareRevalidateRQCom);
        Task<FareRevalidateRS> FareInformativeBestPricingWithoutPnr(FareRevalidateRQ FareRevalidateRQCom);
        Task<BookFlightComRS> BookFlight(FLBookRQ FLbookRQ);
        Task<IssueTicketComRS> IssueTicket(IssueTicketComRq IssueTicketComRQ);

        Task<CancelComRS>CancelBooking(CancelBooking CancelRQ);
        Task<TripDetails> Tripdetails(TripDetailsRq TripRQ);
    }
}
