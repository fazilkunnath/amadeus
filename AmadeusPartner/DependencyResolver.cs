﻿namespace AmadeusPartner
{
    using System.ComponentModel.Composition;
    using BusinessServices;
    using BusinessServices.Interface;
    using Resolver;
    using Helper;


    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {
            registerComponent.RegisterType<IAmadeusServices, AmadeusServices>();
            registerComponent.RegisterType<ISoapHelperAsync, SoapHelperAsync>();
        }
    }
}
