﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmadeusPartner
{
    public class FLAmRS
    {
        public Faremasterpricertravelboardsearchreply fareMasterPricerTravelBoardSearchReply { get; set; }
        public string display { get; set; }
        public string[] complianceSet { get; set; }
    }

    public class Faremasterpricertravelboardsearchreply
    {
        public Replystatus replyStatus { get; set; }
        public Conversionrate conversionRate { get; set; }
        public Flightindex[] flightIndex { get; set; }
        public Recommendation[] recommendation { get; set; }
        public Servicefeesgrp[] serviceFeesGrp { get; set; }
    }

    public class Replystatus
    {
        public Status[] status { get; set; }
    }

    public class Status
    {
        public string advisoryTypeInfo { get; set; }
    }

    public class Conversionrate
    {
        public Conversionratedetail[] conversionRateDetail { get; set; }
    }

    public class Conversionratedetail
    {
        public string currency { get; set; }
    }

    public class Flightindex
    {
        public Requestedsegmentref requestedSegmentRef { get; set; }
        public Groupofflight[] groupOfFlights { get; set; }
    }

    public class Requestedsegmentref
    {
        public int segRef { get; set; }
    }

    public class Groupofflight
    {
        public Propflightgrdetail propFlightGrDetail { get; set; }
        public Flightdetail[] flightDetails { get; set; }
    }

    public class Propflightgrdetail
    {
        public Flightproposal[] flightProposal { get; set; }
    }

    public class Flightproposal
    {
        public string @ref { get; set; }
        public string unitQualifier { get; set; }
    }

    public class Flightdetail
    {
        public Flightinformation flightInformation { get; set; }
        public Flightcharacteristics flightCharacteristics { get; set; }
        public Technicalstop[] technicalStop { get; set; }
    }

    public class Flightinformation
    {
        public Productdatetime productDateTime { get; set; }
        public Location[] location { get; set; }
        public Companyid companyId { get; set; }
        public string flightOrtrainNumber { get; set; }
        public Productdetail productDetail { get; set; }
        public Addproductdetail addProductDetail { get; set; }
        public Attributedetail[] attributeDetails { get; set; }
    }

    public class Productdatetime
    {
        public string dateOfDeparture { get; set; }
        public string timeOfDeparture { get; set; }
        public string dateOfArrival { get; set; }
        public string timeOfArrival { get; set; }
        public int dateVariation { get; set; }
    }

    public class Companyid
    {
        public string marketingCarrier { get; set; }
        public string operatingCarrier { get; set; }
    }

    public class Productdetail
    {
        public string equipmentType { get; set; }
        public int techStopNumber { get; set; }
    }

    public class Addproductdetail
    {
        public string electronicTicketing { get; set; }
        public string productDetailQualifier { get; set; }
    }

    public class Location
    {
        public string locationId { get; set; }
        public string terminal { get; set; }
    }

    public class Attributedetail
    {
        public string attributeType { get; set; }
        public string attributeDescription { get; set; }
    }

    public class Flightcharacteristics
    {
        public string[] inFlightSrv { get; set; }
    }

    public class Technicalstop
    {
        public Stopdetail[] stopDetails { get; set; }
    }

    public class Stopdetail
    {
        public string dateQualifier { get; set; }
        public string date { get; set; }
        public string firstTime { get; set; }
        public string locationId { get; set; }
    }

    public class Recommendation
    {
        public Itemnumber itemNumber { get; set; }
        public Recpriceinfo recPriceInfo { get; set; }
        public Segmentflightref[] segmentFlightRef { get; set; }
        public Paxfareproduct[] paxFareProduct { get; set; }
    }

    public class Itemnumber
    {
        public Itemnumberid itemNumberId { get; set; }
    }

    public class Itemnumberid
    {
        public string number { get; set; }
    }

    public class Recpriceinfo
    {
        public Monetarydetail[] monetaryDetail { get; set; }
    }

    public class Monetarydetail
    {
        public int amount { get; set; }
    }

    public class Segmentflightref
    {
        public Referencingdetail[] referencingDetail { get; set; }
    }

    public class Referencingdetail
    {
        public string refQualifier { get; set; }
        public int refNumber { get; set; }
    }

    public class Paxfareproduct
    {
        public Paxfaredetail paxFareDetail { get; set; }
        public Paxreference[] paxReference { get; set; }
        public Fare[] fare { get; set; }
        public Faredetail[] fareDetails { get; set; }
    }

    public class Paxfaredetail
    {
        public string paxFareNum { get; set; }
        public int totalFareAmount { get; set; }
        public int totalTaxAmount { get; set; }
        public Codesharedetail[] codeShareDetails { get; set; }
        public Pricingticketing pricingTicketing { get; set; }
    }

    public class Pricingticketing
    {
        public string[] priceType { get; set; }
    }

    public class Codesharedetail
    {
        public string transportStageQualifier { get; set; }
        public string company { get; set; }
    }

    public class Paxreference
    {
        public string[] ptc { get; set; }
        public Traveller[] traveller { get; set; }
    }

    public class Traveller
    {
        public int _ref { get; set; }
    }

    public class Fare
    {
        public Pricingmessage pricingMessage { get; set; }
        public Monetaryinformation monetaryInformation { get; set; }
        public string amount { get; internal set; }
    }

    public class Pricingmessage
    {
        public Freetextqualification freeTextQualification { get; set; }
        public string[] description { get; set; }
    }

    public class Freetextqualification
    {
        public string textSubjectQualifier { get; set; }
        public string informationType { get; set; }
    }

    public class Monetaryinformation
    {
        public Monetarydetail1[] monetaryDetail { get; set; }
    }

    public class Monetarydetail1
    {
        public string amountType { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
    }

    public class Faredetail
    {
        public Segmentref segmentRef { get; set; }
        public Groupoffare[] groupOfFares { get; set; }
        public Majcabin[] majCabin { get; set; }
    }

    public class Segmentref
    {
        public int segRef { get; set; }
    }

    public class Groupoffare
    {
        public Productinformation productInformation { get; set; }
        public Ticketinfos ticketInfos { get; set; }
    }

    public class Productinformation
    {
        public Cabinproduct cabinProduct { get; set; }
        public Fareproductdetail fareProductDetail { get; set; }
        public string breakPoint { get; set; }
    }

    public class Cabinproduct
    {
        public string rbd { get; set; }
        public string cabin { get; set; }
        public string avlStatus { get; set; }
    }

    public class Fareproductdetail
    {
        public string fareBasis { get; set; }
        public string passengerType { get; set; }
        public string[] fareType { get; set; }
    }

    public class Ticketinfos
    {
        public Additionalfaredetails additionalFareDetails { get; set; }
    }

    public class Additionalfaredetails
    {
        public string ticketDesignator { get; set; }
    }

    public class Majcabin
    {
        public Bookingclassdetail[] bookingClassDetails { get; set; }
    }

    public class Bookingclassdetail
    {
        public string designator { get; set; }
    }

    public class Servicefeesgrp
    {
        public Servicetypeinfo serviceTypeInfo { get; set; }
        public Servicecoverageinfogrp[] serviceCoverageInfoGrp { get; set; }
        public Globalmessagemarker globalMessageMarker { get; set; }
        public Freebagallowancegrp[] freeBagAllowanceGrp { get; set; }
    }

    public class Servicetypeinfo
    {
        public Carrierfeedetails carrierFeeDetails { get; set; }
    }

    public class Carrierfeedetails
    {
        public string type { get; set; }
    }

    public class Globalmessagemarker
    {
    }

    public class Servicecoverageinfogrp
    {
        public Itemnumberinfo itemNumberInfo { get; set; }
        public Servicecovinfogrp[] serviceCovInfoGrp { get; set; }
    }

    public class Itemnumberinfo
    {
        public Itemnumber1 itemNumber { get; set; }
    }

    public class Itemnumber1
    {
        public string number { get; set; }
    }

    public class Servicecovinfogrp
    {
        public Paxrefinfo paxRefInfo { get; set; }
        public Coverageperflightsinfo[] coveragePerFlightsInfo { get; set; }
        public Refinfo refInfo { get; set; }
    }

    public class Paxrefinfo
    {
        public Travellerdetail[] travellerDetails { get; set; }
    }

    public class Travellerdetail
    {
        public string referenceNumber { get; set; }
    }

    public class Refinfo
    {
        public Referencingdetail1[] referencingDetail { get; set; }
    }

    public class Referencingdetail1
    {
        public string refQualifier { get; set; }
        public int refNumber { get; set; }
    }

    public class Coverageperflightsinfo
    {
        public Numberofitemsdetails numberOfItemsDetails { get; set; }
        public Lastitemsdetail[] lastItemsDetails { get; set; }
    }

    public class Numberofitemsdetails
    {
        public string referenceQualifier { get; set; }
        public string refNum { get; set; }
    }

    public class Lastitemsdetail
    {
        public string refOfLeg { get; set; }
    }

    public class Freebagallowancegrp
    {
        public Freebagallownceinfo freeBagAllownceInfo { get; set; }
        public Itemnumberinfo1 itemNumberInfo { get; set; }
    }

    public class Freebagallownceinfo
    {
        public Baggagedetails baggageDetails { get; set; }
    }

    public class Baggagedetails
    {
        public int freeAllowance { get; set; }
        public string quantityCode { get; set; }
        public string unitQualifier { get; set; }
    }

    public class Itemnumberinfo1
    {
        public Itemnumberdetail[] itemNumberDetails { get; set; }
    }

    public class Itemnumberdetail
    {
        public int number { get; set; }
    }
}