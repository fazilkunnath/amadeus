﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmadeusPartner
{
    public class FareRevalidateAmRQ_N
    {
        public Fareinformativebestpricingwithoutpnr fareInformativeBestPricingWithoutPNR { get; set; }
        public string loginId { get; set; }
        public string password { get; set; }
        public string uuId { get; set; }
    }

    public class Fareinformativebestpricingwithoutpnr
    {
        public Passengersgroup[] passengersGroup { get; set; }
        public Segmentgroup[] segmentGroup { get; set; }
        public Pricingoptiongroup[] pricingOptionGroup { get; set; }
    }

    public class Passengersgroup
    {
        public Segmentrepetitioncontrol segmentRepetitionControl { get; set; }
        public Travellersid travellersID { get; set; }
        public Discountptc discountPtc { get; set; }
    }

    public class Segmentrepetitioncontrol
    {
        public Segmentcontroldetail[] segmentControlDetails { get; set; }
    }

    public class Segmentcontroldetail
    {
        public int quantity { get; set; }
        public int numberOfUnits { get; set; }
    }

    public class Travellersid
    {
        public Travellerdetails[] travellerDetails { get; set; }
    }

    public class Travellerdetails
    {
        public int measurementValue { get; set; }
    }

    public class Discountptc
    {
        public string valueQualifier { get; set; }
    }

    public class Segmentgroup
    {
        public Segmentinformation segmentInformation { get; set; }
    }

    public class Segmentinformation
    {
        public Flightdate flightDate { get; set; }
        public Boardpointdetails boardPointDetails { get; set; }
        public Offpointdetails offpointDetails { get; set; }
        public Companydetails companyDetails { get; set; }
        public Flightidentification flightIdentification { get; set; }
        public Flighttypedetails flightTypeDetails { get; set; }
        public int itemNumber { get; set; }
    }

    public class Flightdate
    {
        public string departureDate { get; set; }
        public string departureTime { get; set; }
        public string arrivalDate { get; set; }
        public string arrivalTime { get; set; }
    }

    public class Boardpointdetails
    {
        public string trueLocationId { get; set; }
    }

    public class Offpointdetails
    {
        public string trueLocationId { get; set; }
    }

    public class Companydetails
    {
        public string marketingCompany { get; set; }
        public string operatingCompany { get; set; }
    }

    public class Flightidentification
    {
        public string flightNumber { get; set; }
        public string bookingClass { get; set; }
    }

    public class Flighttypedetails
    {
        public string[] flightIndicator { get; set; }
    }

    public class Pricingoptiongroup
    {
        public Pricingoptionkey pricingOptionKey { get; set; }
     //   public Currency currency { get; set; }
    }

    public class Pricingoptionkey
    {
        public string pricingOptionKey { get; set; }
    }

    //public class Currency
    //{
    //    public Firstcurrencydetails firstCurrencyDetails { get; set; }
    //}

    //public class Firstcurrencydetails
    //{
    //    public string currencyQualifier { get; set; }
    //    public string currencyIsoCode { get; set; }
    //}
}
