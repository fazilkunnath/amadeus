﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmadeusPartner
{
    public class FareRevalidateAmRS
    {
        public Fareinformativebestpricingwithoutpnrreply fareInformativeBestPricingWithoutPNRReply { get; set; }
    }

    public class Fareinformativebestpricingwithoutpnrreply
    {
        public Messagedetails messageDetails { get; set; }
        public Maingroup mainGroup { get; set; }
    }

    public class Messagedetails
    {
        public Messagefunctiondetails messageFunctionDetails { get; set; }
        public string responseType { get; set; }
    }

    public class Messagefunctiondetails
    {
        public string businessFunction { get; set; }
        public string messageFunction { get; set; }
        public string responsibleAgency { get; set; }
        public string[] additionalMessageFunction { get; set; }
    }

    public class Maingroup
    {
        public Dummysegment dummySegment { get; set; }
        public Convertionrate convertionRate { get; set; }
        public Generalindicatorsgroup generalIndicatorsGroup { get; set; }
        public Pricinggrouplevelgroup[] pricingGroupLevelGroup { get; set; }
    }

    public class Dummysegment
    {
    }

    public class Convertionrate
    {
        public Conversionratedetails conversionRateDetails { get; set; }
    }

    public class Conversionratedetails
    {
        public string rateType { get; set; }
        public float pricingAmount { get; set; }
        public string dutyTaxFeeType { get; set; }
    }

    public class Generalindicatorsgroup
    {
        public Generalindicators generalIndicators { get; set; }
    }

    public class Generalindicators
    {
        public Priceticketdetails priceTicketDetails { get; set; }
    }

    public class Priceticketdetails
    {
        public string[] indicators { get; set; }
    }

    public class Pricinggrouplevelgroup
    {
        public Numberofpax numberOfPax { get; set; }
        public Passengersid[] passengersID { get; set; }
        public Fareinfogroup fareInfoGroup { get; set; }
    }

    public class Numberofpax
    {
        public Segmentcontroldetail1[] segmentControlDetails { get; set; }
    }

    public class Segmentcontroldetail1
    {
        public int quantity { get; set; }
        public int numberOfUnits { get; set; }
    }

    public class Fareinfogroup
    {
        public Emptysegment emptySegment { get; set; }
        public Pricingindicators pricingIndicators { get; set; }
        public Fareamount fareAmount { get; set; }
        public Textdata[] textData { get; set; }
        public Surchargesgroup surchargesGroup { get; set; }
        public Segmentlevelgroup[] segmentLevelGroup { get; set; }
        public Farecomponentdetailsgroup[] fareComponentDetailsGroup { get; set; }
    }

    public class Emptysegment
    {
    }

    public class Pricingindicators
    {
        public string priceTariffType { get; set; }
        public Productdatetimedetails productDateTimeDetails { get; set; }
        public Companydetails1 companyDetails { get; set; }
    }

    public class Productdatetimedetails
    {
        public string departureDate { get; set; }
    }

    public class Companydetails1
    {
        public string otherCompany { get; set; }
    }

    public class Fareamount
    {
        public Monetarydetails monetaryDetails { get; set; }
        public Othermonetarydetail[] otherMonetaryDetails { get; set; }
    }

    public class Monetarydetails
    {
        public string typeQualifier { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
    }

    public class Othermonetarydetail
    {
        public string typeQualifier { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
    }

    public class Surchargesgroup
    {
        public Taxesamount taxesAmount { get; set; }
    }

    public class Taxesamount
    {
        public Taxdetail[] taxDetails { get; set; }
    }

    public class Taxdetail
    {
        public string rate { get; set; }
        public string countryCode { get; set; }
        public string[] type { get; set; }
    }

    public class Textdata
    {
        public Freetextqualification1 freeTextQualification { get; set; }
        public string[] freeText { get; set; }
    }

    public class Freetextqualification1
    {
        public string textSubjectQualifier { get; set; }
        public string informationType { get; set; }
    }

    public class Segmentlevelgroup
    {
        public Segmentinformation1 segmentInformation { get; set; }
        public Additionalinformation additionalInformation { get; set; }
        public Farebasis fareBasis { get; set; }
        public Cabingroup[] cabinGroup { get; set; }
        public Baggageallowance baggageAllowance { get; set; }
        public Ptcsegment ptcSegment { get; set; }
    }

    public class Segmentinformation1
    {
        public Flightdate1 flightDate { get; set; }
        public Boardpointdetails1 boardPointDetails { get; set; }
        public Offpointdetails1 offpointDetails { get; set; }
        public Companydetails11 companyDetails { get; set; }
        public Flightidentification1 flightIdentification { get; set; }
        public int itemNumber { get; set; }
    }

    public class Flightdate1
    {
        public string departureDate { get; set; }
    }

    public class Boardpointdetails1
    {
        public string trueLocationId { get; set; }
    }

    public class Offpointdetails1
    {
        public string trueLocationId { get; set; }
    }

    public class Companydetails11
    {
        public string marketingCompany { get; set; }
    }

    public class Flightidentification1
    {
        public string flightNumber { get; set; }
        public string bookingClass { get; set; }
        public string operationalSuffix { get; set; }
    }

    public class Additionalinformation
    {
        public Priceticketdetails1 priceTicketDetails { get; set; }
        public Productdatetimedetails1 productDateTimeDetails { get; set; }
    }

    public class Priceticketdetails1
    {
        public string[] indicators { get; set; }
    }

    public class Productdatetimedetails1
    {
        public string departureDate { get; set; }
        public string arrivalDate { get; set; }
    }

    public class Farebasis
    {
        public Additionalfaredetails1 additionalFareDetails { get; set; }
    }

    public class Additionalfaredetails1
    {
        public string rateClass { get; set; }
        public string[] secondRateClass { get; set; }
    }

    public class Baggageallowance
    {
        public Baggagedetails1 baggageDetails { get; set; }
    }

    public class Baggagedetails1
    {
        public int freeAllowance { get; set; }
        public string quantityCode { get; set; }
    }

    public class Ptcsegment
    {
        public Quantitydetails quantityDetails { get; set; }
    }

    public class Quantitydetails
    {
        public int numberOfUnit { get; set; }
        public string unitQualifier { get; set; }
    }

    public class Cabingroup
    {
        public Cabinsegment cabinSegment { get; set; }
    }

    public class Cabinsegment
    {
        public Bookingclassdetail1[] bookingClassDetails { get; set; }
    }

    public class Bookingclassdetail1
    {
        public string designator { get; set; }
        public string[] option { get; set; }
    }

    public class Farecomponentdetailsgroup
    {
        public Farecomponentid fareComponentID { get; set; }
        public Marketfarecomponent marketFareComponent { get; set; }
        public Monetaryinformation1 monetaryInformation { get; set; }
        public Componentclassinfo componentClassInfo { get; set; }
        public Farequalifiersdetail fareQualifiersDetail { get; set; }
        public Coupondetailsgroup[] couponDetailsGroup { get; set; }
    }

    public class Farecomponentid
    {
        public Itemnumberdetail1[] itemNumberDetails { get; set; }
    }

    public class Itemnumberdetail1
    {
        public string number { get; set; }
        public string type { get; set; }
    }

    public class Marketfarecomponent
    {
        public Boardpointdetails11 boardPointDetails { get; set; }
        public Offpointdetails11 offpointDetails { get; set; }
    }

    public class Boardpointdetails11
    {
        public string trueLocationId { get; set; }
    }

    public class Offpointdetails11
    {
        public string trueLocationId { get; set; }
    }

    public class Monetaryinformation1
    {
        public Monetarydetails1 monetaryDetails { get; set; }
    }

    public class Monetarydetails1
    {
        public string typeQualifier { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
    }

    public class Componentclassinfo
    {
        public Farebasisdetails fareBasisDetails { get; set; }
    }

    public class Farebasisdetails
    {
        public string rateTariffClass { get; set; }
    }

    public class Farequalifiersdetail
    {
        public Discountdetail[] discountDetails { get; set; }
    }

    public class Discountdetail
    {
        public string fareQualifier { get; set; }
    }

    public class Coupondetailsgroup
    {
        public Productid productId { get; set; }
    }

    public class Productid
    {
        public Referencedetails referenceDetails { get; set; }
    }

    public class Referencedetails
    {
        public string type { get; set; }
        public string value { get; set; }
    }

    public class Passengersid
    {
        public Travellerdetail1[] travellerDetails { get; set; }
    }

    public class Travellerdetail1
    {
        public int measurementValue { get; set; }
    }
}
