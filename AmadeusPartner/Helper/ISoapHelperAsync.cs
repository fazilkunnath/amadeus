﻿namespace AmadeusPartner.Helper
{
    using System.Threading.Tasks;
    public interface ISoapHelperAsync
    {
        Task<string> SendRESTRequestAsync(string RQJson, string RQUrl, string action, string soapAction = null);
        Task<string> SendSOAPRequest(string query, string url, string action, string soapAction = null);
    }
}