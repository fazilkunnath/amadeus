﻿namespace AmadeusPartner.Helper
{
    using System;
    using System.IO;
    using System.Net;
    using System.Threading.Tasks;
    using System.Xml;
    public class SoapHelperAsync : ISoapHelperAsync
    {
        public async Task<string> SendSOAPRequest(string query, string url, string action, string soapAction = null)
        {
            // Create the SOAP envelope
            XmlDocument soapEnvelopeXml = new XmlDocument();

            //string parms = string.Join(string.Empty, parameters.Select(kv => String.Format("<{0}>{1}</{0}>", kv.Key, kv.Value)).ToArray());
            var s = String.Format(query, action, new Uri(url).GetLeftPart(UriPartial.Authority) + "/");
            soapEnvelopeXml.LoadXml(s);

            // Create the web request
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", soapAction ?? url);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";

            // Insert SOAP envelope
            using (Stream stream = await webRequest.GetRequestStreamAsync())
            {
                soapEnvelopeXml.Save(stream);
            }

            // Send request and retrieve result
            string result;
            using (WebResponse response = await webRequest.GetResponseAsync())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    result = await rd.ReadToEndAsync();
                }
            }
            return result;
        }
        public async Task<string> SendRESTRequestAsync(string RQJson, string RQUrl, string action, string soapAction = null)
        {
            string strResult = string.Empty;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(RQUrl);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = action;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(RQJson);
                    // await streamWriter.WriteAsync(RQJson);
                    streamWriter.Flush();
                }
                //  var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //using (WebResponse httpResponse = await httpWebRequest.GetResponseAsync())
                using (var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    if (httpResponse != null)
                    {
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            var responseText = streamReader.ReadToEnd();
                            //var responseText = await streamReader.ReadToEndAsync();
                            Console.WriteLine(responseText);
                            strResult = responseText;
                            return strResult;
                        }
                    }
                    return null;
                }
            }
            catch (WebException ex)
            {
                string errMessage = ex.Message;
                //strResult = errMessage;
                if (ex.Response != null)
                {
                    System.IO.Stream resStr = ex.Response.GetResponseStream();
                    System.IO.StreamReader sr = new System.IO.StreamReader(resStr);
                    string soapResponse = sr.ReadToEnd();
                    sr.Close();
                    resStr.Close();
                    errMessage += Environment.NewLine + soapResponse;
                    strResult = errMessage;
                    ex.Response.Close();
                }
                return null;
            }

        }
    }
}
