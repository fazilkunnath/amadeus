﻿using System;
using System.Net;
using System.Collections.Generic;

using Newtonsoft.Json;

public partial class AirSell
{

    public AirSell()
    {
        AirSellFromRecommendation = new AirSellFromRecommendation();

    }
    [JsonProperty("airSellFromRecommendation")]
    public AirSellFromRecommendation AirSellFromRecommendation { get; set; }

    [JsonProperty("loginId")]
    public string LoginId { get; set; }

    [JsonProperty("password")]
    public string Password { get; set; }

    [JsonProperty("uuId")]
    public string UuId { get; set; }
}

public partial class AirSellFromRecommendation
{
    public AirSellFromRecommendation()
    {
        MessageActionDetails = new MessageActionDetails();

        ItineraryDetails = new List<ItineraryDetail>();
    }
    [JsonProperty("messageActionDetails")]
    public MessageActionDetails MessageActionDetails { get; set; }

    [JsonProperty("itineraryDetails")]
    public List<ItineraryDetail> ItineraryDetails { get; set; }
}
public partial class MessageActionDetails
{
    public MessageActionDetails()
    {
        MessageFunctionDetails = new MessageFunctionDetails();
    }
    [JsonProperty("messageFunctionDetails")]
    public MessageFunctionDetails MessageFunctionDetails{ get; set; }
}

public partial class MessageFunctionDetails
{
    [JsonProperty("messageFunction")]
    public string MessageFunction { get; set; }

    [JsonProperty("additionalMessageFunction")]
    public List<string> AdditionalMessageFunction { get; set; }
}

public partial class ItineraryDetail
{
    [JsonProperty("originDestinationDetails")]
    public OriginDestinationDetails OriginDestinationDetails { get; set; }

    [JsonProperty("message")]
    public Message Message { get; set; }

    [JsonProperty("segmentInformation")]
    public List<SegmentInformation1> SegmentInformation { get; set; }
}

public partial class Message
{
    public Message()
    {
        MessageFunctionDetails = new MessageFunctionDetails();
    }
    [JsonProperty("messageFunctionDetails")]
    public MessageFunctionDetails MessageFunctionDetails { get; set; }

}

//public partial class MessageFunctionDetails
//{

//    [JsonProperty("messageFunction")]
//    public string messageFunction { get; set; }
//}
public partial class OriginDestinationDetails
{
    [JsonProperty("origin")]
    public string Origin { get; set; }

    [JsonProperty("destination")]
    public string Destination { get; set; }
}

public partial class SegmentInformation1
{
    [JsonProperty("travelProductInformation")]
    public TravelProductInformation TravelProductInformation { get; set; }

    [JsonProperty("relatedproductInformation")]
    public RelatedproductInformation RelatedproductInformation { get; set; }
}

public partial class RelatedproductInformation
{
    [JsonProperty("quantity")]
    public long Quantity { get; set; }

    [JsonProperty("statusCode")]
    public List<string> StatusCode { get; set; }
}


public partial class TravelProductInformation
{
    public TravelProductInformation()
    {
        FlightDate = new FlightDate();
        BoardPointDetails = new OintDetails();
        OffpointDetails = new OintDetails();
        CompanyDetails = new CompanyDetails();
        FlightIdentification = new FlightIdentification();
    }
    [JsonProperty("flightDate")]
    public FlightDate FlightDate { get; set; }

    [JsonProperty("boardPointDetails")]
    public OintDetails BoardPointDetails { get; set; }

    [JsonProperty("offpointDetails")]
    public OintDetails OffpointDetails { get; set; }

    [JsonProperty("companyDetails")]
    public CompanyDetails CompanyDetails { get; set; }

    [JsonProperty("flightIdentification")]
    public FlightIdentification FlightIdentification { get; set; }
}

public partial class OintDetails
{
    [JsonProperty("trueLocationId")]
    public string TrueLocationId { get; set; }
}

public partial class CompanyDetails
{
    [JsonProperty("marketingCompany")]
    public string MarketingCompany { get; set; }
}

public partial class FlightDate
{
    [JsonProperty("departureDate")]
    public string DepartureDate { get; set; }
}

public partial class FlightIdentification
{
    [JsonProperty("flightNumber")]
    public string FlightNumber { get; set; }

    [JsonProperty("bookingClass")]
    public string BookingClass { get; set; }
}




public partial class AirSell
{
    public static AirSell FromJson(string json) => JsonConvert.DeserializeObject<AirSell>(json, AirSellConverter.Settings);
}

public static class AirSellSerialize
{
    public static string ToJson(this AirSell self) => JsonConvert.SerializeObject(self, AirSellConverter.Settings);
}

public class AirSellConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
    };
}

