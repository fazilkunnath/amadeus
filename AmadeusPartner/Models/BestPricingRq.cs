﻿
namespace AmadeusPartner.Models
{

    using System.Collections.Generic;

    using Newtonsoft.Json;

    public partial class BestPricingRq
    {


        public BestPricingRq()
        {
            FareInformativeBestPricingWithoutPnr = new FareInformativeBestPricingWithoutPnr();
        }


        [JsonProperty("fareInformativeBestPricingWithoutPNR")]
        public FareInformativeBestPricingWithoutPnr FareInformativeBestPricingWithoutPnr { get; set; }

        [JsonProperty("loginId")]
        public string LoginId { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("uuId")]
        public string UuId { get; set; }
    }

    public partial class FareInformativeBestPricingWithoutPnr
    {
        public FareInformativeBestPricingWithoutPnr()
        {
            PassengersGroup = new List<PassengersGroup>();
            SegmentGroup = new List<SegmentGroup>();
            PricingOptionGroup = new List<PricingOptionGroup>();
        }

        [JsonProperty("passengersGroup")]
        public List<PassengersGroup> PassengersGroup { get; set; }

        [JsonProperty("segmentGroup")]
        public List<SegmentGroup> SegmentGroup { get; set; }

        [JsonProperty("pricingOptionGroup")]
        public List<PricingOptionGroup> PricingOptionGroup { get; set; }
    }

    public partial class PassengersGroup
    {
        public PassengersGroup()
        {
            SegmentRepetitionControl = new SegmentRepetitionControl();
            TravellersId = new TravellersId();
            DiscountPtc = new DiscountPtc();
        }


        [JsonProperty("segmentRepetitionControl")]
        public SegmentRepetitionControl SegmentRepetitionControl { get; set; }

        [JsonProperty("travellersID")]
        public TravellersId TravellersId { get; set; }

        [JsonProperty("discountPtc")]
        public DiscountPtc DiscountPtc { get; set; }
    }

    public partial class DiscountPtc
    {
        [JsonProperty("valueQualifier")]
        public string ValueQualifier { get; set; }
    }

    public partial class SegmentRepetitionControl
    {
        public SegmentRepetitionControl()
        {
            SegmentControlDetails = new List<SegmentControlDetail>();
        }

        [JsonProperty("segmentControlDetails")]
        public List<SegmentControlDetail> SegmentControlDetails { get; set; }
    }

    public partial class SegmentControlDetail
    {
        [JsonProperty("quantity")]
        public long Quantity { get; set; }

        [JsonProperty("numberOfUnits")]
        public long NumberOfUnits { get; set; }
    }

    public partial class TravellersId
    {
        public TravellersId()
        {
            TravellerDetails = new List<TravellerDetail>();
        }
        [JsonProperty("travellerDetails")]
        public List<TravellerDetail> TravellerDetails { get; set; }
    }

    public partial class TravellerDetail
    {
        [JsonProperty("measurementValue")]
        public long MeasurementValue { get; set; }
    }

    public partial class PricingOptionGroup
    {
        //  public PricingOptionGroup()
        //{
        //    PricingOptionKey = new PricingOptionKey();
        //    Currency = new Currency();
        //}
        [JsonProperty("pricingOptionKey")]
        public PricingOptionKey PricingOptionKey { get; set; }

        [JsonProperty("currency")]
        public Currency Currency { get; set; }
    }

    public partial class Currency
    {
        public Currency()
        {
            FirstCurrencyDetails = new FirstCurrencyDetails();
        }
        [JsonProperty("firstCurrencyDetails")]
        public FirstCurrencyDetails FirstCurrencyDetails { get; set; }
    }

    public partial class FirstCurrencyDetails
    {
        [JsonProperty("currencyQualifier")]
        public string CurrencyQualifier { get; set; }

        [JsonProperty("currencyIsoCode")]
        public string CurrencyIsoCode { get; set; }
    }

    public partial class PricingOptionKey
    {
        [JsonProperty("pricingOptionKey")]
        public string PurplePricingOptionKey { get; set; }
    }

    public partial class SegmentGroup
    {

        public SegmentGroup()
        {
            SegmentInformation = new SegmentInformation();
        }

        [JsonProperty("segmentInformation")]
        public SegmentInformation SegmentInformation { get; set; }
    }

    public partial class SegmentInformation
    {
        public SegmentInformation()
        {
            FlightDate = new FlightDate();
            BoardPointDetails = new OintDetails();
            OffpointDetails = new OintDetails();
            CompanyDetails = new CompanyDetails();
            FlightIdentification = new FlightIdentification();
            FlightTypeDetails = new FlightTypeDetails();



        }

        [JsonProperty("flightDate")]
        public FlightDate FlightDate { get; set; }

        [JsonProperty("boardPointDetails")]
        public OintDetails BoardPointDetails { get; set; }

        [JsonProperty("offpointDetails")]
        public OintDetails OffpointDetails { get; set; }

        [JsonProperty("companyDetails")]
        public CompanyDetails CompanyDetails { get; set; }

        [JsonProperty("flightIdentification")]
        public FlightIdentification FlightIdentification { get; set; }

        [JsonProperty("flightTypeDetails")]
        public FlightTypeDetails FlightTypeDetails { get; set; }

        [JsonProperty("itemNumber")]
        public long ItemNumber { get; set; }

    }

    public partial class OintDetails
    {
        [JsonProperty("trueLocationId")]
        public string TrueLocationId { get; set; }
    }

    public partial class CompanyDetails
    {
        [JsonProperty("marketingCompany")]
        public string MarketingCompany { get; set; }

        [JsonProperty("operatingCompany")]
        public string OperatingCompany { get; set; }
    }

    public partial class FlightDate
    {
        [JsonProperty("departureDate")]
        public string DepartureDate { get; set; }

        [JsonProperty("departureTime")]
        public string DepartureTime { get; set; }

        [JsonProperty("arrivalDate")]
        public string ArrivalDate { get; set; }

        [JsonProperty("arrivalTime")]
        public string ArrivalTime { get; set; }
    }

    //public partial class FlightIdentification
    //{
    //    [JsonProperty("flightNumber")]
    //    public string FlightNumber { get; set; }

    //    [JsonProperty("bookingClass")]
    //    public string BookingClass { get; set; }
    //}

    public partial class FlightTypeDetails
    {

        public FlightTypeDetails()
        {
            FlightIndicator = new List<string>();
        }
        [JsonProperty("flightIndicator")]
        public List<string> FlightIndicator { get; set; }
    }

    public partial class BestPricingRq
    {
        public static BestPricingRq FromJson(string json) => JsonConvert.DeserializeObject<BestPricingRq>(json, BestPricingRqConverter.BestPricingRqSettings);
    }

    public static class BestPricingRqSerialize
    {
        public static string ToJson(this BestPricingRq self) => JsonConvert.SerializeObject(self, BestPricingRqConverter.BestPricingRqSettings);
    }

    public class BestPricingRqConverter
    {
        public static readonly JsonSerializerSettings BestPricingRqSettings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            NullValueHandling = NullValueHandling.Ignore
        };
    }
}

