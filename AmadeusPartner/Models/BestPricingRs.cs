﻿
namespace AmadeusPartner.Models
{
    using System.Collections.Generic;

    using Newtonsoft.Json;
    using BusinessEntities;

    public partial class BestPricingRs
    {
        [JsonProperty("fareInformativeBestPricingWithoutPNRReply")]
        public FareInformativeBestPricingWithoutPnrReply FareInformativeBestPricingWithoutPnrReply { get; set; }
    }

    public partial class FareInformativeBestPricingWithoutPnrReply
    {
        [JsonProperty("messageDetails")]
        public MessageDetails MessageDetails { get; set; }

        [JsonProperty("mainGroup")]
        public MainGroup MainGroup { get; set; }
    }

    public partial class MainGroup
    {
        [JsonProperty("dummySegment")]
        public YSegment DummySegment { get; set; }

        [JsonProperty("convertionRate")]
        public ConvertionRate ConvertionRate { get; set; }

        [JsonProperty("generalIndicatorsGroup")]
        public GeneralIndicatorsGroup GeneralIndicatorsGroup { get; set; }

        [JsonProperty("pricingGroupLevelGroup")]
        public List<PricingGroupLevelGroup> PricingGroupLevelGroup { get; set; }
    }

    public partial class ConvertionRate
    {
        [JsonProperty("conversionRateDetails")]
        public ConversionRateDetails ConversionRateDetails { get; set; }
    }

    public partial class ConversionRateDetails
    {
        [JsonProperty("rateType")]
        public string RateType { get; set; }

        [JsonProperty("pricingAmount")]
        public double PricingAmount { get; set; }

        [JsonProperty("dutyTaxFeeType")]
        public string DutyTaxFeeType { get; set; }
    }

    public partial class YSegment
    {
    }

    public partial class GeneralIndicatorsGroup
    {
        [JsonProperty("generalIndicators")]
        public GeneralIndicators GeneralIndicators { get; set; }
    }

    public partial class GeneralIndicators
    {
        [JsonProperty("priceTicketDetails")]
        public PriceTicketDetails PriceTicketDetails { get; set; }
    }

    public partial class PriceTicketDetails
    {
        [JsonProperty("indicators")]
        public List<string> Indicators { get; set; }
    }

    public partial class PricingGroupLevelGroup
    {
        [JsonProperty("numberOfPax")]
        public NumberOfPax NumberOfPax { get; set; }

        [JsonProperty("passengersID")]
        public List<PassengersId> PassengersId { get; set; }

        [JsonProperty("fareInfoGroup")]
        public FareInfoGroup FareInfoGroup { get; set; }
    }

    public partial class FareInfoGroup
    {
        [JsonProperty("emptySegment")]
        public YSegment EmptySegment { get; set; }

        [JsonProperty("pricingIndicators")]
        public PricingIndicators PricingIndicators { get; set; }

        [JsonProperty("fareAmount")]
        public FareAmount FareAmount { get; set; }

        [JsonProperty("textData")]
        public List<TextDatum> TextData { get; set; }

        [JsonProperty("surchargesGroup")]
        public SurchargesGroup SurchargesGroup { get; set; }

        [JsonProperty("segmentLevelGroup")]
        public List<SegmentLevelGroup> SegmentLevelGroup { get; set; }

        [JsonProperty("fareComponentDetailsGroup")]
        public List<FareComponentDetailsGroup> FareComponentDetailsGroup { get; set; }
    }

    public partial class FareAmount
    {
        [JsonProperty("monetaryDetails")]
        public MonetaryDetails MonetaryDetails { get; set; }

        [JsonProperty("otherMonetaryDetails")]
        public List<MonetaryDetails> OtherMonetaryDetails { get; set; }
    }

    public partial class MonetaryDetails
    {
        [JsonProperty("typeQualifier")]
        public string TypeQualifier { get; set; }

        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }

    public partial class FareComponentDetailsGroup
    {
        [JsonProperty("fareComponentID")]
        public FareComponentId FareComponentId { get; set; }

        [JsonProperty("marketFareComponent")]
        public MarketFareComponent MarketFareComponent { get; set; }

        [JsonProperty("monetaryInformation")]
        public MonetaryInformation MonetaryInformation { get; set; }

        [JsonProperty("componentClassInfo")]
        public ComponentClassInfo ComponentClassInfo { get; set; }

        [JsonProperty("fareQualifiersDetail")]
        public FareQualifiersDetail FareQualifiersDetail { get; set; }

        [JsonProperty("couponDetailsGroup")]
        public List<CouponDetailsGroup> CouponDetailsGroup { get; set; }
    }

    public partial class ComponentClassInfo
    {
        [JsonProperty("fareBasisDetails")]
        public FareBasisDetails FareBasisDetails { get; set; }
    }

    public partial class FareBasisDetails
    {
        [JsonProperty("rateTariffClass")]
        public string RateTariffClass { get; set; }
    }

    public partial class CouponDetailsGroup
    {
        [JsonProperty("productId")]
        public ProductId ProductId { get; set; }
    }

    public partial class ProductId
    {
        [JsonProperty("referenceDetails")]
        public ReferenceDetails ReferenceDetails { get; set; }
    }

    public partial class ReferenceDetails
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class FareComponentId
    {
        [JsonProperty("itemNumberDetails")]
        public List<ItemNumberDetail> ItemNumberDetails { get; set; }
    }

    public partial class ItemNumberDetail
    {
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public partial class FareQualifiersDetail
    {
        [JsonProperty("discountDetails")]
        public List<DiscountDetail> DiscountDetails { get; set; }
    }

    public partial class DiscountDetail
    {
        [JsonProperty("fareQualifier")]
        public string FareQualifier { get; set; }
    }

    public partial class MarketFareComponent
    {
        [JsonProperty("boardPointDetails")]
        public OintDetails BoardPointDetails { get; set; }

        [JsonProperty("offpointDetails")]
        public OintDetails OffPointDetails { get; set; }
    }

    //public partial class OintDetails
    //{
    //    [JsonProperty("trueLocationId")]
    //    public string TrueLocationId { get; set; }
    //}

    public partial class MonetaryInformation
    {
        [JsonProperty("monetaryDetails")]
        public MonetaryDetails MonetaryDetails { get; set; }
    }

    public partial class PricingIndicators
    {
        [JsonProperty("priceTariffType")]
        public string PriceTariffType { get; set; }

        [JsonProperty("productDateTimeDetails")]
        public PricingIndicatorsProductDateTimeDetails ProductDateTimeDetails { get; set; }

        [JsonProperty("companyDetails")]
        public PricingIndicatorsCompanyDetails CompanyDetails { get; set; }
    }

    public partial class PricingIndicatorsCompanyDetails
    {
        [JsonProperty("otherCompany")]
        public string OtherCompany { get; set; }
    }

    public partial class PricingIndicatorsProductDateTimeDetails
    {
        [JsonProperty("departureDate")]
        public string DepartureDate { get; set; }
    }

    public partial class SegmentLevelGroup
    {
        [JsonProperty("segmentInformation")]
        public SegmentInformation SegmentInformation { get; set; }

        [JsonProperty("additionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }

        [JsonProperty("fareBasis")]
        public FareBasis FareBasis { get; set; }

        [JsonProperty("cabinGroup")]
        public List<CabinGroup> CabinGroup { get; set; }

        [JsonProperty("baggageAllowance")]
        public BaggageAllowance BaggageAllowance { get; set; }

        [JsonProperty("ptcSegment")]
        public PtcSegment PtcSegment { get; set; }
    }

    public partial class AdditionalInformation
    {
        [JsonProperty("priceTicketDetails")]
        public PriceTicketDetails PriceTicketDetails { get; set; }

        [JsonProperty("productDateTimeDetails")]
        public AdditionalInformationProductDateTimeDetails ProductDateTimeDetails { get; set; }
    }

    public partial class AdditionalInformationProductDateTimeDetails
    {
        [JsonProperty("departureDate")]
        public string DepartureDate { get; set; }

        [JsonProperty("arrivalDate")]
        public string ArrivalDate { get; set; }
    }

    public partial class BaggageAllowance
    {
        [JsonProperty("baggageDetails")]
        public BaggageDetails BaggageDetails { get; set; }
    }

    //public partial class BaggageDetails
    //{
    //    //[JsonProperty("freeAllowance")]
    //    //public long FreeAllowance { get; set; }

    //    [JsonProperty("quantityCode")]
    //    public string QuantityCode { get; set; }
    //}

    public partial class CabinGroup
    {
        [JsonProperty("cabinSegment")]
        public CabinSegment CabinSegment { get; set; }
    }

    public partial class CabinSegment
    {
        [JsonProperty("bookingClassDetails")]
        public List<BookingClassDetail> BookingClassDetails { get; set; }
    }

    public partial class BookingClassDetail
    {
        //[JsonProperty("designator")]
        //public string Designator { get; set; }

        [JsonProperty("option")]
        public List<string> Option { get; set; }
    }

    public partial class FareBasis
    {
        [JsonProperty("additionalFareDetails")]
        public AdditionalFareDetails AdditionalFareDetails { get; set; }
    }

    public partial class AdditionalFareDetails
    {
        [JsonProperty("rateClass")]
        public string RateClass { get; set; }

        [JsonProperty("secondRateClass")]
        public List<string> SecondRateClass { get; set; }
    }

    public partial class PtcSegment
    {
        [JsonProperty("quantityDetails")]
        public QuantityDetails QuantityDetails { get; set; }
    }

    public partial class QuantityDetails
    {
        [JsonProperty("numberOfUnit")]
        public long NumberOfUnit { get; set; }

        [JsonProperty("unitQualifier")]
        public string UnitQualifier { get; set; }
    }

    //public partial class SegmentInformation
    //{
    //    [JsonProperty("flightDate")]
    //    public PricingIndicatorsProductDateTimeDetails FlightDate { get; set; }

    //    [JsonProperty("boardPointDetails")]
    //    public OintDetails BoardPointDetails { get; set; }

    //    [JsonProperty("offpointDetails")]
    //    public OintDetails OffpointDetails { get; set; }

    //    [JsonProperty("companyDetails")]
    //    public SegmentInformationCompanyDetails CompanyDetails { get; set; }

    //    [JsonProperty("flightIdentification")]
    //    public FlightIdentification FlightIdentification { get; set; }

    //    [JsonProperty("itemNumber")]
    //    public long ItemNumber { get; set; }
    //}

    public partial class SegmentInformationCompanyDetails
    {
        [JsonProperty("marketingCompany")]
        public string MarketingCompany { get; set; }
    }

    public partial class FlightIdentification
    {
        [JsonProperty("flightNumber")]
        public string FlightNumber { get; set; }

        [JsonProperty("bookingClass")]
        public string BookingClass { get; set; }

        [JsonProperty("operationalSuffix")]
        public string OperationalSuffix { get; set; }
    }

    public partial class SurchargesGroup
    {
        [JsonProperty("taxesAmount")]
        public TaxesAmount TaxesAmount { get; set; }
    }

    public partial class TaxesAmount
    {
        [JsonProperty("taxDetails")]
        public List<TaxDetail> TaxDetails { get; set; }
    }

    public partial class TaxDetail
    {
        [JsonProperty("rate")]
        public string Rate { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("type")]
        public List<string> Type { get; set; }
    }

    public partial class TextDatum
    {
        [JsonProperty("freeTextQualification")]
        public FreeTextQualification FreeTextQualification { get; set; }

        [JsonProperty("freeText")]
        public List<string> FreeText { get; set; }
    }

    //public partial class FreeTextQualification
    //{
    //    [JsonProperty("textSubjectQualifier")]
    //    public string TextSubjectQualifier { get; set; }

    //    [JsonProperty("informationType")]
    //    public string InformationType { get; set; }
    //}

    public partial class NumberOfPax
    {
        [JsonProperty("segmentControlDetails")]
        public List<SegmentControlDetail> SegmentControlDetails { get; set; }
    }

    //public partial class SegmentControlDetail
    //{
    //    [JsonProperty("quantity")]
    //    public long Quantity { get; set; }

    //    [JsonProperty("numberOfUnits")]
    //    public long NumberOfUnits { get; set; }
    //}

    public partial class PassengersId
    {
        [JsonProperty("travellerDetails")]
        public List<TravellerDetail> TravellerDetails { get; set; }
    }

    //public partial class TravellerDetail
    //{
    //    [JsonProperty("measurementValue")]
    //    public int MeasurementValue { get; set; }
    //}

    public partial class MessageDetails
    {
        [JsonProperty("messageFunctionDetails")]
        public MessageFunctionDetails MessageFunctionDetails { get; set; }

        [JsonProperty("responseType")]
        public string ResponseType { get; set; }
    }

    public partial class MessageFunctionDetails
    {
        [JsonProperty("businessFunction")]
        public string BusinessFunction { get; set; }

        [JsonProperty("messageFunction")]
        public string MessageFunction { get; set; }

        [JsonProperty("responsibleAgency")]
        public string ResponsibleAgency { get; set; }

        [JsonProperty("additionalMessageFunction")]
        public List<string> AdditionalMessageFunction { get; set; }
    }

    public partial class BestPricingRs
    {
        public static BestPricingRs FromJson(string json) => JsonConvert.DeserializeObject<BestPricingRs>(json, BestPricingRsConverter.Settings);
    }

    public static class BestPricingRsSerialize
    {
        public static string ToJson(this BestPricingRs self) => JsonConvert.SerializeObject(self, BestPricingRsConverter.Settings);
    }

    public class BestPricingRsConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            NullValueHandling = NullValueHandling.Ignore
        };
    }
}

