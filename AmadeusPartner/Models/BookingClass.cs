﻿
using Newtonsoft.Json;
using System.Collections.Generic;


public partial class BookingClass
{
    public BookingClass()
    {
        FarePricePnrWithBookingClass = new FarePricePnrWithBookingClass();
    }
    [JsonProperty("farePricePNRWithBookingClass")]
    public FarePricePnrWithBookingClass FarePricePnrWithBookingClass { get; set; }

    [JsonProperty("loginId")]
    public string LoginId { get; set; }

    [JsonProperty("password")]
    public string Password { get; set; }

    [JsonProperty("uuId")]
    public string UuId { get; set; }

}

public partial class FarePricePnrWithBookingClass
{
    public FarePricePnrWithBookingClass()
    {
        PricingOptionGroup = new List<PricingOptionGroup>();
    }
    [JsonProperty("pricingOptionGroup")]
   public List<PricingOptionGroup> PricingOptionGroup { get; set; }
}

public partial class PricingOptionGroup
{
    
    [JsonProperty("pricingOptionKey")]
    public PricingOptionKey PricingOptionKey { get; set; }

    [JsonProperty("currency")]
    public Currency Currency { get; set; }
}

public partial class Currency
{
    public Currency()
    {
        FirstCurrencyDetails = new FirstCurrencyDetails();
    }
    [JsonProperty("firstCurrencyDetails")]
    public FirstCurrencyDetails FirstCurrencyDetails { get; set; }

}

public partial class FirstCurrencyDetails
{
    [JsonProperty("currencyQualifier")]
    public string CurrencyQualifier { get; set; }

    [JsonProperty("currencyIsoCode")]
    public string CurrencyIsoCode { get; set; }
}

public partial class PricingOptionKey
{
    [JsonProperty("pricingOptionKey")]
    public string PurplePricingOptionKey { get; set; }
}

public partial class BookingClass
{
    public static BookingClass FromJson(string json) => JsonConvert.DeserializeObject<BookingClass>(json, BookingClassConverter.Settings);
}

public static class BookingClassSerialize
{
    public static string ToJson(this BookingClass self) => JsonConvert.SerializeObject(self, BookingClassConverter.Settings);
}

public class BookingClassConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
    };
}
