﻿using Newtonsoft.Json;
using System.Collections.Generic;

public partial class BookingClassRs
{
    [JsonProperty("farePricePNRWithBookingClassReply")]
    public FarePricePnrWithBookingClassReply FarePricePnrWithBookingClassReply { get; set; }
}

public partial class FarePricePnrWithBookingClassReply
{
    [JsonProperty("fareList")]
    public List<FareList> FareList { get; set; }
}

public partial class FareList
{
    [JsonProperty("pricingInformation")]
    public PricingInformation PricingInformation { get; set; }

    [JsonProperty("fareReference")]
    public FareReference FareReference { get; set; }

    [JsonProperty("lastTktDate")]
    public LastTktDate LastTktDate { get; set; }

    [JsonProperty("validatingCarrier")]
    public ValidatingCarrier ValidatingCarrier { get; set; }

    [JsonProperty("paxSegReference")]
    public Reference PaxSegReference { get; set; }

    [JsonProperty("fareDataInformation")]
    public FareListFareDataInformation FareDataInformation { get; set; }

    [JsonProperty("taxInformation")]
    public List<TaxInformation> TaxInformation { get; set; }

    [JsonProperty("originDestination")]
    public OriginDestination OriginDestination { get; set; }

    [JsonProperty("segmentInformation")]
    public List<SegmentInformation> SegmentInformation { get; set; }

    [JsonProperty("otherPricingInfo")]
    public List<OtherPricingInfo> OtherPricingInfo { get; set; }

    [JsonProperty("warningInformation")]
    public List<WarningInformation> WarningInformation { get; set; }

    [JsonProperty("fareComponentDetailsGroup")]
    public List<FareComponentDetailsGroup> FareComponentDetailsGroup { get; set; }

    [JsonProperty("endFareList")]
    public EndFareList EndFareList { get; set; }
}

public partial class EndFareList
{
}

//public partial class FareComponentDetailsGroup
//{
//    [JsonProperty("fareComponentID")]
//    public FareComponentId FareComponentId { get; set; }

//    [JsonProperty("marketFareComponent")]
//    public MarketFareComponent MarketFareComponent { get; set; }

//    [JsonProperty("monetaryInformation")]
//    public MonetaryInformation MonetaryInformation { get; set; }

//    [JsonProperty("componentClassInfo")]
//    public ComponentClassInfo ComponentClassInfo { get; set; }

//    [JsonProperty("fareQualifiersDetail")]
//    public FareQualifiersDetail FareQualifiersDetail { get; set; }

//    [JsonProperty("couponDetailsGroup")]
//    public List<CouponDetailsGroup> CouponDetailsGroup { get; set; }
//}

public partial class ComponentClassInfo
{
    //[JsonProperty("fareBasisDetails")]
    //public ComponentClassInfoFareBasisDetails FareBasisDetails { get; set; }
}

public partial class ComponentClassInfoFareBasisDetails
{
    [JsonProperty("rateTariffClass")]
    public string RateTariffClass { get; set; }
}

public partial class CouponDetailsGroup
{
    //[JsonProperty("productId")]
    //public ProductId ProductId { get; set; }
}

public partial class ProductId
{
    //[JsonProperty("referenceDetails")]
    //public ReferenceDetails ReferenceDetails { get; set; }
}

public partial class ReferenceDetails
{
    //[JsonProperty("type")]
    //public string PurpleType { get; set; }

    //[JsonProperty("value")]
    //public string Value { get; set; }
}

public partial class FareComponentId
{
    //[JsonProperty("itemNumberDetails")]
    //public List<ItemNumberDetail> ItemNumberDetails { get; set; }
}

public partial class ItemNumberDetail
{
    //[JsonProperty("number")]
    //public string Number { get; set; }

    //[JsonProperty("type")]
    //public string PurpleType { get; set; }
}

public partial class FareQualifiersDetail
{
    //[JsonProperty("discountDetails")]
    //public List<DiscountDetail> DiscountDetails { get; set; }
}

public partial class DiscountDetail
{
//    [JsonProperty("fareQualifier")]
//    public string FareQualifier { get; set; }
}

public partial class MarketFareComponent
{
    //[JsonProperty("boardPointDetails")]
    //public OintDetails BoardPointDetails { get; set; }

    //[JsonProperty("offpointDetails")]
    //public OintDetails OffpointDetails { get; set; }
}

public partial class OintDetails
{
//    [JsonProperty("trueLocationId")]
//    public string TrueLocationId { get; set; }
}

public partial class MonetaryInformation
{
    //[JsonProperty("monetaryDetails")]
    //public MonetaryDetails MonetaryDetails { get; set; }
}

public partial class MonetaryDetails
{
    //[JsonProperty("typeQualifier")]
    //public string TypeQualifier { get; set; }

    //[JsonProperty("amount")]
    //public string Amount { get; set; }

    //[JsonProperty("currency")]
    //public string Currency { get; set; }
}

public partial class FareListFareDataInformation
{
    [JsonProperty("fareDataMainInformation")]
    public FareDataMainInformation FareDataMainInformation { get; set; }

    [JsonProperty("fareDataSupInformation")]
    public List<FareDataInformationFareDataSupInformation> FareDataSupInformation { get; set; }
}

public partial class FareDataMainInformation
{
    [JsonProperty("fareDataQualifier")]
    public string FareDataQualifier { get; set; }
}

public partial class FareDataInformationFareDataSupInformation
{
    [JsonProperty("fareDataQualifier")]
    public string FareDataQualifier { get; set; }

    [JsonProperty("fareAmount")]
    public string FareAmount { get; set; }

    [JsonProperty("fareCurrency")]
    public string FareCurrency { get; set; }
}

public partial class FareReference
{
    [JsonProperty("referenceType")]
    public string ReferenceType { get; set; }

    [JsonProperty("uniqueReference")]
    public long UniqueReference { get; set; }
}

public partial class LastTktDate
{
    [JsonProperty("businessSemantic")]
    public string BusinessSemantic { get; set; }

    //[JsonProperty("dateTime")]
    ////public DateTime DateTime { get; set; }
}

//public partial class DateTime
//{
//    //[JsonProperty("year")]
//    //public string Year { get; set; }

//    //[JsonProperty("month")]
//    //public string Month { get; set; }

//    //[JsonProperty("day")]
//    //public string Day { get; set; }
//}

public partial class OriginDestination
{
    [JsonProperty("cityCode")]
    public List<string> CityCode { get; set; }
}

public partial class OtherPricingInfo
{
    [JsonProperty("attributeDetails")]
    public List<AttributeDetail> AttributeDetails { get; set; }
}

public partial class AttributeDetail
{
    [JsonProperty("attributeType")]
    public string AttributeType { get; set; }

    [JsonProperty("attributeDescription")]
    public string AttributeDescription { get; set; }
}

public partial class Reference
{
    [JsonProperty("refDetails")]
    public List<RefDetail> RefDetails { get; set; }
}

public partial class RefDetail
{
    [JsonProperty("refQualifier")]
    public string RefQualifier { get; set; }

    [JsonProperty("refNumber")]
    public long RefNumber { get; set; }
}

public partial class PricingInformation
{
    [JsonProperty("tstInformation")]
    public TstInformation TstInformation { get; set; }

    [JsonProperty("fcmi")]
    public string Fcmi { get; set; }
}

public partial class TstInformation
{
    [JsonProperty("tstIndicator")]
    public string TstIndicator { get; set; }
}

public partial class SegmentInformation
{
    [JsonProperty("connexInformation")]
    public ConnexInformation ConnexInformation { get; set; }

    [JsonProperty("segDetails")]
    public SegDetails SegDetails { get; set; }

    [JsonProperty("fareQualifier")]
    public FareQualifier FareQualifier { get; set; }

    [JsonProperty("validityInformation")]
    public List<LastTktDate> ValidityInformation { get; set; }

    [JsonProperty("bagAllowanceInformation")]
    public BagAllowanceInformation BagAllowanceInformation { get; set; }

    [JsonProperty("segmentReference")]
    public Reference SegmentReference { get; set; }

    [JsonProperty("sequenceInformation")]
    public SequenceInformation SequenceInformation { get; set; }
}

public partial class BagAllowanceInformation
{
    [JsonProperty("bagAllowanceDetails")]
    public BagAllowanceDetails BagAllowanceDetails { get; set; }
}

public partial class BagAllowanceDetails
{
    [JsonProperty("baggageWeight")]
    public long BaggageWeight { get; set; }

    [JsonProperty("baggageType")]
    public string BaggageType { get; set; }

    [JsonProperty("measureUnit")]
    public string MeasureUnit { get; set; }
}

public partial class ConnexInformation
{
    [JsonProperty("connecDetails")]
    public ConnecDetails ConnecDetails { get; set; }
}

public partial class ConnecDetails
{
    [JsonProperty("connexType")]
    public string ConnexType { get; set; }
}

public partial class FareQualifier
{
    [JsonProperty("fareBasisDetails")]
    public FareQualifierFareBasisDetails FareBasisDetails { get; set; }
}

public partial class FareQualifierFareBasisDetails
{
    [JsonProperty("primaryCode")]
    public string PrimaryCode { get; set; }

    [JsonProperty("fareBasisCode")]
    public string FareBasisCode { get; set; }

    [JsonProperty("discTktDesignator")]
    public string DiscTktDesignator { get; set; }
}

public partial class SegDetails
{
    [JsonProperty("segmentDetail")]
    public SegmentDetail SegmentDetail { get; set; }
}

public partial class SegmentDetail
{
    [JsonProperty("identification")]
    public string Identification { get; set; }

    [JsonProperty("classOfService")]
    public string ClassOfService { get; set; }
}

public partial class SequenceInformation
{
    [JsonProperty("sequenceSection")]
    public SequenceSection SequenceSection { get; set; }
}

public partial class SequenceSection
{
    [JsonProperty("sequenceNumber")]
    public long SequenceNumber { get; set; }
}

public partial class TaxInformation
{
    [JsonProperty("taxDetails")]
    public TaxDetails TaxDetails { get; set; }

    [JsonProperty("amountDetails")]
    public AmountDetails AmountDetails { get; set; }
}

public partial class AmountDetails
{
    [JsonProperty("fareDataMainInformation")]
    public FareDataInformationFareDataSupInformation FareDataMainInformation { get; set; }
}

public partial class TaxDetails
{
    [JsonProperty("taxQualifier")]
    public string TaxQualifier { get; set; }

    [JsonProperty("taxIdentification")]
    public TaxIdentification TaxIdentification { get; set; }

    [JsonProperty("taxType")]
    public TaxType TaxType { get; set; }

    [JsonProperty("taxNature")]
    public string TaxNature { get; set; }
}

public partial class TaxIdentification
{
    [JsonProperty("taxIdentifier")]
    public string TaxIdentifier { get; set; }
}

public partial class TaxType
{
    [JsonProperty("isoCountry")]
    public string IsoCountry { get; set; }
}

public partial class ValidatingCarrier
{
    [JsonProperty("carrierInformation")]
    public CarrierInformation CarrierInformation { get; set; }
}

public partial class CarrierInformation
{
    [JsonProperty("carrierCode")]
    public string CarrierCode { get; set; }
}

public partial class WarningInformation
{
    [JsonProperty("warningCode")]
    public WarningCode WarningCode { get; set; }

    [JsonProperty("warningText")]
    public WarningText WarningText { get; set; }
}

public partial class WarningCode
{
    [JsonProperty("applicationErrorDetail")]
    public ApplicationErrorDetail ApplicationErrorDetail { get; set; }
}

public partial class ApplicationErrorDetail
{
    [JsonProperty("applicationErrorCode")]
    public string ApplicationErrorCode { get; set; }

    [JsonProperty("codeListQualifier")]
    public string CodeListQualifier { get; set; }

    [JsonProperty("codeListResponsibleAgency")]
    public string CodeListResponsibleAgency { get; set; }
}

public partial class WarningText
{
    [JsonProperty("errorFreeText")]
    public string ErrorFreeText { get; set; }
}

public partial class BookingClassRs
{
    public static BookingClassRs FromJson(string json) => JsonConvert.DeserializeObject<BookingClassRs>(json, BookingClassRsConverter.Settings);
}

public static class BookingClassRsSerialize
{
    public static string ToJson(this BookingClassRs self) => JsonConvert.SerializeObject(self, BookingClassRsConverter.Settings);
}

public class BookingClassRsConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
    };
}