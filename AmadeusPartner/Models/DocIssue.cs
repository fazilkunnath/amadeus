﻿using Newtonsoft.Json;
using System.Collections.Generic;

public partial class DocIssue
{
    public DocIssue()
    {
        DocIssuanceIssueTicket = new DocIssuanceIssueTicket();
    }
    [JsonProperty("docIssuanceIssueTicket")]
    public DocIssuanceIssueTicket DocIssuanceIssueTicket { get; set; }

    [JsonProperty("loginId")]
    public string LoginId { get; set; }

    [JsonProperty("password")]
    public string Password { get; set; }

    [JsonProperty("uuId")]
    public string UuId { get; set; }
}

public partial class DocIssuanceIssueTicket
{
    public DocIssuanceIssueTicket()
    {
        OptionGroup = new List<global::OptionGroup>();
    }
    [JsonProperty("optionGroup")]
    public List<OptionGroup> OptionGroup { get; set; }
}

public partial class OptionGroup
{
    public OptionGroup()
    {
        Switches = new Switches();
    }
    [JsonProperty("switches")]
    public Switches Switches { get; set; }
}

public partial class Switches
{
    public Switches()
    {
        StatusDetails = new StatusDetails();

    }
    [JsonProperty("statusDetails")]
    public StatusDetails StatusDetails { get; set; }
}

public partial class StatusDetails
{
    [JsonProperty("indicator")]
    public string Indicator { get; set; }
}

public partial class DocIssue
{
    public static DocIssue FromJson(string json) => JsonConvert.DeserializeObject<DocIssue>(json, DocIssueConverter.Settings);
}

public static class DocIssueSerialize
{
    public static string ToJson(this DocIssue self) => JsonConvert.SerializeObject(self, DocIssueConverter.Settings);
}

public class DocIssueConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
    };
}
