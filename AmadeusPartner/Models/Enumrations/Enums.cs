﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmadeusPartner.Models.Enumrations
{
    public enum CurrencyEnum
    {
        AED = 1, FCO,USD
    }


    public enum PaxTypeEnum
    {

        ADT ,CHD,INF,CH

    }


    public enum CheckNA
    {
        NA
    }
    public enum EmptyArrayHandling
    {
        Ignore = 0, // Ignore empty array when deserializing (use object current array).
        Set = 1 // Set empty array (replace object's current array with an empty one)
    }

    public enum SegmentEnum
    {
        FP,RF,AP,TK

    }
    public enum Suppenum
    {
        AMA001
    }
}
