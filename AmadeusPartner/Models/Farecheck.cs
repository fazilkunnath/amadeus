﻿using Newtonsoft.Json;
using System.Collections.Generic;

public partial class Farecheck
{

    public Farecheck()
    {
        FareCheckRules = new FareCheckRules();
    }

    [JsonProperty("fareCheckRules")]
    public FareCheckRules FareCheckRules { get; set; }
}

public partial class FareCheckRules
{
    public FareCheckRules()
    {
        MsgType = new MsgType();
        ItemNumber = new ItemNumber();
        FareRule = new FareRule();

    }
    [JsonProperty("msgType")]
    public MsgType MsgType { get; set; }

    [JsonProperty("itemNumber")]
    public ItemNumber ItemNumber { get; set; }

    [JsonProperty("fareRule")]
    public FareRule FareRule { get; set; }
}

public partial class FareRule
{
    public FareRule()
    {
        TarifFareRule = new TarifFareRule();

    }
    [JsonProperty("tarifFareRule")]
    public TarifFareRule TarifFareRule { get; set; }
}

public partial class TarifFareRule
{
    public TarifFareRule()
    {
        RuleSectionId = new List<string>();
    }

    [JsonProperty("ruleSectionId")]
    public List<string> RuleSectionId { get; set; }
}

public partial class ItemNumber
{
    public ItemNumber()
    {
        ItemNumberDetails = new List<ItemNumberDetail>();
    }
    [JsonProperty("itemNumberDetails")]
    public List<ItemNumberDetail> ItemNumberDetails { get; set; }
}

public partial class ItemNumberDetail
{
  //  [JsonProperty("number")]
    //public string Number { get; set; }
}

public partial class MsgType
{
    public MsgType()
    {
        MessageFunctionDetails = new MessageFunctionDetails();
    }
    [JsonProperty("messageFunctionDetails")]
    public MessageFunctionDetails MessageFunctionDetails { get; set; }
}

public partial class MessageFunctionDetails
{
   // [JsonProperty("messageFunction")]
 //   public string MessageFunction { get; set; }
}

public partial class Farecheck
{
    public static Farecheck FromJson(string json) => JsonConvert.DeserializeObject<Farecheck>(json, FarecheckConverter.Settings);
}

public static class FarecheckSerialize
{
    public static string ToJson(this Farecheck self) => JsonConvert.SerializeObject(self, FarecheckConverter.Settings);
}

public class FarecheckConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
    };
}