﻿
using Newtonsoft.Json;
using System.Collections.Generic;
using AmadeusPartner.Models;

public partial class FarecheckRs
{
    [JsonProperty("fareCheckRulesReply")]
    public FareCheckRulesReply FareCheckRulesReply { get; set; }
}

public partial class FareCheckRulesReply
{
    [JsonProperty("transactionType")]
    public TransactionType TransactionType { get; set; }

    [JsonProperty("errorInfo")]
    public ErrorInfo ErrorInfo { get; set; }
    [JsonProperty("tariffInfo")]
    public List<TariffInfo> TariffInfo { get; set; }

    [JsonProperty("flightDetails")]
    public List<FlightDetail1> FlightDetails { get; set; }
}
public partial class ErrorInfo
{
    [JsonProperty("rejectErrorCode")]
    public RejectErrorCode RejectErrorCode { get; set; }

    [JsonProperty("errorFreeText")]
    public ErrorFreeText ErrorFreeText { get; set; }
}

public partial class ErrorFreeText
{
    [JsonProperty("freeText")]
    public List<string> FreeText { get; set; }
}

public partial class RejectErrorCode
{
    [JsonProperty("errorDetails")]
    public ErrorDetails ErrorDetails { get; set; }
}

public partial class ErrorDetails
{
    [JsonProperty("errorCode")]
    public string ErrorCode { get; set; }
}
public partial class FlightDetail1
{
    [JsonProperty("nbOfSegments")]
    public NbOfSegments NbOfSegments { get; set; }

    [JsonProperty("qualificationFareDetails")]
    public List<QualificationFareDetail> QualificationFareDetails { get; set; }

    [JsonProperty("transportService")]
    public List<TransportService> TransportService { get; set; }

    [JsonProperty("flightErrorCode")]
    public List<FlightErrorCode> FlightErrorCode { get; set; }

    [JsonProperty("productInfo")]
    public List<ProductInfo> ProductInfo { get; set; }

    [JsonProperty("fareDetailInfo")]
    public List<FareDetailInfo> FareDetailInfo { get; set; }

    [JsonProperty("odiGrp")]
    public List<OdiGrp> OdiGrp { get; set; }

    [JsonProperty("travellerGrp")]
    public List<TravellerGrp> TravellerGrp { get; set; }

    [JsonProperty("itemGrp")]
    public List<ItemGrp> ItemGrp { get; set; }
    public Finfo Finfo { get; internal set; }
}

public partial class FareDetailInfo
{
    [JsonProperty("nbOfUnits")]
    public NbOfUnits NbOfUnits { get; set; }

    [JsonProperty("fareDeatilInfo")]
    public FareDeatilInfo FareDeatilInfo { get; set; }
}

public partial class FareDeatilInfo
{
    [JsonProperty("fareTypeGrouping")]
    public FareTypeGrouping FareTypeGrouping { get; set; }
}

public partial class FareTypeGrouping
{
    [JsonProperty("pricingGroup")]
    public List<string> PricingGroup { get; set; }
}

public partial class NbOfUnits
{
    [JsonProperty("quantityDetails")]
    public List<QuantityDetail> QuantityDetails { get; set; }
}

public partial class QuantityDetail
{
    [JsonProperty("numberOfUnit")]
    public long NumberOfUnit { get; set; }

    [JsonProperty("unitQualifier")]
    public string UnitQualifier { get; set; }
}

public partial class FlightErrorCode
{
    [JsonProperty("freeTextQualification")]
    public FreeTextQualification FreeTextQualification { get; set; }

    [JsonProperty("freeText")]
    public List<string> FreeText { get; set; }
}

public partial class FreeTextQualification
{
    //[JsonProperty("textSubjectQualifier")]
    //public string TextSubjectQualifier { get; set; }

    [JsonProperty("informationType")]
    public string InformationType { get; set; }
}

public partial class ItemGrp
{
    [JsonProperty("itemNb")]
    public ItemNb ItemNb { get; set; }

    [JsonProperty("unitGrp")]
    public List<UnitGrp> UnitGrp { get; set; }
}

public partial class ItemNb
{
    [JsonProperty("itemNumberDetails")]
    public List<ItemNumberDetail> ItemNumberDetails { get; set; }
}

public partial class ItemNumberDetail
{
    //[JsonProperty("number")]
    //public string Number { get; set; }
}

public partial class UnitGrp
{
    [JsonProperty("nbOfUnits")]
    public NbOfUnits NbOfUnits { get; set; }

    [JsonProperty("unitFareDetails")]
    public FareDeatilInfo UnitFareDetails { get; set; }
}

public partial class NbOfSegments
{
}

public partial class OdiGrp
{
    [JsonProperty("originDestination")]
    public OriginDestination OriginDestination { get; set; }
}

public partial class OriginDestination
{
    [JsonProperty("origin")]
    public string Origin { get; set; }

    [JsonProperty("destination")]
    public string Destination { get; set; }
}

public partial class ProductInfo
{
    [JsonProperty("productDetails")]
    public ProductDetails ProductDetails { get; set; }
}

public partial class ProductDetails
{
    [JsonProperty("bookingClassDetails")]
    public List<BookingClassDetail> BookingClassDetails { get; set; }
}

public partial class BookingClassDetail
{
    [JsonProperty("designator")]
    public string Designator { get; set; }
}

public partial class QualificationFareDetail
{
    [JsonProperty("fareDetails")]
    public FareDetails1 FareDetails { get; set; }

    [JsonProperty("additionalFareDetails")]
    public AdditionalFareDetails AdditionalFareDetails { get; set; }
}

public partial class AdditionalFareDetails
{
    [JsonProperty("rateClass")]
    public string RateClass { get; set; }

    [JsonProperty("fareClass")]
    public List<string> FareClass { get; set; }
}

public partial class FareDetails1
{
    [JsonProperty("qualifier")]
    public string Qualifier { get; set; }

    [JsonProperty("fareCategory")]
    public string FareCategory { get; set; }
}

public partial class TransportService
{
    [JsonProperty("companyIdentification")]
    public CompanyIdentification CompanyIdentification { get; set; }
}

public partial class CompanyIdentification
{
    [JsonProperty("marketingCompany")]
    public string MarketingCompany { get; set; }
}

public partial class TravellerGrp
{
    [JsonProperty("travellerIdentRef")]
    public TravellerIdentRef TravellerIdentRef { get; set; }

    [JsonProperty("fareRulesDetails")]
    public FareRulesDetails FareRulesDetails { get; set; }
}

public partial class FareRulesDetails
{
    [JsonProperty("tariffClassId")]
    public string TariffClassId { get; set; }

    [JsonProperty("ruleSectionId")]
    public List<string> RuleSectionId { get; set; }
}

public partial class TravellerIdentRef
{
    [JsonProperty("referenceDetails")]
    public List<ReferenceDetail> ReferenceDetails { get; set; }
}

public partial class ReferenceDetail
{
    [JsonProperty("type")]
    public string PurpleType { get; set; }

    [JsonProperty("value")]
    public string Value { get; set; }
}

public partial class TariffInfo
{
    [JsonProperty("fareRuleInfo")]
    public FareRuleInfo FareRuleInfo { get; set; }

    [JsonProperty("fareRuleText")]
    public List<FlightErrorCode> FareRuleText { get; set; }
}

public partial class FareRuleInfo
{
    [JsonProperty("ruleSectionLocalId")]
    public string RuleSectionLocalId { get; set; }

    [JsonProperty("ruleCategoryCode")]
    public string RuleCategoryCode { get; set; }
}

public partial class TransactionType
{
    [JsonProperty("messageFunctionDetails")]
    public MessageFunctionDetails MessageFunctionDetails { get; set; }
}

public partial class MessageFunctionDetails
{
    //[JsonProperty("messageFunction")]
    //public string MessageFunction { get; set; }
}

public partial class FarecheckRs
{
    public static FarecheckRs FromJson(string json) => JsonConvert.DeserializeObject<FarecheckRs>(json, FarecheckRsConverter.Settings);
}

public static class FarecheckRsSerialize
{
    public static string ToJson(this FarecheckRs self) => JsonConvert.SerializeObject(self, FarecheckRsConverter.Settings);
}

public class FarecheckRsConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
    };
}