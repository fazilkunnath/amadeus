﻿namespace AmadeusPartner.Models
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public partial class FlightSearchRq
    {
        public FlightSearchRq()
        {
            FareMasterPricerTravelBoardSearch = new FareMasterPricerTravelBoardSearch();
        }

        [JsonProperty("fareMasterPricerTravelBoardSearch")]
        public FareMasterPricerTravelBoardSearch FareMasterPricerTravelBoardSearch { get; set; }

        [JsonProperty("loginId")]
        public string LoginId { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("uuId")]
        public string UuId { get; set; }
    }

    public partial class FareMasterPricerTravelBoardSearch
    {
        public FareMasterPricerTravelBoardSearch()
        {
            NumberOfUnit = new NumberOfUnit();
            PaxReference = new List<PaxReference>();
            FareOptions = new FareOptions();
            TravelFlightInfo = new TravelFlightInfo();
            Itinerary = new List<Itinerary>();
        }

        [JsonProperty("numberOfUnit")]
        public NumberOfUnit NumberOfUnit { get; set; }

        [JsonProperty("paxReference")]
        public List<PaxReference> PaxReference { get; set; }

        [JsonProperty("fareOptions")]
        public FareOptions FareOptions { get; set; }

        [JsonProperty("travelFlightInfo")]
        public TravelFlightInfo TravelFlightInfo { get; set; }

        [JsonProperty("itinerary")]
        public List<Itinerary> Itinerary { get; set; }
    }

    public partial class TravelFlightInfo
    {
        public TravelFlightInfo()
        {
            CabinId = new CabinId();
        }

        [JsonProperty("cabinId")]
        public CabinId CabinId { get; set; }
    }

    public partial class CabinId
    {
        public CabinId()
        {
            Cabin = new List<string>();
        }

        [JsonProperty("cabinQualifier")]
        public string CabinQualifier { get; set; }

        [JsonProperty("cabin")]
        public List<string> Cabin { get; set; }
    }

    public partial class PaxReference
    {
        public PaxReference()
        {
            Traveller = new List<Models.Traveller>();
            Ptc = new List<string>();
        }

        [JsonProperty("ptc")]
        public List<string> Ptc { get; set; }

        [JsonProperty("traveller")]
        public List<Traveller> Traveller { get; set; }
    }

    public partial class Traveller
    {
        [JsonProperty("ref")]
        public long Ref { get; set; }

        [JsonProperty("infantIndicator")]
        public long? InfantIndicator { get; set; }
    }

    public partial class NumberOfUnit
    {
        public NumberOfUnit()
        {
            UnitNumberDetail = new List<UnitNumberDetail>();
        }

        [JsonProperty("unitNumberDetail")]
        public List<UnitNumberDetail> UnitNumberDetail { get; set; }
    }

    public partial class UnitNumberDetail
    {
        [JsonProperty("numberOfUnits")]
        public long NumberOfUnits { get; set; }

        [JsonProperty("typeOfUnit")]
        public string TypeOfUnit { get; set; }
    }

    public partial class Itinerary
    {
        public Itinerary()
        {
            RequestedSegmentRef = new RequestedSegmentRef();
            DepartureLocalization = new DepartureLocalization();
            ArrivalLocalization = new ArrivalLocalization();
            TimeDetails = new TimeDetails();
        }

        [JsonProperty("requestedSegmentRef")]
        public RequestedSegmentRef RequestedSegmentRef { get; set; }

        [JsonProperty("departureLocalization")]
        public DepartureLocalization DepartureLocalization { get; set; }

        [JsonProperty("arrivalLocalization")]
        public ArrivalLocalization ArrivalLocalization { get; set; }

        [JsonProperty("timeDetails")]
        public TimeDetails TimeDetails { get; set; }
    }

    public partial class TimeDetails
    {
        public TimeDetails()
        {
            FirstDateTimeDetail = new FirstDateTimeDetail();
        }

        [JsonProperty("firstDateTimeDetail")]
        public FirstDateTimeDetail FirstDateTimeDetail { get; set; }
    }

    public partial class FirstDateTimeDetail
    {
        [JsonProperty("date")]
        public string Date { get; set; }
    }

    public partial class RequestedSegmentRef
    {
        [JsonProperty("segRef")]
        public long SegRef { get; set; }
    }

    public partial class DepartureLocalization
    {
        public DepartureLocalization()
        {
            DeparturePoint = new ArrivalPointDetails();
        }

        [JsonProperty("departurePoint")]
        public ArrivalPointDetails DeparturePoint { get; set; }
    }

    public partial class ArrivalLocalization
    {
        public ArrivalLocalization()
        {
            ArrivalPointDetails = new ArrivalPointDetails();
        }
        [JsonProperty("arrivalPointDetails")]
        public ArrivalPointDetails ArrivalPointDetails { get; set; }
    }

    public partial class ArrivalPointDetails
    {
        [JsonProperty("locationId")]
        public string LocationId { get; set; }
    }

    public partial class FareOptions
    {
        public FareOptions()
        {
            PricingTickInfo = new PricingTickInfo();
            ConversionRate = new ConversionRate();
        }

        [JsonProperty("pricingTickInfo")]
        public PricingTickInfo PricingTickInfo { get; set; }

        [JsonProperty("conversionRate")]
        public ConversionRate ConversionRate { get; set; }
    }

    public partial class PricingTickInfo
    {
        public PricingTickInfo()
        {
            PricingTicketing = new PricingTicketing();
        }

        [JsonProperty("pricingTicketing")]
        public PricingTicketing PricingTicketing { get; set; }
    }

    public partial class PricingTicketing
    {
        public PricingTicketing()
        {
            PriceType = new List<string>();
        }

        [JsonProperty("priceType")]
        public List<string> PriceType { get; set; }
    }


    public partial class ConversionRate
    {
        public ConversionRate()
        {
            ConversionRateDetail = new List<ConversionRateDetail>();
        }

        [JsonProperty("conversionRateDetail")]
        public List<ConversionRateDetail> ConversionRateDetail { get; set; }
    }

    public partial class ConversionRateDetail
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }
    }

    public partial class FlightSearchRq
    {
        public static FlightSearchRq FromJson(string json) => JsonConvert.DeserializeObject<FlightSearchRq>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this FlightSearchRq self) => JsonConvert.SerializeObject(self, Converter.Settings);

        internal static object ToJson(BestPricingRq bestPricingRq)
        {
            throw new NotImplementedException();
        }
    }

    public class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            NullValueHandling = NullValueHandling.Ignore
        };
    }

    public enum PassengerType
    {
        ADT = 1, CHD, INF
    }
    public enum PriceType
    {
        CUC = 1, IFS
    }

    public enum CabinQualifiers
    {
        MC = 1
    }
    public enum Cabin
    {
        All = 1
    }

}