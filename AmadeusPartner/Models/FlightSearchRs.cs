﻿namespace AmadeusPartner.Models
{
    using System;
    using System.Net;
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public partial class FlightSearchRs
    {
        [JsonProperty("fareMasterPricerTravelBoardSearchReply")]
        public FareMasterPricerTravelBoardSearchReply FareMasterPricerTravelBoardSearchReply { get; set; }

        [JsonProperty("display")]
        public string Display { get; set; }

        [JsonProperty("complianceSet")]
        public List<string> ComplianceSet { get; set; }
    }

    public partial class FareMasterPricerTravelBoardSearchReply
    {
        [JsonProperty("replyStatus")]
        public ReplyStatus ReplyStatus { get; set; }

        [JsonProperty("conversionRate")]
        public ConversionRate ConversionRate { get; set; }

        [JsonProperty("flightIndex")]
        public List<FlightIndex> FlightIndex { get; set; }

        [JsonProperty("recommendation")]
        public List<Recommendation> Recommendation { get; set; }

        [JsonProperty("serviceFeesGrp")]
        public List<ServiceFeesGrp> ServiceFeesGrp { get; set; }
    }

    public partial class ServiceFeesGrp
    {
        [JsonProperty("serviceTypeInfo")]
        public ServiceTypeInfo ServiceTypeInfo { get; set; }

        [JsonProperty("serviceCoverageInfoGrp")]
        public List<ServiceCoverageInfoGrp> ServiceCoverageInfoGrp { get; set; }

        [JsonProperty("globalMessageMarker")]
        public GlobalMessageMarker GlobalMessageMarker { get; set; }

        [JsonProperty("freeBagAllowanceGrp")]
        public List<FreeBagAllowanceGrp> FreeBagAllowanceGrp { get; set; }
    }

    public partial class ServiceTypeInfo
    {
        [JsonProperty("carrierFeeDetails")]
        public CarrierFeeDetails CarrierFeeDetails { get; set; }
    }

    public partial class CarrierFeeDetails
    {
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public partial class ServiceCoverageInfoGrp
    {
        [JsonProperty("itemNumberInfo")]
        public ServiceCoverageInfoGrpItemNumberInfo ItemNumberInfo { get; set; }

        [JsonProperty("serviceCovInfoGrp")]
        public List<ServiceCovInfoGrp> ServiceCovInfoGrp { get; set; }
    }

    public partial class ServiceCovInfoGrp
    {
        [JsonProperty("paxRefInfo")]
        public PaxRefInfo PaxRefInfo { get; set; }

        [JsonProperty("coveragePerFlightsInfo")]
        public List<CoveragePerFlightsInfo> CoveragePerFlightsInfo { get; set; }

        [JsonProperty("refInfo")]
        public SegmentFlightRef RefInfo { get; set; }
    }

    public partial class PaxRefInfo
    {
        [JsonProperty("travellerDetails")]
        public List<TravellerDetail> TravellerDetails { get; set; }
    }

    public partial class TravellerDetail
    {
        [JsonProperty("referenceNumber")]
        public string ReferenceNumber { get; set; }
    }

    public partial class CoveragePerFlightsInfo
    {
        [JsonProperty("numberOfItemsDetails")]
        public NumberOfItemsDetails NumberOfItemsDetails { get; set; }

        [JsonProperty("lastItemsDetails")]
        public List<LastItemsDetail> LastItemsDetails { get; set; }
    }

    public partial class NumberOfItemsDetails
    {
        [JsonProperty("referenceQualifier")]
        public string ReferenceQualifier { get; set; }

        [JsonProperty("refNum")]
        public string RefNum { get; set; }
    }

    public partial class LastItemsDetail
    {
        [JsonProperty("refOfLeg")]
        public string RefOfLeg { get; set; }
    }

    public partial class ServiceCoverageInfoGrpItemNumberInfo
    {
        [JsonProperty("itemNumber")]
        public ItemNumberItemNumberId ItemNumber { get; set; }
    }

    public partial class GlobalMessageMarker
    {
    }

    public partial class FreeBagAllowanceGrp
    {
        [JsonProperty("freeBagAllownceInfo")]
        public FreeBagAllownceInfo FreeBagAllownceInfo { get; set; }

        [JsonProperty("itemNumberInfo")]
        public FreeBagAllowanceGrpItemNumberInfo ItemNumberInfo { get; set; }
    }

    public partial class FreeBagAllowanceGrpItemNumberInfo
    {
        [JsonProperty("itemNumberDetails")]
        public List<ItemNumberDetail> ItemNumberDetails { get; set; }
    }

    public partial class ItemNumberDetail
    {
        [JsonProperty("number")]
        public long Number { get; set; }
    }

    public partial class FreeBagAllownceInfo
    {
        [JsonProperty("baggageDetails")]
        public BaggageDetails BaggageDetails { get; set; }
    }

    public partial class BaggageDetails
    {
        [JsonProperty("freeAllowance")]
        public string FreeAllowance { get; set; }

        [JsonProperty("quantityCode")]
        public string QuantityCode { get; set; }

        [JsonProperty("unitQualifier")]
        public string UnitQualifier { get; set; }
    }

    public partial class ReplyStatus
    {
        public ReplyStatus()
        {
            Status = new List<Models.Status>();
        }
        [JsonProperty("status")]
        public List<Status> Status { get; set; }
    }

    public partial class Status
    {
        [JsonProperty("advisoryTypeInfo")]
        public string AdvisoryTypeInfo { get; set; }
    }

    public partial class Recommendation
    {
        [JsonProperty("itemNumber")]
        public RecommendationItemNumber ItemNumber { get; set; }

        [JsonProperty("recPriceInfo")]
        public RecPriceInfo RecPriceInfo { get; set; }

        [JsonProperty("segmentFlightRef")]
        public List<SegmentFlightRef> SegmentFlightRef { get; set; }

        [JsonProperty("paxFareProduct")]
        public List<PaxFareProduct> PaxFareProduct { get; set; }
    }

    public partial class SegmentFlightRef
    {
        [JsonProperty("referencingDetail")]
        public List<ReferencingDetail> ReferencingDetail { get; set; }
    }

    public partial class ReferencingDetail
    {
        [JsonProperty("refQualifier")]
        public string RefQualifier { get; set; }

        [JsonProperty("refNumber")]
        public int RefNumber { get; set; }
    }

    public partial class RecPriceInfo
    {
        [JsonProperty("monetaryDetail")]
        public List<RecPriceInfoMonetaryDetail> MonetaryDetail { get; set; }
    }

    public partial class RecPriceInfoMonetaryDetail
    {
        [JsonProperty("amount")]
        public double Amount { get; set; }
    }

    public partial class PaxFareProduct
    {
        [JsonProperty("paxFareDetail")]
        public PaxFareDetail PaxFareDetail { get; set; }

        [JsonProperty("paxReference")]
        public List<PaxReference> PaxReference { get; set; }

        [JsonProperty("fare")]
        public List<Fare> Fare { get; set; }

        [JsonProperty("fareDetails")]
        public List<FareDetail> FareDetails { get; set; }
    }

    //public partial class PaxReference
    //{
    //    //[JsonProperty("ptc")]
    //    //public List<string> Ptc { get; set; }

    //    //[JsonProperty("traveller")]
    //    //public List<Traveller> Traveller { get; set; }
    //}

    //public partial class Traveller
    //{
    //    //[JsonProperty("ref")]
    //    //public long Ref { get; set; }

    //    //[JsonProperty("infantIndicator")]
    //    //public long? InfantIndicator { get; set; }
    //}

    public partial class PaxFareDetail
    {
        [JsonProperty("paxFareNum")]
        public string PaxFareNum { get; set; }

        [JsonProperty("totalFareAmount")]
        public double TotalFareAmount { get; set; }

        [JsonProperty("totalTaxAmount")]
        public double TotalTaxAmount { get; set; }

        [JsonProperty("codeShareDetails")]
        public List<CodeShareDetail> CodeShareDetails { get; set; }

        [JsonProperty("pricingTicketing")]
        public PricingTicketing PricingTicketing { get; set; }
    }

    //public partial class PricingTicketing
    //{
    //    //[JsonProperty("priceType")]
    //    //public List<string> PriceType { get; set; }
    //}

    public partial class CodeShareDetail
    {
        [JsonProperty("transportStageQualifier")]
        public string TransportStageQualifier { get; set; }

        [JsonProperty("company")]
        public string Company { get; set; }
    }

    public partial class FareDetail
    {
        [JsonProperty("segmentRef")]
        public EgmentRef SegmentRef { get; set; }

        [JsonProperty("groupOfFares")]
        public List<GroupOfFare> GroupOfFares { get; set; }

        [JsonProperty("majCabin")]
        public List<MajCabin> MajCabin { get; set; }
    }

    public partial class MajCabin
    {
        [JsonProperty("bookingClassDetails")]
        public List<BookingClassDetail> BookingClassDetails { get; set; }
    }

    public partial class BookingClassDetail
    {
        [JsonProperty("designator")]
        public string Designator { get; set; }
    }

    public partial class GroupOfFare
    {
        [JsonProperty("productInformation")]
        public ProductInformation ProductInformation { get; set; }

        [JsonProperty("ticketInfos")]
        public TicketInfos TicketInfos { get; set; }
    }

    public partial class TicketInfos
    {
        [JsonProperty("additionalFareDetails")]
        public AdditionalFareDetails AdditionalFareDetails { get; set; }
    }

    public partial class AdditionalFareDetails
    {
        [JsonProperty("ticketDesignator")]
        public string TicketDesignator { get; set; }
    }

    public partial class ProductInformation
    {
        [JsonProperty("cabinProduct")]
        public CabinProduct CabinProduct { get; set; }

        [JsonProperty("fareProductDetail")]
        public FareProductDetail FareProductDetail { get; set; }

        [JsonProperty("breakPoint")]
        public string BreakPoint { get; set; }
    }

    public partial class FareProductDetail
    {
        [JsonProperty("fareBasis")]
        public string FareBasis { get; set; }

        [JsonProperty("passengerType")]
        public string PassengerType { get; set; }

        [JsonProperty("fareType")]
        public List<string> FareType { get; set; }
    }

    public partial class CabinProduct
    {
        [JsonProperty("rbd")]
        public string Rbd { get; set; }

        [JsonProperty("cabin")]
        public string Cabin { get; set; }

        [JsonProperty("avlStatus")]
        public string AvlStatus { get; set; }
    }

    public partial class Fare
    {
        [JsonProperty("pricingMessage")]
        public PricingMessage PricingMessage { get; set; }

        [JsonProperty("monetaryInformation")]
        public MonetaryInformation MonetaryInformation { get; set; }
    }

    public partial class PricingMessage
    {
        [JsonProperty("freeTextQualification")]
        public FreeTextQualification FreeTextQualification { get; set; }

        [JsonProperty("description")]
        public List<string> Description { get; set; }
    }

    public partial class FreeTextQualification
    {
        [JsonProperty("textSubjectQualifier")]
        public string TextSubjectQualifier { get; set; }

        [JsonProperty("informationType")]
        public string InformationType { get; set; }
    }

    public partial class MonetaryInformation
    {
        [JsonProperty("monetaryDetail")]
        public List<MonetaryInformationMonetaryDetail> MonetaryDetail { get; set; }
    }

    public partial class MonetaryInformationMonetaryDetail
    {
        [JsonProperty("amountType")]
        public string AmountType { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }

    public partial class RecommendationItemNumber
    {
        [JsonProperty("itemNumberId")]
        public ItemNumberItemNumberId ItemNumberId { get; set; }
    }

    public partial class ItemNumberItemNumberId
    {
        [JsonProperty("number")]
        public string Number { get; set; }
    }

    public partial class FlightIndex
    {
        [JsonProperty("requestedSegmentRef")]
        public EgmentRef RequestedSegmentRef { get; set; }

        [JsonProperty("groupOfFlights")]
        public List<GroupOfFlight> GroupOfFlights { get; set; }
    }

    public partial class EgmentRef
    {
        [JsonProperty("segRef")]
        public long SegRef { get; set; }
    }

    public partial class GroupOfFlight
    {
        [JsonProperty("propFlightGrDetail")]
        public PropFlightGrDetail PropFlightGrDetail { get; set; }

        [JsonProperty("flightDetails")]
        public List<FlightDetail> FlightDetails { get; set; }
    }

    public partial class PropFlightGrDetail
    {
        [JsonProperty("flightProposal")]
        public List<FlightProposal> FlightProposal { get; set; }
    }

    public partial class FlightProposal
    {
        [JsonProperty("ref")]
        public string Ref { get; set; }

        [JsonProperty("unitQualifier")]
        public string UnitQualifier { get; set; }
    }

    public partial class FlightDetail
    {
        [JsonProperty("flightInformation")]
        public FlightInformation FlightInformation { get; set; }

        [JsonProperty("flightCharacteristics")]
        public FlightCharacteristics FlightCharacteristics { get; set; }
    }

    public partial class FlightInformation
    {
        [JsonProperty("productDateTime")]
        public ProductDateTime ProductDateTime { get; set; }

        [JsonProperty("location")]
        public List<Location> Location { get; set; }

        [JsonProperty("companyId")]
        public CompanyId CompanyId { get; set; }

        [JsonProperty("flightOrtrainNumber")]
        public string FlightOrtrainNumber { get; set; }

        [JsonProperty("productDetail")]
        public ProductDetail ProductDetail { get; set; }

        [JsonProperty("addProductDetail")]
        public AddProductDetail AddProductDetail { get; set; }

        [JsonProperty("attributeDetails")]
        public List<AttributeDetail> AttributeDetails { get; set; }
    }

    public partial class ProductDetail
    {
        [JsonProperty("equipmentType")]
        public string EquipmentType { get; set; }
    }

    public partial class ProductDateTime
    {
        [JsonProperty("dateOfDeparture")]
        public string DateOfDeparture { get; set; }

        [JsonProperty("timeOfDeparture")]
        public string TimeOfDeparture { get; set; }

        [JsonProperty("dateOfArrival")]
        public string DateOfArrival { get; set; }

        [JsonProperty("timeOfArrival")]
        public string TimeOfArrival { get; set; }

        [JsonProperty("dateVariation")]
        public int? DateVariation { get; set; }
    }

    public partial class Location
    {
        [JsonProperty("locationId")]
        public string LocationId { get; set; }

        [JsonProperty("terminal")]
        public string Terminal { get; set; }
    }

    public partial class CompanyId
    {
        [JsonProperty("marketingCarrier")]
        public string MarketingCarrier { get; set; }

        [JsonProperty("operatingCarrier")]
        public string OperatingCarrier { get; set; }
    }

    public partial class AttributeDetail
    {
        [JsonProperty("attributeType")]
        public string AttributeType { get; set; }

        [JsonProperty("attributeDescription")]
        public string AttributeDescription { get; set; }
    }

    public partial class AddProductDetail
    {
        [JsonProperty("electronicTicketing")]
        public string ElectronicTicketing { get; set; }

        [JsonProperty("productDetailQualifier")]
        public string ProductDetailQualifier { get; set; }
    }

    public partial class FlightCharacteristics
    {
        public FlightCharacteristics()
        {
            InFlightSrv = new List<string>();
        }
        [JsonProperty("inFlightSrv")]
        public List<string> InFlightSrv { get; set; }
    }

    //public partial class ConversionRate
    //{
    //    //[JsonProperty("conversionRateDetail")]
    //    //public List<ConversionRateDetail> ConversionRateDetail { get; set; }
    //}

    //public partial class ConversionRateDetail
    //{
    //    //[JsonProperty("currency")]
    //    //public string Currency { get; set; }
    //}

    public partial class FlightSearchRs
    {
        public static FlightSearchRs FromJson(string json) => JsonConvert.DeserializeObject<FlightSearchRs>(json, Converter.Settings);
    }

    //public static class Serialize
    //{
    //    public static string ToJson(this FlightSearchRs self) => JsonConvert.SerializeObject(self, Converter.Settings);
    //}

    //public class Converter
    //{
    //    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    //    {
    //        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
    //        DateParseHandling = DateParseHandling.None,
    //    };
    //}
}