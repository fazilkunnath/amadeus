﻿
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class PnrAdd
{
    public PnrAdd()
    {
        PnrAddMultiElements = new PnrAddMultiElements();
    }
    [JsonProperty("pnrAddMultiElements")]
    public PnrAddMultiElements PnrAddMultiElements { get; set; }


    [JsonProperty("loginId")]
    public string LoginId { get; set; }

    [JsonProperty("password")]
    public string Password { get; set; }

    [JsonProperty("uuId")]
    public string UuId { get; set; }

}

public partial class PnrAddMultiElements
{
    public PnrAddMultiElements()
    {
        ReservationInfo = new ReservationInfo();
        PnrActions = new PnrActions();
        TravellerInfo = new List<global::TravellerInfo>();
        DataElementsMaster = new DataElementsMaster();
    }
    [JsonProperty("reservationInfo")]
    public ReservationInfo ReservationInfo { get; set; }

    [JsonProperty("pnrActions")]
    public PnrActions PnrActions { get; set; }

    [JsonProperty("travellerInfo")]
    public List<TravellerInfo> TravellerInfo { get; set; }

    [JsonProperty("dataElementsMaster")]
    public DataElementsMaster DataElementsMaster { get; set; }

    public bool serializeTravelleInfo()
    {
        return TravellerInfo.Count > 0;
    }
}

public partial class DataElementsMaster
{
    public DataElementsMaster()
    {
        Marker1 = new Marker1();
        DataElementsIndiv = new List<DataElementsIndiv>();
    }
    [JsonProperty("marker1")]
    public Marker1 Marker1 { get; set; }

    [JsonProperty("dataElementsIndiv")]
    public List<DataElementsIndiv> DataElementsIndiv { get; set; }
}

public partial class DataElementsIndiv
{
//    public DataElementsIndiv()
//    {
//        ElementManagementData = new ElementManagementData();
//        FormOfPayment = new FormOfPayment();
//        FreetextData = new FreetextData();
//        TicketElement = new TicketElement();
//        ServiceRequest = new ServiceRequest();
//        ReferenceForDataElement = new ReferenceForDataElement();


//}
[JsonProperty("elementManagementData")]
    public ElementManagementData ElementManagementData { get; set; }

    [JsonProperty("formOfPayment")]
    public FormOfPayment FormOfPayment { get; set; }

    [JsonProperty("freetextData")]
    public FreetextData FreetextData { get; set; }

    [JsonProperty("ticketElement")]
    public TicketElement TicketElement { get; set; }

    [JsonProperty("serviceRequest")]
    public ServiceRequest ServiceRequest { get; set; }

    [JsonProperty("referenceForDataElement")]
    public ReferenceForDataElement ReferenceForDataElement { get; set; }
}

public partial class ElementManagementData
{
    [JsonProperty("reference")]
    public Reference Reference { get; set; }

    [JsonProperty("segmentName")]
    public string SegmentName { get; set; }
}

public partial class FormOfPayment
{
    public FormOfPayment()
    {
        Fop = new List<global::Fop>();
    }
    [JsonProperty("fop")]
    public List<Fop> Fop { get; set; }
}

public partial class Fop
{
    [JsonProperty("identification")]
    public string Identification { get; set; }
}

public partial class FreetextData
{

    public FreetextData()
    {
        FreetextDetail = new FreetextDetail();
     

    }
    [JsonProperty("freetextDetail")]
    public FreetextDetail FreetextDetail { get; set; }

    [JsonProperty("longFreetext")]
    public string LongFreetext { get; set; }
}

public partial class FreetextDetail
{
    [JsonProperty("subjectQualifier")]
    public string SubjectQualifier { get; set; }

    [JsonProperty("type")]
    public string Type { get; set; }
}

public partial class ReferenceForDataElement
{
    public ReferenceForDataElement()
    {
        Reference = new List<Reference>();
    }
    [JsonProperty("reference")]
    public List<Reference> Reference { get; set; }
}

public partial class Reference
{
    [JsonProperty("qualifier")]
    public string Qualifier { get; set; }

    [JsonProperty("number")]
    public string Number { get; set; }
}

public partial class ServiceRequest
{
    public ServiceRequest()
    {
        Ssr = new Ssr();
    }
    [JsonProperty("ssr")]
    public Ssr Ssr { get; set; }
}

public partial class Ssr
{
    [JsonProperty("type")]
    public string Type { get; set; }

    [JsonProperty("status")]
    public string Status { get; set; }

    [JsonProperty("quantity")]
    public long Quantity { get; set; }

    [JsonProperty("freetext")]
    public List<string> Freetext { get; set; }
}

public partial class TicketElement
{
    public TicketElement()
    {
        Ticket = new Ticket1();
    }
    [JsonProperty("ticket")]
    public Ticket1 Ticket { get; set; }
}

public partial class Ticket1
{
    [JsonProperty("indicator")]
    public string Indicator { get; set; }
}

public partial class Marker1
{
}

public partial class PnrActions
{
    public PnrActions()
    {
        OptionCode = new List<long>();
    }
    [JsonProperty("optionCode")]
    public List<long> OptionCode { get; set; }
}

public partial class ReservationInfo
{
    public ReservationInfo()
    {
        Reservation = new Marker1();
    }
    [JsonProperty("reservation")]
    public Marker1 Reservation { get; set; }
}

public partial class TravellerInfo
{
    public TravellerInfo()
    {
        ElementManagementPassenger = new ElementManagementPassenger();
        PassengerData = new List<PassengerData>();
    }
    [JsonProperty("elementManagementPassenger")]
    public ElementManagementPassenger ElementManagementPassenger { get; set; }

    [JsonProperty("passengerData")]
    public List<PassengerData> PassengerData { get; set; }
}

public partial class ElementManagementPassenger
{
    public ElementManagementPassenger()
    {
        Reference = new Reference();
    }
    [JsonProperty("reference")]
    public Reference Reference { get; set; }

    [JsonProperty("segmentName")]
    public string SegmentName { get; set; }
}

public partial class PassengerData
{
    public PassengerData()
    {
        TravellerInformation = new TravellerInformation();

       // DateOfBirth = new DateOfBirth();
    }
    [JsonProperty("travellerInformation")]
    public TravellerInformation TravellerInformation { get; set; }


    [JsonProperty("dateOfBirth")]
    public DateOfBirth DateOfBirth { get; set; }
}


public partial class DateOfBirth
{
  
    [JsonProperty("dateAndTimeDetails")]
    public DateAndTimeDetails DateAndTimeDetails { get; set; }
}

public partial class DateAndTimeDetails
{
    [JsonProperty("date")]
    public string Date { get; set; }
}
public partial class TravellerInformation
{
    public TravellerInformation()
    {
        Traveller = new Traveller();
        Passenger = new List<Passenger>();

    }
    [JsonProperty("traveller")]
    public Traveller Traveller { get; set; }

    [JsonProperty("passenger")]
    public List<Passenger> Passenger { get; set; }
}

public partial class Passenger
{
    [JsonProperty("firstName")]
    public string FirstName { get; set; }

    [JsonProperty("type")]
    public string Type { get; set; }

    [JsonProperty("infantIndicator")]
    public string InfantIndicator { get; set; }
}

public partial class Traveller
{
    [JsonProperty("surname")]
    public string Surname { get; set; }

    [JsonProperty("quantity")]
    public string Quantity { get; set; }
}

public partial class PnrAdd
{
    public static PnrAdd FromJson(string json) => JsonConvert.DeserializeObject<PnrAdd>(json, PnrAddConverter.Settings);
}

public static class PnrAddSerialize
{
    public static string ToJson(this PnrAdd self) => JsonConvert.SerializeObject(self, PnrAddConverter.Settings);
}

public class PnrAddConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
    };
}
