﻿using Newtonsoft.Json;

public partial class PnrCancel
{
    public PnrCancel()
    {
        PurplePnrCancel = new PurplePnrCancel();
    }
    [JsonProperty("PNR_Cancel")]
    public PurplePnrCancel PurplePnrCancel { get; set; }
}

public partial class PurplePnrCancel
{
    public PurplePnrCancel()
    {
        ReservationInfo = new ReservationInfo1();
        PnrActions1 = new PnrActions1();
        CancelElements = new CancelElements();

    }
    [JsonProperty("reservationInfo")]
    public ReservationInfo1 ReservationInfo { get; set; }

    [JsonProperty("pnrActions")]
    public PnrActions1 PnrActions1 { get; set; }

    [JsonProperty("cancelElements")]
    public CancelElements CancelElements { get; set; }
}

public partial class CancelElements
{
    [JsonProperty("entryType")]
    public string EntryType { get; set; }
}

public partial class PnrActions1
{
    [JsonProperty("optionCode")]
    public string OptionCode { get; set; }
}

public partial class ReservationInfo1
{
    public ReservationInfo1()
    {
        Reservation = new Reservation1();
    }
    [JsonProperty("reservation")]
    public Reservation1 Reservation { get; set; }
}

public partial class Reservation1
{
    [JsonProperty("controlNumber")]
    public string ControlNumber { get; set; }
}

public partial class PnrCancel
{
    public static PnrCancel FromJson(string json) => JsonConvert.DeserializeObject<PnrCancel>(json, PnrCancelConverter.Settings);
}

public static class PnrCancelSerialize
{
    public static string ToJson(this PnrCancel self) => JsonConvert.SerializeObject(self, PnrCancelConverter.Settings);
}

public class PnrCancelConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
    };
}