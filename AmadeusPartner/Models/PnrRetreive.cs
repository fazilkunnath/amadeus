﻿using Newtonsoft.Json;
using System.Collections.Generic;

public partial class PnrRetrieve
{
    public PnrRetrieve()
    {
        PurplePnrRetrieve = new PurplePnrRetrieve();
    }
    [JsonProperty("pnrRetrieve")]
    public PurplePnrRetrieve PurplePnrRetrieve { get; set; }

    [JsonProperty("loginId")]
    public string LoginId { get; set; }

    [JsonProperty("password")]
    public string Password { get; set; }

    [JsonProperty("uuId")]
    public string UuId { get; set; }
}

public partial class PurplePnrRetrieve
{
    public PurplePnrRetrieve()
    {
        RetrievalFacts = new RetrievalFacts();

    }
    [JsonProperty("retrievalFacts")]
    public RetrievalFacts RetrievalFacts { get; set; }
}

public partial class RetrievalFacts
{
    public RetrievalFacts()
    {
        Retrieve = new Retrieve();

        ReservationOrProfileIdentifier = new ReservationOrProfileIdentifier();
    }
    [JsonProperty("retrieve")]
    public Retrieve Retrieve { get; set; }

    [JsonProperty("reservationOrProfileIdentifier")]
    public ReservationOrProfileIdentifier ReservationOrProfileIdentifier { get; set; }
}

public partial class ReservationOrProfileIdentifier
{
    public ReservationOrProfileIdentifier()
    {
        Reservation = new List<global::Reservation>();
    }
    [JsonProperty("reservation")]
    public List<Reservation> Reservation { get; set; }
    public bool ShouldSerializeReservation()
    {
        return Reservation.Count > 0;
    }
}

public partial class Reservation
{
    [JsonProperty("controlNumber", NullValueHandling = NullValueHandling.Ignore)]
    public string ControlNumber { get; set; }
}

public partial class Retrieve
{
    [JsonProperty("type")]
    public long Type { get; set; }
}

public partial class PnrRetrieve
{
    public static PnrRetrieve FromJson(string json) => JsonConvert.DeserializeObject<PnrRetrieve>(json, PnrRetrieveConverter.Settings);
}

public static class PnrRetrieveSerialize
{
    public static string ToJson(this PnrRetrieve self) => JsonConvert.SerializeObject(self, PnrRetrieveConverter.Settings);
}

public class PnrRetrieveConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
    };
}

