﻿namespace AmadeusPartner.Models
{
    using System;
    using System.Net;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    public partial class ResponseEntity
    {
        public ResponseEntity()
        {
            FareMasterPricerTravelBoardSearchReply = new FareMasterPricerTravelBoardSearchReply();
        }

        [JsonProperty("fareMasterPricerTravelBoardSearchReply")]
        public FareMasterPricerTravelBoardSearchReply FareMasterPricerTravelBoardSearchReply { get; set; }
    }

    public partial class FareMasterPricerTravelBoardSearchReply
    {
        public FareMasterPricerTravelBoardSearchReply()
        {
            ReplyStatus = new ReplyStatus();
            ConversionRate = new ConversionRate();
            FlightIndex = new List<Models.FlightIndex>();
            //Recommendation = new List<Models.Recommendation>();
            //ServiceFeesGrp = new List<Models.ServiceFeesGrp>();
        }
        //[JsonProperty("replyStatus")]
        //public ReplyStatus ReplyStatus { get; set; }

        //[JsonProperty("conversionRate")]
        //public ConversionRate ConversionRate { get; set; }

        //[JsonProperty("flightIndex")]
        //public List<FlightIndex> FlightIndex { get; set; }
    }

    //public partial class ReplyStatus
    //{
    //    //[JsonProperty("status")]
    //    //public Status Status { get; set; }
    //}

    //public partial class Status
    //{
    //    //[JsonProperty("advisoryTypeInfo")]
    //    //public string AdvisoryTypeInfo { get; set; }
    //}

    public partial class FlightIndex
    {
        public FlightIndex()
        {
            GroupOfFlights = new List<GroupOfFlight>();
            SegmentRef = new SegmentRef();
            TypeRef = new TypeRef();
            Reccount = new Reccount();
            Fare = new Fare();
            FareDetails = new FareDetails();
            BagDetails = new Bagdetails();
            Description = new Description();
            RequestedSegmentRef = new EgmentRef();
        }

        [JsonProperty("SegmentRef")]
        public SegmentRef SegmentRef { get; set; }

        [JsonProperty("TypeRef")]
        public TypeRef TypeRef { get; set; }

        [JsonProperty("reccount")]
        public Reccount Reccount { get; set; }

        [JsonProperty("fare")]
        public Fare Fare { get; set; }

        [JsonProperty("fareDetails")]
        public FareDetails FareDetails { get; set; }

        [JsonProperty("Bagdetails")]
        public Bagdetails BagDetails { get; set; }

        [JsonProperty("Description")]
        public Description Description { get; set; }

        //[JsonProperty("groupOfFlights")]
        //public List<GroupOfFlight> GroupOfFlights { get; set; }
    }

    public partial class TypeRef
    {
        [JsonProperty("type")]
        public double Type { get; set; }
    }

    public partial class SegmentRef
    {
        [JsonProperty("segRef")]
        public double SegRef { get; set; }

        [JsonProperty("PSessionId")]
        public string PSessionId { get; set; }

        [JsonProperty("supplier")]
        public string Supplier { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }
    }

    public partial class Reccount
    {
        [JsonProperty("rec_id")]
        public double RecId { get; set; }

        [JsonProperty("combi_id")]
        public double CombiId { get; set; }
    }

    public partial class GroupOfFlight
    {
        [JsonProperty("FlightProposal")]
        public FlightProposal FlightProposal { get; set; }

        //[JsonProperty("flightDetails")]
        //public List<FlightDetail> FlightDetails { get; set; }
    }

    public partial class FlightProposal
    {
        [JsonProperty("elapse")]
        public string Elapse { get; set; }

        //[JsonProperty("unitQualifier")]
        //public string UnitQualifier { get; set; }

        [JsonProperty("VAirline")]
        public string VAirline { get; set; }

        [JsonProperty("Ocarrier")]
        public string Ocarrier { get; set; }
    }

    public partial class FlightDetail
    {
        public FlightDetail()
        {
            Finfo = new Finfo();
            //FlightCharacteristics = new FlightCharacteristics();
        }

        [JsonProperty("Finfo")]
        public Finfo Finfo { get; set; }

        //    [JsonProperty("FlightCharacteristics")]
        //    public FlightCharacteristics FlightCharacteristics { get; set; }
    }

    //public partial class FlightCharacteristics
    //{
    //    [JsonProperty("inFlightSrv")]
    //    public string InFlightSrv { get; set; }
    //}

    public partial class Finfo
    {
        public Finfo()
        {
            Location = new Location();
            CompanyId = new CompanyId();
            DateTime = new DateTime();
        }

        [JsonProperty("DateTime")]
        public DateTime DateTime { get; set; }

        [JsonProperty("location")]
        public Location Location { get; set; }

        [JsonProperty("companyId")]
        public CompanyId CompanyId { get; set; }

        [JsonProperty("flightNo")]
        public string FlightNo { get; set; }

        [JsonProperty("Bookingclass")]
        public string Bookingclass { get; set; }

        [JsonProperty("eqpType")]
        public string EqpType { get; set; }

        [JsonProperty("eTicketing")]
        public string ETicketing { get; set; }

        [JsonProperty("attributeType")]
        public string AttributeType { get; set; }

        [JsonProperty("Elapsedtime")]
        public string Elapsedtime { get; set; }
    }

    public partial class Location
    {
        [JsonProperty("locationFrom")]
        public string LocationFrom { get; set; }

        [JsonProperty("Fromterminal")]
        public string Fromterminal { get; set; }

        [JsonProperty("LocationTo")]
        public string LocationTo { get; set; }

        [JsonProperty("Toterminal")]
        public string Toterminal { get; set; }
    }

    public partial class DateTime
    {
        [JsonProperty("Depdate")]
        public string Depdate { get; set; }

        [JsonProperty("Deptime")]
        public string Deptime { get; set; }

        [JsonProperty("Arrdate")]
        public string Arrdate { get; set; }

        [JsonProperty("Arrtime")]
        public string Arrtime { get; set; }

        [JsonProperty("Variation")]
        public string Variation { get; set; }
    }

    public partial class CompanyId
    {
        [JsonProperty("mCarrier")]
        public string MCarrier { get; set; }

        [JsonProperty("oCarrier")]
        public string OCarrier { get; set; }
    }

    public partial class FareDetails
    {
        [JsonProperty("fareBasis")]
        public string FareBasis { get; set; }
    }

    public partial class Fare
    {
        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("taxfare")]
        public double Taxfare { get; set; }

        [JsonProperty("basefare")]
        public double Basefare { get; set; }

        [JsonProperty("MarkupFare")]
        public double MarkupFare { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }

    public partial class Description
    {
        [JsonProperty("Pricingmsg")]
        public string Pricingmsg { get; set; }
    }

    public partial class Bagdetails
    {
        [JsonProperty("freeAllowance")]
        public string FreeAllowance { get; set; }

        [JsonProperty("Qcode")]
        public string Qcode { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }
    }

    public partial class ConversionRate
    {
        //[JsonProperty("conversionRateDetail")]
        //public ConversionRateDetail ConversionRateDetail { get; set; }
        [JsonIgnore]
        [JsonProperty("SessionId")]
        public string SessionId { get; set; }
    }

    //public partial class ConversionRateDetail
    //{
    //    //[JsonProperty("currency")]
    //    //public string Currency { get; set; }
    //}

    public partial class ResponseEntity
    {
        public static ResponseEntity FromJson(string json) => JsonConvert.DeserializeObject<ResponseEntity>(json, Converter.Settings);
    }

    //public static class Serialize
    //{
    //    public static string ToJson(this ResponseEntity self) => JsonConvert.SerializeObject(self, Converter.Settings);
    //}

    //public class Converter
    //{
    //    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    //    {
    //        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
    //        DateParseHandling = DateParseHandling.None,
    //    };
    //}
}