﻿
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class TicketCreate
{
    public TicketCreate()
    {
        TicketCreateTstFromPricing = new TicketCreateTstFromPricing();
    }
    [JsonProperty("ticketCreateTSTFromPricing")]
    public TicketCreateTstFromPricing TicketCreateTstFromPricing { get; set; }

    [JsonProperty("loginId")]
    public string LoginId { get; set; }

    [JsonProperty("password")]
    public string Password { get; set; }

    [JsonProperty("uuId")]
    public string UuId { get; set; }
}

public partial class TicketCreateTstFromPricing
{
    public TicketCreateTstFromPricing()
    {
        PsaList = new List<global::PsaList>();
    }
    [JsonProperty("psaList")]
    public List<PsaList> PsaList { get; set; }
}

public partial class PsaList
{
    public PsaList()
    {
        ItemReference = new ItemReference();
    }
    [JsonProperty("itemReference")]
    public ItemReference ItemReference { get; set; }
}

public partial class ItemReference
{
    [JsonProperty("referenceType")]
    public string ReferenceType { get; set; }

    [JsonProperty("uniqueReference")]
    public long UniqueReference { get; set; }
}

public partial class TicketCreate
{
    public static TicketCreate FromJson(string json) => JsonConvert.DeserializeObject<TicketCreate>(json, TicketCreateConverter.Settings);
}

public static class TicketCreateSerialize
{
    public static string ToJson(this TicketCreate self) => JsonConvert.SerializeObject(self, TicketCreateConverter.Settings);
}

public class TicketCreateConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
    };
}
