﻿
using AmadeusPartner;
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class PnrRetrieveAmRs
{
    [JsonProperty("pnrReply")]
    public PnrReply PnrReply { get; set; }
}

public partial class PnrReply
{
    [JsonProperty("pnrHeader")]
    public List<PnrHeader> PnrHeader { get; set; }

    [JsonProperty("securityInformation")]
    public SecurityInformation SecurityInformation { get; set; }

    [JsonProperty("freetextData")]
    public List<OtherDataFreetext> FreetextData { get; set; }

    [JsonProperty("pnrHeaderTag")]
    public PnrHeaderTag PnrHeaderTag { get; set; }

    [JsonProperty("sbrPOSDetails")]
    public SbrDetails SbrPosDetails { get; set; }

    [JsonProperty("sbrCreationPosDetails")]
    public SbrDetails SbrCreationPosDetails { get; set; }

    [JsonProperty("sbrUpdatorPosDetails")]
    public SbrDetails SbrUpdatorPosDetails { get; set; }

    [JsonProperty("technicalData")]
    public TechnicalData TechnicalData { get; set; }

    [JsonProperty("travellerInfo")]
    public List<TravellerInfo> TravellerInfo { get; set; }

    [JsonProperty("originDestinationDetails")]
    public List<OriginDestinationDetail> OriginDestinationDetails { get; set; }

    [JsonProperty("dataElementsMaster")]
    public DataElementsMaster1 DataElementsMaster { get; set; }

    [JsonProperty("tstData")]
    public List<TstDatum> TstData { get; set; }

    [JsonProperty("pricingRecordGroup")]
    public PricingRecordGroup PricingRecordGroup { get; set; }
}

public partial class DataElementsMaster1
{
    [JsonProperty("marker2")]
    public Marker2 Marker2 { get; set; }

    [JsonProperty("dataElementsIndiv")]
    public List<DataElementsIndiv1> DataElementsIndiv { get; set; }
}
public class Marker2
{ }
public partial class DataElementsIndiv1
{
    [JsonProperty("elementManagementData")]
    public ElementManagement1 ElementManagementData1 { get; set; }

    [JsonProperty("otherDataFreetext")]
    public List<OtherDataFreetext> OtherDataFreetext { get; set; }

    //    [JsonProperty("ticketElement")]
    //    public TicketElement TicketElement { get; set; }

    //    [JsonProperty("serviceRequest")]
    //    public ServiceRequest ServiceRequest { get; set; }

    //    [JsonProperty("referenceForDataElement")]
    //    public ReferenceFor ReferenceForDataElement { get; set; }
}

public partial class ElementManagement1
{
    [JsonProperty("reference")]
    public ElementManagementDataReference Reference { get; set; }

    [JsonProperty("segmentName")]
    public string SegmentName { get; set; }

    [JsonProperty("lineNumber")]
    public long LineNumber { get; set; }
}

public partial class ElementManagementDataReference
{
    [JsonProperty("qualifier")]
    public string Qualifier { get; set; }

    [JsonProperty("number")]
    public long Number { get; set; }
}

public partial class OtherDataFreetext
{
    //[JsonProperty("freetextDetail")]
    //public FreetextDetail FreetextDetail { get; set; }

    [JsonProperty("longFreetext")]
    public string LongFreetext { get; set; }
}

//public partial class FreetextDetail
//{
//    [JsonProperty("subjectQualifier")]
//    public string SubjectQualifier { get; set; }

//    [JsonProperty("type")]
//    public string Type { get; set; }
//}

public partial class ReferenceFor
{
    [JsonProperty("reference")]
    public List<ReferenceForDataElementReference> Reference { get; set; }
}

public partial class ReferenceForDataElementReference
{
    [JsonProperty("qualifier")]
    public string Qualifier { get; set; }

    [JsonProperty("number")]
    public string Number { get; set; }
}

public partial class ServiceRequest
{
    //[JsonProperty("ssr")]
    //   public Ssr Ssr { get; set; }
}

//public partial class Ssr
//{
//    [JsonProperty("type")]
//    public string Type { get; set; }

//    [JsonProperty("status")]
//    public string Status { get; set; }

//    [JsonProperty("quantity")]
//    public long? Quantity { get; set; }

//    [JsonProperty("companyId")]
//    public string CompanyId { get; set; }

//    [JsonProperty("freeText")]
//    public List<string> FreeText { get; set; }
//}

public partial class TicketElement
{
    
    public Ticket1 Ticket1 { get; set; }
}

public partial class Ticket1
{
    //[JsonProperty("indicator")]
    //public string Indicator1 { get; set; }

    [JsonProperty("date")]
    public string Date { get; set; }

    [JsonProperty("officeId")]
    public string OfficeId { get; set; }

    [JsonProperty("electronicTicketFlag")]
    public string ElectronicTicketFlag { get; set; }

    [JsonProperty("airlineCode")]
    public string AirlineCode { get; set; }
}

public partial class OriginDestination
{
}

public partial class OriginDestinationDetail
{
    [JsonProperty("originDestination")]
    public OriginDestination OriginDestination { get; set; }

    [JsonProperty("itineraryInfo")]
    public List<ItineraryInfo> ItineraryInfo { get; set; }
}

public partial class ItineraryInfo
{
    //[JsonProperty("elementManagementItinerary")]
    //public ElementManagement ElementManagementItinerary { get; set; }

    [JsonProperty("travelProduct")]
    public TravelProduct TravelProduct { get; set; }

    [JsonProperty("itineraryMessageAction")]
    public ItineraryMessageAction ItineraryMessageAction { get; set; }

    [JsonProperty("itineraryReservationInfo")]
    public ItineraryReservationInfo ItineraryReservationInfo { get; set; }

    [JsonProperty("relatedProduct")]
    public RelatedProduct RelatedProduct { get; set; }

    [JsonProperty("flightDetail")]
    public FlightDetail1 FlightDetail1 { get; set; }

    [JsonProperty("cabinDetails")]
    public ItineraryInfoCabinDetails CabinDetails { get; set; }

    [JsonProperty("selectionDetails")]
    public SelectionDetails SelectionDetails { get; set; }

    [JsonProperty("legInfo")]
    public List<LegInfo> LegInfo { get; set; }

    [JsonProperty("markerRailTour")]
    public Marker2 MarkerRailTour { get; set; }
}

public partial class ItineraryInfoCabinDetails
{
    [JsonProperty("cabinDetails")]
    public CabinDetailsCabinDetails CabinDetails { get; set; }
}

public partial class CabinDetailsCabinDetails
{
    [JsonProperty("classDesignator")]
    public string ClassDesignator { get; set; }
}

public partial class FlightDetail1
{
    [JsonProperty("productDetails")]
    public FlightDetailProductDetails ProductDetails { get; set; }

    [JsonProperty("departureInformation")]
    public DepartureInformation DepartureInformation { get; set; }

    [JsonProperty("arrivalStationInfo")]
    public ArrivalStationInfo ArrivalStationInfo { get; set; }

    [JsonProperty("mileageTimeDetails")]
    public MileageTimeDetails MileageTimeDetails { get; set; }

    [JsonProperty("timeDetail")]
    public TimeDetail TimeDetail { get; set; }

    [JsonProperty("facilities")]
    public List<Facility> Facilities { get; set; }
}

public partial class ArrivalStationInfo
{
    [JsonProperty("terminal")]
    public string Terminal { get; set; }
}

public partial class DepartureInformation
{
    [JsonProperty("departTerminal")]
    public string DepartTerminal { get; set; }
}

public partial class Facility
{
    [JsonProperty("entertainement")]
    public string Entertainement { get; set; }

    [JsonProperty("entertainementDescription")]
    public string EntertainementDescription { get; set; }
}

public partial class MileageTimeDetails
{
    [JsonProperty("flightLegMileage")]
    public long FlightLegMileage { get; set; }

    [JsonProperty("unitQualifier")]
    public string UnitQualifier { get; set; }
}

public partial class FlightDetailProductDetails
{
    [JsonProperty("equipment")]
    public string Equipment { get; set; }

    [JsonProperty("numOfStops")]
    public long NumOfStops { get; set; }

    [JsonProperty("duration")]
    public string Duration { get; set; }

    [JsonProperty("weekDay")]
    public long WeekDay { get; set; }
}

public partial class TimeDetail
{
    [JsonProperty("checkinTime")]
    public string CheckinTime { get; set; }
}

public partial class ItineraryMessageAction
{
    [JsonProperty("business")]
    public Business Business { get; set; }
}

public partial class Business
{
    [JsonProperty("function")]
    public string Function { get; set; }
}

public partial class ItineraryReservationInfo
{
    [JsonProperty("reservation")]
    public ReservationInfo1 ReservationInfo { get; set; }
}

public partial class ReservationInfo1
{
    [JsonProperty("companyId")]
    public string CompanyId { get; set; }

    [JsonProperty("controlNumber")]
    public string ControlNumber { get; set; }
}

public partial class LegInfo
{
    [JsonProperty("markerLegInfo")]
    public Marker2 MarkerLegInfo { get; set; }

    [JsonProperty("legTravelProduct")]
    public LegTravelProduct LegTravelProduct { get; set; }

    [JsonProperty("interactiveFreeText")]
    public List<InteractiveFreeText> InteractiveFreeText { get; set; }
}

public partial class InteractiveFreeText
{
    [JsonProperty("freeTextQualification")]
    public FreeTextQualification FreeTextQualification { get; set; }

    [JsonProperty("freeText")]
    public string FreeText { get; set; }
}

public partial class FreeTextQualification
{
    [JsonProperty("textSubjectQualifier")]
    public string TextSubjectQualifier { get; set; }
}

public partial class LegTravelProduct
{
    //    [JsonProperty("flightDate")]
    //    public FlightDate FlightDate { get; set; }

    //    [JsonProperty("boardPointDetails")]
    //    public OintDetails BoardPointDetails { get; set; }

    //    [JsonProperty("offpointDetails")]
    //    public OintDetails OffpointDetails { get; set; }
}

//public partial class OintDetails
//{
//    [JsonProperty("trueLocationId")]
//    public string TrueLocationId { get; set; }
//}

//public partial class FlightDate
//{
//    [JsonProperty("departureDate")]
//    public string DepartureDate { get; set; }

//    [JsonProperty("departureTime")]
//    public string DepartureTime { get; set; }

//    [JsonProperty("arrivalDate")]
//    public string ArrivalDate { get; set; }

//    [JsonProperty("arrivalTime")]
//    public string ArrivalTime { get; set; }
//}

public partial class RelatedProduct
{
    [JsonProperty("quantity")]
    public long Quantity { get; set; }

    [JsonProperty("status")]
    public List<string> Status { get; set; }
}

public partial class SelectionDetails
{
    [JsonProperty("selection")]
    public List<Selection> Selection { get; set; }
}

public partial class Selection
{
    [JsonProperty("option")]
    public string Option { get; set; }
}

public partial class TravelProduct
{
    [JsonProperty("product")]
    public Product Product { get; set; }

    [JsonProperty("boardpointDetail")]
    public PointDetail BoardpointDetail { get; set; }

    [JsonProperty("offpointDetail")]
    public PointDetail OffpointDetail { get; set; }

    [JsonProperty("companyDetail")]
    public CompanyDetail CompanyDetail { get; set; }

    [JsonProperty("productDetails")]
    public TravelProductProductDetails ProductDetails { get; set; }

    [JsonProperty("typeDetail")]
    public TypeDetail TypeDetail { get; set; }
}

public partial class PointDetail
{
    [JsonProperty("cityCode")]
    public string CityCode { get; set; }
}

public partial class CompanyDetail
{
    [JsonProperty("identification")]
    public string Identification { get; set; }
}

public partial class Product
{
    [JsonProperty("depDate")]
    public string DepDate { get; set; }

    [JsonProperty("depTime")]
    public string DepTime { get; set; }

    [JsonProperty("arrDate")]
    public string ArrDate { get; set; }

    [JsonProperty("arrTime")]
    public string ArrTime { get; set; }
}

public partial class TravelProductProductDetails
{
    [JsonProperty("identification")]
    public string Identification { get; set; }

    [JsonProperty("classOfService")]
    public string ClassOfService { get; set; }
}

public partial class TypeDetail
{
    [JsonProperty("detail")]
    public string Detail { get; set; }
}

public partial class PnrHeader
{
    [JsonProperty("reservationInfo")]
    public ReservationInfo2 ReservationInfo { get; set; }
}

public partial class ReservationInfo2
{
    [JsonProperty("reservation")]
    public List<Reservation2> Reservation { get; set; }
}

public partial class Reservation2
{
    [JsonProperty("companyId")]
    public string CompanyId { get; set; }

    [JsonProperty("controlNumber")]
    public string ControlNumber { get; set; }

    [JsonProperty("date")]
    public string Date { get; set; }

    [JsonProperty("time")]
    public long Time { get; set; }
}

public partial class PnrHeaderTag
{
    [JsonProperty("statusInformation")]
    public List<StatusInformation> StatusInformation { get; set; }
}

public partial class StatusInformation
{
    [JsonProperty("indicator")]
    public string Indicator { get; set; }
}

public partial class PricingRecordGroup
{
    [JsonProperty("pricingRecordData")]
    public Marker2 PricingRecordData { get; set; }

    [JsonProperty("productPricingQuotationRecord")]
    public List<ProductPricingQuotationRecord> ProductPricingQuotationRecord { get; set; }
}

public partial class ProductPricingQuotationRecord
{
    [JsonProperty("pricingRecordId")]
    public PricingRecordId PricingRecordId { get; set; }

    [JsonProperty("passengerTattoos")]
    public List<PassengerTattoo> PassengerTattoos { get; set; }

    [JsonProperty("documentDetailsGroup")]
    public DocumentDetailsGroup DocumentDetailsGroup { get; set; }
}

public partial class DocumentDetailsGroup
{
    [JsonProperty("totalFare")]
    public MonetaryInformation TotalFare { get; set; }

    [JsonProperty("issueIdentifier")]
    public IssueIdentifier IssueIdentifier { get; set; }

    [JsonProperty("manualIndicator")]
    public ManualIndicator ManualIndicator { get; set; }

    [JsonProperty("officeInformation")]
    public OfficeInformation OfficeInformation { get; set; }

    [JsonProperty("creationDate")]
    public CreationDate CreationDate { get; set; }

    [JsonProperty("couponDetailsGroup")]
    public List<CouponDetailsGroup> CouponDetailsGroup { get; set; }

    [JsonProperty("fareComponentDetailsGroup")]
    public List<FareComponentDetailsGroup> FareComponentDetailsGroup { get; set; }
}

public partial class CouponDetailsGroup
{
    [JsonProperty("productId")]
    public ProductId ProductId { get; set; }
}

public partial class ProductId
{
    [JsonProperty("referenceDetails")]
    public ReferenceDetails ReferenceDetails { get; set; }
}

public partial class ReferenceDetails
{
    [JsonProperty("type")]
    public string Type { get; set; }

    [JsonProperty("value")]
    public string Value { get; set; }
}

public partial class CreationDate
{
    [JsonProperty("businessSemantic")]
    public string BusinessSemantic { get; set; }

    [JsonProperty("dateTime")]
    public CreationDateDateTime DateTime { get; set; }
}

public partial class CreationDateDateTime
{
    [JsonProperty("year")]
    public string Year { get; set; }

    [JsonProperty("month")]
    public string Month { get; set; }

    [JsonProperty("day")]
    public string Day { get; set; }
}

public partial class FareComponentDetailsGroup
{
    [JsonProperty("fareComponentID")]
    public FareComponentId FareComponentId { get; set; }

    [JsonProperty("marketFareComponent")]
    public MarketFareComponent MarketFareComponent { get; set; }

    [JsonProperty("monetaryInformation")]
    public MonetaryInformation MonetaryInformation { get; set; }

    [JsonProperty("componentClassInfo")]
    public ComponentClassInfo ComponentClassInfo { get; set; }

    [JsonProperty("fareQualifiersDetail")]
    public FareQualifiersDetail FareQualifiersDetail { get; set; }

    [JsonProperty("couponDetailsGroup")]
    public List<CouponDetailsGroup> CouponDetailsGroup { get; set; }
}

public partial class ComponentClassInfo
{
    [JsonProperty("fareBasisDetails")]
    public FareBasisDetails FareBasisDetails { get; set; }
}

public partial class FareBasisDetails
{
    [JsonProperty("rateTariffClass")]
    public string RateTariffClass { get; set; }
}

public partial class FareComponentId
{
    [JsonProperty("itemNumberDetails")]
    public List<ItemNumberDetail> ItemNumberDetails { get; set; }
}

public partial class ItemNumberDetail
{
    [JsonProperty("number")]
    public string Number { get; set; }

    [JsonProperty("type")]
    public string Type { get; set; }
}

public partial class FareQualifiersDetail
{
    [JsonProperty("discountDetails")]
    public List<DiscountDetail> DiscountDetails { get; set; }
}

public partial class DiscountDetail
{
    [JsonProperty("fareQualifier")]
    public string FareQualifier { get; set; }
}

public partial class MarketFareComponent
{
    [JsonProperty("boardPointDetails")]
    public OintDetails BoardPointDetails { get; set; }

    [JsonProperty("offpointDetails")]
    public OintDetails OffpointDetails { get; set; }
}

public partial class MonetaryInformation
{
    [JsonProperty("monetaryDetails")]
    public MonetaryDetails MonetaryDetails { get; set; }
}

public partial class MonetaryDetails
{
    [JsonProperty("typeQualifier")]
    public string TypeQualifier { get; set; }

    [JsonProperty("amount")]
    public string Amount { get; set; }

    [JsonProperty("currency")]
    public string Currency { get; set; }
}

public partial class IssueIdentifier
{
    [JsonProperty("priceTicketDetails")]
    public PriceTicketDetails PriceTicketDetails { get; set; }

    [JsonProperty("priceTariffType")]
    public string PriceTariffType { get; set; }
}

public partial class PriceTicketDetails
{
    [JsonProperty("indicators")]
    public string Indicators { get; set; }
}

public partial class ManualIndicator
{
    // [JsonProperty("statusDetails")]
    // public StatusDetails StatusDetails { get; set; }
}

public partial class StatusDetails
{
    //[JsonProperty("indicator")]
    //public string Indicator { get; set; }

    [JsonProperty("action")]
    public string Action { get; set; }
}

public partial class OfficeInformation
{
    [JsonProperty("originIdentification")]
    public OfficeInformationOriginIdentification OriginIdentification { get; set; }
}

public partial class OfficeInformationOriginIdentification
{
    [JsonProperty("inHouseIdentification1")]
    public string InHouseIdentification1 { get; set; }

    [JsonProperty("inHouseIdentification2")]
    public string InHouseIdentification2 { get; set; }
}

public partial class PassengerTattoo
{
    [JsonProperty("passengerReference")]
    public ReferenceDetails PassengerReference { get; set; }
}

public partial class PricingRecordId
{
    [JsonProperty("referenceType")]
    public string ReferenceType { get; set; }

    [JsonProperty("uniqueReference")]
    public string UniqueReference { get; set; }
}

public partial class SbrDetails
{
    [JsonProperty("sbrUserIdentificationOwn")]
    public SbrUserIdentificationOwn SbrUserIdentificationOwn { get; set; }

    [JsonProperty("sbrSystemDetails")]
    public SbrSystemDetails SbrSystemDetails { get; set; }

    [JsonProperty("sbrPreferences")]
    public SbrPreferences SbrPreferences { get; set; }
}

public partial class SbrPreferences
{
    [JsonProperty("userPreferences")]
    public UserPreferences UserPreferences { get; set; }
}

public partial class UserPreferences
{
    [JsonProperty("codedCountry")]
    public string CodedCountry { get; set; }
}

public partial class SbrSystemDetails
{
    [JsonProperty("deliveringSystem")]
    public DeliveringSystem DeliveringSystem { get; set; }
}

public partial class DeliveringSystem
{
    [JsonProperty("companyId")]
    public string CompanyId { get; set; }

    [JsonProperty("locationId")]
    public string LocationId { get; set; }
}

public partial class SbrUserIdentificationOwn
{
    [JsonProperty("originIdentification")]
    public SbrUserIdentificationOwnOriginIdentification OriginIdentification { get; set; }

    [JsonProperty("originatorTypeCode")]
    public string OriginatorTypeCode { get; set; }
}

public partial class SbrUserIdentificationOwnOriginIdentification
{
    [JsonProperty("originatorId")]
    public long OriginatorId { get; set; }

    [JsonProperty("inHouseIdentification1")]
    public string InHouseIdentification1 { get; set; }
}

public partial class SecurityInformation
{
    [JsonProperty("responsibilityInformation")]
    public ResponsibilityInformation ResponsibilityInformation { get; set; }

    [JsonProperty("queueingInformation")]
    public QueueingInformation QueueingInformation { get; set; }

    [JsonProperty("cityCode")]
    public string CityCode { get; set; }

    [JsonProperty("secondRpInformation")]
    public SecondRpInformation SecondRpInformation { get; set; }
}

public partial class QueueingInformation
{
    [JsonProperty("queueingOfficeId")]
    public string QueueingOfficeId { get; set; }
}

public partial class ResponsibilityInformation
{
    [JsonProperty("typeOfPnrElement")]
    public string TypeOfPnrElement { get; set; }

    [JsonProperty("agentId")]
    public string AgentId { get; set; }

    [JsonProperty("officeId")]
    public string OfficeId { get; set; }

    [JsonProperty("iataCode")]
    public string IataCode { get; set; }
}

public partial class SecondRpInformation
{
    [JsonProperty("creationOfficeId")]
    public string CreationOfficeId { get; set; }

    [JsonProperty("agentSignature")]
    public string AgentSignature { get; set; }

    [JsonProperty("creationDate")]
    public string CreationDate { get; set; }

    [JsonProperty("creatorIataCode")]
    public string CreatorIataCode { get; set; }

    [JsonProperty("creationTime")]
    public string CreationTime { get; set; }
}

public partial class TechnicalData
{
    [JsonProperty("enveloppeNumberData")]
    public EnveloppeNumberData EnveloppeNumberData { get; set; }

    [JsonProperty("lastTransmittedEnvelopeNumber")]
    public LastTransmittedEnvelopeNumber LastTransmittedEnvelopeNumber { get; set; }

    [JsonProperty("purgeDateData")]
    public PurgeDateData PurgeDateData { get; set; }
}

public partial class EnveloppeNumberData
{
    [JsonProperty("sequenceDetails")]
    public SequenceDetails SequenceDetails { get; set; }
}

public partial class SequenceDetails
{
    [JsonProperty("number")]
    public string Number { get; set; }
}

public partial class LastTransmittedEnvelopeNumber
{
    [JsonProperty("currentRecord")]
    public long CurrentRecord { get; set; }
}

public partial class PurgeDateData
{
    [JsonProperty("dateTime")]
    public PurgeDateDataDateTime DateTime { get; set; }
}

public partial class PurgeDateDataDateTime
{
    [JsonProperty("year")]
    public long Year { get; set; }

    [JsonProperty("month")]
    public string Month { get; set; }

    [JsonProperty("day")]
    public string Day { get; set; }
}

public partial class TravellerInfo
{
    //[JsonProperty("elementManagementPassenger")]
    //public ElementManagement ElementManagementPassenger { get; set; }

    //    [JsonProperty("passengerData")]
    //    public List<PassengerData> PassengerData { get; set; }

    //    [JsonProperty("enhancedPassengerData")]
    //    public List<EnhancedPassengerDatum> EnhancedPassengerData { get; set; }
}

public partial class EnhancedPassengerDatum
{
    [JsonProperty("enhancedTravellerInformation")]
    public EnhancedTravellerInformation EnhancedTravellerInformation { get; set; }
}

public partial class EnhancedTravellerInformation
{
    [JsonProperty("travellerNameInfo")]
    public TravellerNameInfo TravellerNameInfo { get; set; }

    [JsonProperty("otherPaxNamesDetails")]
    public List<OtherPaxNamesDetail> OtherPaxNamesDetails { get; set; }
}

public partial class OtherPaxNamesDetail
{
    [JsonProperty("nameType")]
    public string NameType { get; set; }

    [JsonProperty("referenceName")]
    public string ReferenceName { get; set; }

    [JsonProperty("displayedName")]
    public string DisplayedName { get; set; }

    [JsonProperty("surname")]
    public string Surname { get; set; }

    [JsonProperty("givenName")]
    public string GivenName { get; set; }
}

public partial class TravellerNameInfo
{
    [JsonProperty("quantity")]
    public long Quantity { get; set; }

    [JsonProperty("type")]
    public string Type { get; set; }
}

//public partial class PassengerData
//{
//    [JsonProperty("travellerInformation")]
//    public TravellerInformation TravellerInformation { get; set; }
//}

public partial class TravellerInformation
{
    //    [JsonProperty("traveller")]
    //    public Traveller Traveller { get; set; }

    // [JsonProperty("passenger")]
    //   public List<Passenger> Passenger { get; set; }
}

//public partial class Passenger
//{
//    [JsonProperty("firstName")]
//    public string FirstName { get; set; }

//    [JsonProperty("type")]
//    public string Type { get; set; }
//}

//public partial class Traveller
//{
//    [JsonProperty("surname")]
//    public string Surname { get; set; }

//    [JsonProperty("quantity")]
//    public long Quantity { get; set; }
//}

public partial class TstDatum
{
    [JsonProperty("tstGeneralInformation")]
    public TstGeneralInformation TstGeneralInformation { get; set; }

    [JsonProperty("tstFreetext")]
    public List<OtherDataFreetext> TstFreetext { get; set; }

    [JsonProperty("fareBasisInfo")]
    public FareBasisInfo FareBasisInfo { get; set; }

    [JsonProperty("fareData")]
    public FareData FareData { get; set; }

    [JsonProperty("segmentAssociation")]
    public SelectionDetails SegmentAssociation { get; set; }

    [JsonProperty("referenceForTstData")]
    public ReferenceFor ReferenceForTstData { get; set; }
}

public partial class FareBasisInfo
{
    [JsonProperty("fareElement")]
    public List<FareElement> FareElement { get; set; }
}

public partial class FareElement
{
    [JsonProperty("primaryCode")]
    public string PrimaryCode { get; set; }

    [JsonProperty("notValidBefore")]
    public string NotValidBefore { get; set; }

    [JsonProperty("notValidAfter")]
    public string NotValidAfter { get; set; }

    [JsonProperty("baggageAllowance")]
    public string BaggageAllowance { get; set; }

    [JsonProperty("fareBasis")]
    public string FareBasis { get; set; }
}

public partial class FareData
{
    [JsonProperty("issueIdentifier")]
    public string IssueIdentifier { get; set; }

    [JsonProperty("monetaryInfo")]
    public List<MonetaryInfo> MonetaryInfo { get; set; }

    [JsonProperty("taxFields")]
    public List<TaxField> TaxFields { get; set; }
}

public partial class MonetaryInfo
{
    [JsonProperty("qualifier")]
    public string Qualifier { get; set; }

    [JsonProperty("amount")]
    public string Amount { get; set; }

    [JsonProperty("currencyCode")]
    public string CurrencyCode { get; set; }
}

public partial class TaxField
{
    [JsonProperty("taxIndicator")]
    public string TaxIndicator { get; set; }

    [JsonProperty("taxCurrency")]
    public string TaxCurrency { get; set; }

    [JsonProperty("taxAmount")]
    public string TaxAmount { get; set; }

    [JsonProperty("taxCountryCode")]
    public string TaxCountryCode { get; set; }

    [JsonProperty("taxNatureCode")]
    public string TaxNatureCode { get; set; }
}

public partial class TstGeneralInformation
{
    [JsonProperty("generalInformation")]
    public GeneralInformation GeneralInformation { get; set; }
}

public partial class GeneralInformation
{
    [JsonProperty("tstReferenceNumber")]
    public string TstReferenceNumber { get; set; }

    [JsonProperty("tstCreationDate")]
    public string TstCreationDate { get; set; }
}

public partial class PnrRetrieveAmRs
{
    public static PnrRetrieveAmRs FromJson(string json) => JsonConvert.DeserializeObject<PnrRetrieveAmRs>(json, PnrRetrieveAmRsConverter.Settings);
}

public static class PnrRetrieveAmRsSerialize
{
    public static string ToJson(this PnrRetrieveAmRs self) => JsonConvert.SerializeObject(self, PnrRetrieveAmRsConverter.Settings);
}

public class PnrRetrieveAmRsConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
        
    };
}

































//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace AmadeusPartner
//{
//    public class pnrRetrieve_AM_RS
//    {
//        public Pnrreply2 pnrReply { get; set; }
//    }

//    public class Pnrreply2
//    {
//        public Pnrheader2[] pnrHeader { get; set; }
//        public Securityinformation2 securityInformation { get; set; }
//        public Freetextdata[] freetextData { get; set; }
//        public Pnrheadertag pnrHeaderTag { get; set; }
//        public Sbrposdetails2 sbrPOSDetails { get; set; }
//        public Sbrcreationposdetails2 sbrCreationPosDetails { get; set; }
//        public Sbrupdatorposdetails2 sbrUpdatorPosDetails { get; set; }
//        public Technicaldata technicalData { get; set; }
//        public Travellerinfo[] travellerInfo { get; set; }
//        public Origindestinationdetail2[] originDestinationDetails { get; set; }
//        public Dataelementsmaster2 dataElementsMaster { get; set; }
//        public Tstdata[] tstData { get; set; }
//        public Pricingrecordgroup pricingRecordGroup { get; set; }
//    }

//    public class Securityinformation2
//    {
//        public Responsibilityinformation2 responsibilityInformation { get; set; }
//        public Queueinginformation queueingInformation { get; set; }
//        public string cityCode { get; set; }
//        public Secondrpinformation secondRpInformation { get; set; }
//    }

//    public class Responsibilityinformation2
//    {
//        public string typeOfPnrElement { get; set; }
//        public string agentId { get; set; }
//        public string officeId { get; set; }
//        public string iataCode { get; set; }
//    }

//    public class Queueinginformation
//    {
//        public string queueingOfficeId { get; set; }
//    }

//    public class Secondrpinformation
//    {
//        public string creationOfficeId { get; set; }
//        public string agentSignature { get; set; }
//        public string creationDate { get; set; }
//        public string creatorIataCode { get; set; }
//        public string creationTime { get; set; }
//    }

//    public class Pnrheadertag
//    {
//        public Statusinformation[] statusInformation { get; set; }
//    }

//    public class Statusinformation
//    {
//        public string indicator { get; set; }
//    }

//    public class Sbrposdetails2
//    {
//        public Sbruseridentificationown3 sbrUserIdentificationOwn { get; set; }
//        public Sbrsystemdetails sbrSystemDetails { get; set; }
//        public Sbrpreferences sbrPreferences { get; set; }
//    }

//    public class Sbruseridentificationown3
//    {
//        public Originidentification4 originIdentification { get; set; }
//        public string originatorTypeCode { get; set; }
//    }

//    public class Originidentification4
//    {
//        public int originatorId { get; set; }
//        public string inHouseIdentification1 { get; set; }
//    }

//    public class Sbrsystemdetails
//    {
//        public Deliveringsystem deliveringSystem { get; set; }
//    }

//    public class Deliveringsystem
//    {
//        public string companyId { get; set; }
//        public string locationId { get; set; }
//    }

//    public class Sbrpreferences
//    {
//        public Userpreferences userPreferences { get; set; }
//    }

//    public class Userpreferences
//    {
//        public string codedCountry { get; set; }
//    }

//    public class Sbrcreationposdetails2
//    {
//        public Sbruseridentificationown4 sbrUserIdentificationOwn { get; set; }
//        public Sbrsystemdetails1 sbrSystemDetails { get; set; }
//        public Sbrpreferences1 sbrPreferences { get; set; }
//    }

//    public class Sbruseridentificationown4
//    {
//        public Originidentification5 originIdentification { get; set; }
//        public string originatorTypeCode { get; set; }
//    }

//    public class Originidentification5
//    {
//        public int originatorId { get; set; }
//        public string inHouseIdentification1 { get; set; }
//    }

//    public class Sbrsystemdetails1
//    {
//        public Deliveringsystem1 deliveringSystem { get; set; }
//    }

//    public class Deliveringsystem1
//    {
//        public string companyId { get; set; }
//        public string locationId { get; set; }
//    }

//    public class Sbrpreferences1
//    {
//        public Userpreferences1 userPreferences { get; set; }
//    }

//    public class Userpreferences1
//    {
//        public string codedCountry { get; set; }
//    }

//    public class Sbrupdatorposdetails2
//    {
//        public Sbruseridentificationown5 sbrUserIdentificationOwn { get; set; }
//        public Sbrsystemdetails2 sbrSystemDetails { get; set; }
//        public Sbrpreferences2 sbrPreferences { get; set; }
//    }

//    public class Sbruseridentificationown5
//    {
//        public Originidentification6 originIdentification { get; set; }
//        public string originatorTypeCode { get; set; }
//    }

//    public class Originidentification6
//    {
//        public int originatorId { get; set; }
//        public string inHouseIdentification1 { get; set; }
//    }

//    public class Sbrsystemdetails2
//    {
//        public Deliveringsystem2 deliveringSystem { get; set; }
//    }

//    public class Deliveringsystem2
//    {
//        public string companyId { get; set; }
//        public string locationId { get; set; }
//    }

//    public class Sbrpreferences2
//    {
//        public Userpreferences2 userPreferences { get; set; }
//    }

//    public class Userpreferences2
//    {
//        public string codedCountry { get; set; }
//    }

//    public class Technicaldata
//    {
//        public Enveloppenumberdata enveloppeNumberData { get; set; }
//        public Lasttransmittedenvelopenumber lastTransmittedEnvelopeNumber { get; set; }
//        public Purgedatedata purgeDateData { get; set; }
//    }

//    public class Enveloppenumberdata
//    {
//        public Sequencedetails sequenceDetails { get; set; }
//    }

//    public class Sequencedetails
//    {
//        public string number { get; set; }
//    }

//    public class Lasttransmittedenvelopenumber
//    {
//        public int currentRecord { get; set; }
//    }

//    public class Purgedatedata
//    {
//        public Datetime dateTime { get; set; }
//    }

//    public class Datetime
//    {
//        public int year { get; set; }
//        public string month { get; set; }
//        public string day { get; set; }
//    }

//    public class Dataelementsmaster2
//    {
//        public Marker3 marker2 { get; set; }
//        public Dataelementsindiv[] dataElementsIndiv { get; set; }
//    }

//    public class Marker3
//    {
//    }

//    public class Dataelementsindiv
//    {
//        public Elementmanagementdata elementManagementData { get; set; }
//        public Otherdatafreetext[] otherDataFreetext { get; set; }
//        public Ticketelement ticketElement { get; set; }
//        public Servicerequest serviceRequest { get; set; }
//        public Referencefordataelement referenceForDataElement { get; set; }
//    }

//    public class Elementmanagementdata
//    {
//        public Reference reference { get; set; }
//        public string segmentName { get; set; }
//        public int lineNumber { get; set; }
//    }

//    public class Reference
//    {
//        public string qualifier { get; set; }
//        public int number { get; set; }
//    }

//    public class Ticketelement
//    {
//        public Ticket ticket { get; set; }
//    }

//    public class Ticket
//    {
//        public string indicator { get; set; }
//        public string date { get; set; }
//        public string officeId { get; set; }
//        public string electronicTicketFlag { get; set; }
//        public string airlineCode { get; set; }
//    }

//    public class Servicerequest
//    {
//        public Ssr ssr { get; set; }
//    }

//    public class Ssr
//    {
//        public string type { get; set; }
//        public string status { get; set; }
//        public int quantity { get; set; }
//        public string companyId { get; set; }
//        public string[] freeText { get; set; }
//    }

//    public class Referencefordataelement
//    {
//        public Reference1[] reference { get; set; }
//    }

//    public class Reference1
//    {
//        public string qualifier { get; set; }
//        public string number { get; set; }
//    }

//    public class Otherdatafreetext
//    {
//        public Freetextdetail freetextDetail { get; set; }
//        public string longFreetext { get; set; }
//    }

//    public class Freetextdetail
//    {
//        public string subjectQualifier { get; set; }
//        public string type { get; set; }
//    }

//    public class Pricingrecordgroup
//    {
//        public Pricingrecorddata pricingRecordData { get; set; }
//        public Productpricingquotationrecord[] productPricingQuotationRecord { get; set; }
//    }

//    public class Pricingrecorddata
//    {
//    }

//    public class Productpricingquotationrecord
//    {
//        public Pricingrecordid pricingRecordId { get; set; }
//        public Passengertattoo[] passengerTattoos { get; set; }
//        public Documentdetailsgroup documentDetailsGroup { get; set; }
//    }

//    public class Pricingrecordid
//    {
//        public string referenceType { get; set; }
//        public string uniqueReference { get; set; }
//    }

//    public class Documentdetailsgroup
//    {
//        public Totalfare totalFare { get; set; }
//        public Issueidentifier issueIdentifier { get; set; }
//        public Manualindicator manualIndicator { get; set; }
//        public Officeinformation officeInformation { get; set; }
//        public Creationdate creationDate { get; set; }
//        public Coupondetailsgroup2[] couponDetailsGroup { get; set; }
//        public Farecomponentdetailsgroup2[] fareComponentDetailsGroup { get; set; }
//    }

//    public class Totalfare
//    {
//        public Monetarydetails2 monetaryDetails { get; set; }
//    }

//    public class Monetarydetails2
//    {
//        public string typeQualifier { get; set; }
//        public string amount { get; set; }
//        public string currency { get; set; }
//    }

//    public class Issueidentifier
//    {
//        public Priceticketdetails2 priceTicketDetails { get; set; }
//        public string priceTariffType { get; set; }
//    }

//    public class Priceticketdetails2
//    {
//        public string indicators { get; set; }
//    }

//    public class Manualindicator
//    {
//        public Statusdetails statusDetails { get; set; }
//    }

//    public class Statusdetails
//    {
//        public string indicator { get; set; }
//        public string action { get; set; }
//    }

//    public class Officeinformation
//    {
//        public Originidentification3 originIdentification { get; set; }
//    }

//    public class Originidentification3
//    {
//        public string inHouseIdentification1 { get; set; }
//        public string inHouseIdentification2 { get; set; }
//    }

//    public class Creationdate
//    {
//        public string businessSemantic { get; set; }
//        public Datetime1 dateTime { get; set; }
//    }

//    public class Datetime1
//    {
//        public string year { get; set; }
//        public string month { get; set; }
//        public string day { get; set; }
//    }

//    public class Coupondetailsgroup2
//    {
//        public Productid2 productId { get; set; }
//    }

//    public class Productid2
//    {
//        public Referencedetails2 referenceDetails { get; set; }
//    }

//    public class Referencedetails2
//    {
//        public string type { get; set; }
//        public string value { get; set; }
//    }

//    public class Farecomponentdetailsgroup2
//    {
//        public Farecomponentid2 fareComponentID { get; set; }
//        public Marketfarecomponent2 marketFareComponent { get; set; }
//        public Monetaryinformation2 monetaryInformation { get; set; }
//        public Componentclassinfo2 componentClassInfo { get; set; }
//        public Farequalifiersdetail2 fareQualifiersDetail { get; set; }
//        public Coupondetailsgroup1[] couponDetailsGroup { get; set; }
//    }

//    public class Farecomponentid2
//    {
//        public Itemnumberdetail2[] itemNumberDetails { get; set; }
//    }

//    public class Itemnumberdetail2
//    {
//        public string number { get; set; }
//        public string type { get; set; }
//    }

//    public class Marketfarecomponent2
//    {
//        public Boardpointdetails2 boardPointDetails { get; set; }
//        public Offpointdetails2 offpointDetails { get; set; }
//    }

//    public class Boardpointdetails2
//    {
//        public string trueLocationId { get; set; }
//    }

//    public class Offpointdetails2
//    {
//        public string trueLocationId { get; set; }
//    }

//    public class Monetaryinformation2
//    {
//        public Monetarydetails3 monetaryDetails { get; set; }
//    }

//    public class Monetarydetails3
//    {
//        public string typeQualifier { get; set; }
//        public string amount { get; set; }
//        public string currency { get; set; }
//    }

//    public class Componentclassinfo2
//    {
//        public Farebasisdetails2 fareBasisDetails { get; set; }
//    }

//    public class Farebasisdetails2
//    {
//        public string rateTariffClass { get; set; }
//    }

//    public class Farequalifiersdetail2
//    {
//        public Discountdetail2[] discountDetails { get; set; }
//    }

//    public class Discountdetail2
//    {
//        public string fareQualifier { get; set; }
//    }

//    public class Coupondetailsgroup1
//    {
//        public Productid1 productId { get; set; }
//    }

//    public class Productid1
//    {
//        public Referencedetails1 referenceDetails { get; set; }
//    }

//    public class Referencedetails1
//    {
//        public string type { get; set; }
//        public string value { get; set; }
//    }

//    public class Passengertattoo
//    {
//        public Passengerreference passengerReference { get; set; }
//    }

//    public class Passengerreference
//    {
//        public string type { get; set; }
//        public string value { get; set; }
//    }

//    public class Pnrheader2
//    {
//        public Reservationinfo2 reservationInfo { get; set; }
//    }

//    public class Reservationinfo2
//    {
//        public Reservation2[] reservation { get; set; }
//    }

//    public class Reservation2
//    {
//        public string companyId { get; set; }
//        public string controlNumber { get; set; }
//        public string date { get; set; }
//        public int time { get; set; }
//    }

//    public class Freetextdata
//    {
//        public Freetextdetail1 freetextDetail { get; set; }
//        public string longFreetext { get; set; }
//    }

//    public class Freetextdetail1
//    {
//        public string subjectQualifier { get; set; }
//        public string type { get; set; }
//    }

//    public class Travellerinfo
//    {
//        public Elementmanagementpassenger elementManagementPassenger { get; set; }
//        public Passengerdata[] passengerData { get; set; }
//        public Enhancedpassengerdata[] enhancedPassengerData { get; set; }
//    }

//    public class Elementmanagementpassenger
//    {
//        public Reference2 reference { get; set; }
//        public string segmentName { get; set; }
//        public int lineNumber { get; set; }
//    }

//    public class Reference2
//    {
//        public string qualifier { get; set; }
//        public int number { get; set; }
//    }

//    public class Passengerdata
//    {
//        public Travellerinformation travellerInformation { get; set; }
//    }

//    public class Travellerinformation
//    {
//        public Traveller2 traveller { get; set; }
//        public Passenger[] passenger { get; set; }
//    }

//    public class Traveller2
//    {
//        public string surname { get; set; }
//        public int quantity { get; set; }
//    }

//    public class Passenger
//    {
//        public string firstName { get; set; }
//        public string infantIndicator { get; internal set; }
//        public string type { get; set; }
//    }

//    public class Enhancedpassengerdata
//    {
//        public Enhancedtravellerinformation enhancedTravellerInformation { get; set; }
//    }

//    public class Enhancedtravellerinformation
//    {
//        public Travellernameinfo travellerNameInfo { get; set; }
//        public Otherpaxnamesdetail[] otherPaxNamesDetails { get; set; }
//    }

//    public class Travellernameinfo
//    {
//        public int quantity { get; set; }
//        public string type { get; set; }
//    }

//    public class Otherpaxnamesdetail
//    {
//        public string nameType { get; set; }
//        public string referenceName { get; set; }
//        public string displayedName { get; set; }
//        public string surname { get; set; }
//        public string givenName { get; set; }
//    }

//    public class Origindestinationdetail2
//    {
//        public Origindestination2 originDestination { get; set; }
//        public Itineraryinfo[] itineraryInfo { get; set; }
//    }

//    public class Origindestination2
//    {
//    }

//    public class Itineraryinfo
//    {
//        public Elementmanagementitinerary elementManagementItinerary { get; set; }
//        public Travelproduct travelProduct { get; set; }
//        public Itinerarymessageaction itineraryMessageAction { get; set; }
//        public Itineraryreservationinfo itineraryReservationInfo { get; set; }
//        public Relatedproduct relatedProduct { get; set; }
//        public Flightdetail2 flightDetail { get; set; }
//        public Cabindetails cabinDetails { get; set; }
//        public Selectiondetails selectionDetails { get; set; }
//        public Leginfo[] legInfo { get; set; }
//        public Markerrailtour markerRailTour { get; set; }
//    }

//    public class Elementmanagementitinerary
//    {
//        public Reference3 reference { get; set; }
//        public string segmentName { get; set; }
//        public int lineNumber { get; set; }
//    }

//    public class Reference3
//    {
//        public string qualifier { get; set; }
//        public int number { get; set; }
//    }

//    public class Travelproduct
//    {
//        public Product product { get; set; }
//        public Boardpointdetail boardpointDetail { get; set; }
//        public Offpointdetail offpointDetail { get; set; }
//        public Companydetail companyDetail { get; set; }
//        public Productdetails productDetails { get; set; }
//        public Typedetail typeDetail { get; set; }
//    }

//    public class Product
//    {
//        public string depDate { get; set; }
//        public string depTime { get; set; }
//        public string arrDate { get; set; }
//        public string arrTime { get; set; }
//    }

//    public class Boardpointdetail
//    {
//        public string cityCode { get; set; }
//    }

//    public class Offpointdetail
//    {
//        public string cityCode { get; set; }
//    }

//    public class Companydetail
//    {
//        public string identification { get; set; }
//    }

//    public class Productdetails
//    {
//        public string identification { get; set; }
//        public string classOfService { get; set; }
//    }

//    public class Typedetail
//    {
//        public string detail { get; set; }
//    }

//    public class Itinerarymessageaction
//    {
//        public Business business { get; set; }
//    }

//    public class Business
//    {
//        public string function { get; set; }
//    }

//    public class Itineraryreservationinfo
//    {
//        public Reservation1 reservation { get; set; }
//    }

//    public class Reservation1
//    {
//        public string companyId { get; set; }
//        public string controlNumber { get; set; }
//    }

//    public class Relatedproduct
//    {
//        public int quantity { get; set; }
//        public string[] status { get; set; }
//    }

//    public class Flightdetail2
//    {
//        public Productdetails1 productDetails { get; set; }
//        public Departureinformation departureInformation { get; set; }
//        public Arrivalstationinfo arrivalStationInfo { get; set; }
//        public Mileagetimedetails mileageTimeDetails { get; set; }
//        public Timedetail timeDetail { get; set; }
//        public Facility[] facilities { get; set; }
//    }

//    public class Productdetails1
//    {
//        public string equipment { get; set; }
//        public int numOfStops { get; set; }
//        public string duration { get; set; }
//        public int weekDay { get; set; }
//    }

//    public class Departureinformation
//    {
//        public string departTerminal { get; set; }
//    }

//    public class Arrivalstationinfo
//    {
//        public string terminal { get; set; }
//    }

//    public class Mileagetimedetails
//    {
//        public int flightLegMileage { get; set; }
//        public string unitQualifier { get; set; }
//    }

//    public class Timedetail
//    {
//        public string checkinTime { get; set; }
//    }

//    public class Facility
//    {
//        public string entertainement { get; set; }
//        public string entertainementDescription { get; set; }
//    }

//    public class Cabindetails
//    {
//        public Cabindetails1 cabinDetails { get; set; }
//    }

//    public class Cabindetails1
//    {
//        public string classDesignator { get; set; }
//    }

//    public class Selectiondetails
//    {
//        public Selection[] selection { get; set; }
//    }

//    public class Selection
//    {
//        public string option { get; set; }
//    }

//    public class Markerrailtour
//    {
//    }

//    public class Leginfo
//    {
//        public Markerleginfo markerLegInfo { get; set; }
//        public Legtravelproduct legTravelProduct { get; set; }
//        public Interactivefreetext[] interactiveFreeText { get; set; }
//    }

//    public class Markerleginfo
//    {
//    }

//    public class Legtravelproduct
//    {
//        public Flightdate2 flightDate { get; set; }
//        public Boardpointdetails3 boardPointDetails { get; set; }
//        public Offpointdetails3 offpointDetails { get; set; }
//    }

//    public class Flightdate2
//    {
//        public string departureDate { get; set; }
//        public string departureTime { get; set; }
//        public string arrivalDate { get; set; }
//        public string arrivalTime { get; set; }
//    }

//    public class Boardpointdetails3
//    {
//        public string trueLocationId { get; set; }
//    }

//    public class Offpointdetails3
//    {
//        public string trueLocationId { get; set; }
//    }

//    public class Interactivefreetext
//    {
//        public Freetextqualification2 freeTextQualification { get; set; }
//        public string freeText { get; set; }
//    }

//    public class Freetextqualification2
//    {
//        public string textSubjectQualifier { get; set; }
//    }

//    public class Tstdata
//    {
//        public Tstgeneralinformation tstGeneralInformation { get; set; }
//        public Tstfreetext[] tstFreetext { get; set; }
//        public Farebasisinfo fareBasisInfo { get; set; }
//        public Faredata fareData { get; set; }
//        public Segmentassociation segmentAssociation { get; set; }
//        public Referencefortstdata referenceForTstData { get; set; }
//    }

//    public class Tstgeneralinformation
//    {
//        public Generalinformation generalInformation { get; set; }
//    }

//    public class Generalinformation
//    {
//        public string tstReferenceNumber { get; set; }
//        public string tstCreationDate { get; set; }
//    }

//    public class Farebasisinfo
//    {
//        public Fareelement[] fareElement { get; set; }
//    }

//    public class Fareelement
//    {
//        public string primaryCode { get; set; }
//        public string notValidBefore { get; set; }
//        public string notValidAfter { get; set; }
//        public string baggageAllowance { get; set; }
//        public string fareBasis { get; set; }
//    }

//    public class Faredata
//    {
//        public string issueIdentifier { get; set; }
//        public Monetaryinfo[] monetaryInfo { get; set; }
//        public Taxfield[] taxFields { get; set; }
//    }

//    public class Monetaryinfo
//    {
//        public string qualifier { get; set; }
//        public string amount { get; set; }
//        public string currencyCode { get; set; }
//    }

//    public class Taxfield
//    {
//        public string taxIndicator { get; set; }
//        public string taxCurrency { get; set; }
//        public string taxAmount { get; set; }
//        public string taxCountryCode { get; set; }
//        public string taxNatureCode { get; set; }
//    }

//    public class Segmentassociation
//    {
//        public Selection1[] selection { get; set; }
//    }

//    public class Selection1
//    {
//        public string option { get; set; }
//    }

//    public class Referencefortstdata
//    {
//        public Reference4[] reference { get; set; }
//    }

//    public class Reference4
//    {
//        public string qualifier { get; set; }
//        public string number { get; set; }
//    }

//    public class Tstfreetext
//    {
//        public Freetextdetail2 freetextDetail { get; set; }
//        public string longFreetext { get; set; }
//    }

//    public class Freetextdetail2
//    {
//        public string subjectQualifier { get; set; }
//        public string type { get; set; }
//    }
//}