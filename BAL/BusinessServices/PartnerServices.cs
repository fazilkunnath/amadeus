﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAL.BusinessServices.Interface;
using BusinessEntities.Entity;
using DAL.Core.Repositories;
using DAL.Sessions;

namespace BAL.BusinessServices
{
    public class PartnerServices : IPartnerServices
    {
        // IPartnerRepository repository;
        //protected readonly IRepositoryContainer _repository;
        //public PartnerServices(RepositorySize size = RepositorySize.Medium, string repositoryKey = "common")
        //{
        //    _repository = RepositorySesssion.GetRepository(size, repositoryKey);
        //}
        public async Task<IEnumerable<PartnerEntity>> GetPartners()
        {
            RepositorySize size = RepositorySize.Medium; string repositoryKey = "common";
            var _repository = RepositorySesssion.GetRepository(size, repositoryKey);
            return _repository.PartnerRepository.GetPartnerData();
        }

        public IEnumerable<PartnerEntity> GetPartnersSync()
        {
            RepositorySize size = RepositorySize.Medium; string repositoryKey = "common";
            var _repository = RepositorySesssion.GetRepository(size, repositoryKey);
            return _repository.PartnerRepository.GetPartnerData();
        }
    }
}
