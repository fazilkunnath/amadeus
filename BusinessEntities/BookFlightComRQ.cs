﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace BusinessEntities
{
    public partial class FLBookRQ
    {
        public FLBookRQ()
        {
            BookFlight = new BookFlight();
        }
        [JsonProperty("bookFlight")]
        public BookFlight BookFlight { get; set; }
    }


    public partial class BookFlight
    {
        public BookFlight()
        {
            SupplierAgencyDetails = new List<BusinessEntities.SupplierAgencyDetails>();
            FDetails = new FDetails();
            CustomerInfo = new CustomerInfo();
            PaymentInfo = new PaymentInfo();
            TravelerInfo = new List<BusinessEntities.TravelerInfo>();
            FlLegGroup = new List<BusinessEntities.FlLegGroup>();
            AddressLine = new List<string>();



        }

        [JsonProperty("agencyCode")]
        public string AgencyCode { get; set; }

        [JsonProperty("supplierAgencyDetails")]
        public List<SupplierAgencyDetails> SupplierAgencyDetails { get; set; }

        [JsonProperty("faresourcecode")]
        public string Faresourcecode { get; set; }

        [JsonProperty("sessionId")]
        public string SessionId { get; set; }

        [JsonProperty("target")]
        public string Target { get; set; }

        [JsonProperty("fdetails")]
        public FDetails FDetails { get; set; }

        [JsonProperty("customerInfo")]
        public CustomerInfo CustomerInfo { get; set; }

        [JsonProperty("paymentInfo")]
        public PaymentInfo PaymentInfo { get; set; }

        [JsonProperty("travelerInfo")]
        public List<TravelerInfo> TravelerInfo { get; set; }
        [JsonProperty("flLegGroup")]
        public List<FlLegGroup> FlLegGroup { get; set; }
        [JsonProperty("areaCityCode")]
        public string AreaCityCode { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("addressLine")]
        public List<string> AddressLine { get; set; }

        [JsonProperty("cityName")]
        public string CityName { get; set; }

        [JsonProperty("cityCode")]
        public string CityCode { get; set; }

        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("adt")]
        public string Adt { get; set; }

        [JsonProperty("chd")]
        public string Chd { get; set; }

        [JsonProperty("inf")]
        public string Inf { get; set; }
    }

    public partial class FDetails
    {
        [JsonProperty("flightId")]
        public string FlightId { get; set; }

        [JsonProperty("clientIp")]
        public string ClientIp { get; set; }

        [JsonProperty("confirmedPrice")]
        public string ConfirmedPrice { get; set; }

        [JsonProperty("confirmedCurrency")]
        public string ConfirmedCurrency { get; set; }


        [JsonProperty("supplierOGPrice")]
        public string SupplierOGPrice { get; set; }

    }


    public partial class CustomerInfo
    {
        [JsonProperty("sex")]
        public string Sex { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("companyName")]
        public string CompanyName { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("houseNo")]
        public string HouseNo { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("phoneCountry")]
        public string PhoneCountry { get; set; }

        [JsonProperty("phoneArea")]
        public string PhoneArea { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }

    public partial class PaymentInfo
    {
        [JsonProperty("paymentCode")]
        public string PaymentCode { get; set; }

        [JsonProperty("holder")]
        public string Holder { get; set; }

        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("cvc")]
        public string Cvc { get; set; }

        [JsonProperty("expiry")]
        public string Expiry { get; set; }
    }

    public class SupplierAgencyDetails
    {
        [JsonProperty("supplierId")]
        public long SupplierId { get; set; }

        [JsonProperty("baseUrl")]
        public string BaseUrl { get; set; }

        [JsonProperty("requestUrl")]
        public object RequestUrl { get; set; }

        [JsonProperty("agencyId")]
        public long AgencyId { get; set; }
        [JsonProperty("AgencyCode")]
        public string AgencyCode { get; set; }

        [JsonProperty("supplierName")]
        public string SupplierName { get; set; }

        [JsonProperty("supplierCode")]
        public string SupplierCode { get; set; }

        [JsonProperty("accountNumber")]
        public string AccountNumber { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("toCurrency")]
        public string ToCurrency { get; set; }
        [JsonProperty("toROEValue")]
        public float ToROEValue { get; set; }

    }
    public class TravelerInfo
    {
        [JsonProperty("passengerTypeQuantity")]
        public string PassengerTypeQuantity { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("givenName")]
        public string GivenName { get; set; }

        [JsonProperty("namePrefix")]
        public string NamePrefix { get; set; }

        [JsonProperty("surname")]
        public string Surname { get; set; }

        [JsonProperty("quantity")]
        public string Quantity { get; set; }

        [JsonProperty("birthDate")]
        public string BirthDate { get; set; }

        [JsonProperty("_docType")]
        public string DocType { get; set; }

        [JsonProperty("_docId")]
        public string DocId { get; set; }

        [JsonProperty("_issuanceDate")]
        public string IssuanceDate { get; set; }

        [JsonProperty("_docIssueCountry")]
        public string DocIssueCountry { get; set; }

        [JsonProperty("_expireDate")]
        public string ExpireDate { get; set; }

        [JsonProperty("_nationality")]
        public string Nationality { get; set; }
        [JsonProperty("_frequentFlyerNumber")]
        public string FrequentFlyerNumber { get; set; }
        [JsonProperty("_fFAirlineCode")]
        public string FFAirlineCode { get; set; }
    }

    public class FlLegGroup
    {

        public FlLegGroup()
        {
            Segments = new List<Segment>();
        }
        [JsonProperty("elapsedtime")]
        public string Elapsedtime { get; set; }

        [JsonProperty("from")]
        public string From { get; set; }

        [JsonProperty("to")]
        public string To { get; set; }

        [JsonProperty("segments")]
        public List<Segment> Segments { get; set; }
    }

    public class Segment
    {
        [JsonProperty("fareBasis")]
        public string FareBasis { get; set; }

        [JsonProperty("departureDate")]
        public string DepartureDate { get; set; }

        [JsonProperty("departureTime")]
        public string DepartureTime { get; set; }

        [JsonProperty("arrivalDate")]
        public string ArrivalDate { get; set; }

        [JsonProperty("arrivalTime")]
        public string ArrivalTime { get; set; }

        [JsonProperty("departureFrom")]
        public string DepartureFrom { get; set; }

        [JsonProperty("departureTo")]
        public string DepartureTo { get; set; }

        [JsonProperty("marketingCompany")]
        public string MarketingCompany { get; set; }

        [JsonProperty("operatingCompany")]
        public string OperatingCompany { get; set; }

        [JsonProperty("flightNumber")]
        public string FlightNumber { get; set; }

        [JsonProperty("bookingClass")]
        public string BookingClass { get; set; }

        [JsonProperty("terminalTo")]
        public string TerminalTo { get; set; }

        [JsonProperty("terminalFrom")]
        public string TerminalFrom { get; set; }

        [JsonProperty("flightequip")]
        public string Flightequip { get; set; }
    }
}






//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace BusinessEntities
//{
//    public class FLbookRQ
//    {
//        public Bookflight BookFlight { get; set; }
//    }

//    public class Bookflight
//    {
//        public string AgencyCode { get; set; }
//        public Supplieragencydetailsbook[] SupplierAgencyDetailsBook { get; set; }
//        public string Faresourcecode { get; set; }
//        public string SessionId { get; set; }
//        public string Target { get; set; }
//        public Fdetails Fdetails { get; set; }
//        public Customerinfo CustomerInfo { get; set; }
//        public Paymentinfo PaymentInfo { get; set; }
//        public Travelerinfo[] TravelerInfo { get; set; }
//        public Flleggroup[] FLLegGroup { get; set; }
//        public string AreaCityCode { get; set; }
//        public string PhoneNumber { get; set; }
//        public string Email { get; set; }
//        public string[] AddressLine { get; set; }
//        public string CityName { get; set; }
//        public string CityCode { get; set; }
//        public string PostalCode { get; set; }
//        public int ADT { get; set; }
//        public int CHD { get; set; }
//        public int INF { get; set; }
//    }

//    public class Fdetails
//    {
//        public string FlightId { get; set; }
//        public string ClientIP { get; set; }
//        public string ConfirmedPrice { get; set; }
//        public string ConfirmedCurrency { get; set; }
//    }

//    public class Customerinfo
//    {
//        public string Sex { get; set; }
//        public string FirstName { get; set; }
//        public string LastName { get; set; }
//        public string CompanyName { get; set; }
//        public string Street { get; set; }
//        public string HouseNo { get; set; }
//        public string Zip { get; set; }
//        public string City { get; set; }
//        public string CountryCode { get; set; }
//        public string PhoneCountry { get; set; }
//        public string PhoneArea { get; set; }
//        public string PhoneNumber { get; set; }
//        public string Email { get; set; }
//    }

//    public class Paymentinfo
//    {
//        public string PaymentCode { get; set; }
//        public string Holder { get; set; }
//        public string Number { get; set; }
//        public string CVC { get; set; }
//        public string Expiry { get; set; }
//    }

//    public class Supplieragencydetailsbook
//    {
//        public int SupplierId { get; set; }
//        public string BaseUrl { get; set; }
//        public object RequestUrl { get; set; }
//        public int AgencyID { get; set; }
//        public string SupplierName { get; set; }
//        public string SupplierCode { get; set; }
//        public string AccountNumber { get; set; }
//        public string UserName { get; set; }
//        public string Password { get; set; }
//        public string Status { get; set; }
//    }

//    public class Travelerinfo
//    {
//        public string PassengerTypeQuantity { get; set; }
//        public string Gender { get; set; }
//        public string GivenName { get; set; }
//        public string NamePrefix { get; set; }
//        public string Surname { get; set; }
//        public string quantity { get; set; }
//        public string BirthDate { get; set; }
//        public string _DocType { get; set; }
//        public string _DocID { get; set; }
//        public string _DocIssueCountry { get; set; }
//        public string _ExpireDate { get; set; }
//        public string _Nationality { get; set; }
//    }

//    public class Flleggroup
//    {
//        public string elapsedtime { get; set; }
//        public string from { get; set; }
//        public string to { get; set; }
//        public Segment[] segments { get; set; }
//    }

//    public class Segment
//    {
//        public string FareBasis { get; set; }
//        public string DepartureDate { get; set; }
//        public string DepartureTime { get; set; }
//        public string ArrivalDate { get; set; }
//        public string ArrivalTime { get; set; }
//        public string DepartureFrom { get; set; }
//        public string DepartureTo { get; set; }
//        public string MarketingCompany { get; set; }
//        public string OperatingCompany { get; set; }
//        public string FlightNumber { get; set; }
//        public string BookingClass { get; set; }
//    }
//}
