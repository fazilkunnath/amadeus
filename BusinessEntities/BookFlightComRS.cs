﻿
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class BookFlightComRS
{
    public BookFlightComRS()
    {
        BookFlightResult = new BookFlightResult();
    }
    [JsonProperty("bookFlightResult")]
    public BookFlightResult BookFlightResult { get; set; }
}

public partial class BookFlightResult
{
    public BookFlightResult()
    {
        Errors = new List<Error>();
    }
    [JsonProperty("errors")]
    public List<Error> Errors { get; set; }

    [JsonProperty("bookingId")]
    public string BookingId { get; set; }

    [JsonProperty("status")]
    public string Status { get; set; }

    [JsonProperty("success")]
    public string Success { get; set; }

    [JsonProperty("target")]
    public string Target { get; set; }

    [JsonProperty("tktTimeLimit")]
    public string TktTimeLimit { get; set; }

    [JsonProperty("uniqueID")]
    public string UniqueId { get; set; }

    [JsonProperty("airlinepnr")]
    public string Airlinepnr { get; set; }

    [JsonProperty("sessionid")]
    public string Sessionid { get; set; }

    [JsonProperty("faresourcecode")]
    public string Faresourcecode { get; set; }

    [JsonProperty("agencyCode")]
    public string AgencyCode { get; set; }

    [JsonProperty("newPrice")]
    public string NewPrice { get; set; }

    [JsonProperty("newPriceCurrencyConv")]
    public string NewPriceCurrencyConv { get; set; }
}

public partial class Error
{
    [JsonProperty("code")]
    public string Code { get; set; }

    [JsonProperty("meassage")]
    public string Meassage { get; set; }
}

public partial class BookFlightComRS
{
    public static BookFlightComRS FromJson(string json) => JsonConvert.DeserializeObject<BookFlightComRS>(json, BookFlightComRSConverter.Settings);
}

public static class BookFlightComRSSerialize
{
    public static string ToJson(this BookFlightComRS self) => JsonConvert.SerializeObject(self, BookFlightComRSConverter.Settings);
}

public class BookFlightComRSConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
       // MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
    };
}