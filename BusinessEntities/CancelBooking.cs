﻿using Newtonsoft.Json;

public partial class CancelBooking
{
    [JsonProperty("UniqueID")]
    public string UniqueId { get; set; }

    [JsonProperty("Target")]
    public string Target { get; set; }

    [JsonProperty("AgencyCode")]
    public string AgencyCode { get; set; }

    [JsonProperty("accountNumber")]
    public string AccountNumber { get; set; }

    [JsonProperty("password")]
    public string Password { get; set; }

    [JsonProperty("userName")]
    public string UserName { get; set; }

}

public partial class CancelBooking
{
    public static CancelBooking FromJson(string json) => JsonConvert.DeserializeObject<CancelBooking>(json, CancelBookingConverter.Settings);
}

public static class CancelBookingSerialize
{
    public static string ToJson(this CancelBooking self) => JsonConvert.SerializeObject(self, CancelBookingConverter.Settings);
}

public class CancelBookingConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
       //    MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
    };
}