﻿
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class CancelComRS
{
    [JsonProperty("errors")]
    public List<Errors> Error { get; set; }

    [JsonProperty("success")]
    public bool Success { get; set; }

    [JsonProperty("target")]
    public string Target { get; set; }

    [JsonProperty("uniqueID")]
    public string UniqueId { get; set; }
}

public partial class Errors
{
    [JsonProperty("code")]
    public string Code1 { get; set; }

    [JsonProperty("message")]
    public string Message { get; set; }
}
public partial class CancelComRs
{
    public static CancelComRs FromJson(string json) => JsonConvert.DeserializeObject<CancelComRs>(json, CancelComRsConverter.Settings);
}

public static class CancelComRsSerialize
{
    public static string ToJson(this CancelComRs self) => JsonConvert.SerializeObject(self, CancelComRsConverter.Settings);
}

public class CancelComRsConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        //MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
    };
}