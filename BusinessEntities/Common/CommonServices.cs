﻿namespace BusinessEntities.Common
{
    using System;
    using System.IO;

    public static class CommonServices
    {
        public static DateTime GetDateTime(string dateTime)
        {
            return Convert.ToDateTime(dateTime,
                           System.Globalization.CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        }

        public static string GetDateTimeIntoString(DateTime dateTime)
        {
            return dateTime.ToString("ddMMyyyy");
        }

        public static string GetDateTimeIntoString(DateTime dateTime, string formate)
        {
            return dateTime.ToString(formate);
        }


        public static void SaveLog(string logName, string agency, string supplier, string rQ, string rS)
        {
            try
            {
                string date = DateTime.Now.ToString("dd-MM-yyyy");
                //string storePath = @"D:\Sourcecode" + "/LOGRQRS/" + agency + "/" + date + "/" + supplier + "/" + logName + "/";
                string storePath = @"C:\Inetpub\vhosts\oneviewitsolutions.com\API\logs\amadeus\LOGRQRS\" + agency + "/" + date + "/" + supplier + "/" + logName + "/";

                if (!Directory.Exists(storePath))
                    Directory.CreateDirectory(storePath);
                string timeStamp = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss-ffff");
                string filenameRq = storePath + "/" + logName + "_" + timeStamp + "_RQ.json";
                File.WriteAllText(filenameRq, rQ);
                string filenameRs = storePath + "/" + logName + "_" + timeStamp + "_RS.json";
                File.WriteAllText(filenameRs, rS);
            }
            catch (Exception)
            {

            }
        }


    }



}
