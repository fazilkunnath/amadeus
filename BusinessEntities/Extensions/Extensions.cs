﻿namespace BusinessEntities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    public static class EnumUtil
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }
    }

    public static class DateFormate
    {
        public static string GetFormatedDate(this string date)
        {
            string DD = date.Split('-')[0];
            string MM = date.Split('-')[1];
            string YY = date.Split('-')[2];
            YY = YY.Substring(YY.Length - 2);
            return DD + MM + YY;
        }
        public static string GetFormatedDate(this string date, string formate)
        {
            return Convert.ToDateTime(date).ToString(formate);
        }


        public static string GetFormatedDate(this DateTime date, string formate)
        {
            return Convert.ToDateTime(date).ToString(formate);
        }

        public static string GetFormatedDate(this DateTime date)
        {
            return Convert.ToDateTime(date).ToString("ddMMyyyy");
        }

        public static string GetFormatedDate1(this DateTime date)
        {
            return Convert.ToDateTime(date).ToString("ddMMMyy");
        }

    }




}
