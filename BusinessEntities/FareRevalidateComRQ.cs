﻿
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class FareRevalidateRQ
{
    [JsonProperty("CommonRequestFarePricer")]
    public CommonRequestFarePricer CommonRequestFarePricer { get; set; }
}

public partial class CommonRequestFarePricer
{
    [JsonProperty("body")]
    public Body Body { get; set; }

    [JsonProperty("supplierAgencyDetails")]
    public List<SupplierAgencyDetail> SupplierAgencyDetails { get; set; }
}

public partial class Body
{
    [JsonProperty("airRevalidate")]
    public AirRevalidate AirRevalidate { get; set; }
}

public partial class AirRevalidate
{
    [JsonProperty("aRAgencyCode")]
    public string ArAgencyCode { get; set; }

    [JsonProperty("aRSupplierCode")]
    public string ArSupplierCode { get; set; }

    [JsonProperty("fareSourceCode")]
    public string FareSourceCode { get; set; }

    [JsonProperty("sessionId")]
    public string SessionId { get; set; }

    [JsonProperty("target")]
    public string Target { get; set; }

    [JsonProperty("adt")]
    public int Adt { get; set; }

    [JsonProperty("chd")]
    public int Chd { get; set; }

    [JsonProperty("inf")]
    public int Inf { get; set; }

    [JsonProperty("segmentGroup")]
    public List<SegmentGroup> SegmentGroup { get; set; }
}

public partial class SegmentGroup
{
    [JsonProperty("segmentInformation")]
    public SegmentInformation SegmentInformation { get; set; }
}

public partial class SegmentInformation
{
    [JsonProperty("flightDate")]
    public FlightDate FlightDate { get; set; }

    [JsonProperty("boardPointDetails")]
    public OintDetails BoardPointDetails { get; set; }

    [JsonProperty("offpointDetails")]
    public OintDetails OffpointDetails { get; set; }

    [JsonProperty("companyDetails")]
    public CompanyDetails CompanyDetails { get; set; }

    [JsonProperty("flightIdentification")]
    public FlightIdentification FlightIdentification { get; set; }

    [JsonProperty("flightTypeDetails")]
    public FlightTypeDetails FlightTypeDetails { get; set; }
}

public partial class OintDetails
{
    [JsonProperty("trueLocationId")]
    public string TrueLocationId { get; set; }
}

public partial class CompanyDetails
{
    [JsonProperty("marketingCompany")]
    public string MarketingCompany { get; set; }

    [JsonProperty("operatingCompany")]
    public string OperatingCompany { get; set; }
}

public partial class FlightDate
{
    [JsonProperty("departureDate")]
    public string DepartureDate { get; set; }

    [JsonProperty("departureTime")]
    public string DepartureTime { get; set; }

    [JsonProperty("arrivalDate")]
    public string ArrivalDate { get; set; }

    [JsonProperty("arrivalTime")]
    public string ArrivalTime { get; set; }
}

public partial class FlightIdentification
{
    [JsonProperty("flightNumber")]
    public string FlightNumber { get; set; }

    [JsonProperty("bookingClass")]
    public string BookingClass { get; set; }
}

public partial class FlightTypeDetails
{
    [JsonProperty("flightIndicator")]
    public string FlightIndicator { get; set; }

    [JsonProperty("itemNumber")]
    public long ItemNumber { get; set; }
}

public partial class SupplierAgencyDetail
{
    //[JsonProperty("AgencyID")]
    //public long AgencyId { get; set; }

    //[JsonProperty("SupplierId")]
    //public long SupplierId { get; set; }

    //[JsonProperty("BaseUrl")]
    //public string BaseUrl { get; set; }

    //[JsonProperty("RequestUrl")]
    //public object RequestUrl { get; set; }

    //[JsonProperty("SupplierName")]
    //public string SupplierName { get; set; }

    //[JsonProperty("SupplierCode")]
    //public string SupplierCode { get; set; }

    //[JsonProperty("AccountNumber")]
    //public string AccountNumber { get; set; }

    //[JsonProperty("UserName")]
    //public string UserName { get; set; }

    //[JsonProperty("Password")]
    //public string Password { get; set; }

    //[JsonProperty("Status")]
    //public string Status { get; set; }

    [JsonProperty("toCurrency")]
    public string ToCurrency { get; set; }
    [JsonProperty("toROEValue")]
    public string ToROEValue { get; set; }
}

public partial class FareRevalidateRQ
{
    public static FareRevalidateRQ FromJson(string json) => JsonConvert.DeserializeObject<FareRevalidateRQ>(json, FareRevalidateRQConverter.Settings);
}

public static class FareRevalidateRQSerialize
{
    public static string ToJson(this FareRevalidateRQ self) => JsonConvert.SerializeObject(self, FareRevalidateRQConverter.Settings);
}

public class FareRevalidateRQConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
       //MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
    };
}
