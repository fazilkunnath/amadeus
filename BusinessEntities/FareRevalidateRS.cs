﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessEntities
{
    using Newtonsoft.Json;
    public partial class FareRevalidateRS
    {
        public FareRevalidateRS()
        {
            FareInformationWithoutPnrReply = new FareInformationWithoutPnrReply();
        }
       
        public FareInformationWithoutPnrReply FareInformationWithoutPnrReply { get; set; }
    }

    
    public class FareInformationWithoutPnrReply
    {
        public FareInformationWithoutPnrReply()
        {
            FlightFareDetails = new FlightFareDetails();
            PaxFareDetails = new List<PaxFareDetail>();
            AirSegments = new List<AirSegment>();
            FareErrors = new List<FareError>();
            AirFareRule = new List<AirFareRule>();
        }
        
        [JsonProperty("fareSessionId")]
        public string FareSessionId { get; set; }

        [JsonProperty("fareFareSourceCode")]
        public string FareFareSourceCode { get; set; }

        [JsonProperty("fareAgencyCode")]
        public string FareAgencyCode { get; set; }

        [JsonProperty("fSupplier")]
        public string FSupplier { get; set; }

        [JsonProperty("flightFareDetails")]
        public FlightFareDetails FlightFareDetails { get; set; }

        [JsonProperty("paxFareDetails")]
        public List<PaxFareDetail> PaxFareDetails { get; set; }

        [JsonProperty("airSegments")]
        public List<AirSegment> AirSegments { get; set; }

        [JsonProperty("fareErrors")]
        public List<FareError> FareErrors { get; set; }

        [JsonProperty("airFareRule")]
        public List<AirFareRule> AirFareRule { get; set; }

        [JsonProperty("success")]
        public string Success { get; set; }

        [JsonProperty("target")]
        public string Target { get; set; }

        [JsonProperty("supplierOGPrice")]
        public string SupplierOGPrice { get; set; }
    }

    public partial class FlightFareDetails
    {
        public FlightFareDetails()
        {
            SurchargesGroup = new Surcharges();
        }

        [JsonProperty("totalPax")]
        public string TotalPax { get; set; }

        [JsonProperty("totalFare")]
        public string TotalFare { get; set; }

        [JsonProperty("basefare")]
        public string Basefare { get; set; }

        [JsonProperty("taxFare")]
        public string TaxFare { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("surchargesGroup")]
        public Surcharges SurchargesGroup { get; set; }
    }
    public partial class Surcharges
    {
        [JsonProperty("amount")]
        public string Amount { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }
    }

    public partial class PaxFareDetail
    {
        public PaxFareDetail()
        {
            Surcharges = new Surcharges();
        }


        [JsonProperty("totalPax")]
        public string TotalPax { get; set; }

        [JsonProperty("totalPaxUnites")]
        public string TotalPaxUnites { get; set; }

        [JsonProperty("paxType")]
        public string PaxType { get; set; }

        [JsonProperty("totalFare")]
        public string TotalFare { get; set; }

        [JsonProperty("basefare")]
        public string Basefare { get; set; }

        [JsonProperty("taxFare")]
        public string TaxFare { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("surcharges")]
        public Surcharges Surcharges { get; set; }
    }

    public partial class AirSegment
    {
        public AirSegment()
        {
            SegInfo = new SegInfo();
            CabinGroup = new CabinGroup();
            BaggageInformations = new List<BaggageInformation>();
        }

        [JsonProperty("segDirectionID")]
        public long SegDirectionId { get; set; }

        [JsonProperty("originCity")]
        public string OriginCity { get; set; }

        [JsonProperty("departureCity")]
        public string DepartureCity { get; set; }

        [JsonProperty("segInfo")]
        public SegInfo SegInfo { get; set; }

        [JsonProperty("fareSourceCode")]
        public string FareSourceCode { get; set; }

        [JsonProperty("cabinGroup")]
        public CabinGroup CabinGroup { get; set; }

        [JsonProperty("baggageInformations")]
        public List<BaggageInformation> BaggageInformations { get; set; }


    }

    public partial class SegInfo
    {
        public SegInfo()
        {
            FlData = new FlData();
            DateandTimeDetails = new DateandTimeDetails();
        }


        [JsonProperty("flData")]
        public FlData FlData { get; set; }

        [JsonProperty("dateandTimeDetails")]
        public DateandTimeDetails DateandTimeDetails { get; set; }

       
    }
  
    public partial class FlData
    {
        [JsonProperty("boardPoint")]
        public string BoardPoint { get; set; }

        [JsonProperty("boardTerminal")]
        public string BoardTerminal { get; set; }

        [JsonProperty("offpoint")]
        public string Offpoint { get; set; }

        [JsonProperty("offTerminal")]
        public string OffTerminal { get; set; }

        [JsonProperty("marketingCompany")]
        public string MarketingCompany { get; set; }

        [JsonProperty("operatingCompany")]
        public string OperatingCompany { get; set; }

        [JsonProperty("flightNumber")]
        public string FlightNumber { get; set; }

        [JsonProperty("bookingClass")]
        public string BookingClass { get; set; }

        [JsonProperty("equiptype")]
        public string Equiptype { get; set; }

        [JsonProperty("durationtime")]
        public string Durationtime { get; set; }
    }

    public partial class DateandTimeDetails
    {
        [JsonProperty("departureDate")]
        public string DepartureDate { get; set; }

        [JsonProperty("departureTime")]
        public string DepartureTime { get; set; }

        [JsonProperty("arrivalDate")]
        public string ArrivalDate { get; set; }

        [JsonProperty("arrivalTime")]
        public string ArrivalTime { get; set; }
    }

    public partial class CabinGroup
    {
        [JsonProperty("designator")]
        public string Designator { get; set; }
    }

    public partial class BaggageInformation
    {
        [JsonProperty("bagPaxType")]
        public string BagPaxType { get; set; }

        [JsonProperty("baggage")]
        public string Baggage { get; set; }

        [JsonProperty("unit")]
        public string Unit { get; set; }
    }
    public partial class FareError
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("errormessage")]
        public string Errormessage { get; set; }
    }

    public partial class AirFareRule
    {

        public AirFareRule()
        {
            FareRuleDetails = new List<FareRuleDetail>();
        }

        [JsonProperty("airline")]
        public string Airline { get; set; }

        [JsonProperty("departure")]
        public string Departure { get; set; }

        [JsonProperty("arrival")]
        public string Arrival { get; set; }

        [JsonProperty("fareRuleDetails")]
        public List<FareRuleDetail> FareRuleDetails { get; set; }
    }
    public partial class FareRuleDetail
    {
        [JsonProperty("rulehead")]
        public string Rulehead { get; set; }

        [JsonProperty("ruleBody")]
        public string RuleBody { get; set; }
    }

    public partial class FareRevalidateRS
    {
        public static FareRevalidateRS FromJson(string json) => JsonConvert.DeserializeObject<FareRevalidateRS>(json, FareRevalidateRSConverter.Settings);
    }

    public static class RevalidateEntitySerialize
    {
        public static string ToJson(this FareRevalidateRS self) => JsonConvert.SerializeObject(self, FareRevalidateRSConverter.Settings);
    }

    public class FareRevalidateRSConverter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
           // MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
           
        };
    }



}
