﻿using Newtonsoft.Json;
using System.Collections.Generic;

public partial class IssueTicketComRq
{
    public IssueTicketComRq()
    {
        TicketCreateTstFromPricing = new TicketCreateTstFromPricing();
        SupplierAgencyDetails = new List<SupplierAgencyDetail>();
    }
    [JsonProperty("ticketCreateTSTFromPricing")]
    public TicketCreateTstFromPricing TicketCreateTstFromPricing { get; set; }

    [JsonProperty("SupplierAgencyDetails")]
    public List<SupplierAgencyDetail> SupplierAgencyDetails { get; set; }

    //[JsonProperty("psaList")]
    //public List<PsaList> PsaList { get; set; }
}

//public partial class PsaList
//{
//    //[JsonProperty("itemReference")]
//    //public ItemReference ItemReference { get; set; }
//}

//public partial class ItemReference
//{

//    [JsonProperty("referenceType")]
//    public string ReferenceType { get; set; }

//    [JsonProperty("uniqueReference")]
//    public string UniqueReference { get; set; }
//}

public partial class SupplierAgencyDetail
{
    [JsonProperty("AgencyID")]
    public long AgencyId { get; set; }
    [JsonProperty("AgencyCode")]
    public string AgencyCode { get; set; }

    [JsonProperty("SupplierId")]
    public long SupplierId { get; set; }

    [JsonProperty("BaseUrl")]
    public string BaseUrl { get; set; }

    [JsonProperty("RequestUrl")]
    public object RequestUrl { get; set; }

    [JsonProperty("SupplierName")]
    public string SupplierName { get; set; }

    [JsonProperty("SupplierCode")]
    public string SupplierCode { get; set; }

    [JsonProperty("AccountNumber")]
    public string AccountNumber { get; set; }

    [JsonProperty("UserName")]
    public string UserName { get; set; }

    [JsonProperty("Password")]
    public string Password { get; set; }

    [JsonProperty("Status")]
    public string Status { get; set; }
}

public partial class TicketCreateTstFromPricing
{
    [JsonProperty("AgencyCode")]
    public string AgencyCode { get; set; }

    [JsonProperty("SupplierCode")]
    public string SupplierCode { get; set; }

    [JsonProperty("FareSourceCode")]
    public string FareSourceCode { get; set; }

    [JsonProperty("SessionId")]
    public string SessionId { get; set; }

    [JsonProperty("Target")]
    public string Target { get; set; }

    [JsonProperty("UniqueID")]
    public string UniqueId { get; set; }

    [JsonProperty("BookingRefID")]
    public string BookingRefId { get; set; }

    [JsonProperty("UserID")]
    public object UserId { get; set; }
}

public partial class IssueTicketComRq
{
    public static IssueTicketComRq FromJson(string json) => JsonConvert.DeserializeObject<IssueTicketComRq>(json, IssueTicketComRqConverter.Settings);
}

public static class IssueTicketComRqSerialize
{
    public static string ToJson(this IssueTicketComRq self) => JsonConvert.SerializeObject(self, IssueTicketComRqConverter.Settings);
}

public class IssueTicketComRqConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        // MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling = NullValueHandling.Ignore
    };
}