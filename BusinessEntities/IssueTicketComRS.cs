﻿using System.Collections.Generic;

namespace BusinessEntities
{
    using Newtonsoft.Json;
    public class IssueTicketComRS
    {
        public IssueTicketComRS()
        {
            TripDetailsResult = new TripDetailsResult();
        }
        [JsonProperty("tripDetailsResult")]
        public TripDetailsResult TripDetailsResult { get; set; }
    }

    public class TripDetailsResult
    {
        public TripDetailsResult()
        {
            TkErrors = new List<BusinessEntities.TkErrors>();
            ItineraryInformation = new List<BusinessEntities.ItineraryInformation>();
            ReservationItem = new List<ReservationItem>();
            TripTotalFare = new TripTotalFare();

        }
        [JsonProperty("success")]
        public string Success { get; set; }

        [JsonProperty("target")]
        public string Target { get; set; }

        [JsonProperty("ticketStatus")]
        public string TicketStatus { get; set; }

        [JsonProperty("bookingStatus")]
        public string BookingStatus { get; set; }

        [JsonProperty("uniqueID")]
        public string UniqueId { get; set; }

        [JsonProperty("tkErrors")]
        public List<TkErrors> TkErrors { get; set; }

        [JsonProperty("itineraryInformation")]
        public List<ItineraryInformation> ItineraryInformation { get; set; }

        [JsonProperty("reservationItem")]
        public List<ReservationItem> ReservationItem { get; set; }

        [JsonProperty("tripTotalFare")]
        public TripTotalFare TripTotalFare { get; set; }

        [JsonProperty("bookingId")]
        public object BookingId { get; set; }
    }

    public class TripTotalFare
    {
        [JsonProperty("equiFare")]
        public string EquiFare { get; set; }

        [JsonProperty("tax")]
        public string Tax { get; set; }

        [JsonProperty("totalFare")]
        public string TotalFare { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }
    public partial class TkErrors
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("meassage")]
        public string Meassage { get; set; }
    }

    public class ItineraryInformation
    {
        public ItineraryInformation()
        {
            CustomerInformation = new CustomerInformation();
            ETickets = new ETickets();
            ItineraryPricing = new ItineraryPricing();
        }
        [JsonProperty("customerInformation")]
        public CustomerInformation CustomerInformation { get; set; }

        [JsonProperty("eTickets")]
        public ETickets ETickets { get; set; }

        [JsonProperty("itineraryPricing")]
        public ItineraryPricing ItineraryPricing { get; set; }
    }
    public partial class CustomerInformation
    {
        public CustomerInformation()
        {
            Age = new Age();
            Paxdetails = new Paxdetails();
        }
        [JsonProperty("age")]
        public Age Age { get; set; }

        [JsonProperty("paxdetails")]
        public Paxdetails Paxdetails { get; set; }
    }

    public partial class Age
    {

        [JsonProperty("months")]
        public string Months { get; set; }

        [JsonProperty("years")]
        public string Years { get; set; }
    }

    public partial class Paxdetails
    {
        [JsonProperty("passengerFirstName")]
        public string PassengerFirstName { get; set; }

        [JsonProperty("passengerLastName")]
        public string PassengerLastName { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("dateOfBirth")]
        public string DateOfBirth { get; set; }

        [JsonProperty("emailAddress")]
        public string EmailAddress { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("knownTravelerNo")]
        public string KnownTravelerNo { get; set; }

        [JsonProperty("nameNumber")]
        public string NameNumber { get; set; }

        [JsonProperty("nationalID")]
        public string NationalId { get; set; }

        [JsonProperty("passengerNationality")]
        public string PassengerNationality { get; set; }

        [JsonProperty("passengerType")]
        public string PassengerType { get; set; }

        [JsonProperty("passportExpiresOn")]
        public object PassportExpiresOn { get; set; }

        [JsonProperty("passportIssueCountry")]
        public string PassportIssueCountry { get; set; }

        [JsonProperty("passportNationality")]
        public string PassportNationality { get; set; }

        [JsonProperty("passportNumber")]
        public string PassportNumber { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("postCode")]
        public string PostCode { get; set; }
    }

    public partial class ETickets
    {
        [JsonProperty("itemRPH")]
        public string ItemRph { get; set; }

        [JsonProperty("eTicketNumber")]
        public string ETicketNumber { get; set; }

        [JsonProperty("ssRs")]
        public string SsRs { get; set; }
    }
    public partial class ItineraryPricing
    {
        [JsonProperty("equiFare")]
        public string EquiFare { get; set; }

        [JsonProperty("tax")]
        public string Tax { get; set; }

        [JsonProperty("totalFare")]
        public string TotalFare { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }
    }


    public partial class ReservationItem
    {
        [JsonProperty("airEquipmentType")]
        public string AirEquipmentType { get; set; }

        [JsonProperty("airlinePNR")]
        public string AirlinePnr { get; set; }

        [JsonProperty("arrivalCode")]
        public string ArrivalCode { get; set; }

        [JsonProperty("arrivalDateTime")]
        public string ArrivalDateTime { get; set; }

        [JsonProperty("arrivalTer")]
        public string ArrivalTer { get; set; }

        [JsonProperty("baggage")]
        public string Baggage { get; set; }

        [JsonProperty("cabinClassText")]
        public string CabinClassText { get; set; }

        [JsonProperty("departureCode")]
        public string DepartureCode { get; set; }

        [JsonProperty("departureDateTime")]
        public string DepartureDateTime { get; set; }

        [JsonProperty("departureTer")]
        public string DepartureTer { get; set; }

        [JsonProperty("flightNumber")]
        public string FlightNumber { get; set; }

        [JsonProperty("itemRPH")]
        public string ItemRph { get; set; }

        [JsonProperty("jDuration")]
        public string JDuration { get; set; }

        [JsonProperty("marketingCode")]
        public string MarketingCode { get; set; }

        [JsonProperty("totalpax")]
        public string Totalpax { get; set; }

        [JsonProperty("operatingCode")]
        public string OperatingCode { get; set; }

        [JsonProperty("stopQuantity")]
        public string StopQuantity { get; set; }
    }
}