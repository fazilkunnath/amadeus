﻿
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class TripDetails
{
    public TripDetails()
    {
       
        TravelItinerary = new TravelItinerary();

    }
    [JsonProperty("errors")]
    public List<object> Errors { get; set; }

    [JsonProperty("success")]
    public bool Success { get; set; }

    [JsonProperty("target")]
    public string Target { get; set; }

    [JsonProperty("travelItinerary")]
    public TravelItinerary TravelItinerary { get; set; }
}

public partial class TravelItinerary
{
    public TravelItinerary()
    {
        ItineraryInfo = new ItineraryInfo1();


    }
    [JsonProperty("bookingNotes")]
    public List<object> BookingNotes { get; set; }

    [JsonProperty("bookingStatus")]
    public string BookingStatus { get; set; }

    [JsonProperty("itineraryInfo")]
    public ItineraryInfo1 ItineraryInfo { get; set; }

    [JsonProperty("ticketStatus")]
    public string TicketStatus { get; set; }

    [JsonProperty("uniqueID")]
    public string UniqueId { get; set; }
}

public partial class ItineraryInfo1
{
    public ItineraryInfo1()
    {
        CustomerInfos = new List<CustomerInfo>();
        ItineraryPricing = new ItineraryPricing1();
        ReservationItems = new List<ReservationItem1>();
        TripDetailsPtcFareBreakdowns = new List<TripDetailsPtcFareBreakdown>();

    }
    [JsonProperty("bookedReservedItems")]
    public List<object> BookedReservedItems { get; set; }

    [JsonProperty("clientUTCOffset")]
    public long ClientUtcOffset { get; set; }

    [JsonProperty("customerInfos")]
    public List<CustomerInfo> CustomerInfos { get; set; }

    [JsonProperty("itineraryPricing")]
    public ItineraryPricing1 ItineraryPricing { get; set; }

    [JsonProperty("reservationItems")]
    public List<ReservationItem1> ReservationItems { get; set; }

    [JsonProperty("tripDetailsPTC_FareBreakdowns")]
    public List<TripDetailsPtcFareBreakdown> TripDetailsPtcFareBreakdowns { get; set; }
}

public partial class CustomerInfo
{
    public CustomerInfo()
    {
        Customer = new Customer();
        SsRs = new List<SsR>();
    }
    [JsonProperty("customer")]
    public Customer Customer { get; set; }

    [JsonProperty("eTickets")]
    public List<object> ETickets { get; set; }

    [JsonProperty("ssRs")]
    public List<SsR> SsRs { get; set; }
}

public partial class Customer
{
    public Customer()
    {
        Age = new Age();
        PaxName = new PaxName();
    }
    [JsonProperty("age")]
    public Age Age { get; set; }

    [JsonProperty("dateOfBirth")]
    public string DateOfBirth { get; set; }

    [JsonProperty("emailAddress")]
    public string EmailAddress { get; set; }

    [JsonProperty("gender")]
    public string Gender { get; set; }

    [JsonProperty("knownTravelerNo")]
    public string KnownTravelerNo { get; set; }

    [JsonProperty("nameNumber")]
    public string NameNumber { get; set; }

    [JsonProperty("nationalID")]
    public string NationalId { get; set; }

    [JsonProperty("passengerNationality")]
    public string PassengerNationality { get; set; }

    [JsonProperty("passengerType")]
    public string PassengerType { get; set; }

    [JsonProperty("passportExpiresOn")]
    public string PassportExpiresOn { get; set; }

    [JsonProperty("passportIssuanceCountry")]
    public string PassportIssuanceCountry { get; set; }

    [JsonProperty("passportNationality")]
    public string PassportNationality { get; set; }

    [JsonProperty("passportNumber")]
    public string PassportNumber { get; set; }

    [JsonProperty("paxName")]
    public PaxName PaxName { get; set; }

    [JsonProperty("phoneNumber")]
    public string PhoneNumber { get; set; }

    [JsonProperty("postCode")]
    public string PostCode { get; set; }

    [JsonProperty("redressNo")]
    public string RedressNo { get; set; }
}

public partial class Age
{
    [JsonProperty("months")]
    public string Months { get; set; }

    [JsonProperty("years")]
    public string Years { get; set; }
}

public partial class PaxName
{
    [JsonProperty("passengerFirstName")]
    public string PassengerFirstName { get; set; }

    [JsonProperty("passengerLastName")]
    public string PassengerLastName { get; set; }

    [JsonProperty("passengerTitle")]
    public string PassengerTitle { get; set; }
}

public partial class SsR
{
    [JsonProperty("itemRPH")]
    public long ItemRph { get; set; }

    [JsonProperty("mealPreference")]
    public string MealPreference { get; set; }

    [JsonProperty("seatPreference")]
    public string SeatPreference { get; set; }
}

public partial class ItineraryPricing1
{
    public ItineraryPricing1()
    {
        EquiFare = new EquiFare();
    }
    //public string Currency { get; set; }
    [JsonProperty("equiFare")]
    public EquiFare EquiFare { get; set; }

    [JsonProperty("tax")]
    public EquiFare Tax { get; set; }

    [JsonProperty("totalFare")]
    public EquiFare TotalFare { get; set; }
}

public partial class EquiFare
{
    [JsonProperty("amount")]
    public string Amount { get; set; }

    [JsonProperty("currencyCode")]
    public string CurrencyCode { get; set; }

    [JsonProperty("decimalPlaces")]
    public long DecimalPlaces { get; set; }
}

public partial class ReservationItem1
{
    [JsonProperty("airEquipmentType")]
    public string AirEquipmentType { get; set; }

    [JsonProperty("airlinePNR")]
    public string AirlinePnr { get; set; }

    [JsonProperty("arrivalAirportLocationCode")]
    public string ArrivalAirportLocationCode { get; set; }

    [JsonProperty("arrivalDateTime")]
    public System.DateTime ArrivalDateTime { get; set; }

    [JsonProperty("arrivalTerminal")]
    public string ArrivalTerminal { get; set; }

    [JsonProperty("baggage")]
    public string Baggage { get; set; }

    [JsonProperty("cabinClassText")]
    public string CabinClassText { get; set; }

    [JsonProperty("departureAirportLocationCode")]
    public string DepartureAirportLocationCode { get; set; }

    [JsonProperty("departureDateTime")]
    public System.DateTime DepartureDateTime { get; set; }

    [JsonProperty("departureTerminal")]
    public string DepartureTerminal { get; set; }

    [JsonProperty("flightNumber")]
    public string FlightNumber { get; set; }

    [JsonProperty("itemRPH")]
    public long ItemRph { get; set; }

    [JsonProperty("journeyDuration")]
    public string JourneyDuration { get; set; }

    [JsonProperty("marketingAirlineCode")]
    public string MarketingAirlineCode { get; set; }

    [JsonProperty("numberInParty")]
    public long NumberInParty { get; set; }

    [JsonProperty("operatingAirlineCode")]
    public string OperatingAirlineCode { get; set; }

    [JsonProperty("resBookDesigCode")]
    public string ResBookDesigCode { get; set; }

    [JsonProperty("resBookDesigText")]
    public string ResBookDesigText { get; set; }

    [JsonProperty("stopQuantity")]
    public long StopQuantity { get; set; }
}

public partial class TripDetailsPtcFareBreakdown
{
    public TripDetailsPtcFareBreakdown()
    {
        PassengerTypeQuantity = new PassengerTypeQuantity(); ;
        TripDetailsPassengerFare = new ItineraryPricing1();
    }
    [JsonProperty("passengerTypeQuantity")]
    public PassengerTypeQuantity PassengerTypeQuantity { get; set; }

    [JsonProperty("tripDetailsPassengerFare")]
    public ItineraryPricing1 TripDetailsPassengerFare { get; set; }
}

public partial class PassengerTypeQuantity
{
    [JsonProperty("code")]
    public long Code { get; set; }

    [JsonProperty("quantity")]
    public long Quantity { get; set; }
}

public partial class TripDetails
{
    public static TripDetails FromJson(string json) => JsonConvert.DeserializeObject<TripDetails>(json, TripDetailsConverter.Settings);
}

public static class TripDetailsSerialize
{
    public static string ToJson(this TripDetails self) => JsonConvert.SerializeObject(self, TripDetailsConverter.Settings);
}

public class TripDetailsConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
       // MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore
    };
}