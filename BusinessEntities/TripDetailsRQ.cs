﻿
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class TripDetailsRq
{
    [JsonProperty("UniqueID")]
    public string UniqueId { get; set; }

    [JsonProperty("Target")]
    public string Target { get; set; }

    [JsonProperty("SupplierAgencyDetails")]
    public List<SupplierAgencyDetail> SupplierAgencyDetails { get; set; }
}

public partial class SupplierAgencyDetail
{
//    [JsonProperty("AgencyID")]
//    public long AgencyId { get; set; }

//    [JsonProperty("SupplierId")]
//    public long SupplierId { get; set; }

//    [JsonProperty("BaseUrl")]
//    public object BaseUrl { get; set; }

//    [JsonProperty("RequestUrl")]
//    public object RequestUrl { get; set; }

//    [JsonProperty("SupplierName")]
//    public string SupplierName { get; set; }

//    [JsonProperty("SupplierCode")]
//    public string SupplierCode { get; set; }

//    [JsonProperty("AccountNumber")]
//    public string AccountNumber { get; set; }

//    [JsonProperty("UserName")]
//    public string UserName { get; set; }

//    [JsonProperty("Password")]
//    public string Password { get; set; }

//    [JsonProperty("Status")]
//    public string Status { get; set; }
}

public partial class TripDetailsRq
{
    public static TripDetailsRq FromJson(string json) => JsonConvert.DeserializeObject<TripDetailsRq>(json, TripDetailsRqConverter.Settings);
}

public static class TripDetailsRqSerialize
{
    public static string ToJson(this TripDetailsRq self) => JsonConvert.SerializeObject(self, TripDetailsRqConverter.Settings);
}

public class TripDetailsRqConverter
{
    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    {
        //MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        NullValueHandling=NullValueHandling.Ignore

    };
}