namespace DAL1.Core.Domain
{
    using System;


    public partial class Partner
    {
        public int PartnerID { get; set; }


        public string Name { get; set; }


        public string Address { get; set; }


        public string ContactPerson { get; set; }


        public string ContactTelephone { get; set; }


        public string WebSite { get; set; }


        public string Email { get; set; }


        public string API { get; set; }

        public int? RecordStatus { get; set; }

        public DateTime? Createddate { get; set; }

        public int? CreatedUser { get; set; }

        public DateTime? Modifieddate { get; set; }

        public int? ModifiedUser { get; set; }
    }
}
