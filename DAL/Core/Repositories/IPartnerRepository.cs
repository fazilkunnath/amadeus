﻿namespace DAL.Core.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using BusinessEntities.Entity;

    public interface IPartnerRepository 
    {
        List<PartnerEntity> GetPartnerData();

        Task<List<PartnerEntity>> GetAsyncPartnerData();
    }
}