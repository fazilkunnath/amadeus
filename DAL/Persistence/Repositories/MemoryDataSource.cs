﻿using System.Collections.Generic;
using BusinessEntities.Entity;

namespace DAL.Persistence.Repositories
{
    public class MemoryDataSource
    {
        public List<PartnerEntity> Partners { get; private set; }

        public MemoryDataSource()
        {
            Partners = new List<PartnerEntity>();
        }
    }
}
