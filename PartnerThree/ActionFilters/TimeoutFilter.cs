﻿namespace PartnerThree.ActionFilters
{
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;
    using BusinessEntities;
    using Newtonsoft.Json;

    public class TimeoutFilter : ActionFilterAttribute
    {
        public int Timeout { get; set; }
        public object Data { get; set; }
        public TimeoutFilter()
        {
            this.Timeout = int.MaxValue;
        }
        public TimeoutFilter(int timeout)
        {
            this.Timeout = timeout;
        }
        public TimeoutFilter(object result)
        {
            Data = result;
        }

        public override async Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {

            var controller = actionContext.ControllerContext.Controller;
            var controllerType = controller.GetType();
            var action = controllerType.GetMethod(actionContext.ActionDescriptor.ActionName);
            var tokenSource = new CancellationTokenSource();
            var timeout = this.TimeoutTask(this.Timeout);
            object result = null;

            var work = Task.Run(() =>
            {
                result = action.Invoke(controller, actionContext.ActionArguments.Values.ToArray());
            }, tokenSource.Token);

            var finishedTask = await Task.WhenAny(timeout, work);

            if (finishedTask == timeout)
            {
                tokenSource.Cancel();
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.RequestTimeout, result);
            }
            else
            {
                Task<HttpResponseMessage> responseModel = (Task<HttpResponseMessage>)result;
                var partnerResponse = responseModel.Result.Content.ReadAsStringAsync().Result;
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK, JsonConvert.DeserializeObject<FLcommonRS>(partnerResponse));

            }
        }

        private async Task TimeoutTask(int timeoutValue)
        {
            await Task.Delay(timeoutValue);
        }

    }
}