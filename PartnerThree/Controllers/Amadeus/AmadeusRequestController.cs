﻿namespace PartnerThree.Controllers.Amadeus
{
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using AmadeusPartner.BusinessServices.Interface;
    using ErrorHelper;
    using BusinessEntities;
    using System.Threading.Tasks;
    using ActionFilters;
    using System.Web;


    [RoutePrefix("amadeus-request")]
    public class AmadeusRequestController : ApiController
    {
        private IAmadeusServices services;
        public AmadeusRequestController(IAmadeusServices _services)
        {
            this.services = _services;
        }


        [HttpPost]
        [Route("mptb-search")]
        //[TimeoutFilter(50000)]
       // [TimeoutFilter(50000)]
        public async Task<HttpResponseMessage> MptbSearch(Rootobject data)
        {
            var response = await services.MptbSearch(data);
            if (response != null)
                return Request.CreateResponse(HttpStatusCode.OK, response);

            throw new ExternalApiException(1000, "Amadeus not found", HttpStatusCode.NotFound);
        }


        //[HttpPost]
        //[Route("mptb-search")]
        //[TimeoutFilter(50000)]
        //public async Task<HttpResponseMessage> MptbSearch(Rootobject data)
        //{
        //    try
        //    {
        //        //HttpContext.Current.Server.ScriptTimeout = 10;
        //        var response = await services.MptbSearch(data);
        //        if (response != null)
        //            return Request.CreateResponse(HttpStatusCode.OK, response);

        //        throw new ExternalApiException(1000, "Amadeus not found", HttpStatusCode.NotFound);
        //    }
        //    catch (System.Exception ex)
        //    {

        //        throw new ExternalApiException(1001, "Amadeus not found", HttpStatusCode.NotFound);
        //    }

        //}

        [HttpPost]
        [Route("fareinfo-best-price")]
        public async Task<HttpResponseMessage> fareInformativeBestPricingWithoutPNR(FareRevalidateRQ FareRevalidateRQCom)
        {
            var data =  await services.FareInformativeBestPricingWithoutPnr(FareRevalidateRQCom);
            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, data);

            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }

        [HttpPost]
        [Route("book-flight")]
        public async Task<HttpResponseMessage> BookFlight(FLBookRQ FLbookRQ)
        {
            var data = await services.BookFlight(FLbookRQ);
            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, data);

            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }

        [HttpPost]
        [Route("issue-ticket")]
        public async Task<HttpResponseMessage> IssueTicket(IssueTicketComRq IssueTicketComRQ)
        {
            var data = await services.IssueTicket(IssueTicketComRQ);
            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, data);

            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }



        [HttpPost]
        [Route("cancel-booking")]
        public async Task<HttpResponseMessage> CancelBooking(CancelBooking CancelRQ,TripDetailsRq TripRQ)
        {
            var data = await services.CancelBooking(CancelRQ);
            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, data);

            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }

        [HttpPost]
        [Route("trip-details")]
        public async Task<HttpResponseMessage> Tripdetails(TripDetailsRq TripRQ)
        {

            var data = await services.Tripdetails(TripRQ);
            if (data != null)
                return Request.CreateResponse(HttpStatusCode.OK, data);

            throw new ApiDataException(1000, "Partner not found", HttpStatusCode.NotFound);
        }

    }
}
