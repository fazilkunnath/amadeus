﻿using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace PartnerThree.Controllers
{
    public class BaseController : ApiController
    {
        protected async Task<T> RunTask<T>(Task<T> action, int timeout)
        {
            var timeoutTask = Task.Delay(timeout);
            var firstTaskFinished = await Task.WhenAny(timeoutTask, action);

            if (firstTaskFinished == timeoutTask)
            {
                throw new Exception("Timeout");
            }

            return action.Result;
        }
    }
}